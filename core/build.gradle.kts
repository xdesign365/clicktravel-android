
configurations.create("compileClasspath")

plugins {
    kotlin("plugin.serialization")
    kotlin("multiplatform")
    id("org.jetbrains.kotlin.native.cocoapods")
}

val libraryVersion: String by project
val xdesignRepositoryVersion: String by project
val kotlin_version: String by project
val mockkVersion: String by project
val buildForIos: String by project

version = libraryVersion

kotlin {
    jvm()

    if (buildForIos == "true") {
        val buildForDevice = project.findProperty("kotlin.native.cocoapods.target") == "ios_arm"
        if (buildForDevice) {
            iosArm64("ios")
            iosArm32("iOS32")

            val iOSMain by sourceSets.creating
            sourceSets["iOS64Main"].dependsOn(iOSMain)
            sourceSets["iOS32Main"].dependsOn(iOSMain)
        } else {
            iosX64("ios")
        }

        cocoapods {
            summary = "Some description for a Kotlin/Native module"
            homepage = "Link to a Kotlin/Native module homepage"
        }
    }

    sourceSets {
        val coroutinesVersion: String by project
        val serializationVersion: String by project
        val klockVersion: String by project

        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlin_version")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-common:$coroutinesVersion")
                implementation("com.soywiz:krypto:0.4.0")
                api("com.soywiz.korlibs.klock:klock:$klockVersion")

                // Serialization
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.1.0")
            }
        }

        val jvmMain by getting {
            dependencies {
                implementation(kotlin("stdlib"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
//                implementation("com.soywiz.korlibs.klock:klock-jvm:$klockVersion")
            }
        }

        val jvmTest by getting {
            dependencies {
                dependsOn(commonMain)
                dependsOn(jvmMain)
                implementation("org.jetbrains.kotlin:kotlin-test:$kotlin_version")
                implementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version")
                implementation("io.mockk:mockk:$mockkVersion")

            }
        }

        if (buildForIos == "true") {
            val iosMain by getting {
                dependencies {
                    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-native:$coroutinesVersion")
                    implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-native:$serializationVersion")
                }
            }
        }
    }
}
