package com.clicktravel.core.usecases

import com.clicktravel.core.model.BookingItem
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.UniqueJourney

interface UserJourneysUseCase {
    suspend fun getJourneyTypesForCurrentDay(): List<TravelTypes>
    suspend fun getAllJourneys() : List<UniqueJourney>
    suspend fun getBookingFromId(bookingId : String) : BookingItem
    suspend fun sync()
    suspend fun clearData()
}