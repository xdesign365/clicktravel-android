package com.clicktravel.core.usecases

import com.clicktravel.repository.retriever.PaymentInstructionsRetriever

interface HotelUseCase {

    suspend fun resendPaymentInstructions(
        teamId: String,
        bookingId: String,
        travellerEmail: String,
        hotelEmail: String?
    )

}

class HotelUseCaseImpl(
    private val retriever: PaymentInstructionsRetriever
): HotelUseCase {
    override suspend fun resendPaymentInstructions(
        teamId: String,
        bookingId: String,
        travellerEmail: String,
        hotelEmail: String?
    ) = retriever.resendPaymentInstructions(
        teamId = teamId,
        bookingId = bookingId,
        travellerEmail = travellerEmail,
        hotelEmail = hotelEmail
    )
}