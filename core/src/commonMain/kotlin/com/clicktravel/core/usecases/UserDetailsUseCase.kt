package com.clicktravel.core.usecases

import com.clicktravel.core.model.UserDetails
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.repository.retriever.NoArgumentRetriever
import com.clicktravel.repository.retriever.UpdateCurrentUserProfile

interface UserDetailsUseCase {
    suspend fun updateCurrentUser()
    suspend fun updateCurrentUserDetails(userDetails: UserDetails)
    suspend fun getUserDetails(): UserDetails
    fun getAvatarUrl(firstName: String, lastName: String): String
    suspend fun deleteUser()
}

class UserDetailsUseCaseImpl(
    private val remoteUserDetailsRetriever: NoArgumentRetriever<UserDetails>,
    private val updateCurrentUserProfile: UpdateCurrentUserProfile,
    private val writeLocalUserDetailsRepo: WriteOnlyRepository<UserDetails>,
    private val readLocalUserDetailsRepo: NoArgumentRetriever<UserDetails?>
) : UserDetailsUseCase {
    private var cache: UserDetails? = null

    suspend fun initialise() {
        cache = readLocalUserDetailsRepo.execute()
    }

    override suspend fun updateCurrentUser() {
        remoteUserDetailsRetriever
            .execute()
            .also {
                cache = it
                writeLocalUserDetailsRepo.storeData(it)
            }
    }

    override suspend fun updateCurrentUserDetails(userDetails: UserDetails) {
        updateCurrentUserProfile
            .updateCurrentUser(userDetails)
            .also {
                cache = userDetails
                writeLocalUserDetailsRepo.storeData(userDetails)
            }
    }

    override suspend fun getUserDetails(): UserDetails {

        return cache
            ?.let { it }
            ?: readLocalUserDetailsRepo.execute()
            ?: remoteUserDetailsRetriever
                .execute()
                .also {
                    cache = it
                    writeLocalUserDetailsRepo.storeData(it)
                }
    }

    override fun getAvatarUrl(firstName: String, lastName: String): String {
        val initials = "${firstName.first()}${lastName.first()}".toUpperCase()
        return "https://i0.wp.com/static-assets.clicktravel.com/avatars/$initials.jpg"
    }

    override suspend fun deleteUser() {
        cache = null
        writeLocalUserDetailsRepo.clearData()
    }

}