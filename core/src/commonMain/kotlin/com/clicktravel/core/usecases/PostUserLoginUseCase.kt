package com.clicktravel.core.usecases

import com.clicktravel.core.model.team.Team
import com.clicktravel.core.model.team.TeamsAndSelectedTeamIndex
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.repository.retriever.NoArgumentRetriever

// Not to be confused with authentication use case - this is the whole process
// which we go through when logging in and selecting a team if one has not already been
// selected, or when the user changes teams
interface PostUserLoginUseCase {
    suspend fun teamSelectionRequired(): Boolean
    suspend fun getTeams(): TeamsAndSelectedTeamIndex
    suspend fun selectTeam(team: Team)
}

class PostUserLoginUseCaseImpl(
    private val teamRetriever: NoArgumentRetriever<List<Team>>,
    private val selectedTeamRetriever: NoArgumentRetriever<Team?>,
    private val writeOnlyRepository: WriteOnlyRepository<Team>
) :
    PostUserLoginUseCase,
    WriteOnlyRepository<Team> by writeOnlyRepository
{

    private var cache: List<Team> = listOf()

    override suspend fun teamSelectionRequired(): Boolean {
        return if (selectedTeamRetriever.execute() == null) {
            teamRetriever.execute().also {
                cache = it
            }.let {
                if (it.size > 1) {// If there is only one team, we return
                    true
                } else {
                    writeOnlyRepository.storeData(it.first())
                    false
                }
            }
        } else {
            false
        }
    }

    override suspend fun getTeams(): TeamsAndSelectedTeamIndex {
        val teams = teamRetriever.execute()
        val selectedIndex = selectedTeamRetriever.execute()?.let(teams::indexOf)
        cache = teams

        return TeamsAndSelectedTeamIndex(
            teams = if (cache.isEmpty()) teams else cache,
            selectedIndex = selectedIndex ?: 0
        )
    }

    override suspend fun selectTeam(team: Team) = writeOnlyRepository.storeData(team)
}