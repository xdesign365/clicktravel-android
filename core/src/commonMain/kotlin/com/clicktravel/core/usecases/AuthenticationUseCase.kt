package com.clicktravel.core.usecases

import com.clicktravel.core.Settings
import com.clicktravel.core.feature.oauth.Encoder
import com.clicktravel.core.feature.oauth.Hasher
import com.clicktravel.core.model.auth.OauthModel
import com.clicktravel.core.model.team.Team
import com.clicktravel.core.util.ExpiryTimeQuery
import com.clicktravel.core.util.ExpiryTimeQueryImpl
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.itinerary.util.CompositeLogger
import com.clicktravel.repository.retriever.ArgumentRetriever
import com.clicktravel.repository.retriever.NoArgumentRetriever
import com.clicktravel.repository.retriever.RefreshAuthRetriever
import kotlin.random.Random

interface AuthenticationUseCase {
    fun buildLoginUrl(): String
    fun shouldKeepWebView(url: String): Boolean
    suspend fun completeLogin(url: String): OauthModel
    suspend fun isUserAlreadyLoggedIn(): OauthModel?
    suspend fun logout()
}

class AuthenticationUseCaseImpl(
    private val hasher: Hasher,
    private val encoder: Encoder,
    private val finalStageTokenRetriever: ArgumentRetriever<String, OauthModel>,
    private val retrieveLocalOauth: NoArgumentRetriever<OauthModel?>,
    private val refreshAuthRetriever: RefreshAuthRetriever,
    private val saveLocalOauth: WriteOnlyRepository<OauthModel>,
    private val timeQuery: ExpiryTimeQuery = ExpiryTimeQueryImpl(),
    private val teamRepository: WriteOnlyRepository<Team>,
    private val selectedTeamRepository: WriteOnlyRepository<Team>,
    private val journeys: UserJourneysUseCase
) : AuthenticationUseCase {

    private val state: String by lazy {
        with(StringBuilder()) {
            for (i in 0..30) {
                append(Random.nextInt(32, 122).toChar())
            }

            encoder.base64Encode(toString())
                .replace("=", "")
                .replace("\n", "")
        }
    }

    private val codeChallenge: String by lazy {
        with(StringBuilder()) {
            for (i in 0..44) {
                append(Random.nextInt(32, 127).toChar())
            }

            encoder.base64Encode(toString())
                .replace("=", "")
                .replace("\n", "")
        }
    }

    private val hashedCodeChannge: String by lazy {
        hasher.hash(codeChallenge)
    }

    override fun buildLoginUrl() = buildString {
        append(Settings.getProperty(Settings.LOGIN_URL))
        append("oauth/authorize?")
        append("client_id=${Settings.getProperty(Settings.CLIENT_ID)}")
        append("&redirect_uri=${Settings.getProperty(Settings.REDIRECT_URI)}")
        append("&code_challenge_method=S256")
        append("&scope=Default")
        append("&state=$state")
        append("&response_type=code")
        append("&code_challenge=$hashedCodeChannge")
    }


    override fun shouldKeepWebView(url: String) =
        url == buildLoginUrl() || url.startsWith(Settings.getProperty(Settings.REDIRECT_URI)).not()

    override suspend fun completeLogin(url: String) =
        finalStageTokenRetriever.execute(
            buildAuthUrl(
                getPropertyForUrl(
                    url,
                    CODE
                )
            )
        )
            .also { saveLocalOauth.storeData(it.copy(expiresIn = timeQuery.timeNowPlusExpiryTime(it.expiresIn))) }

    override suspend fun isUserAlreadyLoggedIn() = retrieveLocalOauth.execute()
        ?.let {
            if (timeQuery.hasTimeExpired(it.expiresIn)) {
                return try {
                    refreshAuthRetriever.refresh(it.refreshToken)
                } catch (exception: Throwable) {
                    CompositeLogger.logError(exception)
                    null
                }
            } else {
                it
            }
        }

    override suspend fun logout() {
        saveLocalOauth.clearData()
        teamRepository.clearData()
        selectedTeamRepository.clearData()
        journeys.clearData()
    }

    private fun buildAuthUrl(code: String) = buildString {
        append(Settings.getProperty(Settings.LOGIN_URL))
        append("oauth/token?")
        append("&redirect_uri=${Settings.getProperty(Settings.REDIRECT_URI)}")
        append("&grant_type=authorization_code")
        append("&client_id=${Settings.getProperty(Settings.CLIENT_ID)}")
        append("&code_challenge_method=S256")
        append("&code_verifier=$codeChallenge")
        append("&code=$code")
    }

    private fun getPropertyForUrl(url: String, property: String): String {
        val map = mutableMapOf<String, String>()

        url.split("?").last()
            .split("&")
            .forEach {

                with(it.split("=")) {
                    map[first()] = last()
                }
            }

        return map[property]!!
    }

    companion object {
        const val CODE = "code"
    }
}
