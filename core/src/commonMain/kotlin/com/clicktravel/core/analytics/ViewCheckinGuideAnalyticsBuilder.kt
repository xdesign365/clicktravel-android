package com.clicktravel.core.analytics

import com.clicktravel.core.model.FlightBooking
import com.soywiz.klock.DateTime
import com.soywiz.klock.DateTimeTz
import kotlin.math.roundToInt

class ViewCheckinGuideAnalyticsBuilder(private val getDate: () -> DateTimeTz = { DateTime.nowLocal() }) {

    fun build(booking: FlightBooking): Map<String, String> {
        val departure = booking.journeys.first().segments.first().legDeparture
        val returnDeparture = if (booking.journeys.size > 1) {
            booking.journeys.last().segments.first().legDeparture
        } else {
            null
        }

        return mapOf(
            AnalyticsStatics.userId to "user-id-goes-here-when-available",
            AnalyticsStatics.teamId to "team-id-goes-here-when-available",
            AnalyticsStatics.daysUntilOutbound to (departure - getDate()).days.roundToInt().toString(),
            AnalyticsStatics.daysUntilReturn to if (returnDeparture != null) {
                (returnDeparture - getDate()).days.roundToInt().toString()
            } else {
                "N/A"
            }
        )
    }

}