package com.clicktravel.core.analytics

object AnalyticsStatics {

    // Analytics titles
    const val initiateCall = "INITIATE_CALL"
    const val getDirections = "GET_DIRECTIONS"
    const val updateProfile = "UPDATE_PROFILE"
    const val viewCheckinGuide = "VIEW_CHECK-IN_GUIDE"
    const val viewETicket = "VIEW_ETICKET"
    const val resendPaymentInstructions = "RESEND_PAYMENT_INSTRUCTIONS"
    const val requestMobileNumber = "REQUEST_MOBILE_NUMBER"
    const val manageBooking = "MANAGE_BOOKING"
    const val viewNotification = "VIEW_NOTIFICATION"
    const val viewBookingDetails = "VIEW_BOOKING_DETAILS"
    const val forcedLogout = "FORCED_LOG_OUT"

    // Analytics event properties
    const val userId = "UserId"
    const val teamId = "TeamId"
    const val date = "Date"
    const val daysUntilOutbound = "DaysUntilOutbound"
    const val daysUntilReturn = "DaysUntilReturn"
    const val journeyType = "JourneyType"
    const val daysUntilBooking = "DaysUntilBooking"
    const val productType = "ProductType"
    const val manageType = "ManageType"
    const val nameUpdated = "NameUpdated"
    const val emailUpdated = "EmailUpdated"
    const val mobileUpdated = "MobileUpdated"
    const val hasETicket = "hasETicket"
    const val mapAvailable = "MapAvailable"
    const val logoutLocation = "LogoutLocation"
    const val logoutReason = "LogoutReason"

    // Analytics user properties
    const val neededTrainDirections = "NeededTrainDirections"
    const val neededAirportDirections = "NeededAirportDirections"
    const val neededHotelDirections = "NeededHotelDirections"
    const val hasBookings = "HasBookings"
    const val hasMultipleTeams = "HasMultipleTeams"
}