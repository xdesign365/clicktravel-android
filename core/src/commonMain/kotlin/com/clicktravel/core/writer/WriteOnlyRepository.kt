package com.clicktravel.core.writer

interface WriteOnlyRepository<StorageType> {
    suspend fun storeData(storeData: StorageType)
    suspend fun clearData()
}