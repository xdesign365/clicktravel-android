package com.clicktravel.core.writer

interface WriteOnlyRepositoryWithResponse<StorageType, ReturnType> {
    suspend fun storeData(storeData: StorageType): ReturnType
    suspend fun clearData()
}