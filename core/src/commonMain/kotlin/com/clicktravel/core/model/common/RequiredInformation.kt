package com.clicktravel.core.model.common

data class RequiredInformation(
    val key: String,
    val value: String
)