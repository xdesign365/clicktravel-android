package com.clicktravel.core.model

enum class TravelTypes {
    TRAIN,
    FLIGHT,
    HOTEL,
    TRAVELCARD
}