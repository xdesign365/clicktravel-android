package com.clicktravel.core.model.trains

// We'll get it as: Standard, Coach H: Seat 40
data class SeatReservation(val coachClass: String, val coach: String, val seat: Int)

fun SeatReservation.toUi() = "$coachClass, Coach $coach: Seat $seat"