package com.clicktravel.core.model.trains.enums

enum class PassengerType {
    ADULT,
    CHILD
}