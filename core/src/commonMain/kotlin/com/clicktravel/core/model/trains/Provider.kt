package com.clicktravel.core.model.trains

// London North Eastern Railway
data class Provider(val name: String, val code : String?=null)