package com.clicktravel.core.model


sealed class BookingsQuery

object AllUser : BookingsQuery()
class SpecificUser(val name: String) : BookingsQuery()