package com.clicktravel.core.model


data class FutureBookings(val items : List<BookingItem>)