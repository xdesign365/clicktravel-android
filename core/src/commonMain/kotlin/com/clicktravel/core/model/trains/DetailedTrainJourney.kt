package com.clicktravel.core.model.trains

import com.clicktravel.core.model.common.TrainStationOrAirport
import com.soywiz.klock.DateTimeTz

data class DetailedTrainJourney(
    val journeyId: String,
    val departureStation: TrainStationOrAirport,
    val arrivalStation: TrainStationOrAirport,
    val ticketType: TicketType,
    val departureDateTime: DateTimeTz,
    val segments: List<TrainSegment> = listOf(),
    val couponId: String? = null
)