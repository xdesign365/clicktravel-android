package com.clicktravel.core.model.team

data class Team(
    val id: String,
    val name: String,
    val ownerId: String,
    val shortId: String
)