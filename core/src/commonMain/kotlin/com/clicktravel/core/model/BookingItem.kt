package com.clicktravel.core.model

import com.clicktravel.core.model.common.Address
import com.clicktravel.core.model.common.RequiredInformation
import com.clicktravel.core.model.flights.DetailedFlightJourney
import com.clicktravel.core.model.trains.DetailedTrainJourney
import com.clicktravel.core.model.trains.Note
import com.clicktravel.core.model.trains.Railcard
import com.clicktravel.core.model.trains.enums.DeliveryOption
import com.clicktravel.core.model.trains.enums.PassengerType
import com.clicktravel.core.ui.models.ClickLocation
import com.soywiz.klock.DateTimeTz

sealed class BookingItem(
    val note: Note? = null,
    val field: List<RequiredInformation>,
    val teamId: String
)

data class TrainBooking(
    val bookingId: String,
    val bookingTravelType: TravelTypes,
    val passengerType: PassengerType,
    val railcard: Railcard? = null,
    val ticketCollectionReference: String,
    val address: Address,
    val forTheAttentionOf: String? = null,
    val deliveryOption: DeliveryOption,
    val journeys: List<DetailedTrainJourney>,
    val fareType: String,
    val fareClass: String,
    val notes: Note?,
    val fields: List<RequiredInformation> = listOf(),
    val team: String
) : BookingItem(notes, fields, team)


data class FlightBooking(
    val bookingId: String,
    val bookingTravelType: TravelTypes,
    val notes: Note?,
    val journeys: List<DetailedFlightJourney>,
    val ticketCollectionReference: String,
    val ticketNumbers: List<String>,
    val emailRequired: Boolean,
    val emailForCheckIn: String?,
    val fields: List<RequiredInformation> = listOf(),
    val team: String
) : BookingItem(notes, fields, team)


enum class BillingType {
    GUARANTEE_ONLY,
    ROOM_ONLY,
    BILLBACK
}

data class HotelBooking(
    val bookingId: String,
    val bookingTravelType: TravelTypes,
    val cancellationPolicy: String?,
    val numberOfAdults: Int,
    val numberOfChildren: Int,
    val roomType: String,
    val mealIncluded: String? = null,
    val specialRequest: String? = null,
    val checkinDate: DateTimeTz,
    val checkoutDate: DateTimeTz,
    val hotelName: String,
    val hotelEmail: String? = null,
    val hotelAddress: String,
    val hotelPhoneNumber: String? = null,
    val notes: Note?,
    val fields: List<RequiredInformation>,
    val team: String,
    val location: ClickLocation? = null,
    val billingType: BillingType,
    val isPremierInn: Boolean,
    val travellerEmail: String,
    val billbackElements: HotelBillbackElements?,
    val supportedBillbackOptions: HotelSupportedBillbackOptions?
) : BookingItem(notes, fields, team)

data class HotelBillbackElements(
    val allCosts: Boolean,
    val breakfast: Boolean,
    val roomRate: Boolean,
    val parking: Boolean,
    val wifi: Boolean,
    val mealSupplement: MealSupplementBillbackElement?
)

data class MealSupplementBillbackElement(
    val enabled: Boolean,
    val allowance: String?,
    val currency: String?
)

data class HotelSupportedBillbackOptions(
    val breakfast: Boolean,
    val roomRate: Boolean,
    val parking: Boolean,
    val wifi: Boolean,
    val mealSupplement: Boolean
)

fun HotelBooking.durationOfStayInDays(): Int = checkoutDate.minus(checkinDate).days.toInt()

data class TravelCard(
    val bookingId: String,
    val travelDate: DateTimeTz,
    val deliveryOption: DeliveryOption,
    val totalFare: String,
    val fareCurrency: String,
    val passengerType: PassengerType,
    val ctrReference: String,
    val ticketDescription: String,
    val ticketType: String,
    val notes: Note?,
    val fields: List<RequiredInformation>,
    val team: String,
    val travelTypes: TravelTypes,
    val address: Address,
    val forTheAttentionOf: String? = null
) : BookingItem(notes, fields, team)