package com.clicktravel.core.model.auth

data class OauthModel(
    val accessToken: String,
    val expiresIn: Long,
    val tokenType: String,
    val refreshToken: String
)