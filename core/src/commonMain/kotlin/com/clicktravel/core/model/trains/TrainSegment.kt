package com.clicktravel.core.model.trains

import com.clicktravel.core.model.common.TrainStationOrAirport
import com.soywiz.klock.DateTime
import com.soywiz.klock.DateTimeTz

enum class ConnectionOrJourneyType {
    TRAIN,
    WAIT,
    FERRY,
    UNDERGROUND,
    WALK,
    FLIGHT,
    BUS
}

data class TrainSegment(
    val travelType: ConnectionOrJourneyType,
    val legDeparture: DateTimeTz,
    val legArrival: DateTimeTz,
    val arrivalLocation: TrainStationOrAirport,
    val departureLocation : TrainStationOrAirport,
    val provider: Provider,
    val travelTime: Long,
    val reservation: SeatReservation? = null,
    val trainClassInformation: TrainClassInformation,
    val connectionTime: Long
)