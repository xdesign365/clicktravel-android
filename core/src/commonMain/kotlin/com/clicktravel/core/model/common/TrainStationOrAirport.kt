package com.clicktravel.core.model.common

import com.clicktravel.core.ui.models.ClickLocation

// E.g. Birmingham, BHX
data class TrainStationOrAirport(val name: String, val code: String, val geo : ClickLocation?=null, val trainCrs: String?=null)