package com.clicktravel.core.model.hotels

data class Occupants(
    val adults: Int,
    val children: Int
)