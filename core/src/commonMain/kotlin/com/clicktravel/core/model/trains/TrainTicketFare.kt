package com.clicktravel.core.model.trains

// E.g. Valid only Virgin Trains services
data class TrainTicketFare(val description: String)