package com.clicktravel.core.model.flights

data class BaggageAllowance(
    val items: Int,
    val weight: Double
)