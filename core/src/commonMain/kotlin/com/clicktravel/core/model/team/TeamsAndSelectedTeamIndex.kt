package com.clicktravel.core.model.team

data class TeamsAndSelectedTeamIndex(
    val teams: List<Team>,
    val selectedIndex: Int = 0
)