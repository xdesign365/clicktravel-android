package com.clicktravel.core.model.trains

data class TicketType(val type: String)