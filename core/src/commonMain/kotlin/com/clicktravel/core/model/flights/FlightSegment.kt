package com.clicktravel.core.model.flights

import com.clicktravel.core.model.common.TrainStationOrAirport
import com.clicktravel.core.model.trains.Provider
import com.soywiz.klock.DateTime
import com.soywiz.klock.DateTimeTz

data class FlightSegment(
    val segment: String,
    val legDeparture: DateTimeTz,
    val legArrival: DateTimeTz,
    val arrivalLocation: TrainStationOrAirport,
    val departureLocation: TrainStationOrAirport,
    val travelTime: Long,
    val cabinClass: CabinClass,
    val operatingAirline: Provider,
    val baggageAllowance: BaggageAllowance,
    val flightNumber : String
)