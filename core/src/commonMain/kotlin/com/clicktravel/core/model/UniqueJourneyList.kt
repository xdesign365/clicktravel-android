package com.clicktravel.core.model

import com.clicktravel.core.util.exhaustive

data class UniqueJourneyList(val uniqueJourneyList: List<UniqueJourney>) {

    val sortedJourneyList: List<UniqueJourney> = uniqueJourneyList.sortedBy {
        when (it.bookingItem) {
            is TrainBooking -> it.bookingItem.journeys[it.journeyNumber].segments.first().legDeparture.local
            is FlightBooking -> it.bookingItem.journeys[it.journeyNumber].segments.first().legDeparture.local
            is HotelBooking -> it.bookingItem.checkinDate.local
            is TravelCard -> it.bookingItem.travelDate.local
        }.exhaustive
    }
}