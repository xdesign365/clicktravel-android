package com.clicktravel.core.model.common

data class Address(val firstLine: String, val secondLine: String, val thirdLine: String)

fun Address.toCsv() = "$firstLine, $secondLine, $thirdLine"