package com.clicktravel.core.model.flights

data class CabinClass(
    val cabinClass : String,
    val code : String
)