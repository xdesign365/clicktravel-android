package com.clicktravel.core.model.flights

data class DetailedFlightJourney(
    val segments : List<FlightSegment>,
    val journeyId : String
)