package com.clicktravel.core.model.trains

// E.g. Train has First & Standard class seating
data class TrainClassInformation(val classDetails: String? = null)