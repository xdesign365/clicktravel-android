package com.clicktravel.core.model.trains.enums

enum class DeliveryOption {
    COLLECT_AT_STATION,
    POST,
    SPECIAL_DELIVERY,
    FIRST_CLASS_POST,
    E_TICKET,
    NONE
}