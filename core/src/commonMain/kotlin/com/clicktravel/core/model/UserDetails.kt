package com.clicktravel.core.model

data class UserDetails(
    val id: String,
    val firstName: String,
    val lastName: String,
    val mobileNumber: String?,
    val phoneNumber: String?,
    val primaryEmailAddress: String?,
    val title: String?
)