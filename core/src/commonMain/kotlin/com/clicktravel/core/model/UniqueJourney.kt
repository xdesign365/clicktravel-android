package com.clicktravel.core.model

data class UniqueJourney(
    val bookingItem: BookingItem,
    val journeyNumber: Int
)