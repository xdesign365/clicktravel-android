package com.clicktravel.core

object Settings {

    private val properties = mutableMapOf<String, String>()

    fun setupDefaults(map: Map<String, String>) {
        properties.putAll(map)
    }

    fun getProperty(property: String) =
        properties[property] ?: throw IllegalArgumentException("$property does not exist")

    fun setProperty(property: String, value: String) {
        properties[property] = value
    }


    const val MAIN_URL = "MAIN_URL"
    const val USER_URL = "USER_URL"
    const val LOGIN_URL = "LOGIN_URL"
    const val CLIENT_ID = "CLIENT_ID"
    const val REDIRECT_URI = "REDIRECT_URI"
    const val CANCEL_BOOKING_URL = "CANCEL_BOOKNG_URL"
    const val VIEW_BOOKING_URL = "VIEW_BOOKING_URL"
}