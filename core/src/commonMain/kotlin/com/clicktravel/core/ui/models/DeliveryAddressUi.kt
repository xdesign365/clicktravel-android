package com.clicktravel.core.ui.models

import com.clicktravel.core.model.*
import com.clicktravel.core.model.common.toCsv
import com.clicktravel.core.model.trains.enums.DeliveryOption

data class DeliveryAddressUi(
    val deliveryAddress: String? = null,
    val deliveryOption: DeliveryOption,
    val forTheAttentionOf: String? = null
)


fun UniqueJourney.toDeliveryAddress(): DeliveryAddressUi? {
    return when (val booking = bookingItem) {
        is TrainBooking -> DeliveryAddressUi(
            deliveryAddress = if (booking.deliveryOption == DeliveryOption.POST ||
                booking.deliveryOption == DeliveryOption.FIRST_CLASS_POST ||
                booking.deliveryOption == DeliveryOption.SPECIAL_DELIVERY
            )
                booking.address.toCsv()
            else
                null,
            deliveryOption = booking.deliveryOption,
            forTheAttentionOf = booking.forTheAttentionOf
        )
        is FlightBooking -> DeliveryAddressUi(deliveryOption = DeliveryOption.COLLECT_AT_STATION)
        is HotelBooking -> null
        is TravelCard -> DeliveryAddressUi(
            deliveryAddress = if (booking.deliveryOption == DeliveryOption.POST ||
                booking.deliveryOption == DeliveryOption.FIRST_CLASS_POST ||
                booking.deliveryOption == DeliveryOption.SPECIAL_DELIVERY
            )
                booking.address.toCsv()
            else
                null,
            deliveryOption = booking.deliveryOption,
            forTheAttentionOf = booking.forTheAttentionOf
        )
    }
}
