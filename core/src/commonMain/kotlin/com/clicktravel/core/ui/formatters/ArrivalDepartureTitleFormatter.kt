package com.clicktravel.core.ui.formatters

import com.clicktravel.core.model.common.TrainStationOrAirport
import com.clicktravel.core.ui.models.toTrainUi
import com.clicktravel.core.ui.models.toUi
import com.clicktravel.core.util.DateFormatter
import com.clicktravel.core.util.HoursDisplayFormatter
import com.soywiz.klock.DateTime

interface ArrivalDepartureTitleFormatter {
    fun parse(dateTime: DateTime, trainStationOrAirport: TrainStationOrAirport): String
}


class TrainDepartureTitleFormatter(private val dateFormatter: DateFormatter = HoursDisplayFormatter()) :
    ArrivalDepartureTitleFormatter {

    override fun parse(dateTime: DateTime, trainStationOrAirport: TrainStationOrAirport): String {
        return "${dateFormatter.format(dateTime.localUnadjusted)} ${trainStationOrAirport.toTrainUi()}"
    }
}

class FlightDepartureTitleFormatter(private val dateFormatter: DateFormatter = HoursDisplayFormatter()) :
    ArrivalDepartureTitleFormatter {

    override fun parse(dateTime: DateTime, trainStationOrAirport: TrainStationOrAirport): String {
        return "${dateFormatter.format(dateTime.localUnadjusted)} ${trainStationOrAirport.copy(
            name = trainStationOrAirport.name.split(",").first()
        ).toUi()}"
    }
}