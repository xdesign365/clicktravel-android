package com.clicktravel.core.ui.models

import com.clicktravel.core.model.trains.ConnectionOrJourneyType

sealed class LegUi(val type: ConnectionOrJourneyType)

class JourneyLegUi(
    val journeyArrivalTitle: String? = null,
    val providerName: String,
    val journeyTime: String,
    val coachAndSeatInfo: String? = null,
    val seatingClass: String? = null,
    val ticketRoute: String? = null,
    val journeyDepartureTitle: String? = null,
    val topIcon: Icon = Icon.GREY_ICON,
    val bottomIcon: Icon = Icon.GREY_ICON,
    type: ConnectionOrJourneyType,
    val baggageAllowance: String? = null
) : LegUi(type)

class ConnectionLegUi(
    type: ConnectionOrJourneyType,
    val connectionText: String
) : LegUi(type)


