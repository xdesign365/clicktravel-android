package com.clicktravel.core.ui.builders

import com.clicktravel.core.model.FlightBooking
import com.clicktravel.core.model.flights.BaggageAllowance
import com.clicktravel.core.model.flights.FlightSegment
import com.clicktravel.core.model.trains.ConnectionOrJourneyType
import com.clicktravel.core.model.trains.enums.DeliveryOption
import com.clicktravel.core.ui.formatters.ArrivalDepartureTitleFormatter
import com.clicktravel.core.ui.formatters.FlightDepartureTitleFormatter
import com.clicktravel.core.ui.models.*
import com.clicktravel.core.util.DateFormatter
import com.clicktravel.core.util.HoursDisplayFormatter
import kotlin.math.roundToInt

class UiBuilderFlights(
    private val yearlyDateFormatter: DateFormatter,
    private val arrivalDepartureTitleFormatter: ArrivalDepartureTitleFormatter = FlightDepartureTitleFormatter()
) : BaseUiBuilder<FlightBooking>() {

    override fun buildTicket(booking: FlightBooking, selectedIndex: Int) = convertFlightTicketToUi(
        selectedIndex, booking
    )

    private fun convertFlightTicketToUi(journeyNumber: Int, bookingItem: FlightBooking) =
        mutableListOf<TicketUi>().apply {
            bookingItem.journeys.forEachIndexed { index, journey ->
                TicketUi(
                    ticketDate = yearlyDateFormatter.format(journey.segments.first().legDeparture),
                    bottomIcon = ConnectionOrJourneyType.FLIGHT.toEndIcon(),
                    topIcon = ConnectionOrJourneyType.FLIGHT.toStartIcon(),
                    ticketType = DeliveryOption.NONE,
                    arrivalTitle = arrivalDepartureTitleFormatter.parse(
                        journey.segments.last().legArrival.local,
                        journey.segments.last().arrivalLocation
                    ),
                    departureTitle = arrivalDepartureTitleFormatter.parse(
                        journey.segments.first().legDeparture.local,
                        journey.segments.first().departureLocation
                    ),
                    selectedTicket = index == journeyNumber,
                    legs = journey.segments.flightSegmentToUi(),
                    bookingId = bookingItem.bookingId,
                    journeyId = journey.journeyId
                ).let(::add)
            }
        }

    private fun List<FlightSegment>.flightSegmentToUi(
        dateFormatter: DateFormatter = HoursDisplayFormatter()
    ): List<LegUi> {
        val returnValue = mutableListOf<LegUi>()

        forEachIndexed { index, journey ->

            if (index > 0 && index != size) {
                returnValue.add(
                    buildConnectionLeg(this[index - 1].legArrival, journey.legDeparture)
                )
            }

            returnValue.add(
                JourneyLegUi(
                    journeyArrivalTitle = getJourneyArrivalTitle(dateFormatter, journey, index),
                    journeyDepartureTitle = getJourneyDepartureTitle(dateFormatter, journey, index),
                    topIcon = if (index == 0) {
                        Icon.NONE
                    } else {
                        Icon.GREY_ICON
                    },
                    bottomIcon = previousJourneyIcon(this, index),
                    coachAndSeatInfo = getCoachAndSeatInfo(journey),
                    journeyTime = journey.travelTime.minutesToDisplayableTime(),
                    providerName = getProviderName(journey),
                    type = ConnectionOrJourneyType.FLIGHT,
                    baggageAllowance = getBaggageAllowance(journey.baggageAllowance)
                )
            )
        }

        return returnValue
    }

    private fun getJourneyDepartureTitle(
        dateFormatter: DateFormatter,
        journey: FlightSegment,
        index: Int
    ): String? {
        return journeyTitleIfNotFirstItem(
            "${dateFormatter.format(journey.legDeparture)} ${journey.departureLocation.copy(
                name = journey.departureLocation.name.split(",").first()
            ).toUi()}",
            index
        )
    }

    private fun List<FlightSegment>.getJourneyArrivalTitle(
        dateFormatter: DateFormatter,
        journey: FlightSegment,
        index: Int
    ): String? {
        return journeyTitleIfNotLastItem(
            "${dateFormatter.format(journey.legArrival)} ${journey.arrivalLocation.copy(
                name = journey.arrivalLocation.name.split(",").first()
            ).toUi()}",
            index,
            this
        )
    }

    private fun getBaggageAllowance(allowance: BaggageAllowance): String =
        if (allowance.items == 0) {
            "No checked baggage"
        } else {
            if (allowance.weight == 0.0) {
                if (allowance.items > 1) {
                    "${allowance.items} bags"
                } else {
                    "${allowance.items} bag"
                }
            } else {
                "${allowance.items} x ${allowance.weight.roundToInt()}kg bag"
            }
        }

    private fun getProviderName(journey: FlightSegment) =
        "${journey.operatingAirline.name} (${journey.operatingAirline.code} ${journey.flightNumber})"

    private fun getCoachAndSeatInfo(journey: FlightSegment) =
        journey.cabinClass.cabinClass.toLowerCase().capitalize()
}