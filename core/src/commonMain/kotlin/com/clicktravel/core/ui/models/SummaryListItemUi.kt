package com.clicktravel.core.ui.models

import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.trains.Railcard
import com.soywiz.klock.DateTimeTz

sealed class SummaryListItemUi

data class UniqueJourneyListItem(
    val bookingId: String,
    val journeyNumber: Int,
    val travelType: TravelTypes,
    val title: String,
    val subtitle: String,
    val bookingReferenceTitle: String? = null,
    val time: String,
    val railcard: Railcard? = null,
    val isReturnTicket: Boolean = false,
    val isEticket: Boolean = false,
    val ticketNumber: String? = null,
    val dateTimeTz: DateTimeTz
) : SummaryListItemUi()

data class HotelStayListItem(
    val name: String,
    val address: String,
    val nightsStay: String,
    val dateTimeTz: DateTimeTz,
    val travelType: TravelTypes = TravelTypes.HOTEL,
    val bookingId: String
) : SummaryListItemUi()

data class TravelCardListItem(
    val bookingId: String,
    val description: String,
    val type: String,
    val travelType: TravelTypes = TravelTypes.TRAVELCARD,
    val dateTimeTz: DateTimeTz
) : SummaryListItemUi()

data class DateHeaderListItem(
    val headerTitle: String,
    val dateTimeTz: DateTimeTz
) : SummaryListItemUi()