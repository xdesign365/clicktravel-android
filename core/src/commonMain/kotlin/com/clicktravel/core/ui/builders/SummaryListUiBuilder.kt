package com.clicktravel.core.ui.builders

import com.clicktravel.core.model.*
import com.clicktravel.core.model.trains.enums.DeliveryOption
import com.clicktravel.core.ui.formatters.FlightDepartureTitleFormatter
import com.clicktravel.core.ui.formatters.TrainDepartureTitleFormatter
import com.clicktravel.core.ui.models.*
import com.clicktravel.core.util.*

class SummaryListUiBuilder(private val dateFormatter: DateFormatter = SummaryUiDateFormatter()) {

    fun build(list: UniqueJourneyList) = mutableListOf<SummaryListItemUi>().apply {

        if (list.sortedJourneyList.isNotEmpty()) {
            val firstItem = list.sortedJourneyList.first().startDate()
            var currentDateHeader = firstItem
                // Add the header which is the date of the first item
                .also {
                    add(DateHeaderListItem(dateFormatter.format(it), it))
                }

            list.sortedJourneyList.forEach {
                if (currentDateHeader.sameDay(it.startDate()).not()) {
                    val startDate = it.startDate()
                    add(DateHeaderListItem(dateFormatter.format(startDate), it.startDate()))
                    currentDateHeader = startDate
                }

                add(
                    when (it.bookingItem) {
                        is TrainBooking -> uniqueJourneyListItemForTrain(it.bookingItem, it)
                        is FlightBooking -> uniqueJourneyListItemForFlight(it.bookingItem, it)
                        is HotelBooking -> uniqueJourneyListItemForHotel(it.bookingItem)
                        is TravelCard -> uniqueJourneyListItemForTravelCard(it.bookingItem)
                    }.exhaustive
                )
            }
        }
    }

    private fun uniqueJourneyListItemForTravelCard(
        bookingItem: TravelCard
    ) =
        TravelCardListItem(
            bookingId = bookingItem.bookingId,
            description = bookingItem.ticketDescription,
            type = bookingItem.ticketType,
            dateTimeTz = bookingItem.travelDate
        )

    private fun uniqueJourneyListItemForHotel(bookingItem: HotelBooking) =
        HotelStayListItem(
            name = bookingItem.hotelName,
            address = bookingItem.hotelAddress,
            nightsStay = bookingItem.checkinDate.daysBetween(bookingItem.checkoutDate).toString(),
            dateTimeTz = bookingItem.checkinDate,
            bookingId = bookingItem.bookingId
        )

    private fun uniqueJourneyListItemForFlight(
        bookingItem: FlightBooking,
        it: UniqueJourney
    ): UniqueJourneyListItem {
        val formatter = FlightDepartureTitleFormatter()

        val journey = bookingItem.journeys[it.journeyNumber]
        val minutes =
            (journey.segments.last().legArrival - journey.segments.first().legDeparture)
                .toDisplayable()

        return UniqueJourneyListItem(
            bookingItem.bookingId,
            it.journeyNumber,
            TravelTypes.FLIGHT,
            formatter.parse(
                journey.segments.first().legDeparture.local,
                journey.segments.first().departureLocation
            ),
            formatter.parse(
                journey.segments.last().legArrival.local,
                journey.segments.last().arrivalLocation
            ),
            bookingReferenceTitle = "Booking Ref. ${bookingItem.ticketCollectionReference}",
            ticketNumber = if (bookingItem.ticketNumbers.isNotEmpty()) {
                "Ticket no. ${bookingItem.ticketNumbers.first()}"
            } else {
                null
            },
            time = minutes,
            dateTimeTz = journey.segments.first().legDeparture
        )
    }

    private fun uniqueJourneyListItemForTrain(
        bookingItem: TrainBooking,
        it: UniqueJourney
    ): UniqueJourneyListItem {
        val formatter = TrainDepartureTitleFormatter()
        val journey = bookingItem.journeys[it.journeyNumber]
        val minutes =
            (journey.segments.last().legArrival - journey.departureDateTime)
                .toDisplayable()

        return UniqueJourneyListItem(
            bookingItem.bookingId,
            it.journeyNumber,
            TravelTypes.TRAIN,
            formatter.parse(
                journey.departureDateTime.local,
                journey.departureStation
            ),
            formatter.parse(
                journey.segments.last().legArrival.local,
                journey.segments.last().arrivalLocation
            ),
            if (bookingItem.deliveryOption == DeliveryOption.COLLECT_AT_STATION) {
                bookingItem.ticketCollectionReference
            } else {
                null
            },
            time = minutes,
            railcard = bookingItem.railcard,
            ticketNumber = if (bookingItem.deliveryOption == DeliveryOption.COLLECT_AT_STATION) {
                "Ticket Collection Reference"
            } else {
                null
            },
            isReturnTicket = bookingItem.journeys.size > 1 || bookingItem.journeys[0].ticketType.type.toLowerCase().contains(
                "return"
            ),
            isEticket = bookingItem.deliveryOption == DeliveryOption.E_TICKET,
            dateTimeTz = journey.segments.first().legDeparture
        )
    }

    private fun UniqueJourney.startDate() = when (this.bookingItem) {
        is TrainBooking -> bookingItem.journeys[journeyNumber].departureDateTime
        is FlightBooking -> bookingItem.journeys[journeyNumber].segments.first().legDeparture
        is HotelBooking -> bookingItem.checkinDate
        is TravelCard -> bookingItem.travelDate
    }.exhaustive
}