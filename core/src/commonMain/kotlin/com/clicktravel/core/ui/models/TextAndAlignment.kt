package com.clicktravel.core.ui.models

data class TextAndAlignment(
    val text: String,
    val centerAligned: Boolean
)