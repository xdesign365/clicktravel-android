package com.clicktravel.core.ui.models

import com.clicktravel.core.model.hotels.Occupants

data class HotelDetailsUi(
    val occupants: Occupants,
    val roomDescription: String,
    val mealIncluded: String? = null,
    val specialRequest: String? = null,
    val lengthOfStay: Int,
    val checkInDate: String,
    val checkOutDate: String,
    val billbackText: TextAndAlignment
)