package com.clicktravel.core.ui.models

import com.soywiz.klock.TimeSpan

fun TimeSpan.toDisplayable() = this.minutes.toLong().minutesToDisplayableTime()

fun Long.minutesToDisplayableTime(): String {
    val mins = this % 60
    var hours = (this - mins) / 60
    val days = (hours - (hours % 24 )) / 24
    hours %= 24

    val returnValue = StringBuilder()

    if (days > 0) {
        returnValue.append("${days}days ")
    }

    if (hours > 0) {
        returnValue.append("${hours}hrs ")
    }


    returnValue.append("${mins}mins")

    return returnValue.toString()
}