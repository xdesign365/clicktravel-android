package com.clicktravel.core.ui.models


import com.clicktravel.core.model.*
import com.clicktravel.core.model.trains.Railcard
import com.clicktravel.core.model.trains.enums.DeliveryOption
import com.clicktravel.core.util.exhaustive
import com.soywiz.klock.DateTimeTz

data class ClickLocation(val lat: Double, val lon: Double)

sealed class JourneyHeader(
    val title: String,
    val travelType: TravelTypes,
    val railcard: Railcard? = null,
    val ticketReference: String? = null,
    val subtitleText: String? = null,
    val mapReference: ClickLocation? = null,
    val travelCardTicketType: String? = null,
    val travelCardTravelDate: DateTimeTz? = null
) {
    fun isHasRailcard() = railcard != null
    fun isShowTicketReference() = ticketReference != null
}

class TrainJourneyHeader(
    title: String,
    travelType: TravelTypes,
    railcard: Railcard? = null,
    ticketReference: String? = null,
    subtitleText: String? = null,
    mapReference: ClickLocation? = null
) : JourneyHeader(title, travelType, railcard, ticketReference, subtitleText, mapReference)


class FlightJourneyHeader(
    title: String,
    travelType: TravelTypes,
    railcard: Railcard? = null,
    ticketReference: String? = null,
    subtitleText: String? = null,
    mapReference: ClickLocation? = null
) : JourneyHeader(title, travelType, railcard, ticketReference, subtitleText, mapReference)

class HotelJourneyHeader(
    title: String,
    travelType: TravelTypes,
    subtitleText: String? = null,
    mapReference: ClickLocation? = null,
    val email: String? = null,
    val phone: String? = null
) : JourneyHeader(title, travelType, null, null, subtitleText, mapReference)

class TravelCardHeader(
    title: String,
    travelType: TravelTypes,
    subtitleText: String,
    cardReference: String?,
    ticketType: String,
    travelDate: DateTimeTz? = null,
    val deliveryOption: DeliveryOption
) : JourneyHeader(
    title = title,
    travelType = travelType,
    railcard = null,
    ticketReference = cardReference,
    subtitleText = subtitleText,
    mapReference = null,
    travelCardTicketType = ticketType,
    travelCardTravelDate = travelDate
)

fun UniqueJourney.toJourneyHeader() =
    when (bookingItem) {
        is TrainBooking -> handleJourneyHeaderForTrain(bookingItem)
        is FlightBooking -> handleJourneyHeaderForFlight(bookingItem)
        is HotelBooking -> handleJourneyHeaderForHotel(bookingItem)
        is TravelCard -> handleHeaderForTravelCard(bookingItem)
    }.exhaustive

private fun handleHeaderForTravelCard(bookingItem: TravelCard) =
    TravelCardHeader(
        title = bookingItem.ticketDescription,
        travelType = bookingItem.travelTypes,
        subtitleText = "",
        travelDate = bookingItem.travelDate,
        ticketType = bookingItem.ticketType,
        cardReference = if (bookingItem.deliveryOption == DeliveryOption.COLLECT_AT_STATION) {
            bookingItem.ctrReference
        } else {
            null
        },
        deliveryOption = bookingItem.deliveryOption
    )

private fun handleJourneyHeaderForHotel(bookingItem: HotelBooking) =
    HotelJourneyHeader(
        bookingItem.hotelName,
        TravelTypes.HOTEL,
        bookingItem.hotelAddress,
        mapReference = bookingItem.location,
        email = bookingItem.hotelEmail,
        phone = bookingItem.hotelPhoneNumber
    )

private fun UniqueJourney.handleJourneyHeaderForFlight(bookingItem: FlightBooking): JourneyHeader {
    val title = if (bookingItem.journeys[journeyNumber].segments.size == 1) {
        handleSingleLegFlightTitle(bookingItem)
    } else {
        handleMultiLegFlight(bookingItem)
    }

    return FlightJourneyHeader(
        title,
        TravelTypes.FLIGHT,
        ticketReference = bookingItem.ticketCollectionReference,
        mapReference = bookingItem.journeys[journeyNumber].segments.first().departureLocation.geo,
        subtitleText = bookingItem.ticketNumbers.let { if (it.isNotEmpty()) it.first() else "N/A" }
    )
}

private fun UniqueJourney.handleJourneyHeaderForTrain(bookingItem: TrainBooking): JourneyHeader {
    return TrainJourneyHeader(
        title = with(bookingItem.journeys[journeyNumber]) {
            "${departureStation.name} → ${arrivalStation.name}"
        },
        travelType = TravelTypes.TRAIN,
        railcard = bookingItem.railcard,
        ticketReference = if (bookingItem.deliveryOption == DeliveryOption.COLLECT_AT_STATION) {
            bookingItem.ticketCollectionReference
        } else {
            null
        },
        mapReference = bookingItem.journeys[journeyNumber].departureStation.geo
    )
}

private fun UniqueJourney.handleMultiLegFlight(bookingItem: FlightBooking) =
    with(StringBuilder()) {
        val departureLocation =
            bookingItem.journeys[journeyNumber].segments.first().departureLocation
        val arrivalLocation =
            bookingItem.journeys[journeyNumber].segments.last().arrivalLocation

        append("${departureLocation.code} → ")
        for (i in 1 until bookingItem.journeys[journeyNumber].segments.size) {
            val item = bookingItem.journeys[journeyNumber].segments[i].departureLocation
            append("${item.code} → ")
        }

        append(arrivalLocation.code)
        toString()
    }


private fun UniqueJourney.handleSingleLegFlightTitle(bookingItem: FlightBooking): String {
    val departureLocation =
        bookingItem.journeys[journeyNumber].segments.first().departureLocation
    val arrivalLocation =
        bookingItem.journeys[journeyNumber].segments.last().arrivalLocation


    return "${departureLocation.name.split(",").first()}, ${departureLocation.name.split(",")[1].trim()} (${departureLocation.code})" +
            " → " +
            "${arrivalLocation.name.split(",").first()}, ${arrivalLocation.name.split(",")[1].trim()} (${arrivalLocation.code})"
}
