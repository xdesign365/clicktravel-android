package com.clicktravel.core.ui.models

import com.clicktravel.core.model.trains.SeatReservation

class SeatParser {

    fun parseSeat(
        seatingClass: String,
        seatNumber: String
    ) = if (seatNumber == "NOPL") {
        null
    } else {
        val data = Regex("([A-Z])([0-9]{1,})([A-Z]?)")
            .find(seatNumber)

        val seat = data?.groupValues?.get(2)?.toInt()
        val coach = data?.groupValues?.get(1)
        if (seat != null && coach != null) {
            SeatReservation(seatingClass.toLowerCase().capitalize(), coach, seat)
        } else {
            null
        }
    }
}