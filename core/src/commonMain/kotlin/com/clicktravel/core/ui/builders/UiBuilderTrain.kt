package com.clicktravel.core.ui.builders

import com.clicktravel.core.model.TrainBooking
import com.clicktravel.core.model.trains.ConnectionOrJourneyType
import com.clicktravel.core.model.trains.TrainSegment
import com.clicktravel.core.model.trains.toUi
import com.clicktravel.core.ui.formatters.ArrivalDepartureTitleFormatter
import com.clicktravel.core.ui.formatters.TrainDepartureTitleFormatter
import com.clicktravel.core.ui.models.*
import com.clicktravel.core.util.DateFormatter

class UiBuilderTrain(
    private val yearlyDateFormatter: DateFormatter,
    private val arrivalDepartureTitleFormatter: ArrivalDepartureTitleFormatter = TrainDepartureTitleFormatter()
) : BaseUiBuilder<TrainBooking>() {


    override fun buildTicket(booking: TrainBooking, selectedIndex: Int) =
        convertTrainTicketToUi(selectedIndex, booking)


    private fun convertTrainTicketToUi(journeyNumber: Int, bookingItem: TrainBooking) =
        mutableListOf<TicketUi>().apply {
            bookingItem.journeys.forEachIndexed { index, journey ->
                TicketUi(
                    ticketTitle = journey.ticketType.type,
                    arrivalTitle = arrivalDepartureTitleFormatter.parse(
                        journey.segments.last().legArrival.local,
                        journey.segments.last().arrivalLocation
                    ),
                    ticketDate = yearlyDateFormatter.format(journey.departureDateTime),
                    legs = journey.segments.toUi(
                        ticketRoute = bookingItem.fareType
                    ),
                    selectedTicket = index == journeyNumber,
                    ticketType = bookingItem.deliveryOption,
                    departureTitle = arrivalDepartureTitleFormatter.parse(
                        journey.segments.first().legDeparture.local,
                        journey.segments.first().departureLocation
                    ),
                    topIcon = journey.segments.first().travelType.toStartIcon(),
                    bottomIcon = journey.segments.last().travelType.toEndIcon(),
                    bookingId = bookingItem.bookingId,
                    journeyId = journey.journeyId
                ).let(::add)
            }
        }

    private fun List<TrainSegment>.toUi(
        ticketRoute: String
    ): List<LegUi> {
        val returnValue = mutableListOf<LegUi>()

        forEachIndexed { index, journey ->

            if (index > 0) {
                if ((journey.travelType == ConnectionOrJourneyType.TRAIN || journey.travelType == ConnectionOrJourneyType.FERRY || journey.travelType == ConnectionOrJourneyType.BUS) &&
                    (this[index - 1].travelType == ConnectionOrJourneyType.TRAIN || this[index - 1].travelType == ConnectionOrJourneyType.FERRY || this[index - 1].travelType == ConnectionOrJourneyType.BUS)
                ) {
                    returnValue.add(
                        buildConnectionLeg(this[index - 1].legArrival, journey.legDeparture)
                    )
                }
            }

            when (journey.travelType) {
                ConnectionOrJourneyType.TRAIN -> JourneyLegUi(
                    // If this is the last train station
                    // we won't display the arrival station on our train_leg card,
                    // as it'll be on the journey ticket already
                    journeyArrivalTitle = journeyTitleIfNotLastItem(
                        arrivalDepartureTitleFormatter.parse(
                            journey.legArrival.local,
                            journey.arrivalLocation
                        ),
                        index,
                        this
                    ),
                    providerName = journey.provider.name,
                    journeyTime = journey.travelTime.minutesToDisplayableTime(),
                    coachAndSeatInfo = journey.reservation?.toUi(),
                    seatingClass = journey.trainClassInformation.classDetails,
                    ticketRoute = ticketRoute,
                    type = journey.travelType,
                    // If this is the first train station
                    // we won't display the departure station on our train_leg card,
                    // as it'll be on the journey ticket already
                    journeyDepartureTitle = journeyTitleIfNotFirstItem(
                        arrivalDepartureTitleFormatter.parse(
                            journey.legDeparture.local, journey.departureLocation
                        ), index
                    ),
                    bottomIcon = previousJourneyIcon(this, index),
                    topIcon = nextJourneyIcon(this, index)
                )
                ConnectionOrJourneyType.WAIT -> ConnectionLegUi(
                    type = journey.travelType,
                    connectionText = journey.arrivalLocation.name
                )
                ConnectionOrJourneyType.FERRY -> JourneyLegUi(
                    // If this is the last train station
                    // we won't display the arrival station on our train_leg card,
                    // as it'll be on the journey ticket already
                    journeyArrivalTitle = journeyTitleIfNotLastItem(
                        arrivalDepartureTitleFormatter.parse(
                            journey.legArrival.local,
                            journey.arrivalLocation
                        ),
                        index,
                        this
                    ),
                    providerName = journey.provider.name,
                    journeyTime = journey.travelTime.minutesToDisplayableTime(),
                    coachAndSeatInfo = journey.reservation?.toUi(),
                    seatingClass = journey.trainClassInformation.classDetails,
                    ticketRoute = ticketRoute,
                    type = journey.travelType,
                    // If this is the first train station
                    // we won't display the departure station on our train_leg card,
                    // as it'll be on the journey ticket already
                    journeyDepartureTitle = journeyTitleIfNotFirstItem(
                        arrivalDepartureTitleFormatter.parse(
                            journey.legDeparture.local,
                            journey.departureLocation
                        ), index
                    ),
                    bottomIcon = previousJourneyIcon(this, index),
                    topIcon = nextJourneyIcon(this, index)
                )
                ConnectionOrJourneyType.UNDERGROUND -> ConnectionLegUi(
                    type = journey.travelType,
                    connectionText = "Take the underground from ${journey.departureLocation.name} to ${journey.arrivalLocation.name}"
                )
                ConnectionOrJourneyType.WALK -> ConnectionLegUi(
                    type = journey.travelType,
                    connectionText = "Walk from ${journey.departureLocation.name} to ${journey.arrivalLocation.name}"
                )
                ConnectionOrJourneyType.BUS -> JourneyLegUi(
                    // If this is the last train station
                    // we won't display the arrival station on our train_leg card,
                    // as it'll be on the journey ticket already
                    journeyArrivalTitle = journeyTitleIfNotLastItem(
                        arrivalDepartureTitleFormatter.parse(
                            journey.legArrival.local,
                            journey.arrivalLocation
                        ),
                        index,
                        this
                    ),
                    providerName = journey.provider.name,
                    journeyTime = journey.travelTime.minutesToDisplayableTime(),
                    coachAndSeatInfo = journey.reservation?.toUi(),
                    seatingClass = journey.trainClassInformation.classDetails,
                    ticketRoute = ticketRoute,
                    type = journey.travelType,
                    // If this is the first train station
                    // we won't display the departure station on our train_leg card,
                    // as it'll be on the journey ticket already
                    journeyDepartureTitle = journeyTitleIfNotFirstItem(
                        arrivalDepartureTitleFormatter.parse(
                            journey.legDeparture.local,
                            journey.departureLocation
                        ), index
                    ),
                    bottomIcon = previousJourneyIcon(this, index),
                    topIcon = nextJourneyIcon(this, index)
                )
                else -> ConnectionLegUi(
                    type = journey.travelType,
                    connectionText = "Flight from ${journey.departureLocation.name} to ${journey.arrivalLocation.name}"
                )
            }.let(returnValue::add)
        }

        return returnValue
    }


    private fun nextJourneyIcon(legs: List<TrainSegment>, index: Int) = if (index > 0) {
        val indexOfPrevious = legs.indexOfPreviousTrainOrFerryJourney(index)
        if (indexOfPrevious >= 0 && legs.size >= index + 1 && legs[indexOfPrevious].travelType != legs[index].travelType && // index out of bounds - length 2 index was -1
            legs.currentIndexAndPreviousJourneyAreTrainOrFerry(index, indexOfPrevious)
        ) {
            when (legs[index].travelType) {
                ConnectionOrJourneyType.TRAIN -> Icon.TRAIN_ICON
                ConnectionOrJourneyType.FERRY -> Icon.FERRY_ICON
                ConnectionOrJourneyType.BUS -> Icon.BUS_ICON
                ConnectionOrJourneyType.WALK -> Icon.WALK_ICON
                else -> Icon.TRAIN_ICON
            }
        } else {
            Icon.GREY_ICON
        }
    } else {
        Icon.NONE
    }

    private fun List<TrainSegment>.indexOfPreviousTrainOrFerryJourney(startingAt: Int): Int {
        var temp = startingAt - 1

        while (temp >= 0) {
            if (this[temp].travelType == ConnectionOrJourneyType.FERRY ||
                this[temp].travelType == ConnectionOrJourneyType.TRAIN ||
                this[temp].travelType == ConnectionOrJourneyType.BUS ||
                this[temp].travelType == ConnectionOrJourneyType.WALK
            ) {
                break
            }

            temp--
        }

        return temp
    }

    private fun List<TrainSegment>.currentIndexAndPreviousJourneyAreTrainOrFerry(
        index: Int,
        previousIndex: Int
    ) =
        (this[previousIndex].travelType == ConnectionOrJourneyType.TRAIN || this[previousIndex].travelType == ConnectionOrJourneyType.FERRY || this[previousIndex].travelType == ConnectionOrJourneyType.BUS || this[previousIndex].travelType == ConnectionOrJourneyType.WALK) &&
                (this[index].travelType == ConnectionOrJourneyType.TRAIN || this[index].travelType == ConnectionOrJourneyType.FERRY || this[index].travelType == ConnectionOrJourneyType.BUS || this[index].travelType == ConnectionOrJourneyType.WALK)


}