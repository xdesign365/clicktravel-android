package com.clicktravel.core.ui.models

import com.clicktravel.core.model.common.TrainStationOrAirport
import com.clicktravel.core.model.trains.enums.DeliveryOption
import com.clicktravel.core.ui.builders.UiModelBuilder
import com.clicktravel.core.model.UniqueJourney


/**
 * High level view of a single ticket icon. Will cover the 'box' which has the start date,
 * start destination + icons and also contain each of the LegUIs
 */
data class TicketUi(
    val ticketTitle: String? = null,
    val arrivalTitle: String,
    val departureTitle: String,
    val ticketDate: String,
    val selectedTicket: Boolean = false,
    val legs: List<LegUi>,
    val ticketType: DeliveryOption,
    val topIcon: Icon,
    val bottomIcon: Icon,
    val bookingId : String,
    val journeyId : String
)

/**
 * Take a unique journey and return the legs for that journey in a displayable format
 */
fun UniqueJourney.toTicketUi() =
    UiModelBuilder().uniqueJourney(this)

fun TrainStationOrAirport.toUi() = "$name ($code)"

fun TrainStationOrAirport.toTrainUi(): String {
    return if(trainCrs != null) {
        "$name ($trainCrs)"
    } else {
        name
    }
}
