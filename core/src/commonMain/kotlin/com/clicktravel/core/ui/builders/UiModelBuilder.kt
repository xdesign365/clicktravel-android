@file:Suppress("UNCHECKED_CAST")

package com.clicktravel.core.ui.builders

import com.clicktravel.core.model.BookingItem
import com.clicktravel.core.model.FlightBooking
import com.clicktravel.core.model.TrainBooking
import com.clicktravel.core.model.UniqueJourney
import com.clicktravel.core.model.trains.ConnectionOrJourneyType
import com.clicktravel.core.ui.models.ConnectionLegUi
import com.clicktravel.core.ui.models.Icon
import com.clicktravel.core.ui.models.TicketUi
import com.clicktravel.core.ui.models.toDisplayable
import com.clicktravel.core.util.DateFormatter
import com.clicktravel.core.util.YearDisplayFormatter
import com.soywiz.klock.DateTimeTz


interface UiBuilder<in T : BookingItem> {
    fun buildTicket(booking: T, selectedIndex: Int): List<TicketUi>
}

abstract class BaseUiBuilder<T : BookingItem> : UiBuilder<T> {

    fun buildConnectionLeg(previousArrival: DateTimeTz, nextDeparture: DateTimeTz) =
        ConnectionLegUi(
            type = ConnectionOrJourneyType.WAIT,
            connectionText = "Connection\n${nextDeparture.minus(previousArrival).toDisplayable()}"
        )

    fun journeyTitleIfNotFirstItem(title: String, index: Int) = if (index == 0) {
        null
    } else {
        title
    }

    fun journeyTitleIfNotLastItem(title: String, index: Int, allItems: List<*>) =
        if (index == allItems.size - 1) {
            null
        } else {
            title
        }

    fun previousJourneyIcon(legs: List<*>, index: Int) = if (index == legs.size - 1) {
        Icon.NONE
    } else {
        Icon.GREY_ICON
    }
}

object UiBuilderFactory {

    private val yearlyDateFormatter: DateFormatter = YearDisplayFormatter()

    fun <T : BookingItem> builerForBooking(bookingItem: T): UiBuilder<T> =
        when (bookingItem) {
            is TrainBooking -> UiBuilderTrain(
                yearlyDateFormatter
            ) as UiBuilder<T>
            is FlightBooking -> UiBuilderFlights(
                yearlyDateFormatter
            ) as UiBuilder<T>
            else -> object : UiBuilder<BookingItem> {
                override fun buildTicket(booking: BookingItem, selectedIndex: Int): List<TicketUi> =
                    listOf() // No tickets for a hotel booking or travel card
            }
        }
}

class UiModelBuilder {

    val list = mutableListOf<TicketUi>()

    fun uniqueJourney(journey: UniqueJourney) =
        UiBuilderFactory.builerForBooking(
            journey.bookingItem
        ).buildTicket(journey.bookingItem, journey.journeyNumber)

}