package com.clicktravel.core.ui.models

import com.clicktravel.core.model.trains.ConnectionOrJourneyType

enum class Icon {
    GREY_ICON,
    FERRY_ICON,
    LIGHTHOUSE_ICON,
    TRAIN_ICON,
    TRAIN_DESTINATION_ICON,
    WALK_ICON,
    UNDERGROUND_ICON,
    AIRPORT_ARRIVAL_ICON,
    FLIGHT_ICON,
    BUS_ICON,
    NONE
}

fun ConnectionOrJourneyType.toStartIcon() = when (this) {
    ConnectionOrJourneyType.TRAIN -> Icon.TRAIN_ICON
    ConnectionOrJourneyType.WAIT -> Icon.GREY_ICON
    ConnectionOrJourneyType.FERRY -> Icon.FERRY_ICON
    ConnectionOrJourneyType.UNDERGROUND -> Icon.UNDERGROUND_ICON
    ConnectionOrJourneyType.WALK -> Icon.WALK_ICON
    ConnectionOrJourneyType.FLIGHT -> Icon.FLIGHT_ICON
    ConnectionOrJourneyType.BUS -> Icon.BUS_ICON
}

fun ConnectionOrJourneyType.toEndIcon() = when (this) {
    ConnectionOrJourneyType.TRAIN -> Icon.TRAIN_DESTINATION_ICON
    ConnectionOrJourneyType.WAIT -> Icon.GREY_ICON
    ConnectionOrJourneyType.FERRY -> Icon.LIGHTHOUSE_ICON
    ConnectionOrJourneyType.UNDERGROUND -> Icon.TRAIN_DESTINATION_ICON
    ConnectionOrJourneyType.WALK -> Icon.TRAIN_DESTINATION_ICON
    ConnectionOrJourneyType.FLIGHT -> Icon.AIRPORT_ARRIVAL_ICON
    ConnectionOrJourneyType.BUS -> Icon.BUS_ICON
}