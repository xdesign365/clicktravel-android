package com.clicktravel.core.util

import com.clicktravel.core.model.UniqueJourney
import com.clicktravel.core.model.team.Team

fun List<UniqueJourney>.filteredList(team: Team) =
    filter {
        it.bookingItem.teamId == team.id
    }
