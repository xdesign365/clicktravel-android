package com.clicktravel.core.util

import com.soywiz.klock.DateTime
import com.soywiz.klock.seconds

interface ExpiryTimeQuery {
    fun hasTimeExpired(expiryDate: Long): Boolean
    fun timeNow(): DateTime
    fun timeNowPlusExpiryTime(expiryTimeInSeconds: Long): Long
}

class ExpiryTimeQueryImpl(private val getDateTimeFn: (() -> DateTime) = { DateTime.now() }) :
    ExpiryTimeQuery {
    override fun hasTimeExpired(expiryDate: Long) =
        DateTime.fromUnix(expiryDate) - timeNow() < 0.seconds

    override fun timeNow() = getDateTimeFn()

    override fun timeNowPlusExpiryTime(expiryTimeInSeconds: Long) =
        (timeNow() + expiryTimeInSeconds.seconds).unixMillisLong
}
