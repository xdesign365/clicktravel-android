package com.clicktravel.core.util

import com.soywiz.klock.*


interface DateFormatter {
    fun parse(dateAsString: String): DateTimeTz
    fun format(date: DateTimeTz): String
}

class UtcDateFormatter : DateFormatter {

    // Rough parser for the date. The date comes back as mostly UTC but with a different
    // time zone format which is not supported by the library. This adds a colon to the format
    // to change it from +1000 to +10:00 which the library supports - but only if it needs it
    // as an added complication, the library replaces 00:00 with a Z when it converts, so we need to
    // check this too
    override fun parse(dateAsString: String): DateTimeTz {
        // todo we should remove this once they have fixed the inconsistent date format issue on their end
        val retVal = try {
            if (dateAsString[dateAsString.length - 3] == ':' || dateAsString[dateAsString.length - 1].toLowerCase() == 'z') {
                DateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").parse(dateAsString)
            } else {
                DateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").parse(
                    "${dateAsString.substring(
                        0,
                        dateAsString.length - 2
                    )}:${dateAsString.substring(
                        dateAsString.length - 2,
                        dateAsString.length
                    )}"
                )
            }
        } catch (exception: Throwable) {
            DateFormat("yyyy-MM-dd").parse(dateAsString.substring(0, 10))
        }

        return retVal
    }

    override fun format(date: DateTimeTz) = date.toString("yyyy-MM-dd'T'HH:mm:ssXXX")
}

class YearDisplayFormatter : DateFormatter {
    private val fmt = DateFormat("dd/MM/yy")

    override fun parse(dateAsString: String) = fmt.parse(dateAsString)
    override fun format(date: DateTimeTz) = fmt.format(date.local)
}

class HoursDisplayFormatter : DateFormatter {
    private val fmt = DateFormat("HH:mm")

    override fun parse(dateAsString: String) = fmt.parse(dateAsString)
    override fun format(date: DateTimeTz) = fmt.format(date.local)
}

class SummaryUiDateFormatter : DateFormatter {
    override fun parse(dateAsString: String): DateTimeTz {
        throw NotImplementedError()
    }

    override fun format(date: DateTimeTz): String {
        return "${date.dayOfMonth} ${date.month.localName.capitalize()} ${date.year.year}"
    }

}