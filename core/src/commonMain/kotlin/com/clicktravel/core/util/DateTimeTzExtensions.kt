package com.clicktravel.core.util

import com.soywiz.klock.DateTime
import com.soywiz.klock.DateTimeTz
import kotlin.math.absoluteValue

fun DateTimeTz.daysBetween(otherDate: DateTimeTz): Int {
    val rawFirstDay = DateTimeTz.fromUnixLocal(
        DateTime.invoke(
            year = this.year,
            month = this.month,
            day = this.dayOfMonth
        ).unixMillisLong
    )
    val rawSecondDay = DateTimeTz.fromUnixLocal(
        DateTime.invoke(
            year = otherDate.year,
            month = otherDate.month,
            day = otherDate.dayOfMonth
        ).unixMillisLong
    )

    return rawFirstDay.minus(rawSecondDay).days.toInt().absoluteValue

}

fun daysUntilBooking(bookingDate: DateTimeTz): Int = DateTimeTz.nowLocal().daysBetween(bookingDate)