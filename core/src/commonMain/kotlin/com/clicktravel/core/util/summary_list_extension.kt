package com.clicktravel.core.util

import com.clicktravel.core.ui.models.DateHeaderListItem
import com.clicktravel.core.ui.models.SummaryListItemUi
import com.soywiz.klock.DateTimeTz

fun List<SummaryListItemUi>.indexOfCurrentDate(currentDate: DateTimeTz): Int {

    var returnIndex = 0
    if (isNotEmpty()) {
        val result = firstOrNull {
            if (it is DateHeaderListItem) {
                it.dateTimeTz.sameDay(currentDate)
            } else {
                false
            }
        }

        returnIndex = if (result == null) {
            val filteredResults = filter {
                // Get all the days in the future
                it is DateHeaderListItem && (it.dateTimeTz - currentDate).seconds >= 0
            }

            if (filteredResults.isEmpty()) {
                size - 1
            } else {
                indexOf(filteredResults.first())
            }
        } else {
            indexOf(result)
        }

    }

    return returnIndex
}


fun DateTimeTz.sameDay(other: DateTimeTz) =
    other.dayOfMonth == dayOfMonth && other.month == month && other.year == year