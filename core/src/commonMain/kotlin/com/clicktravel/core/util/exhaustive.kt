package com.clicktravel.core.util

val <T> T.exhaustive: T
    get() = this