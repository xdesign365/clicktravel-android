package com.clicktravel.itinerary.util

typealias LogException = (Throwable) -> Unit

object CompositeLogger {

    private val errorLogger = mutableListOf<LogException>()

    fun logError(error: Throwable) {
        errorLogger.forEach { it(error) }
    }

    fun register(exceptionLogger: LogException) {
        errorLogger.add(exceptionLogger)
    }

}