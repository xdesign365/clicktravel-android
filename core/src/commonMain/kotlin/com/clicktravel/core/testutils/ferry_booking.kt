package com.clicktravel.core.testutils

import com.clicktravel.core.model.TrainBooking
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.common.Address
import com.clicktravel.core.model.common.RequiredInformation
import com.clicktravel.core.model.common.TrainStationOrAirport
import com.clicktravel.core.model.trains.*
import com.clicktravel.core.model.trains.enums.DeliveryOption
import com.clicktravel.core.model.trains.enums.PassengerType
import com.soywiz.klock.DateTime

val ferryBookingSegmentOne = TrainSegment(
    travelType = ConnectionOrJourneyType.TRAIN,
    travelTime = 7,
    legDeparture = DateTime.parse("2019-11-27T16:35:00+0000"),
    legArrival = DateTime.parse("2019-11-27T16:42:00+0000"),
    connectionTime = 0,
    provider = Provider("Island Line"),
    trainClassInformation = TrainClassInformation("Train has Standard class seating only"),
    departureLocation = TrainStationOrAirport(
        "Ryde St Johns Road",
        "5543",
        trainCrs = "RYR"
    ),
    arrivalLocation = TrainStationOrAirport(
        "Ryde Pier Head",
        "5541",
        trainCrs = "RYP"
    )
)

val ferryBookingSegmentTwo = TrainSegment(
    travelType = ConnectionOrJourneyType.FERRY,
    departureLocation = TrainStationOrAirport(
        "Ryde Pier Head",
        "5541",
        trainCrs = "RYP"
    ),
    arrivalLocation = TrainStationOrAirport(
        "Portsmouth Harbour",
        "5540",
        trainCrs = "PMH"
    ),
    provider = Provider("South Western Railway"),
    trainClassInformation = TrainClassInformation("Train has Standard class seating only"),
    legArrival = DateTime.parse("2019-11-27T17:09:00+0000"),
    legDeparture = DateTime.parse("2019-11-27T16:47:00+0000"),
    travelTime = 22,
    connectionTime = 0
)

val ferryBookingSegmentThree = TrainSegment(
    travelType = ConnectionOrJourneyType.TRAIN,
    departureLocation = TrainStationOrAirport(
        "Portsmouth Harbour",
        "5540",
        trainCrs = "PMH"
    ),
    connectionTime = 0,
    travelTime = 45,
    legDeparture = DateTime.parse("2019-11-27T17:23:00+0000"),
    legArrival = DateTime.parse("2019-11-27T18:08:00+0000"),
    trainClassInformation = TrainClassInformation("Train has Standard class seating only"),
    provider = Provider("Great Western Railway"),
    arrivalLocation = TrainStationOrAirport(
        "Southampton Central",
        "5932",
        trainCrs = "SOU"
    )
)

val ferryBookingJourney = DetailedTrainJourney(
    journeyId = "10000",
    couponId = "1",
    segments = listOf(
        ferryBookingSegmentOne,
        ferryBookingSegmentTwo,
        ferryBookingSegmentThree
    ),
    arrivalStation = TrainStationOrAirport(
        "Southampton Central",
        "5932",
        trainCrs = "SOU"
    ),
    departureStation = TrainStationOrAirport(
        "Ryde St Johns Road",
        "5543",
        trainCrs = "RYR"
    ),
    ticketType = TicketType("Anytime Day Single"),
    departureDateTime = DateTime.parse("2019-11-27T16:35:00+0000")
)

val ferryBooking = TrainBooking(
    journeys = listOf(ferryBookingJourney),
    bookingId = "200116513",
    notes = Note(
        "Sample text"
    ),
    address = Address("", "", ""),
    deliveryOption = DeliveryOption.E_TICKET,
    passengerType = PassengerType.ADULT,
    fareType = "Valid for travel by any permitted route and Wightlink crossing.",
    fareClass = "STANDARD",
    bookingTravelType = TravelTypes.TRAIN,
    ticketCollectionReference = "SOLD_VIA_ETICKET",
    fields = emptyList(),
    team = "f89103c5-fc65-4ece-9d74-3d727f272d88"
)

val ferryBookingJson = """{
          "id": "200116513",
          "booker": {
            "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
            "firstName": "Chris",
            "lastName": "Hern",
            "emailAddress": "mhairi.duff@xdesign.com"
          },
          "payments": {
            "Account": {
              "chargedAmount": {
                "amount": "30.70"
                "currency": "GBP"
              }
            }
          },
          "createdDateTime": "2019-11-27T16:18:24.028Z",
          "confirmedDate": "2019-11-27T16:19:42.161Z",
          "serviceDate": "2019-11-27",
          "status": "CONFIRMED",
          "internalStatus": "CONFIRMED",
          "cancellable": true,
          "refundable": false,
          "exchangeable": {
            "value": false
          },
          "product": {
            "id": "a74171be-210a-401d-a053-d01d529f89ec",
            "travelType": "TRAIN",
            "createdDate": "2019-11-27T16:16:43.778Z",
            "serviceDate": "2019-11-27",
            "status": "CONFIRMING",
            "details": {
              "type": "TrainProductDetails",
              "minRate": {
                "supplier": {
                  "amount": "28.2",
                  "currency": "GBP"
                }
              },
              "minRatePerPerson": {
                "supplier": {
                  "amount": "28.2",
                  "currency": "GBP"
                }
              },
              "deliveryOptions": [],
              "journeys": [
                {
                  "id": "10000",
                  "offPeakAvailable": false,
                  "superOffPeakAvailable": false,
                  "legs": [
                    {
                      "id": "1",
                      "serviceProvider": {
                        "code": "IL",
                        "mode": "TRAIN",
                        "name": "Island Line"
                      },
                      "travelTimeMinutes": 7,
                      "depart": {
                        "location": {
                          "code": "5543",
                          "name": "Ryde St Johns Road",
                          "crs": "RYR"
                        },
                        "dateTime": "2019-11-27T16:35:00+0000"
                      },
                      "arrive": {
                        "location": {
                          "code": "5541",
                          "name": "Ryde Pier Head",
                          "crs": "RYP"
                        },
                        "dateTime": "2019-11-27T16:42:00+0000"
                      },
                      "ultimateDestination": {
                        "code": "5541",
                        "name": "Ryde Pier Head",
                        "crs": "RYP"
                      },
                      "seatingClass": "Train has Standard class seating only"
                    },
                    {
                      "id": "2",
                      "serviceProvider": {
                        "code": "SW",
                        "mode": "FERRY",
                        "name": "South Western Railway"
                      },
                      "travelTimeMinutes": 22,
                      "depart": {
                        "location": {
                          "code": "5541",
                          "name": "Ryde Pier Head",
                          "crs": "RYP"
                        },
                        "dateTime": "2019-11-27T16:47:00+0000"
                      },
                      "arrive": {
                        "location": {
                          "code": "5540",
                          "name": "Portsmouth Harbour",
                          "crs": "PMH"
                        },
                        "dateTime": "2019-11-27T17:09:00+0000"
                      },
                      "ultimateDestination": {
                        "code": "5540",
                        "name": "Portsmouth Harbour",
                        "crs": "PMH"
                      },
                      "seatingClass": "Train has Standard class seating only"
                    },
                    {
                      "id": "3",
                      "serviceProvider": {
                        "code": "GW",
                        "mode": "TRAIN",
                        "name": "Great Western Railway"
                      },
                      "travelTimeMinutes": 45,
                      "depart": {
                        "location": {
                          "code": "5540",
                          "name": "Portsmouth Harbour",
                          "crs": "PMH"
                        },
                        "dateTime": "2019-11-27T17:23:00+0000"
                      },
                      "arrive": {
                        "location": {
                          "code": "5932",
                          "name": "Southampton Central",
                          "crs": "SOU"
                        },
                        "dateTime": "2019-11-27T18:08:00+0000"
                      },
                      "ultimateDestination": {
                        "code": "3899",
                        "name": "Cardiff Central",
                        "crs": "CDF"
                      },
                      "seatingClass": "Train has Standard class seating only"
                    }
                  ],
                  "ticketType": {
                    "code": "SDS",
                    "name": "Anytime Day Single"
                  },
                  "direction": "OUTBOUND",
                  "refundableUntil": "2019-12-25",
                  "couponId": "1"
                }
              ],
              "holdable": false,
              "legs": [],
              "fees": []
            },
            "formOfPayment": {
              "accountType": "CREDIT_ACCOUNT",
              "type": "CREDIT_ACCOUNT",
              "id": "1109059",
              "associatedCostType": "billing",
              "creditAccountId": "1109059",
              "creditAccountName": "xDesign",
              "status": "ACTIVE",
              "chargeRoutePlan": false
            },
            "bookingDetails": {
              "type": "TrainProductBookingDetails",
              "billbackOverrides": [],
              "delivery": {
                "option": "E_TICKET",
                "address": []
              }
            },
            "subProducts": [
              {
                "id": "0",
                "reference": "OPR000628D",
                "supplierReference": "OPR000628D",
                "sfsReference": "T133156",
                "sfsProductId": "OPT0df100b6-c33e-429d-8170-5ad208d69251_8efa2e1c-bd0d-4822-bd7a-f4e35864ebfd",
                "channel": "OPR",
                "details": {
                  "type": "TrainSubProductDetails",
                  "fees": [],
                  "estimatedServiceCost": {
                    "billing": {
                      "amount": "28.20",
                      "currency": "GBP"
                    },
                    "supplier": {
                      "amount": "28.20",
                      "currency": "GBP"
                    },
                    "preferred": {
                      "amount": "28.20",
                      "currency": "GBP"
                    }
                  },
                  "totalEstimatedCost": {
                    "billing": {
                      "amount": "28.20",
                      "currency": "GBP"
                    },
                    "supplier": {
                      "amount": "28.20",
                      "currency": "GBP"
                    },
                    "preferred": {
                      "amount": "28.20",
                      "currency": "GBP"
                    }
                  },
                  "rules": [],
                  "surcharges": [],
                  "passengerType": "ADULT",
                  "ctrReference": "SOLD_VIA_ETICKET",
                  "fares": [
                    {
                      "id": "30002",
                      "ticketType": {
                        "code": "SDS",
                        "name": "Anytime Day Single"
                      },
                      "fareRoute": {
                        "code": "00981",
                        "name": "Valid for travel by any permitted route and Wightlink crossing."
                      },
                      "fareClass": "STANDARD",
                      "journeyType": "SINGLE",
                      "crossLondon": false,
                      "cancellable": true,
                      "origin": {
                        "code": "5543",
                        "name": "Ryde St Johns Road"
                      },
                      "destination": {
                        "code": "5932",
                        "name": "Southampton Central"
                      },
                      "fareConditionsId": "27112019SDS"
                    }
                  ],
                  "requirements": [],
                  "features": [],
                  "information": [],
                  "seatReservations": [],
                  "sentLoyaltySchemes": []
                },
                "traveller": {
                  "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
                  "userId": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
                  "organisationId": "f89103c5-fc65-4ece-9d74-3d727f272d88",
                  "organisationName": "xDesign",
                  "firstName": "Chris",
                  "lastName": "Hern",
                  "emailAddress": "mhairi.duff@xdesign.com",
                  "phoneNumber": "+44 7890 123456",
                  "mobileNumber": "+44 7890 123456",
                  "hasApisDetails": true,
                  "registered": true,
                  "guest": false,
                  "passengerAssistanceConfigured": false
                },
                "bookingDetails": {
                  "type": "TrainSubProductBookingDetails",
                  "requiredInformation": "[]",
                  "flightAdditions": [],
                  "requirements": [],
                  "passengerAssistanceDeclined": false
                },
                "policyComplianceState": "COMPLIANT",
                "couponId": "1",
                "ticketNumbers": [],
                "notes": [
                  {
                    "value": "Sample text",
                    "user": {
                      "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
                      "firstName": "Chris",
                      "lastName": "Hern",
                      "emailAddress": "mhairi.duff@xdesign.com"
                    },
                    "creationDateTime": "2019-11-27T16:17:19.476Z"
                  }
                ]
              }
            ],
            "searchId": "OPT0df100b6-c33e-429d-8170-5ad208d69251"
          },
          "paidWithVouchers": false
        }"""
