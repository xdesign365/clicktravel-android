package com.clicktravel.core.testutils

import com.clicktravel.core.model.TrainBooking
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.common.Address
import com.clicktravel.core.model.common.TrainStationOrAirport
import com.clicktravel.core.model.trains.*
import com.clicktravel.core.model.trains.enums.DeliveryOption
import com.clicktravel.core.model.trains.enums.PassengerType
import com.soywiz.klock.DateTime

val ferryBookingJourneySingle = DetailedTrainJourney(
    journeyId = "10000",
    segments = listOf(
        ferryBookingSegmentTwo
    ),
    arrivalStation = TrainStationOrAirport(
        "Southampton Central",
        "SOU"
    ),
    departureStation = TrainStationOrAirport(
        "Ryde St Johns Road",
        "RYR"
    ),
    ticketType = TicketType("Anytime Day Single"),
    departureDateTime = DateTime.parse("2019-11-27T16:35:00+0000")
)

val ferryBookingSingleLeg = TrainBooking(
    journeys = listOf(ferryBookingJourneySingle),
    bookingId = "NewBookingId",
    notes = Note(
        "Sample text"
    ),
    address = Address("", "", ""),
    deliveryOption = DeliveryOption.E_TICKET,
    passengerType = PassengerType.ADULT,
    fareType = "Valid for travel by any permitted route and Wightlink crossing.",
    fareClass = "STANDARD",
    bookingTravelType = TravelTypes.TRAIN,
    ticketCollectionReference = "SOLD_VIA_ETICKET",
    team = "f89103c5-fc65-4ece-9d74-3d727f272d88"
)

val ferryBookingSegmenWalk = TrainSegment(
    travelType = ConnectionOrJourneyType.WALK,
    departureLocation = TrainStationOrAirport(
        "Ryde Pier Head",
        "RYP"
    ),
    arrivalLocation = TrainStationOrAirport(
        "Portsmouth Harbour",
        "PMH"
    ),
    provider = Provider("WALK"),
    trainClassInformation = TrainClassInformation("Train has Standard class seating only"),
    legArrival = DateTime.parse("2019-11-27T18:19:00+0000"),
    legDeparture = DateTime.parse("2019-11-27T17:40:00+0000"),
    travelTime = 30,
    connectionTime = 0
)

val ferryBookingSegmentWithWalkFinalLeg = TrainSegment(
    travelType = ConnectionOrJourneyType.TRAIN,
    departureLocation = TrainStationOrAirport(
        "Portsmouth Harbour",
        "PMH"
    ),
    connectionTime = 0,
    travelTime = 60,
    legArrival = DateTime.parse("2019-11-27T18:21:00+0000"),
    legDeparture = DateTime.parse("2019-11-27T19:00:00+0000"),
    trainClassInformation = TrainClassInformation("Train has Standard class seating only"),
    provider = Provider("Great Western Railway"),
    arrivalLocation = TrainStationOrAirport(
        "Southampton Central",
        "SOU"
    )
)


val ferryBookingJourneyWithWalk = DetailedTrainJourney(
    journeyId = "10000",
    segments = listOf(
        ferryBookingSegmentTwo,
        ferryBookingSegmenWalk,
        ferryBookingSegmentWithWalkFinalLeg
    ),
    arrivalStation = TrainStationOrAirport(
        "Southampton Central",
        "SOU"
    ),
    departureStation = TrainStationOrAirport(
        "Ryde St Johns Road",
        "RYR"
    ),
    ticketType = TicketType("Anytime Day Single"),
    departureDateTime = DateTime.parse("2019-11-27T16:35:00+0000")
)

val ferryBookingWithWalk = TrainBooking(
    journeys = listOf(ferryBookingJourneyWithWalk),
    bookingId = "1028380123",
    notes = Note(
        "Sample text"
    ),
    address = Address("", "", ""),
    deliveryOption = DeliveryOption.E_TICKET,
    passengerType = PassengerType.ADULT,
    fareType = "Valid for travel by any permitted route and Wightlink crossing.",
    fareClass = "STANDARD",
    bookingTravelType = TravelTypes.TRAIN,
    ticketCollectionReference = "SOLD_VIA_ETICKET",
    team = "f89103c5-fc65-4ece-9d74-3d727f272d88"
)
