package com.clicktravel.core.testutils

val savedJsonValues = """{
    "id": "200116344",
    "booker": {
        "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
        "firstName": "Chris",
        "lastName": "Hern",
        "emailAddress": "mhairi.duff@xdesign.com"
    },
    "payments": {
        "Account": {
            "chargedAmount": {
                "amount": "17.50",
                "currency": "GBP"
            }
        }
    },
    "createdDateTime": "2019-11-25T14:29:06.850Z",
    "confirmedDate": "2019-11-25T14:30:24.378Z",
    "serviceDate": "2019-12-02",
    "status": "CONFIRMED",
    "internalStatus": "CONFIRMED",
    "cancellable": false,
    "refundable": false,
    "exchangeable": {
        "value": false,
        "reason": "TICKET_VALUE",
        "fee": {
            "preferred": {
                "amount": "17.60",
                "currency": "GBP"
            }
        }
    },
    "product": {
        "id": "5da1120c-6b66-4b0f-a814-40b45aa6cf2f",
        "travelType": "TRAIN",
        "createdDate": "2019-11-25T14:28:23.817Z",
        "serviceDate": "2019-12-02",
        "status": "CONFIRMING",
        "details": {
            "type": "TrainProductDetails",
            "minRate": {
                "supplier": {
                    "amount": "16",
                    "currency": "GBP"
                }
            },
            "minRatePerPerson": {
                "supplier": {
                    "amount": "16.0",
                    "currency": "GBP"
                }
            },
            "deliveryOptions": [],
            "journeys": [
                {
                    "id": "10003",
                    "offPeakAvailable": false,
                    "superOffPeakAvailable": false,
                    "legs": [
                        {
                            "id": "1",
                            "serviceProvider": {
                                "code": "CH",
                                "mode": "TRAIN",
                                "name": "Chiltern Railways"
                            },
                            "travelTimeMinutes": 100,
                            "depart": {
                                "location": {
                                    "code": "1475",
                                    "name": "London Marylebone",
                                    "crs": "MYB"
                                },
                                "dateTime": "2019-12-02T08:14:00+0000"
                            },
                            "arrive": {
                                "location": {
                                    "code": "4515",
                                    "name": "Birmingham Moor Street",
                                    "crs": "BMO"
                                },
                                "dateTime": "2019-12-02T09:54:00+0000"
                            },
                            "seatNumber": "NOPL",
                            "ultimateDestination": {
                                "code": "1006",
                                "name": "Birmingham Snow Hill",
                                "crs": "BSW"
                            },
                            "seatingClass": "Train has Standard class seating only"
                        },
                        {
                            "id": "2",
                            "serviceProvider": {
                                "code": "LM",
                                "mode": "TRAIN",
                                "name": "West Midlands Trains"
                            },
                            "travelTimeMinutes": 2,
                            "depart": {
                                "location": {
                                    "code": "4515",
                                    "name": "Birmingham Moor Street",
                                    "crs": "BMO"
                                },
                                "dateTime": "2019-12-02T09:59:00+0000"
                            },
                            "arrive": {
                                "location": {
                                    "code": "1006",
                                    "name": "Birmingham Snow Hill",
                                    "crs": "BSW"
                                },
                                "dateTime": "2019-12-02T10:01:00+0000"
                            },
                            "ultimateDestination": {
                                "code": "4581",
                                "name": "Kidderminster",
                                "crs": "KID"
                            },
                            "seatingClass": "Train has Standard class seating only"
                        },
                        {
                            "id": "3",
                            "serviceProvider": {
                                "code": "UNDERGROUND",
                                "mode": "UNDERGROUND",
                                "name": "UNDERGROUND"
                            },
                            "travelTimeMinutes": 15,
                            "depart": {
                                "location": {
                                    "code": "1006",
                                    "name": "Birmingham Snow Hill",
                                    "crs": "BSW"
                                },
                                "dateTime": "2019-12-02T10:01:00+0000"
                            },
                            "arrive": {
                                "location": {
                                    "code": "1127",
                                    "name": "Birmingham New Street",
                                    "crs": "BHM"
                                },
                                "dateTime": "2019-12-02T10:16:00+0000"
                            },
                            "ultimateDestination": {},
                            "seatingClass": "Train seating class not known"
                        }
                        
                    ],
                    "ticketType": {
                        "code": "2GC",
                        "name": "Advance Single"
                    },
                    "direction": "OUTBOUND",
                    "refundableUntil": "2019-12-30",
                    "couponId": "1"
                }
            ],
            "holdable": false,
            "legs": [],
            "fees": []
        },
        "formOfPayment": {
            "accountType": "CREDIT_ACCOUNT",
            "type": "CREDIT_ACCOUNT",
            "id": "1109059",
            "associatedCostType": "billing",
            "creditAccountId": "1109059",
            "creditAccountName": "xDesign",
            "status": "ACTIVE",
            "chargeRoutePlan": false
        },
        "bookingDetails": {
            "type": "TrainProductBookingDetails",
            "billbackOverrides": [],
            "delivery": {
                "option": "E_TICKET",
                "address": []
            }
        },
        "subProducts": [
            {
                "id": "0",
                "reference": "OPR0006277",
                "supplierReference": "OPR0006277",
                "sfsReference": "T133099",
                "sfsProductId": "OPT07e4d388-47fa-4f83-9296-8c3683ce2b01_483e981f-e4f2-4129-92ff-86335f26e7d0",
                "channel": "OPR",
                "details": {
                    "type": "TrainSubProductDetails",
                    "fees": [],
                    "estimatedServiceCost": {
                        "billing": {
                            "amount": "16.00",
                            "currency": "GBP"
                        },
                        "supplier": {
                            "amount": "16.00",
                            "currency": "GBP"
                        },
                        "preferred": {
                            "amount": "16.00",
                            "currency": "GBP"
                        }
                    },
                    "totalEstimatedCost": {
                        "billing": {
                            "amount": "16.00",
                            "currency": "GBP"
                        },
                        "supplier": {
                            "amount": "16.00",
                            "currency": "GBP"
                        },
                        "preferred": {
                            "amount": "16.00",
                            "currency": "GBP"
                        }
                    },
                    "rules": [],
                    "surcharges": [],
                    "passengerType": "ADULT",
                    "ctrReference": "SOLD_VIA_ETICKET",
                    "fares": [
                        {
                            "id": "30069",
                            "ticketRestriction": "CA",
                            "ticketType": {
                                "code": "2GC",
                                "name": "Advance Single"
                            },
                            "fareRoute": {
                                "code": "00402",
                                "name": "Only valid on booked trains and required connecting services."
                            },
                            "fareClass": "STANDARD",
                            "journeyType": "SINGLE",
                            "crossLondon": false,
                            "cancellable": false,
                            "origin": {
                                "code": "1072",
                                "name": "London Terminals"
                            },
                            "destination": {
                                "code": "0418",
                                "name": "Birmingham New St, Moor St or Snow Hill"
                            },
                            "fareConditionsId": "021220192GCCA"
                        }
                    ],
                    "requirements": [],
                    "features": [],
                    "information": [],
                    "seatReservations": [],
                    "sentLoyaltySchemes": []
                },
                "traveller": {
                    "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
                    "userId": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
                    "organisationId": "f89103c5-fc65-4ece-9d74-3d727f272d88",
                    "organisationName": "xDesign",
                    "firstName": "Chris",
                    "lastName": "Hern",
                    "emailAddress": "mhairi.duff@xdesign.com",
                    "phoneNumber": "+44 7890 123456",
                    "mobileNumber": "+44 7890 123456",
                    "hasApisDetails": true,
                    "registered": true,
                    "guest": false,
                    "passengerAssistanceConfigured": false
                },
                "bookingDetails": {
                    "type": "TrainSubProductBookingDetails",
                    "requiredInformation": "[{\"id\":\"what-is-your-favourite-colour\",\"type\":\"TEXT\",\"label\":\"What is your favourite colour?\",\"value\":\"red\"},{\"id\":\"is-there-money-in-the-banana-stand\",\"type\":\"BOOLEAN\",\"label\":\"Is there money in the banana stand?\",\"value\":false},{\"id\":\"which-department-do-you-work-in\",\"type\":\"SELECT\",\"label\":\"Which department do you work in?\",\"value\":{\"label\":\"Finance\",\"code\":\"finance\"}}]",
                    "flightAdditions": [],
                    "requirements": [],
                    "passengerAssistanceDeclined": false
                },
                "policyComplianceState": "COMPLIANT",
                "couponId": "1",
                "ticketNumbers": [],
                "notes": [
                    {
                        "value": "Another note!",
                        "user": {
                            "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
                            "firstName": "Chris",
                            "lastName": "Hern",
                            "emailAddress": "mhairi.duff@xdesign.com"
                        },
                        "impersonatedBy": {
                            "id": "036d12c1-5876-4f63-812f-63ede6c983d6",
                            "firstName": "Tom",
                            "lastName": "Eagles",
                            "emailAddress": "tom.eagles@clicktravel.com"
                        },
                        "creationDateTime": "2019-11-25T14:28:54.574Z"
                    }
                ]
            }
        ],
        "searchId": "OPT07e4d388-47fa-4f83-9296-8c3683ce2b01"
    },
    "paidWithVouchers": false
}
"""

