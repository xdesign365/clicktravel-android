package com.clicktravel.core.testutils


val selectedHotel = """{
    "id": "200120262",
    "booker": {
        "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
        "firstName": "Chris",
        "lastName": "Hern",
        "emailAddress": "mhairi.duff@xdesign.com"
    },
    "payments": {
        "virtualCard": {
            "paymentId": "fb05184c-7990-4941-975e-dd60444206df",
            "panHint": "9629",
            "chargedAmount": {
                "amount": "6574.43",
                "currency": "GBP"
            }
        }
    },
    "createdDateTime": "2020-01-14T10:33:51.616Z",
    "confirmedDate": "2020-01-14T10:34:08.430Z",
    "serviceDate": "2020-01-22",
    "status": "CONFIRMED",
    "internalStatus": "CONFIRMED",
    "cancellable": true,
    "refundable": true,
    "exchangeable": {},
    "product": {
        "id": "2875026c-bf35-46e3-9bc9-1d454c6a4128",
        "travelType": "HOTEL",
        "createdDate": "2020-01-14T10:33:00.058Z",
        "serviceDate": "2020-01-22",
        "status": "CONFIRMING",
        "details": {
            "type": "HotelProductDetails",
            "countryCode": "GB",
            "minRate": {
                "billing": {
                    "amount": "186.99",
                    "currency": "GBP"
                },
                "supplier": {
                    "amount": "186.99",
                    "currency": "GBP"
                },
                "preferred": {
                    "amount": "186.99",
                    "currency": "GBP"
                }
            },
            "distance": 0.2381300763628881,
            "propertyName": "Corinthia Hotel London",
            "address": [
                "Whitehall Place",
                "London",
                "SW1A 2BD"
            ],
            "upc": "226490",
            "checkInDate": "2020-01-22",
            "checkOutDate": "2020-01-23",
            "billingConfiguration": {
                "type": "GUARANTEE_ONLY",
                "billbackElements": {
                    "allCosts": {
                        "enabled": false
                    },
                    "breakfast": {
                        "enabled": false
                    },
                    "roomRate": {
                        "enabled": false
                    },
                    "parking": {
                        "enabled": false
                    },
                    "wifi": {
                        "enabled": false
                    },
                    "mealSupplement": {
                        "enabled": false
                    }
                }
            },
            "profileImage": {
                "id": "05c5cf50-6963-4f6e-b94a-3f6cf6c9f516",
                "urls": [
                    "https://pdmedia.uat.travel.cloud/226490/pv/images/05c5cf50-6963-4f6e-b94a-3f6cf6c9f516_z.jpg",
                    "https://pdmedia.uat.travel.cloud/226490/pv/images/05c5cf50-6963-4f6e-b94a-3f6cf6c9f516.jpg",
                    "https://pdmedia.uat.travel.cloud/226490/pv/images/05c5cf50-6963-4f6e-b94a-3f6cf6c9f516_n.jpg",
                    "https://pdmedia.uat.travel.cloud/226490/pv/images/05c5cf50-6963-4f6e-b94a-3f6cf6c9f516_m.jpg",
                    "https://pdmedia.uat.travel.cloud/226490/pv/images/05c5cf50-6963-4f6e-b94a-3f6cf6c9f516_t.jpg"
                ]
            },
            "geocode": {
                "latitude": 51.506098,
                "longitude": -0.124969
            },
            "starRating": "UNKNOWN",
            "deliveryOptions": [],
            "holdable": false,
            "supportedBillbackOptions": {
                "breakfast": false,
                "parking": true,
                "roomRate": true,
                "wifi": true,
                "mealSupplement": false
            },
            "legs": [],
            "fees": []
        },
        "formOfPayment": {
            "accountType": "CREDIT_ACCOUNT",
            "type": "CREDIT_ACCOUNT",
            "id": "1109059",
            "associatedCostType": "billing",
            "creditAccountId": "1109059",
            "creditAccountName": "xDesign",
            "status": "ACTIVE",
            "chargeRoutePlan": false
        },
        "bookingDetails": {
            "type": "HotelProductBookingDetails",
            "billingConfiguration": {
                "selected": "ROOM_ONLY"
            },
            "billbackOverrides": []
        },
        "subProducts": [
            {
                "id": "0",
                "reference": "fba2f4f8-62dc-4d5e-9898-99a0f9dcc624",
                "supplierReference": "9d9174bd-eae9-47a8-8ef4-f5ea117fb034",
                "pin": "b5518771-a9f3-45e0-bcba-cb17a2f4453f",
                "sfsReference": "H233897",
                "sfsProductId": "1_2c0d246d501b0caeec34ce3f992776d2_226490_1",
                "channel": "MOC",
                "details": {
                    "type": "HotelSubProductDetails",
                    "fees": [],
                    "rateType": "STANDARD",
                    "estimatedServiceCost": {
                        "billing": {
                            "amount": "6574.43",
                            "currency": "GBP"
                        },
                        "supplier": {
                            "amount": "3713.00",
                            "currency": "AED"
                        },
                        "preferred": {
                            "amount": "6574.43",
                            "currency": "GBP"
                        }
                    },
                    "totalEstimatedCost": {
                        "billing": {
                            "amount": "6574.43",
                            "currency": "GBP"
                        },
                        "supplier": {
                            "amount": "3713.00",
                            "currency": "AED"
                        },
                        "preferred": {
                            "amount": "6574.43",
                            "currency": "GBP"
                        }
                    },
                    "rules": [],
                    "surcharges": [],
                    "prepay": "NO",
                    "cancelAmendTerms": "If cancelled or modified up to 1 day before the date of arrival, no fee will be charged. If cancelled or modified later or in case of no-show, 100 percent of the first night will be charged.",
                    "roomType": "Double",
                    "numberOfAdults": 3,
                    "numberOfChildren": 1,
                    "roomRate": {
                        "billing": {
                            "amount": "6574.43",
                            "currency": "GBP"
                        },
                        "supplier": {
                            "amount": "3713.00",
                            "currency": "AED"
                        },
                        "preferred": {
                            "amount": "6574.43",
                            "currency": "GBP"
                        }
                    },
                    "additions": [],
                    "requirements": [],
                    "features": [],
                    "information": [],
                    "seatReservations": [],
                    "sentLoyaltySchemes": [],
                    "refundable": true
                },
                "traveller": {
                    "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
                    "userId": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
                    "organisationId": "f89103c5-fc65-4ece-9d74-3d727f272d88",
                    "organisationName": "xDesign",
                    "firstName": "Chris",
                    "lastName": "Hern",
                    "emailAddress": "mhairi.duff@xdesign.com",
                    "phoneNumber": "+44 7890 123456",
                    "mobileNumber": "+44 7890 123456",
                    "hasApisDetails": true,
                    "registered": true,
                    "guest": false,
                    "passengerAssistanceConfigured": false
                },
                "bookingDetails": {
                    "type": "HotelSubProductBookingDetails",
                    "requiredInformation": "[{\"id\":\"what-is-your-favourite-colour\",\"type\":\"TEXT\",\"label\":\"What is your favourite colour?\",\"value\":\"asdasd\"},{\"id\":\"is-there-money-in-the-banana-stand\",\"type\":\"BOOLEAN\",\"label\":\"Is there money in the banana stand?\",\"value\":false},{\"id\":\"which-department-do-you-work-in\",\"type\":\"SELECT\",\"label\":\"Which department do you work in?\",\"value\":{\"label\":\"Finance\",\"code\":\"finance\"}}]",
                    "specialRequest": "This is my special request",
                    "additions": [],
                    "flightAdditions": [],
                    "requirements": [],
                    "passengerAssistanceDeclined": false
                },
                "policyComplianceState": "COMPLIANT",
                "ticketNumbers": [],
                "notes": []
            }
        ],
        "searchId": "0842e339-be72-462f-adf7-6d785154bc30"
    },
    "paidWithVouchers": false
}"""