package com.clicktravel.core.testutils

val sampleTravelcard = """ {
            "id": "200120288",
            "booker": {
                "id": "d08e0a19-04ed-475a-97e1-d3b1ffbe76f7",
                "firstName": "Monica",
                "lastName": "One",
                "emailAddress": "monica.nanglu+click1@xdesign.com"
            },
            "payments": {
                "Account": {
                    "chargedAmount": {
                        "amount": "43.16",
                        "currency": "GBP"
                    }
                }
            },
            "createdDateTime": "2020-01-14T11:23:30.375Z",
            "confirmedDate": "2020-01-14T11:24:38.035Z",
            "serviceDate": "2020-01-20",
            "status": "CONFIRMED",
            "internalStatus": "CONFIRMED",
            "cancellable": true,
            "refundable": false,
            "exchangeable": {},
            "product": {
                "id": "024061d8-7f14-40ee-895e-b2e4f42b4b4c",
                "travelType": "TRAVELCARD",
                "createdDate": "2020-01-14T11:23:03.374Z",
                "serviceDate": "2020-01-20",
                "status": "CONFIRMING",
                "details": {
                    "type": "TravelcardProductDetails",
                    "deliveryOptions": [],
                    "travelDate": "2020-01-20",
                    "holdable": false,
                    "legs": [],
                    "fees": []
                },
                "formOfPayment": {
                    "accountType": "CREDIT_ACCOUNT",
                    "type": "CREDIT_ACCOUNT",
                    "id": "1109059",
                    "associatedCostType": "billing",
                    "creditAccountId": "1109059",
                    "creditAccountName": "xDesign",
                    "status": "ACTIVE",
                    "chargeRoutePlan": false
                },
                "bookingDetails": {
                    "type": "TravelcardProductBookingDetails",
                    "billbackOverrides": [],
                    "delivery": {
                        "option": "COLLECT_AT_STATION",
                        "address": []
                    }
                },
                "subProducts": [
                    {
                        "id": "0",
                        "reference": "OPR0006321",
                        "supplierReference": "OPR0006321",
                        "sfsReference": "T134055",
                        "sfsProductId": "7_OPTbe3ef40c-abfa-452d-bd99-8f90869061d1_8792b962-10dd-47e8-92cf-f4cb37b17124",
                        "channel": "OPR",
                        "details": {
                            "type": "TravelcardSubProductDetails",
                            "estimatedServiceCost": {
                                "billing": {
                                    "amount": "15.66",
                                    "currency": "GBP"
                                },
                                "supplier": {
                                    "amount": "15.66",
                                    "currency": "GBP"
                                },
                                "preferred": {
                                    "amount": "15.66",
                                    "currency": "GBP"
                                }
                            },
                            "totalEstimatedCost": {
                                "billing": {
                                    "amount": "15.66",
                                    "currency": "GBP"
                                },
                                "supplier": {
                                    "amount": "15.66",
                                    "currency": "GBP"
                                },
                                "preferred": {
                                    "amount": "15.66",
                                    "currency": "GBP"
                                }
                            },
                            "rules": [],
                            "surcharges": [],
                            "passengerType": "ADULT",
                            "ctrReference": "JWKB34CR",
                            "name": "London Travelcard Zones 1-6",
                            "code": "0035",
                            "ticketType": {
                                "category": "Off-Peak",
                                "code": "ODT",
                                "name": "Off-Peak Day Travelcard"
                            },
                            "requirements": [],
                            "features": [],
                            "information": [],
                            "seatReservations": [],
                            "sentLoyaltySchemes": []
                        },
                        "traveller": {
                            "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
                            "userId": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
                            "organisationId": "f89103c5-fc65-4ece-9d74-3d727f272d88",
                            "organisationName": "xDesign",
                            "firstName": "Chris",
                            "lastName": "Hern",
                            "emailAddress": "mhairi.duff@xdesign.com",
                            "phoneNumber": "+44 7890 123456",
                            "mobileNumber": "+44 7890 123456",
                            "hasApisDetails": true,
                            "registered": true,
                            "guest": false,
                            "passengerAssistanceConfigured": false
                        },
                        "bookingDetails": {
                            "type": "TravelcardSubProductBookingDetails",
                            "requiredInformation": "[{\"id\":\"what-is-your-favourite-colour\",\"type\":\"TEXT\",\"label\":\"What is your favourite colour?\",\"value\":\"BLUE\"},{\"id\":\"is-there-money-in-the-banana-stand\",\"type\":\"BOOLEAN\",\"label\":\"Is there money in the banana stand?\",\"value\":false},{\"id\":\"which-department-do-you-work-in\",\"type\":\"SELECT\",\"label\":\"Which department do you work in?\",\"value\":{\"label\":\"Department\",\"code\":\"department\"}}]",
                            "flightAdditions": [],
                            "requirements": []
                        },
                        "policyComplianceState": "COMPLIANT",
                        "ticketNumbers": [],
                        "notes": []
                    }
                ],
                "searchId": "OPTbe3ef40c-abfa-452d-bd99-8f90869061d1"
            },
            "paidWithVouchers": false
        }"""