package com.clicktravel.core.testutils

import com.clicktravel.core.model.FlightBooking
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.common.TrainStationOrAirport
import com.clicktravel.core.model.flights.BaggageAllowance
import com.clicktravel.core.model.flights.CabinClass
import com.clicktravel.core.model.flights.DetailedFlightJourney
import com.clicktravel.core.model.flights.FlightSegment
import com.clicktravel.core.model.trains.Provider
import com.clicktravel.core.util.UtcDateFormatter


private val globalparser = UtcDateFormatter()
val flightJourneysOther = listOf(
    DetailedFlightJourney(
        segments = listOf(
            FlightSegment(
                flightNumber = "2493",
                segment = "1",
                departureLocation = TrainStationOrAirport(
                    "Edinburgh Airport, Edinburgh, GB",
                    "EDI"
                ),
                arrivalLocation = TrainStationOrAirport(
                    "Franz Josef Strauss Airport, Munich, DE",
                    "MUC"
                ),
                legArrival = globalparser.parse("2019-11-09T14:25:00+0100"),
                legDeparture = globalparser.parse("2019-11-09T11:20:00+0000"),
                travelTime = 125,
                operatingAirline = Provider("Lufthansa", "LH"),
                cabinClass = CabinClass("ECONOMY", "Q"),
                baggageAllowance = BaggageAllowance(1, 23.0)
            ), FlightSegment(
                segment = "2",
                flightNumber = "1206",
                arrivalLocation = TrainStationOrAirport(
                    "Suvarnabhumi Airport, Bangkok, TH",
                    "BKK"
                ),
                departureLocation = TrainStationOrAirport(
                    "Franz Josef Strauss Airport, Munich, DE",
                    "MUC"
                ),
                legDeparture = globalparser.parse("2019-11-09T18:20:00+0100"),
                legArrival = globalparser.parse("2019-11-10T10:50:00+0700"),
                travelTime = 630,
                cabinClass = CabinClass("ECONOMY", "Q"),
                operatingAirline = Provider("Eurowings", "EW"),
                baggageAllowance = BaggageAllowance(1, 23.0)
            )
        ),

        journeyId = "TVVDLUJLSy0yMDE5LTExLTA5VDE4OjIwOjAwLTIwMTktMTEtMTBUMTA6NTA6MDAtRVc="
    )
)
val matchingOtherJson = FlightBooking(
    bookingId = "200112067",
    bookingTravelType = TravelTypes.FLIGHT,
    notes = null,
    journeys = flightJourneysOther,
    ticketCollectionReference = "QS8NOR",
    ticketNumbers = listOf("22057483187353"),
    emailRequired = false,
    emailForCheckIn = null,
    team = "f89103c5-fc65-4ece-9d74-3d727f272d88"
)

val otherBookingItemJson = """{
      "id": "200112067",
      "booker": {
        "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
        "firstName": "Chris",
        "lastName": "Hern",
        "emailAddress": "mhairi.duff@xdesign.com"
      },
      "payments": {
        "Account": {
          "chargedAmount": {
            "amount": "504.48",
            "currency": "GBP"
          }
        }
      },
      "checkInInfo": {
                "emailRequired": false
            },
      "createdDateTime": "2019-10-11T08:59:18.642Z",
      "confirmedDate": "2019-10-11T09:00:06.370Z",
      "serviceDate": "2019-11-09",
      "status": "CONFIRMED",
      "cancellable": true,
      "refundable": false,
      "exchangeable": {},
      "product": {
        "id": "be5b59e5-9437-4982-8336-5c57ef06b7bc",
        "travelType": "FLIGHT",
        "createdDate": "2019-10-11T08:58:59.802Z",
        "serviceDate": "2019-11-09",
        "status": "CONFIRMING",
        "details": {
          "type": "FlightProductDetails",
          "minRate": {
            "billing": {
              "amount": "202.00",
              "currency": "GBP"
            },
            "supplier": {
              "amount": "202.00",
              "currency": "GBP"
            },
            "preferred": {
              "amount": "202.00",
              "currency": "GBP"
            }
          },
          "codeshareCarrier": {
            "code": "LH",
            "name": "Lufthansa"
          },
          "flights": [
            {
              "id": "TVVDLUJLSy0yMDE5LTExLTA5VDE4OjIwOjAwLTIwMTktMTEtMTBUMTA6NTA6MDAtRVc=",
              "cabinClass": {
                "code": "Q",
                "name": "ECONOMY"
              },
              "segments": [
                {
                  "number": 1,
                  "fareRulesId": "772013f1214d47b824a6a029d5b8ef97",
                  "vendorAirline": {
                    "code": "LH",
                    "name": "Lufthansa"
                  },
                  "operatingAirline": {
                    "code": "LH",
                    "name": "Lufthansa",
                    "alliance": "Star Alliance"
                  },
                  "travelTimeMinutes": 125,
                  "travelDistance": 1330,
                  "aircraft": "Airbus A319",
                  "flightNumber": "2493",
                  "flightNumbers": [
                    "2493"
                  ],
                  "cabinClass": {
                    "code": "Q",
                    "name": "ECONOMY"
                  },
                  "depart": {
                    "location": {
                      "code": "EDI",
                      "name": "Edinburgh Airport, Edinburgh, GB",
                      "timezone": "Europe/London",
                      "countryCode": "GB"
                    },
                    "dateTime": "2019-11-09T11:20:00+0000"
                  },
                  "arrive": {
                    "location": {
                      "code": "MUC",
                      "name": "Franz Josef Strauss Airport, Munich, DE",
                      "timezone": "Europe/Berlin",
                      "countryCode": "DE"
                    },
                    "dateTime": "2019-11-09T14:25:00+0100",
                    "terminal": "2"
                  },
                  "baggageAllowance": {
                    "pieces": 1,
                    "weight": 23.0
                  },
                  "segmentId": "FLIGHT1SEGMENT1",
                  "legs": [
                    {
                      "aircraft": "Airbus A319",
                      "arrive": {
                        "location": {
                          "code": "MUC",
                          "name": "Franz Josef Strauss Airport, Munich, DE",
                          "timezone": "Europe/Berlin",
                          "countryCode": "DE"
                        },
                        "dateTime": "2019-11-09T14:25:00+0100",
                        "terminal": "2"
                      },
                      "depart": {
                        "location": {
                          "code": "EDI",
                          "name": "Edinburgh Airport, Edinburgh, GB",
                          "timezone": "Europe/London",
                          "countryCode": "GB"
                        },
                        "dateTime": "2019-11-09T11:20:00+0000"
                      },
                      "legId": "FLIGHT1SEGMENT1LEG1",
                      "number": 1
                    }
                  ],
                  "package": {
                    "name": "Economy Restricted",
                    "title": "NULL",
                    "image": "NULL",
                    "baggageAllowance": {
                      "pieces": 1,
                      "weight": 23.0
                    },
                    "additions": [
                      {
                        "id": "86080bd3-b189-4850-84bd-ed48a0931cb5",
                        "type": "MEAL_BEVERAGE",
                        "included": true,
                        "description": "Food and Beverages"
                      },
                      {
                        "type": "BAGGAGE",
                        "included": false
                      },
                      {
                        "type": "SEAT",
                        "included": false
                      },
                      {
                        "type": "GROUND_TRANSPORTATION",
                        "included": false
                      },
                      {
                        "type": "LOUNGE",
                        "included": false
                      },
                      {
                        "type": "MEDICAL",
                        "included": false
                      },
                      {
                        "type": "PRIORITY_BOARDING",
                        "included": false
                      },
                      {
                        "type": "SPORTS_EQUIPMENT",
                        "included": false
                      },
                      {
                        "type": "FLEXIBILITY",
                        "included": false
                      },
                      {
                        "type": "OTHER",
                        "included": false
                      }
                    ]
                  }
                },
                {
                  "number": 2,
                  "fareRulesId": "772013f1214d47b824a6a029d5b8ef97",
                  "vendorAirline": {
                    "code": "LH",
                    "name": "Lufthansa"
                  },
                  "operatingAirline": {
                    "code": "EW",
                    "name": "Eurowings"
                  },
                  "travelTimeMinutes": 630,
                  "travelDistance": 8798,
                  "aircraft": "Airbus A330-200",
                  "flightNumber": "1206",
                  "flightNumbers": [
                    "5446"
                  ],
                  "cabinClass": {
                    "code": "Q",
                    "name": "ECONOMY"
                  },
                  "depart": {
                    "location": {
                      "code": "MUC",
                      "name": "Franz Josef Strauss Airport, Munich, DE",
                      "timezone": "Europe/Berlin",
                      "countryCode": "DE"
                    },
                    "dateTime": "2019-11-09T18:20:00+0100",
                    "terminal": "2"
                  },
                  "arrive": {
                    "location": {
                      "code": "BKK",
                      "name": "Suvarnabhumi Airport, Bangkok, TH",
                      "timezone": "Asia/Bangkok",
                      "countryCode": "TH"
                    },
                    "dateTime": "2019-11-10T10:50:00+0700"
                  },
                  "baggageAllowance": {
                    "pieces": 1,
                    "weight": 23.0
                  },
                  "segmentId": "FLIGHT1SEGMENT2",
                  "legs": [
                    {
                      "aircraft": "Airbus A330-200",
                      "arrive": {
                        "location": {
                          "code": "BKK",
                          "name": "Suvarnabhumi Airport, Bangkok, TH",
                          "timezone": "Asia/Bangkok",
                          "countryCode": "TH"
                        },
                        "dateTime": "2019-11-10T10:50:00+0700"
                      },
                      "depart": {
                        "location": {
                          "code": "MUC",
                          "name": "Franz Josef Strauss Airport, Munich, DE",
                          "timezone": "Europe/Berlin",
                          "countryCode": "DE"
                        },
                        "dateTime": "2019-11-09T18:20:00+0100",
                        "terminal": "2"
                      },
                      "legId": "FLIGHT1SEGMENT2LEG1",
                      "number": 1
                    }
                  ],
                  "package": {
                    "name": "Economy Restricted",
                    "title": "NULL",
                    "image": "NULL",
                    "baggageAllowance": {
                      "pieces": 1,
                      "weight": 23.0
                    },
                    "additions": [
                      {
                        "id": "86080bd3-b189-4850-84bd-ed48a0931cb5",
                        "type": "MEAL_BEVERAGE",
                        "included": true,
                        "description": "Food and Beverages"
                      },
                      {
                        "type": "BAGGAGE",
                        "included": false
                      },
                      {
                        "type": "SEAT",
                        "included": false
                      },
                      {
                        "type": "GROUND_TRANSPORTATION",
                        "included": false
                      },
                      {
                        "type": "LOUNGE",
                        "included": false
                      },
                      {
                        "type": "MEDICAL",
                        "included": false
                      },
                      {
                        "type": "PRIORITY_BOARDING",
                        "included": false
                      },
                      {
                        "type": "SPORTS_EQUIPMENT",
                        "included": false
                      },
                      {
                        "type": "FLEXIBILITY",
                        "included": false
                      },
                      {
                        "type": "OTHER",
                        "included": false
                      }
                    ]
                  }
                }
              ]
            }
          ],
          "deliveryOptions": [],
          "estaRequired": false,
          "holdable": true,
          "legs": [],
          "fees": []
        },
        "formOfPayment": {
          "accountType": "CREDIT_ACCOUNT",
          "type": "CREDIT_ACCOUNT",
          "id": "1109059",
          "associatedCostType": "billing",
          "creditAccountId": "1109059",
          "creditAccountName": "xDesign",
          "status": "ACTIVE",
          "chargeRoutePlan": false
        },
        "subProducts": [
          {
            "id": "0",
            "reference": "LMQ21U",
            "supplierReference": "QS8NOR",
            "sfsReference": "F124209",
            "sfsProductId": "FLX688b2e99-e1a6-40c5-9e13-18e6a3d65ac3_9f0b56d2-156b-4944-88e9-0ddbdc94f972",
            "channel": "FLX",
            "details": {
              "type": "FlightSubProductDetails",
              "fees": [
                {
                  "type": "VISA",
                  "corporate": true,
                  "billing": {
                    "amount": "13.30",
                    "currency": "GBP"
                  },
                  "supplier": {
                    "amount": "13.30",
                    "currency": "GBP"
                  },
                  "preferred": {
                    "amount": "13.30",
                    "currency": "GBP"
                  }
                },
                {
                  "type": "MASTER_CARD",
                  "corporate": true,
                  "billing": {
                    "amount": "13.30",
                    "currency": "GBP"
                  },
                  "supplier": {
                    "amount": "13.30",
                    "currency": "GBP"
                  },
                  "preferred": {
                    "amount": "13.30",
                    "currency": "GBP"
                  }
                },
                {
                  "type": "AMERICAN_EXPRESS",
                  "corporate": true,
                  "billing": {
                    "amount": "13.30",
                    "currency": "GBP"
                  },
                  "supplier": {
                    "amount": "13.30",
                    "currency": "GBP"
                  },
                  "preferred": {
                    "amount": "13.30",
                    "currency": "GBP"
                  }
                }
              ],
              "estimatedServiceCost": {
                "billing": {
                  "amount": "500.88",
                  "currency": "GBP"
                },
                "supplier": {
                  "amount": "500.88",
                  "currency": "GBP"
                },
                "preferred": {
                  "amount": "500.88",
                  "currency": "GBP"
                }
              },
              "totalEstimatedCost": {
                "billing": {
                  "amount": "500.88",
                  "currency": "GBP"
                },
                "supplier": {
                  "amount": "500.88",
                  "currency": "GBP"
                },
                "preferred": {
                  "amount": "500.88",
                  "currency": "GBP"
                }
              },
              "totalEstimatedCostWithAdditions": {
                "billing": {
                  "amount": "500.88",
                  "currency": "GBP"
                },
                "supplier": {
                  "amount": "500.88",
                  "currency": "GBP"
                },
                "preferred": {
                  "amount": "500.88",
                  "currency": "GBP"
                }
              },
              "rules": [],
              "surcharges": [],
              "codeshareCarrier": {
                "code": "LH",
                "name": "Lufthansa"
              },
              "passengerType": "ADULT",
              "baggageAllowance": {
                "pieces": 1,
                "weight": 23.0
              },
              "taxAmount": {
                "billing": {
                  "amount": "224.88",
                  "currency": "GBP"
                },
                "supplier": {
                  "amount": "224.88",
                  "currency": "GBP"
                },
                "preferred": {
                  "amount": "224.88",
                  "currency": "GBP"
                }
              },
              "baseAmount": {
                "billing": {
                  "amount": "276.00",
                  "currency": "GBP"
                },
                "supplier": {
                  "amount": "276.00",
                  "currency": "GBP"
                },
                "preferred": {
                  "amount": "276.00",
                  "currency": "GBP"
                }
              },
              "apisRequired": false,
              "hasApisDetails": false,
              "firstNightAccommodationRequired": false,
              "requirements": [],
              "features": [],
              "information": [],
              "seatReservations": [],
              "canPreBookSeats": false,
              "supportsSeatMap": false,
              "sentLoyaltySchemes": []
            },
            "traveller": {
              "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
              "userId": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
              "organisationId": "f89103c5-fc65-4ece-9d74-3d727f272d88",
              "organisationName": "xDesign",
              "firstName": "Mhairi",
              "lastName": "Duff",
              "emailAddress": "mhairi.duff@xdesign.com",
              "hasApisDetails": true,
              "registered": true,
              "guest": false,
              "passengerAssistanceConfigured": false
            },
            "bookingDetails": {
              "type": "FlightSubProductBookingDetails",
              "requiredInformation": "[]",
              "flightAdditions": [],
              "requirements": [],
              "passengerAssistanceDeclined": false
            },
            "policyComplianceState": "COMPLIANT",
            "ticketNumbers": [
              "22057483187353"
            ],
            "notes": []
          }
        ],
        "searchId": "FLX688b2e99-e1a6-40c5-9e13-18e6a3d65ac3"
      },
      "paidWithVouchers": false
    }"""