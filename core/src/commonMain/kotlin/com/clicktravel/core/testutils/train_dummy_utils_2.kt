package com.clicktravel.core.testutils

import com.clicktravel.core.model.TrainBooking
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.common.Address
import com.clicktravel.core.model.common.RequiredInformation
import com.clicktravel.core.model.common.TrainStationOrAirport
import com.clicktravel.core.model.trains.*
import com.clicktravel.core.model.trains.enums.DeliveryOption
import com.clicktravel.core.model.trains.enums.PassengerType
import com.clicktravel.core.util.UtcDateFormatter

val cheltenhamBookingWithETicket = TrainBooking(
    address = Address("", "", ""),
    deliveryOption = DeliveryOption.E_TICKET,
    bookingId = "200116080",
    passengerType = PassengerType.ADULT,
    bookingTravelType = TravelTypes.TRAIN,
    ticketCollectionReference = "SOLD_VIA_ETICKET",
    fareType = "Valid on CrossCountry services only. Advance tickets are valid only on the booked services.",
    journeys = listOf(
        DetailedTrainJourney(
            departureStation = TrainStationOrAirport(
                "Birmingham New Street",
                "1127",
                trainCrs = "BHM"
            ),
            arrivalStation = TrainStationOrAirport(
                "Cheltenham Spa",
                "4731",
                trainCrs = "CNM"
            ),
            ticketType = TicketType("Advance Single"),
            departureDateTime = UtcDateFormatter().parse("2019-11-29T07:42:00+0000"),
            segments = listOf(
                TrainSegment(
                    travelType = ConnectionOrJourneyType.TRAIN,
                    departureLocation = TrainStationOrAirport(
                        "Birmingham New Street",
                        "1127",
                        trainCrs = "BHM"
                    ),
                    arrivalLocation = TrainStationOrAirport(
                        "Cheltenham Spa",
                        "4731",
                        trainCrs = "CNM"
                    ),
                    legDeparture = UtcDateFormatter().parse("2019-11-29T07:42:00+0000"),
                    trainClassInformation = TrainClassInformation("Train has First & Standard class seating"),
                    provider = Provider("CrossCountry"),
                    connectionTime = 0,
                    travelTime = 42,
                    legArrival = UtcDateFormatter().parse("2019-11-29T08:24:00+0000"),
                    reservation = SeatReservation(coachClass = "Standard", coach = "D", seat = 17)
                )
            ),
            journeyId = "10002",
            couponId = "1"
        )
    ),
    fareClass = "STANDARD",
    notes = null,
    fields = listOf(
        RequiredInformation("What is your favourite colour?", "Abc"),
        RequiredInformation("Is there money in the banana stand?", "false"),
        RequiredInformation("Which department do you work in?", "Banana stand")
    ).sortedBy { it.key },
    team = "f89103c5-fc65-4ece-9d74-3d727f272d88"
)

val trainJson2 = " {\n" +
        "            \"id\": \"200116080\",\n" +
        "            \"booker\": {\n" +
        "                \"id\": \"61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11\",\n" +
        "                \"firstName\": \"Chris\",\n" +
        "                \"lastName\": \"Hern\",\n" +
        "                \"emailAddress\": \"mhairi.duff@xdesign.com\"\n" +
        "            },\n" +
        "            \"payments\": {\n" +
        "                \"Account\": {\n" +
        "                    \"chargedAmount\": {\n" +
        "                        \"amount\": \"25.65\",\n" +
        "                        \"currency\": \"GBP\"\n" +
        "                    }\n" +
        "                }\n" +
        "            },\n" +
        "            \"createdDateTime\": \"2019-11-21T10:40:26.786Z\",\n" +
        "            \"confirmedDate\": \"2019-11-21T10:41:38.046Z\",\n" +
        "            \"serviceDate\": \"2019-11-29\",\n" +
        "            \"status\": \"CONFIRMED\",\n" +
        "            \"internalStatus\": \"CONFIRMED\",\n" +
        "            \"cancellable\": false,\n" +
        "            \"refundable\": false,\n" +
        "            \"exchangeable\": {\n" +
        "                \"value\": true\n" +
        "            },\n" +
        "            \"product\": {\n" +
        "                \"id\": \"f5544f66-bb2f-47c7-ac8c-49bf2ad131f8\",\n" +
        "                \"travelType\": \"TRAIN\",\n" +
        "                \"createdDate\": \"2019-11-21T10:39:56.631Z\",\n" +
        "                \"serviceDate\": \"2019-11-29\",\n" +
        "                \"status\": \"CONFIRMING\",\n" +
        "                \"details\": {\n" +
        "                    \"type\": \"TrainProductDetails\",\n" +
        "                    \"minRate\": {\n" +
        "                        \"supplier\": {\n" +
        "                            \"amount\": \"23.150\",\n" +
        "                            \"currency\": \"GBP\"\n" +
        "                        }\n" +
        "                    },\n" +
        "                    \"minRatePerPerson\": {\n" +
        "                        \"supplier\": {\n" +
        "                            \"amount\": \"23.15\",\n" +
        "                            \"currency\": \"GBP\"\n" +
        "                        }\n" +
        "                    },\n" +
        "                    \"deliveryOptions\": [],\n" +
        "                    \"journeys\": [\n" +
        "                        {\n" +
        "                            \"id\": \"10002\",\n" +
        "                            \"offPeakAvailable\": false,\n" +
        "                            \"superOffPeakAvailable\": false,\n" +
        "                            \"legs\": [\n" +
        "                                {\n" +
        "                                    \"id\": \"1\",\n" +
        "                                    \"serviceProvider\": {\n" +
        "                                        \"code\": \"XC\",\n" +
        "                                        \"mode\": \"TRAIN\",\n" +
        "                                        \"name\": \"CrossCountry\"\n" +
        "                                    },\n" +
        "                                    \"travelTimeMinutes\": 42,\n" +
        "                                    \"depart\": {\n" +
        "                                        \"location\": {\n" +
        "                                            \"code\": \"1127\",\n" +
        "                                            \"name\": \"Birmingham New Street\",\n" +
        "                                            \"crs\": \"BHM\"\n" +
        "                                        },\n" +
        "                                        \"dateTime\": \"2019-11-29T07:42:00+0000\"\n" +
        "                                    },\n" +
        "                                    \"arrive\": {\n" +
        "                                        \"location\": {\n" +
        "                                            \"code\": \"4731\",\n" +
        "                                            \"name\": \"Cheltenham Spa\",\n" +
        "                                            \"crs\": \"CNM\"\n" +
        "                                        },\n" +
        "                                        \"dateTime\": \"2019-11-29T08:24:00+0000\"\n" +
        "                                    },\n" +
        "                                    \"seatNumber\": \"D17A\",\n" +
        "                                    \"ultimateDestination\": {\n" +
        "                                        \"code\": \"3231\",\n" +
        "                                        \"name\": \"Bristol Temple Meads\",\n" +
        "                                        \"crs\": \"BRI\"\n" +
        "                                    },\n" +
        "                                    \"seatingClass\": \"Train has First & Standard class seating\"\n" +
        "                                }\n" +
        "                            ],\n" +
        "                            \"ticketType\": {\n" +
        "                                \"code\": \"MGS\",\n" +
        "                                \"name\": \"Advance Single\"\n" +
        "                            },\n" +
        "                            \"direction\": \"OUTBOUND\",\n" +
        "                            \"refundableUntil\": \"2019-12-27\",\n" +
        "                            \"couponId\": \"1\"\n" +
        "                        }\n" +
        "                    ],\n" +
        "                    \"holdable\": false,\n" +
        "                    \"legs\": [],\n" +
        "                    \"fees\": []\n" +
        "                },\n" +
        "                \"formOfPayment\": {\n" +
        "                    \"accountType\": \"CREDIT_ACCOUNT\",\n" +
        "                    \"type\": \"CREDIT_ACCOUNT\",\n" +
        "                    \"id\": \"1109059\",\n" +
        "                    \"associatedCostType\": \"billing\",\n" +
        "                    \"creditAccountId\": \"1109059\",\n" +
        "                    \"creditAccountName\": \"xDesign\",\n" +
        "                    \"status\": \"ACTIVE\",\n" +
        "                    \"chargeRoutePlan\": false\n" +
        "                },\n" +
        "                \"bookingDetails\": {\n" +
        "                    \"type\": \"TrainProductBookingDetails\",\n" +
        "                    \"billbackOverrides\": [],\n" +
        "                    \"delivery\": {\n" +
        "                        \"option\": \"E_TICKET\",\n" +
        "                        \"address\": []\n" +
        "                    }\n" +
        "                },\n" +
        "                \"subProducts\": [\n" +
        "                    {\n" +
        "                        \"id\": \"0\",\n" +
        "                        \"reference\": \"OPR0006272\",\n" +
        "                        \"supplierReference\": \"OPR0006272\",\n" +
        "                        \"sfsReference\": \"T133036\",\n" +
        "                        \"sfsProductId\": \"OPT4095715d-072e-4a5c-b33d-429719ccad6a_bbe0fdab-ee6d-4997-b450-641d6f5747cb\",\n" +
        "                        \"channel\": \"OPR\",\n" +
        "                        \"details\": {\n" +
        "                            \"type\": \"TrainSubProductDetails\",\n" +
        "                            \"fees\": [],\n" +
        "                            \"estimatedServiceCost\": {\n" +
        "                                \"billing\": {\n" +
        "                                    \"amount\": \"23.15\",\n" +
        "                                    \"currency\": \"GBP\"\n" +
        "                                },\n" +
        "                                \"supplier\": {\n" +
        "                                    \"amount\": \"23.15\",\n" +
        "                                    \"currency\": \"GBP\"\n" +
        "                                },\n" +
        "                                \"preferred\": {\n" +
        "                                    \"amount\": \"23.15\",\n" +
        "                                    \"currency\": \"GBP\"\n" +
        "                                }\n" +
        "                            },\n" +
        "                            \"totalEstimatedCost\": {\n" +
        "                                \"billing\": {\n" +
        "                                    \"amount\": \"23.15\",\n" +
        "                                    \"currency\": \"GBP\"\n" +
        "                                },\n" +
        "                                \"supplier\": {\n" +
        "                                    \"amount\": \"23.15\",\n" +
        "                                    \"currency\": \"GBP\"\n" +
        "                                },\n" +
        "                                \"preferred\": {\n" +
        "                                    \"amount\": \"23.15\",\n" +
        "                                    \"currency\": \"GBP\"\n" +
        "                                }\n" +
        "                            },\n" +
        "                            \"rules\": [],\n" +
        "                            \"surcharges\": [],\n" +
        "                            \"passengerType\": \"ADULT\",\n" +
        "                            \"ctrReference\": \"SOLD_VIA_ETICKET\",\n" +
        "                            \"fares\": [\n" +
        "                                {\n" +
        "                                    \"id\": \"30015\",\n" +
        "                                    \"ticketRestriction\": \"XV\",\n" +
        "                                    \"ticketType\": {\n" +
        "                                        \"code\": \"MGS\",\n" +
        "                                        \"name\": \"Advance Single\"\n" +
        "                                    },\n" +
        "                                    \"fareRoute\": {\n" +
        "                                        \"code\": \"00024\",\n" +
        "                                        \"name\": \"Valid on CrossCountry services only. Advance tickets are valid only on the booked services.\"\n" +
        "                                    },\n" +
        "                                    \"fareClass\": \"STANDARD\",\n" +
        "                                    \"journeyType\": \"SINGLE\",\n" +
        "                                    \"crossLondon\": false,\n" +
        "                                    \"cancellable\": false,\n" +
        "                                    \"origin\": {\n" +
        "                                        \"code\": \"0418\",\n" +
        "                                        \"name\": \"Birmingham New St, Moor St or Snow Hill\"\n" +
        "                                    },\n" +
        "                                    \"destination\": {\n" +
        "                                        \"code\": \"4731\",\n" +
        "                                        \"name\": \"Cheltenham Spa\"\n" +
        "                                    },\n" +
        "                                    \"fareConditionsId\": \"29112019MGSXV\"\n" +
        "                                }\n" +
        "                            ],\n" +
        "                            \"requirements\": [],\n" +
        "                            \"features\": [],\n" +
        "                            \"information\": [],\n" +
        "                            \"seatReservations\": [],\n" +
        "                            \"sentLoyaltySchemes\": []\n" +
        "                        },\n" +
        "                        \"traveller\": {\n" +
        "                            \"id\": \"61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11\",\n" +
        "                            \"userId\": \"61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11\",\n" +
        "                            \"organisationId\": \"f89103c5-fc65-4ece-9d74-3d727f272d88\",\n" +
        "                            \"organisationName\": \"xDesign\",\n" +
        "                            \"firstName\": \"Chris\",\n" +
        "                            \"lastName\": \"Hern\",\n" +
        "                            \"emailAddress\": \"mhairi.duff@xdesign.com\",\n" +
        "                            \"phoneNumber\": \"+44 7890 123456\",\n" +
        "                            \"mobileNumber\": \"+44 7890 123456\",\n" +
        "                            \"hasApisDetails\": true,\n" +
        "                            \"registered\": true,\n" +
        "                            \"guest\": false,\n" +
        "                            \"passengerAssistanceConfigured\": false\n" +
        "                        },\n" +
        "                        \"bookingDetails\": {\n" +
        "                            \"type\": \"TrainSubProductBookingDetails\",\n" +
        "                            \"requiredInformation\": \"[{\\\"id\\\":\\\"what-is-your-favourite-colour\\\",\\\"type\\\":\\\"TEXT\\\",\\\"label\\\":\\\"What is your favourite colour?\\\",\\\"value\\\":\\\"Abc\\\"},{\\\"id\\\":\\\"is-there-money-in-the-banana-stand\\\",\\\"type\\\":\\\"BOOLEAN\\\",\\\"label\\\":\\\"Is there money in the banana stand?\\\",\\\"value\\\":false},{\\\"id\\\":\\\"which-department-do-you-work-in\\\",\\\"type\\\":\\\"SELECT\\\",\\\"label\\\":\\\"Which department do you work in?\\\",\\\"value\\\":{\\\"label\\\":\\\"Banana stand\\\",\\\"code\\\":\\\"banana-stand \\\"}}]\",\n" +
        "                            \"flightAdditions\": [],\n" +
        "                            \"requirements\": [],\n" +
        "                            \"passengerAssistanceDeclined\": false\n" +
        "                        },\n" +
        "                        \"policyComplianceState\": \"COMPLIANT\",\n" +
        "                        \"couponId\": \"1\",\n" +
        "                        \"ticketNumbers\": [],\n" +
        "                        \"notes\": []\n" +
        "                    }\n" +
        "                ],\n" +
        "                \"searchId\": \"OPT4095715d-072e-4a5c-b33d-429719ccad6a\"\n" +
        "            },\n" +
        "            \"paidWithVouchers\": false\n" +
        "        }"