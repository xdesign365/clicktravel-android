package com.clicktravel.core.testutils

import com.clicktravel.core.model.FlightBooking
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.common.TrainStationOrAirport
import com.clicktravel.core.model.flights.BaggageAllowance
import com.clicktravel.core.model.flights.CabinClass
import com.clicktravel.core.model.flights.DetailedFlightJourney
import com.clicktravel.core.model.flights.FlightSegment
import com.clicktravel.core.model.trains.Note
import com.clicktravel.core.model.trains.Provider
import com.clicktravel.core.util.UtcDateFormatter


private val globalparser = UtcDateFormatter()

val flightJourneys = listOf(
    DetailedFlightJourney(
        segments = listOf(
            FlightSegment(
                segment = "1",
                legDeparture = globalparser.parse("2019-10-31T06:00:00+0000"),
                legArrival = globalparser.parse("2019-10-31T09:45:00+0100"),
                departureLocation = TrainStationOrAirport(
                    "Birmingham International Airport, Birmingham, GB",
                    "BHX"
                ),
                arrivalLocation = TrainStationOrAirport(
                    "Charles de Gaulle Airport, Paris, FR",
                    "CDG"
                ),
                travelTime = 165,
                cabinClass = CabinClass("ECONOMY", "R"),
                operatingAirline = Provider("Air France", "AF"),
                baggageAllowance = BaggageAllowance(0, 0.0),
                flightNumber = "3007"
            )
        ),
        journeyId = "QkhYLUNERy0yMDE5LTEwLTMxVDA2OjAwOjAwLTIwMTktMTAtMzFUMDk6NDU6MDAtQUY="
    ),
    DetailedFlightJourney(
        segments = listOf(
            FlightSegment(
                segment = "1",
                arrivalLocation = TrainStationOrAirport(
                    "Birmingham International Airport, Birmingham, GB",
                    "BHX"
                ),
                departureLocation = TrainStationOrAirport(
                    "Charles de Gaulle Airport, Paris, FR",
                    "CDG"
                ),
                legDeparture = globalparser.parse("2019-11-07T13:00:00+0100"),
                legArrival = globalparser.parse("2019-11-07T13:15:00+0000"),
                travelTime = 75,
                cabinClass = CabinClass("ECONOMY", "R"),
                operatingAirline = Provider("Air France", "AF"),
                baggageAllowance = BaggageAllowance(0, 0.0),
                flightNumber = "3000"
            )
        ),
        journeyId = "Q0RHLUJIWC0yMDE5LTExLTA3VDEzOjAwOjAwLTIwMTktMTEtMDdUMTM6MTU6MDAtQUY="
    )
)

val flightBookingMatchingObject = FlightBooking(
    bookingId = "200111726",
    bookingTravelType = TravelTypes.FLIGHT,
    notes = Note("Test note"),
    journeys = flightJourneys,
    ticketCollectionReference = "7f028287-74d1-47ad-a186-aa4e84ba5a96",
    ticketNumbers = listOf("TicketNumberForMhairiDuff"),
    emailRequired = true,
    emailForCheckIn = "checkin@email.com",
    team = "f89103c5-fc65-4ece-9d74-3d727f272d88"
)

val flightBookingJson = """{
      "id": "200111726",
      "booker": {
        "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
        "firstName": "Chris",
        "lastName": "Hern",
        "emailAddress": "mhairi.duff@xdesign.com"
      },
      "payments": {
        "virtualCard": {
          "paymentId": "8d0c66e7-d736-4945-8642-cfad959ac22b",
          "panHint": "0001",
          "chargedAmount": {
            "amount": "403.60",
            "currency": "GBP"
          }
        }
      },
      "checkInInfo": {
                "emailRequired": true,
                "emailForCheckIn": "checkin@email.com"
            },
      "createdDateTime": "2019-10-08T13:14:01.040Z",
      "confirmedDate": "2019-10-08T13:14:43.537Z",
      "serviceDate": "2019-10-31",
      "status": "CONFIRMED",
      "cancellable": true,
      "refundable": false,
      "exchangeable": {},
      "product": {
        "id": "3924dc05-c8dc-4f01-b28e-1df5ddc4f589",
        "travelType": "FLIGHT",
        "createdDate": "2019-10-08T13:13:17.895Z",
        "serviceDate": "2019-10-31",
        "status": "CONFIRMING",
        "details": {
          "type": "FlightProductDetails",
          "minRate": {
            "billing": {
              "amount": "400.00",
              "currency": "GBP"
            },
            "supplier": {
              "amount": "400.00",
              "currency": "GBP"
            },
            "preferred": {
              "amount": "400.00",
              "currency": "GBP"
            }
          },
          "codeshareCarrier": {
            "code": "BE",
            "name": "Flybe"
          },
          "flights": [
            {
              "id": "QkhYLUNERy0yMDE5LTEwLTMxVDA2OjAwOjAwLTIwMTktMTAtMzFUMDk6NDU6MDAtQUY=",
              "cabinClass": {
                "code": "R",
                "name": "ECONOMY"
              },
              "segments": [
                {
                  "number": 1,
                  "fareRulesId": "d41d081a82ba65e00ed8639597c6ea1b",
                  "vendorAirline": {
                    "code": "AF",
                    "name": "Air France"
                  },
                  "operatingAirline": {
                    "code": "AF",
                    "name": "Air France",
                    "alliance": "Sky Team"
                  },
                  "travelTimeMinutes": 165,
                  "travelDistance": 488,
                  "aircraft": "AIRBUS J138",
                  "flightNumber": "3007",
                  "flightNumbers": [
                    "3007"
                  ],
                  "cabinClass": {
                    "code": "R",
                    "name": "ECONOMY"
                  },
                  "depart": {
                    "location": {
                      "code": "BHX",
                      "name": "Birmingham International Airport, Birmingham, GB",
                      "timezone": "Europe/London",
                      "countryCode": "GB"
                    },
                    "dateTime": "2019-10-31T06:00:00+0000"
                  },
                  "arrive": {
                    "location": {
                      "code": "CDG",
                      "name": "Charles de Gaulle Airport, Paris, FR",
                      "timezone": "Europe/Paris",
                      "countryCode": "FR"
                    },
                    "dateTime": "2019-10-31T09:45:00+0100"
                  },
                  "baggageAllowance": {
                    "pieces": 0,
                    "weight": 0.0
                  },
                  "segmentId": "+nw8FicSRvy2wplTeKJ77w==",
                  "legs": [
                    {
                      "aircraft": "AIRBUS J138",
                      "arrive": {
                        "location": {
                          "code": "CDG",
                          "name": "Charles de Gaulle Airport, Paris, FR",
                          "timezone": "Europe/Paris",
                          "countryCode": "FR"
                        },
                        "dateTime": "2019-10-31T09:45:00+0100"
                      },
                      "depart": {
                        "location": {
                          "code": "BHX",
                          "name": "Birmingham International Airport, Birmingham, GB",
                          "timezone": "Europe/London",
                          "countryCode": "GB"
                        },
                        "dateTime": "2019-10-31T06:00:00+0000"
                      },
                      "legId": "ZwMLmdRLQjGAqaISJQ0/qQ==",
                      "number": 1
                    }
                  ],
                  "package": {
                    "name": "Standard",
                    "title": "NULL",
                    "image": "NULL",
                    "baggageAllowance": {
                      "pieces": 0,
                      "weight": 0.0
                    },
                    "additions": [
                      {
                        "id": "9331b92b-430c-40a9-a77c-81051c6366b6",
                        "type": "FLEXIBILITY",
                        "included": true,
                        "description": "This fare is refundable"
                      },
                      {
                        "type": "BAGGAGE",
                        "included": false
                      },
                      {
                        "type": "SEAT",
                        "included": false
                      },
                      {
                        "type": "GROUND_TRANSPORTATION",
                        "included": false
                      },
                      {
                        "type": "LOUNGE",
                        "included": false
                      },
                      {
                        "type": "MEDICAL",
                        "included": false
                      },
                      {
                        "type": "MEAL_BEVERAGE",
                        "included": false
                      },
                      {
                        "type": "PRIORITY_BOARDING",
                        "included": false
                      },
                      {
                        "type": "SPORTS_EQUIPMENT",
                        "included": false
                      },
                      {
                        "type": "OTHER",
                        "included": false
                      }
                    ]
                  }
                }
              ]
            },
            {
              "id": "Q0RHLUJIWC0yMDE5LTExLTA3VDEzOjAwOjAwLTIwMTktMTEtMDdUMTM6MTU6MDAtQUY=",
              "cabinClass": {
                "code": "R",
                "name": "ECONOMY"
              },
              "segments": [
                {
                  "number": 1,
                  "fareRulesId": "d41d081a82ba65e00ed8639597c6ea1b",
                  "vendorAirline": {
                    "code": "AF",
                    "name": "Air France"
                  },
                  "operatingAirline": {
                    "code": "AF",
                    "name": "Air France",
                    "alliance": "Sky Team"
                  },
                  "travelTimeMinutes": 75,
                  "travelDistance": 488,
                  "aircraft": "AIRBUS J138",
                  "flightNumber": "3000",
                  "flightNumbers": [
                    "3000"
                  ],
                  "cabinClass": {
                    "code": "R",
                    "name": "ECONOMY"
                  },
                  "depart": {
                    "location": {
                      "code": "CDG",
                      "name": "Charles de Gaulle Airport, Paris, FR",
                      "timezone": "Europe/Paris",
                      "countryCode": "FR"
                    },
                    "dateTime": "2019-11-07T13:00:00+0100"
                  },
                  "arrive": {
                    "location": {
                      "code": "BHX",
                      "name": "Birmingham International Airport, Birmingham, GB",
                      "timezone": "Europe/London",
                      "countryCode": "GB"
                    },
                    "dateTime": "2019-11-07T13:15:00+0000"
                  },
                  "baggageAllowance": {
                    "pieces": 0,
                    "weight": 0.0
                  },
                  "segmentId": "ERMPCy9TRyyu6WEyQP474Q==",
                  "legs": [
                    {
                      "aircraft": "AIRBUS J138",
                      "arrive": {
                        "location": {
                          "code": "BHX",
                          "name": "Birmingham International Airport, Birmingham, GB",
                          "timezone": "Europe/London",
                          "countryCode": "GB"
                        },
                        "dateTime": "2019-11-07T13:15:00+0000"
                      },
                      "depart": {
                        "location": {
                          "code": "CDG",
                          "name": "Charles de Gaulle Airport, Paris, FR",
                          "timezone": "Europe/Paris",
                          "countryCode": "FR"
                        },
                        "dateTime": "2019-11-07T13:00:00+0100"
                      },
                      "legId": "Qh1s8mTKRFqxx5ZAX8uKfQ==",
                      "number": 1
                    }
                  ],
                  "package": {
                    "name": "Standard",
                    "title": "NULL",
                    "image": "NULL",
                    "baggageAllowance": {
                      "pieces": 0,
                      "weight": 0.0
                    },
                    "additions": [
                      {
                        "id": "05b4121a-4164-4cfe-bbe4-240496376cc1",
                        "type": "FLEXIBILITY",
                        "included": true,
                        "description": "This fare is refundable"
                      },
                      {
                        "type": "BAGGAGE",
                        "included": false
                      },
                      {
                        "type": "SEAT",
                        "included": false
                      },
                      {
                        "type": "GROUND_TRANSPORTATION",
                        "included": false
                      },
                      {
                        "type": "LOUNGE",
                        "included": false
                      },
                      {
                        "type": "MEDICAL",
                        "included": false
                      },
                      {
                        "type": "MEAL_BEVERAGE",
                        "included": false
                      },
                      {
                        "type": "PRIORITY_BOARDING",
                        "included": false
                      },
                      {
                        "type": "SPORTS_EQUIPMENT",
                        "included": false
                      },
                      {
                        "type": "OTHER",
                        "included": false
                      }
                    ]
                  }
                }
              ]
            }
          ],
          "deliveryOptions": [],
          "estaRequired": false,
          "holdable": false,
          "legs": [],
          "fees": []
        },
        "formOfPayment": {
          "accountType": "CREDIT_ACCOUNT",
          "type": "CREDIT_ACCOUNT",
          "id": "1109059",
          "associatedCostType": "billing",
          "creditAccountId": "1109059",
          "creditAccountName": "xDesign",
          "status": "ACTIVE",
          "chargeRoutePlan": false
        },
        "subProducts": [
          {
            "id": "0",
            "reference": "7f028287-74d1-47ad-a186-aa4e84ba5a96",
            "supplierReference": "7f028287-74d1-47ad-a186-aa4e84ba5a96",
            "sfsReference": "F124147",
            "sfsProductId": "MOCa2d88408-1736-4fb7-a310-a1889b0b36f7_6641f0f0-ec06-4e5f-b895-ac456debf8e9",
            "channel": "MOC",
            "details": {
              "type": "FlightSubProductDetails",
              "fees": [
                {
                  "type": "AMERICAN_EXPRESS",
                  "corporate": true,
                  "billing": {
                    "amount": "0.86",
                    "currency": "GBP"
                  },
                  "supplier": {
                    "amount": "0.86",
                    "currency": "GBP"
                  },
                  "preferred": {
                    "amount": "0.86",
                    "currency": "GBP"
                  }
                },
                {
                  "type": "VISA",
                  "corporate": true,
                  "billing": {
                    "amount": "0.86",
                    "currency": "GBP"
                  },
                  "supplier": {
                    "amount": "0.86",
                    "currency": "GBP"
                  },
                  "preferred": {
                    "amount": "0.86",
                    "currency": "GBP"
                  }
                },
                {
                  "type": "MASTER_CARD",
                  "corporate": true,
                  "billing": {
                    "amount": "0.86",
                    "currency": "GBP"
                  },
                  "supplier": {
                    "amount": "0.86",
                    "currency": "GBP"
                  },
                  "preferred": {
                    "amount": "0.86",
                    "currency": "GBP"
                  }
                }
              ],
              "estimatedServiceCost": {
                "billing": {
                  "amount": "400.00",
                  "currency": "GBP"
                },
                "supplier": {
                  "amount": "400.00",
                  "currency": "GBP"
                },
                "preferred": {
                  "amount": "400.00",
                  "currency": "GBP"
                }
              },
              "totalEstimatedCost": {
                "billing": {
                  "amount": "400.00",
                  "currency": "GBP"
                },
                "supplier": {
                  "amount": "400.00",
                  "currency": "GBP"
                },
                "preferred": {
                  "amount": "400.00",
                  "currency": "GBP"
                }
              },
              "totalEstimatedCostWithAdditions": {
                "billing": {
                  "amount": "400.00",
                  "currency": "GBP"
                },
                "supplier": {
                  "amount": "400.00",
                  "currency": "GBP"
                },
                "preferred": {
                  "amount": "400.00",
                  "currency": "GBP"
                }
              },
              "rules": [],
              "surcharges": [],
              "codeshareCarrier": {
                "code": "BE",
                "name": "Flybe"
              },
              "passengerType": "ADULT",
              "baggageAllowance": {
                "pieces": 0,
                "weight": 0.0
              },
              "taxAmount": {
                "billing": {
                  "amount": "30.00",
                  "currency": "GBP"
                },
                "supplier": {
                  "amount": "30.00",
                  "currency": "GBP"
                },
                "preferred": {
                  "amount": "30.00",
                  "currency": "GBP"
                }
              },
              "baseAmount": {
                "billing": {
                  "amount": "370.00",
                  "currency": "GBP"
                },
                "supplier": {
                  "amount": "370.00",
                  "currency": "GBP"
                },
                "preferred": {
                  "amount": "370.00",
                  "currency": "GBP"
                }
              },
              "apisRequired": false,
              "hasApisDetails": false,
              "firstNightAccommodationRequired": false,
              "requirements": [],
              "features": [],
              "information": [],
              "seatReservations": [],
              "canPreBookSeats": false,
              "supportsSeatMap": false,
              "sentLoyaltySchemes": []
            },
            "traveller": {
              "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
              "userId": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
              "organisationId": "f89103c5-fc65-4ece-9d74-3d727f272d88",
              "organisationName": "xDesign",
              "firstName": "Mhairi",
              "lastName": "Duff",
              "emailAddress": "mhairi.duff@xdesign.com",
              "hasApisDetails": true,
              "registered": true,
              "guest": false,
              "passengerAssistanceConfigured": false
            },
            "bookingDetails": {
              "type": "FlightSubProductBookingDetails",
              "requiredInformation": "[]",
              "flightAdditions": [],
              "requirements": [],
              "passengerAssistanceDeclined": false
            },
            "policyComplianceState": "COMPLIANT",
            "ticketNumbers": [
              "TicketNumberForMhairiDuff"
            ],
            "notes": [
              {
                "value": "Test note",
                "user": {
                  "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
                  "firstName": "Chris",
                  "lastName": "Hern",
                  "emailAddress": "mhairi.duff@xdesign.com"
                },
                "creationDateTime": "2019-10-08T13:13:41.273Z"
              }
            ]
          }
        ],
        "searchId": "MOCa2d88408-1736-4fb7-a310-a1889b0b36f7"
      },
      "paidWithVouchers": false
    }"""