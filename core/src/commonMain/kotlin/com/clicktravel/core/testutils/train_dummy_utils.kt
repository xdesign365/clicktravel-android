package com.clicktravel.core.testutils

import com.clicktravel.core.model.TrainBooking
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.common.Address
import com.clicktravel.core.model.common.TrainStationOrAirport
import com.clicktravel.core.model.trains.*
import com.clicktravel.core.model.trains.enums.DeliveryOption
import com.clicktravel.core.model.trains.enums.PassengerType
import com.clicktravel.core.util.UtcDateFormatter

val portsmouth = TrainStationOrAirport(
    "Portsmouth & Southsea",
    "5537",
    trainCrs = "PMS"
)
val superOffPeak = TicketType("Super Off-Peak Return")
val edinburgh =
    TrainStationOrAirport("Edinburgh", "9328", trainCrs = "EDB")
val londonEastern = Provider("London North Eastern Railway")
val underground = Provider("Underground")
val southWesternRailway = Provider("South Western Railway")
val northEasternRailway = Provider("London North Eastern Railway")
val waterloo = TrainStationOrAirport(
    "London Waterloo",
    "5598",
    trainCrs= "WAT"
)
val kingsCross = TrainStationOrAirport(
    "London Kings Cross",
    "6121",
    trainCrs="KGX"
)
val kingCrossTube = TrainStationOrAirport(
    "Kings Cross N Lt",
    "0600",
    trainCrs = "ZZZ"
)
val waterlooTube = TrainStationOrAirport(
    "Waterloo (Underground)",
    "0747",
    trainCrs = "ZWA"
)
val newcastle =
    TrainStationOrAirport("Newcastle", "7728", trainCrs= "NCL")


private val globalparser = UtcDateFormatter()

val matchingDataModelReturn = DetailedTrainJourney(
    journeyId = "20000",
    couponId = "30101",
    departureStation = portsmouth,
    departureDateTime = globalparser.parse("2019-10-16T17:19:00+0100"),
    ticketType = superOffPeak,
    arrivalStation = edinburgh,
    segments = listOf(
        TrainSegment(
            legDeparture = globalparser.parse("2019-10-16T17:19:00+0100"),
            legArrival = globalparser.parse("2019-10-16T18:59:00+0100"),
            travelTime = 100,
            connectionTime = 0,
            arrivalLocation = waterloo,
            provider = southWesternRailway,
            trainClassInformation = TrainClassInformation("Train has First & Standard class seating"),
            travelType = ConnectionOrJourneyType.TRAIN,
            departureLocation = portsmouth
        ),
        TrainSegment(
            legDeparture = globalparser.parse("2019-10-16T18:59:00+0100"),
            legArrival = globalparser.parse("2019-10-16T20:00:00+0100"),
            travelTime = 61,
            connectionTime = 0,
            arrivalLocation = kingCrossTube,
            provider = underground,
            trainClassInformation = TrainClassInformation("Train seating class not known"),
            travelType = ConnectionOrJourneyType.UNDERGROUND,
            departureLocation = waterlooTube
        ),
        TrainSegment(
            legDeparture = globalparser.parse("2019-10-16T20:00:00+0100"),
            legArrival = globalparser.parse("2019-10-16T22:54:00+0100"),
            travelTime = 174,
            connectionTime = 0,
            arrivalLocation = newcastle,
            provider = northEasternRailway,
            trainClassInformation = TrainClassInformation("Train has First & Standard class seating"),
            travelType = ConnectionOrJourneyType.TRAIN,
            departureLocation = kingsCross
        ),
        TrainSegment(
            legDeparture = globalparser.parse("2019-10-17T06:22:00+0100"),
            legArrival = globalparser.parse("2019-10-17T08:07:00+0100"),
            travelTime = 105,
            connectionTime = 0,
            arrivalLocation = edinburgh,
            provider = londonEastern,
            trainClassInformation = TrainClassInformation("Train has First & Standard class seating"),
            travelType = ConnectionOrJourneyType.TRAIN,
            departureLocation = newcastle
        )
    )

)

val matchingDataModelOutbound = DetailedTrainJourney(
    departureStation = edinburgh,
    departureDateTime = globalparser.parse("2019-10-07T16:30:00+0100"),
    ticketType = superOffPeak,
    arrivalStation = portsmouth,
    journeyId = "10002",
    segments = listOf(
        TrainSegment(
            legDeparture = globalparser.parse("2019-10-07T16:30:00+0100"),
            legArrival = globalparser.parse("2019-10-07T20:53:00+0100"),
            travelTime = 263,
            connectionTime = 0,
            arrivalLocation = kingsCross,
            provider = londonEastern,
            trainClassInformation = TrainClassInformation("Train has First & Standard class seating"),
            travelType = ConnectionOrJourneyType.TRAIN,
            departureLocation = edinburgh
        ),
        TrainSegment(
            legDeparture = globalparser.parse("2019-10-07T20:53:00+0100"),
            legArrival = globalparser.parse("2019-10-07T22:00:00+0100"),
            travelTime = 67,
            connectionTime = 0,
            arrivalLocation = waterlooTube,
            provider = underground,
            trainClassInformation = TrainClassInformation("Train seating class not known"),
            travelType = ConnectionOrJourneyType.UNDERGROUND,
            departureLocation = kingCrossTube
        ),
        TrainSegment(
            legDeparture = globalparser.parse("2019-10-07T22:00:00+0100"),
            legArrival = globalparser.parse("2019-10-07T23:31:00+0100"),
            travelTime = 91,
            connectionTime = 0,
            arrivalLocation = portsmouth,
            provider = southWesternRailway,
            trainClassInformation = TrainClassInformation("Train has First & Standard class seating"),
            travelType = ConnectionOrJourneyType.TRAIN,
            departureLocation = waterloo
        )
    ),
    couponId = "30102"
)

val booking = TrainBooking(
    address = Address("", "", ""),
    deliveryOption = DeliveryOption.COLLECT_AT_STATION,
    ticketCollectionReference = "3TK45KFG",
    railcard = null,
    passengerType = PassengerType.ADULT,
    bookingId = "200111619",
    bookingTravelType = TravelTypes.TRAIN,
    journeys = listOf(
        matchingDataModelOutbound,
        matchingDataModelReturn
    ),
    fareType = "Travel is allowed via any permitted route.",
    fareClass = "STANDARD",
    notes = Note("gfnfcgncdncdcnbc"),
    team = "f89103c5-fc65-4ece-9d74-3d727f272d88"
)

val bookingWithRailcard = TrainBooking(
    address = Address("AnotherAddress", "9 Belford Road", "Edinburgh, EH4 3DE"),
    deliveryOption = DeliveryOption.POST,
    ticketCollectionReference = "3TK45KFG",
    railcard = Railcard("16-25 Railcard"),
    passengerType = PassengerType.ADULT,
    forTheAttentionOf = "Test name",
    bookingId = "123123123",
    bookingTravelType = TravelTypes.TRAIN,
    journeys = listOf(
        matchingDataModelOutbound,
        matchingDataModelReturn
    ),
    fareType = "Travel is allowed via any permitted route.",
    fareClass = "STANDARD",
    notes = Note("gfnfcgncdncdcnbc"),
    team = "f89103c5-fc65-4ece-9d74-3d727f272d88"
)

val bookingWithRailcardAndSpecialDeliveryOption = TrainBooking(
    address = Address("AnotherAddress", "9 Belford Road", "Edinburgh, EH4 3DE"),
    deliveryOption = DeliveryOption.SPECIAL_DELIVERY,
    ticketCollectionReference = "SHOULDNOTSEETHIS",
    railcard = Railcard("16-25 Railcard"),
    forTheAttentionOf = "Test name",
    passengerType = PassengerType.ADULT,
    bookingId = "4234123",
    bookingTravelType = TravelTypes.TRAIN,
    journeys = listOf(
        matchingDataModelOutbound,
        matchingDataModelReturn
    ),
    fareType = "Travel is allowed via any permitted route.",
    fareClass = "STANDARD",
    notes = Note("gfnfcgncdncdcnbc"),
    team = "f89103c5-fc65-4ece-9d74-3d727f272d88"
)
val bookingWithRailcardAndFirstClassPostDeliveryOption = TrainBooking(
    address = Address("AnotherAddress", "9 Belford Road", "Edinburgh, EH4 3DE"),
    deliveryOption = DeliveryOption.FIRST_CLASS_POST,
    ticketCollectionReference = "SHOULDNOTSEETHIS",
    forTheAttentionOf = "Test name",
    railcard = null,
    passengerType = PassengerType.ADULT,
    bookingId = "21casdasd123123",
    bookingTravelType = TravelTypes.TRAIN,
    journeys = listOf(
        matchingDataModelOutbound,
        matchingDataModelReturn
    ),
    fareType = "Travel is allowed via any permitted route.",
    fareClass = "STANDARD",
    notes = Note("gfnfcgncdncdcnbc"),
    team = "f89103c5-fc65-4ece-9d74-3d727f272d88"
)


val trainJson = """{
          "id": "200111619",
          "booker": {
            "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
            "firstName": "Mhairi",
            "lastName": "Duff",
            "emailAddress": "mhairi.duff@xdesign.com"
          },
          "payments": {
            "Account": {
              "chargedAmount": {
                "amount": "178.56",
                "currency": "GBP"
              }
            },
            "paymentCard": {
              "chargedAmount": {
                "amount": "178.56",
                "currency": "GBP"
              },
              "cToken": "ff02e686-aaee-4c69-8962-ae9804a39129"
            }
          },
          "createdDateTime": "2019-10-07T14:33:21.137Z",
          "confirmedDate": "2019-10-07T14:34:05.331Z",
          "serviceDate": "2019-10-07",
          "status": "CONFIRMED",
          "cancellable": true,
          "refundable": false,
          "exchangeable": {
            "value": false
          },
          "product": {
            "id": "1bf738e7-e588-4d45-80-e125728e8d441c",
            "travelType": "TRAIN",
            "createdDate": "2019-10-07T14:32:19.209Z",
            "serviceDate": "2019-10-07",
            "status": "CONFIRMING",
            "details": {
              "type": "TrainProductDetails",
              "minRate": {
                "supplier": {
                  "amount": "173.90",
                  "currency": "GBP"
                }
              },
              "minRatePerPerson": {
                "supplier": {
                  "amount": "173.9",
                  "currency": "GBP"
                }
              },
              "deliveryOptions": [],
              "journeys": [
                {
                  "id": "10002",
                  "offPeakAvailable": false,
                  "superOffPeakAvailable": false,
                  "legs": [
                    {
                      "id": "1",
                      "serviceProvider": {
                        "code": "GR",
                        "mode": "TRAIN",
                        "name": "London North Eastern Railway"
                      },
                      "travelTimeMinutes": 263,
                      "depart": {
                        "location": {
                          "code": "9328",
                          "name": "Edinburgh",
                          "crs": "EDB"
                        },
                        "dateTime": "2019-10-07T16:30:00+0100"
                      },
                      "arrive": {
                        "location": {
                          "code": "6121",
                          "name": "London Kings Cross",
                          "crs": "KGX"
                        },
                        "dateTime": "2019-10-07T20:53:00+0100"
                      },
                      "ultimateDestination": {
                        "code": "6121",
                        "name": "London Kings Cross",
                        "crs": "KGX"
                      },
                      "seatingClass": "Train has First & Standard class seating"
                    },
                    {
                      "id": "2",
                      "serviceProvider": {
                        "code": "UNDERGROUND",
                        "mode": "UNDERGROUND",
                        "name": "Underground"
                      },
                      "travelTimeMinutes": 67,
                      "depart": {
                        "location": {
                          "code": "0600",
                          "name": "Kings Cross N Lt",
                          "crs": "ZZZ"
                        },
                        "dateTime": "2019-10-07T20:53:00+0100"
                      },
                      "arrive": {
                        "location": {
                          "code": "0747",
                          "name": "Waterloo (Underground)",
                          "crs": "ZWA"
                        },
                        "dateTime": "2019-10-07T22:00:00+0100"
                      },
                      "ultimateDestination": {},
                      "seatingClass": "Train seating class not known"
                    },
                    {
                      "id": "3",
                      "serviceProvider": {
                        "code": "SW",
                        "mode": "TRAIN",
                        "name": "South Western Railway"
                      },
                      "travelTimeMinutes": 91,
                      "depart": {
                        "location": {
                          "code": "5598",
                          "name": "London Waterloo",
                          "crs": "WAT"
                        },
                        "dateTime": "2019-10-07T22:00:00+0100"
                      },
                      "arrive": {
                        "location": {
                          "code": "5537",
                          "name": "Portsmouth & Southsea",
                          "crs": "PMS"
                        },
                        "dateTime": "2019-10-07T23:31:00+0100"
                      },
                      "ultimateDestination": {
                        "code": "5540",
                        "name": "Portsmouth Harbour",
                        "crs": "PMH"
                      },
                      "seatingClass": "Train has First & Standard class seating"
                    }
                  ],
                  "ticketType": {
                    "code": "SSR",
                    "name": "Super Off-Peak Return"
                  },
                  "direction": "OUTBOUND",
                  "refundableUntil": "2019-12-04",
                  "couponId": "30102"
                },
                {
                  "id": "20000",
                  "offPeakAvailable": false,
                  "superOffPeakAvailable": false,
                  "legs": [
                    {
                      "id": "1",
                      "serviceProvider": {
                        "code": "SW",
                        "mode": "TRAIN",
                        "name": "South Western Railway"
                      },
                      "travelTimeMinutes": 100,
                      "depart": {
                        "location": {
                          "code": "5537",
                          "name": "Portsmouth & Southsea",
                          "crs": "PMS"
                        },
                        "dateTime": "2019-10-16T17:19:00+0100"
                      },
                      "arrive": {
                        "location": {
                          "code": "5598",
                          "name": "London Waterloo",
                          "crs": "WAT"
                        },
                        "dateTime": "2019-10-16T18:59:00+0100"
                      },
                      "ultimateDestination": {
                        "code": "5598",
                        "name": "London Waterloo",
                        "crs": "WAT"
                      },
                      "seatingClass": "Train has First & Standard class seating"
                    },
                    {
                      "id": "2",
                      "serviceProvider": {
                        "code": "UNDERGROUND",
                        "mode": "UNDERGROUND",
                        "name": "Underground"
                      },
                      "travelTimeMinutes": 61,
                      "depart": {
                        "location": {
                          "code": "0747",
                          "name": "Waterloo (Underground)",
                          "crs": "ZWA"
                        },
                        "dateTime": "2019-10-16T18:59:00+0100"
                      },
                      "arrive": {
                        "location": {
                          "code": "0600",
                          "name": "Kings Cross N Lt",
                          "crs": "ZZZ"
                        },
                        "dateTime": "2019-10-16T20:00:00+0100"
                      },
                      "ultimateDestination": {},
                      "seatingClass": "Train seating class not known"
                    },
                    {
                      "id": "3",
                      "serviceProvider": {
                        "code": "GR",
                        "mode": "TRAIN",
                        "name": "London North Eastern Railway"
                      },
                      "travelTimeMinutes": 174,
                      "depart": {
                        "location": {
                          "code": "6121",
                          "name": "London Kings Cross",
                          "crs": "KGX"
                        },
                        "dateTime": "2019-10-16T20:00:00+0100"
                      },
                      "arrive": {
                        "location": {
                          "code": "7728",
                          "name": "Newcastle",
                          "crs": "NCL"
                        },
                        "dateTime": "2019-10-16T22:54:00+0100"
                      },
                      "ultimateDestination": {
                        "code": "7640",
                        "name": "Sunderland",
                        "crs": "SUN"
                      },
                      "seatingClass": "Train has First & Standard class seating"
                    },
                    {
                      "id": "4",
                      "serviceProvider": {
                        "code": "GR",
                        "mode": "TRAIN",
                        "name": "London North Eastern Railway"
                      },
                      "travelTimeMinutes": 105,
                      "depart": {
                        "location": {
                          "code": "7728",
                          "name": "Newcastle",
                          "crs": "NCL"
                        },
                        "dateTime": "2019-10-17T06:22:00+0100"
                      },
                      "arrive": {
                        "location": {
                          "code": "9328",
                          "name": "Edinburgh",
                          "crs": "EDB"
                        },
                        "dateTime": "2019-10-17T08:07:00+0100"
                      },
                      "ultimateDestination": {
                        "code": "9328",
                        "name": "Edinburgh",
                        "crs": "EDB"
                      },
                      "seatingClass": "Train has First & Standard class seating"
                    }
                  ],
                  "ticketType": {
                    "code": "SSR",
                    "name": "Super Off-Peak Return"
                  },
                  "direction": "RETURN",
                  "refundableUntil": "2019-12-04",
                  "couponId": "30101"
                }
              ],
              "holdable": false,
              "legs": [],
              "fees": []
            },
            "formOfPayment": {
              "type": "CARD",
              "id": "74574816-21e3-4c4f-b4d5-5639e86dcfe5",
              "associatedCostType": "supplier",
              "cardType": "VISA",
              "startDate": "02/12",
              "expiryDate": "05/24",
              "panHint": "5559",
              "alias": "Test",
              "validated": false,
              "corporate": false,
              "cardSafeVerificationStatus": "NOT_STARTED",
              "cToken": "ff02e686-aaee-4c69-8962-ae9804a39129"
            },
            "bookingDetails": {
              "type": "TrainProductBookingDetails",
              "billbackOverrides": [],
              "delivery": {
                "option": "COLLECT_AT_STATION",
                "address": []
              }
            },
            "subProducts": [
              {
                "id": "0",
                "reference": "R000003DB7",
                "supplierReference": "R000003DB7",
                "sfsReference": "T131944",
                "sfsProductId": "OPRa5f94b8e-ef0a-40ee-bd18-cdce45ea2bbe_6a666d76-6b6e-4590-a1fa-29440e4f80d1",
                "channel": "OPR",
                "details": {
                  "type": "TrainSubProductDetails",
                  "fees": [],
                  "estimatedServiceCost": {
                    "billing": {
                      "amount": "173.90",
                      "currency": "GBP"
                    },
                    "supplier": {
                      "amount": "173.90",
                      "currency": "GBP"
                    },
                    "preferred": {
                      "amount": "173.90",
                      "currency": "GBP"
                    }
                  },
                  "totalEstimatedCost": {
                    "billing": {
                      "amount": "176.06",
                      "currency": "GBP"
                    },
                    "supplier": {
                      "amount": "176.06",
                      "currency": "GBP"
                    },
                    "preferred": {
                      "amount": "176.06",
                      "currency": "GBP"
                    }
                  },
                  "rules": [],
                  "surcharges": [],
                  "passengerType": "ADULT",
                  "ctrReference": "3TK45KFG",
                  "fares": [
                    {
                      "id": "30076",
                      "ticketRestriction": "1K",
                      "ticketType": {
                        "code": "SSR",
                        "name": "Super Off-Peak Return"
                      },
                      "fareRoute": {
                        "code": "00000",
                        "name": "Travel is allowed via any permitted route."
                      },
                      "fareClass": "STANDARD",
                      "journeyType": "RETURN",
                      "crossLondon": true,
                      "cancellable": true,
                      "origin": {
                        "code": "9328",
                        "name": "Edinburgh"
                      },
                      "destination": {
                        "code": "0440",
                        "name": "Portsmouth & Southsea or Harbour"
                      },
                      "fareConditionsId": "07102019SSR1K"
                    }
                  ],
                  "requirements": [],
                  "features": [],
                  "information": [],
                  "seatReservations": [],
                  "sentLoyaltySchemes": []
                },
                "traveller": {
                  "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
                  "userId": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
                  "organisationId": "f89103c5-fc65-4ece-9d74-3d727f272d88",
                  "organisationName": "xDesign",
                  "firstName": "Mhairi",
                  "lastName": "Duff",
                  "emailAddress": "mhairi.duff@xdesign.com",
                  "hasApisDetails": false,
                  "registered": true,
                  "guest": false,
                  "passengerAssistanceConfigured": false
                },
                "bookingDetails": {
                  "type": "TrainSubProductBookingDetails",
                  "requiredInformation": "[]",
                  "flightAdditions": [],
                  "requirements": [],
                  "passengerAssistanceDeclined": false
                },
                "policyComplianceState": "COMPLIANT",
                "couponId": "30102",
                "ticketNumbers": [],
                "notes": [
                  {
                    "value": "gfnfcgncdncdcnbc",
                    "user": {
                      "id": "61d7b1b0-8156-43ea-a0c6-d4d4dba1fe11",
                      "firstName": "Mhairi",
                      "lastName": "Duff",
                      "emailAddress": "mhairi.duff@xdesign.com"
                    },
                    "creationDateTime": "2019-10-07T14:33:14.144Z"
                  }
                ]
              }
            ],
            "searchId": "OPRa5f94b8e-ef0a-40ee-bd18-cdce45ea2bbe"
          },
          "paidWithVouchers": false
        }"""
