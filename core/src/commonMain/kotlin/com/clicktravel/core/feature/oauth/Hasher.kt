package com.clicktravel.core.feature.oauth

interface Hasher {
    fun hash(hash: String): String
}

