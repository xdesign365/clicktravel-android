package com.clicktravel.core.feature.oauth

interface Encoder {
    fun base64Encode(stringToEncode : String) : String
    fun base64Encode(bytesToEncode: ByteArray) : String
    fun base64Decode(bytesToDecode : ByteArray) : ByteArray
}