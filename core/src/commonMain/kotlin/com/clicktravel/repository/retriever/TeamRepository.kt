package com.clicktravel.repository.retriever

import com.clicktravel.core.model.team.Team

interface TeamRepository {

    suspend fun getTeams(): List<Team>

    suspend fun storeTeams(storeData: List<Team>)

    suspend fun clearData()
}