package com.clicktravel.repository.retriever

interface NoArgumentRetriever<ReturnType> {
    suspend fun execute(): ReturnType
}
