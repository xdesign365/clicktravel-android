package com.clicktravel.repository.retriever

import com.clicktravel.core.model.UserDetails

interface UpdateCurrentUserProfile {
    suspend fun updateCurrentUser(userDetails: UserDetails)
}