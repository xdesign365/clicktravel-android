package com.clicktravel.repository.retriever

interface PaymentInstructionsRetriever{

    suspend fun resendPaymentInstructions(
        teamId: String,
        bookingId: String,
        travellerEmail: String,
        hotelEmail: String?
    )

}