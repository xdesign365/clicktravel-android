package com.clicktravel.repository.retriever

interface NumberOfTimesAppOpenedRepo {

    suspend fun getNumber(): Int

    suspend fun incrementNumber()

}