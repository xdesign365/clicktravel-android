package com.clicktravel.repository.retriever

import com.clicktravel.repository.callback.GenericCallback


interface ArgumentCallbackRetriever<OptionsType, ReturnType> {
    fun execute(request: OptionsType, callback: GenericCallback<ReturnType>)
}

