package com.clicktravel.repository.retriever

/**
 * The idea with this class is to have a cached value, but one that can go stale at which point
 * it will be refreshed when you try to retrieve the value.
 *
 */
interface StalableMemoryRepository<T> {
    suspend fun forceRefresh(): T
    suspend fun getValue(): T
}