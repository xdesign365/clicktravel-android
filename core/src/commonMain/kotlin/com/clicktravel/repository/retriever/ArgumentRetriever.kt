package com.clicktravel.repository.retriever

interface ArgumentRetriever<RequestType, ReturnType> {
    suspend fun execute(request : RequestType) : ReturnType
}