package com.clicktravel.repository.retriever

import com.clicktravel.core.model.auth.OauthModel

interface RefreshAuthRetriever {

    suspend fun refresh(refreshToken: String): OauthModel

}