package com.clicktravel.repository.retriever

import com.clicktravel.repository.callback.GenericCallback

interface NoArgumentCallbackRetriever<ReturnType> {
    fun execute(callback: GenericCallback<ReturnType>)
}