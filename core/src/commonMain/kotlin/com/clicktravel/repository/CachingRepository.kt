package com.clicktravel.repository

import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.repository.retriever.NoArgumentRetriever

interface CachingDataRepository<ReturnType> {
    suspend fun synchronise()
}

class CachingDataRepositoryImpl<ReturnType>(
    private val retriever: NoArgumentRetriever<ReturnType>,
    private val store: WriteOnlyRepository<ReturnType>
) : CachingDataRepository<ReturnType> {

    override suspend fun synchronise() =
        store.storeData(retriever.execute())

}