package com.clicktravel.repository.callback

class SimpleCallback<T>(private val success: (T) -> Unit, private val failure: (Throwable) -> Unit) :
    GenericCallback<T> {
    override fun onSuccess(value: T) = success(value)

    override fun onError(exception: Throwable) = failure(exception)
}