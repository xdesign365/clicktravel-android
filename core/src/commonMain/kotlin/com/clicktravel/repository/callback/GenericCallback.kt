package com.clicktravel.repository.callback

interface GenericCallback<T> {
    fun onSuccess(value: T)
    fun onError(exception: Throwable)
}

