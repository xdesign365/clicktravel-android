package com.clicktravel.core.usecases

import com.clicktravel.core.model.team.Team
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.repository.retriever.NoArgumentRetriever
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class PostUserLoginUseCaseImplTest {

    private val fakeTeam = Team("a", "b", "c", "d")
    private val fakeTeamA = Team("e", "f", "g", "h")

    private val retriever = mockk<NoArgumentRetriever<List<Team>>>()
    private val storedTeam = mockk<NoArgumentRetriever<Team?>>()
    private val writeTeam = mockk<WriteOnlyRepository<Team>>()

    @Test
    fun `test that if we only have one team it will get stored for next time and saved as the selected team`() =
        runBlocking {

            // Given & When
            coEvery { storedTeam.execute() } coAnswers { null }
            coEvery { retriever.execute() } coAnswers { listOf(fakeTeam) }
            coEvery { writeTeam.storeData(fakeTeam) } coAnswers {}

            val postUser = PostUserLoginUseCaseImpl(retriever, storedTeam, writeTeam)

            // Then
            assertFalse(postUser.teamSelectionRequired())

            coVerify {
                writeTeam.storeData(fakeTeam)
            }
        }

    @Test
    fun `test that if we have multiple teams and no selected team then we get true from team selection required`() =
        runBlocking {
            // Given & When
            coEvery { storedTeam.execute() } coAnswers { null }
            coEvery { retriever.execute() } coAnswers { listOf(fakeTeam, fakeTeamA) }

            val postUser = PostUserLoginUseCaseImpl(retriever, storedTeam, writeTeam)

            // Then
            assertTrue(postUser.teamSelectionRequired())
        }

    @Test
    fun `test that if we have stored a team previously then we dont require team selection`() =
        runBlocking {
            // Given & When
            coEvery { storedTeam.execute() } coAnswers { fakeTeam }

            val postUser = PostUserLoginUseCaseImpl(retriever, storedTeam, writeTeam)

            // Then
            assertFalse(postUser.teamSelectionRequired())
        }

    @Test
    fun `select team updates the selected team in the repository`() =
        runBlocking {
            val team = Team(
                id = "id",
                name = "name",
                ownerId = "ownerId",
                shortId = "shortId"
            )
            // Given & When
            coEvery { writeTeam.storeData(team) } coAnswers { Unit }

            val postUser = PostUserLoginUseCaseImpl(retriever, storedTeam, writeTeam).apply {
                selectTeam(team)
            }

            // Then
            coVerify { writeTeam.storeData(team) }
        }


}