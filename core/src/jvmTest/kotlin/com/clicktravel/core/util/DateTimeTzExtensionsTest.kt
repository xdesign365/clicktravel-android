package com.clicktravel.core.util

import com.soywiz.klock.DateTimeTz
import org.junit.Ignore
import org.junit.Test
import kotlin.test.assertEquals

class DateTimeTzExtensionsTest {

    @Test
    fun `less than 24 hours same day returns 0`() {
        val currentDateTime = DateTimeTz.fromUnixLocal(1580392800000) // 30 January 2020 14:00
        val checkInDateTime = DateTimeTz.fromUnixLocal(1580418000000) // 30 January 2020 21:00

        val expected = 0
        val actual = currentDateTime.daysBetween(checkInDateTime)

        assertEquals(expected, actual)
    }

    @Test
    fun `less than 24 hours same day returns 0 reversed`() {
        val currentDateTime = DateTimeTz.fromUnixLocal(1580392800000) // 30 January 2020 14:00
        val checkInDateTime = DateTimeTz.fromUnixLocal(1580418000000) // 30 January 2020 21:00

        val expected = 0
        val actual = checkInDateTime.daysBetween(currentDateTime)

        assertEquals(expected, actual)
    }

    @Test
    fun `more than 24 hours between but next day returns 1`() {
        val currentDateTime = DateTimeTz.fromUnixLocal(1580418000000) // 30 January 2020 21:00
        val checkInDateTime = DateTimeTz.fromUnixLocal(1580511600000) // 31 January 2020 23:00

        val expected = 1
        val actual = currentDateTime.daysBetween(checkInDateTime)

        assertEquals(expected, actual)
    }

    @Test
    fun `less than 24 hours between but different days returns 1 reversed`() {
        val currentDateTime = DateTimeTz.fromUnixLocal(1580418000000) // 30 January 2020 21:00
        val checkInDateTime = DateTimeTz.fromUnixLocal(1580461200000) // 31 January 2020 09:00

        val expected = 1
        val actual = checkInDateTime.daysBetween(currentDateTime)

        assertEquals(expected, actual)
    }

    @Test
    fun `more than 24 hours between but next day returns 1 reversed`() {
        val currentDateTime = DateTimeTz.fromUnixLocal(1580418000000) // 30 January 2020 21:00
        val checkInDateTime = DateTimeTz.fromUnixLocal(1580511600000) // 31 January 2020 23:00

        val expected = 1
        val actual = checkInDateTime.daysBetween(currentDateTime)

        assertEquals(expected, actual)
    }

    @Test
    @Ignore
    fun `works with daylight saving time`() {
        val currentDateTime = DateTimeTz.fromUnixLocal(1581339600000) // 10 February 2020 13:00
        val checkInDateTime = DateTimeTz.fromUnixLocal(1589328000000) // 13 May 2020 00:00

        val expected = 93 // todo could be failing due to clocks changing!
        val actual = currentDateTime.daysBetween(checkInDateTime)

        assertEquals(expected, actual)
    }

}

