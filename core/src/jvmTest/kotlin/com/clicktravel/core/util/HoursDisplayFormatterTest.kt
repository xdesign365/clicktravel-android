package com.clicktravel.core.util

import com.soywiz.klock.DateTime
import org.junit.Test
import kotlin.test.assertEquals

class HoursDisplayFormatterTest {

    @Test
    fun `test that we can parse a day to hours`() {
        with(HoursDisplayFormatter()) {
            assertEquals(
                "07:02",
                format(DateTime.invoke(2019, 11, 22, 7, 2, 0, 0).local)
            )

            assertEquals(
                "16:30",
                format(DateTime.invoke(2019, 11, 22, 16, 30, 0, 0).local)
            )
        }
    }
}