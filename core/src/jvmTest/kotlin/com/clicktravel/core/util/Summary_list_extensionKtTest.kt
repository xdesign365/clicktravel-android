package com.clicktravel.core.util

import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.testutils.booking
import com.clicktravel.core.ui.models.DateHeaderListItem
import com.clicktravel.core.ui.models.UniqueJourneyListItem
import org.junit.Test

import org.junit.Assert.*

class Summary_list_extensionKtTest {
    private val globalparser = UtcDateFormatter()

    private val sampleList = listOf(
        DateHeaderListItem(
            "7 October 2019",
            globalparser.parse("2019-10-07T16:30:00+0100")
        ),
        UniqueJourneyListItem(
            bookingId = booking.bookingId,
            journeyNumber = 0,
            travelType = TravelTypes.TRAIN,
            title = "16:30 Edinburgh (EDB)",
            subtitle = "23:31 Portsmouth & Southsea (PMS)",
            ticketNumber = "Ticket Collection Reference",
            railcard = null,
            bookingReferenceTitle = "3TK45KFG",
            isReturnTicket = true,
            time = "7hrs 1mins",
            dateTimeTz = booking.journeys.first().departureDateTime
        ),
        DateHeaderListItem(
            "16 October 2019",
            globalparser.parse("2019-10-16T17:19:00+0100")
        ),
        UniqueJourneyListItem(
            booking.bookingId, 1,
            TravelTypes.TRAIN,
            "17:19 Portsmouth & Southsea (PMS)",
            "08:07 Edinburgh (EDB)",
            ticketNumber = "Ticket Collection Reference",
            railcard = null,
            bookingReferenceTitle = "3TK45KFG",
            isReturnTicket = true,
            time = "14hrs 48mins",
            dateTimeTz = booking.journeys.last().departureDateTime
        )
    )

    @Test
    fun `test that we can get the correct date from a list of summary items`() {
        sampleList.indexOfCurrentDate(globalparser.parse("2019-10-07T19:30:00+0100"))
            .let {
                assertEquals(0, it)
            }
    }

    @Test
    fun `test that we can get the correct date from a summary items for a different date`() {
        sampleList.indexOfCurrentDate(globalparser.parse("2019-10-16T19:30:00+0100"))
            .let {
                assertEquals(2, it)
            }
    }

    @Test
    fun `test that we can get the correct date from a summary items for a date thats tomorrow`() {
        sampleList.indexOfCurrentDate(globalparser.parse("2019-10-14T19:30:00+0100"))
            .let {
                assertEquals(2, it)
            }
    }

    @Test
    fun `test that we can get the correct date from a summary items for a date when there is no value`() {
        sampleList.indexOfCurrentDate(globalparser.parse("2019-10-17T19:30:00+0100"))
            .let {
                assertEquals(3, it)
            }
    }
}