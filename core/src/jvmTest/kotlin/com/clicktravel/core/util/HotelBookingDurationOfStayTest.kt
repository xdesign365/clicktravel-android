package com.clicktravel.core.util

import com.clicktravel.core.model.*
import com.soywiz.klock.DateTimeTz
import org.junit.Test
import kotlin.test.assertEquals

class HotelBookingDurationOfStayTest {

    @Test
    fun `typical duration of hotel stay is correctly calculated`() {

        val booking = HotelBooking(
            bookingId = "bookingId",
            bookingTravelType = TravelTypes.HOTEL,
            cancellationPolicy = "cancellationPolicy",
            numberOfAdults = 3,
            numberOfChildren = 2,
            roomType = "Double",
            mealIncluded = "Breakfast included",
            specialRequest = "Dummy special request",
            checkinDate = DateTimeTz.fromUnixLocal(1579691913000), // Wed, 22 Jan 2020 11:18:33
            checkoutDate = DateTimeTz.fromUnixLocal(1580123913000), // Mon, 27 Jan 2020 11:18:33
            hotelName = "hotelName",
            hotelEmail = "hotelEmail",
            hotelAddress = "hotelAddress",
            hotelPhoneNumber = "hotelPhoneNumber",
            notes = null,
            fields = emptyList(),
            team = "team",
            billingType = BillingType.ROOM_ONLY,
            isPremierInn = false,
            travellerEmail = "travellerEmail",
            billbackElements = HotelBillbackElements(
                allCosts = false,
                breakfast = false,
                roomRate = false,
                parking = false,
                wifi = false,
                mealSupplement = MealSupplementBillbackElement(
                    enabled = false,
                    allowance = null,
                    currency = null
                )
            ),
            supportedBillbackOptions = HotelSupportedBillbackOptions(
                breakfast = false,
                roomRate = false,
                parking = false,
                wifi = false,
                mealSupplement = false
            )
        )

        val expected = 5
        val actual = booking.durationOfStayInDays()

        assertEquals(expected, actual)

    }

    //todo fix this should be 1 day as overlapping
//    @Test
//    fun `duration is correctly calculated when booking is 2 hours away but on the next day`() {
//
//        val booking = HotelBooking(
//            bookingId = "bookingId",
//            bookingTravelType = TravelTypes.HOTEL,
//            cancellationPolicy = "cancellationPolicy",
//            numberOfAdults = 3,
//            numberOfChildren = 2,
//            roomType = "Double",
//            mealIncluded = "Breakfast included",
//            specialRequest = "Dummy special request",
//            checkinDate = DateTimeTz.fromUnixLocal(1580166000000), // Monday, 27 January 2020 23:00:00
//            checkoutDate = DateTimeTz.fromUnixLocal(1580173200000), // Tuesday, 28 January 2020 01:00:00
//            hotelName = "hotelName",
//            hotelEmail = "hotelEmail",
//            hotelAddress = "hotelAddress",
//            hotelPhoneNumber = "hotelPhoneNumber",
//            notes = null,
//            fields = emptyList(),
//            team = "team",
//            billingType = BillingType.ROOM_ONLY,
//            isPremierInn = false,
//            travellerEmail = "travellerEmail",
//            billbackElements = HotelBillbackElements(
//                allCosts = false,
//                breakfast = false,
//                roomRate = false,
//                parking = false,
//                wifi = false,
//                mealSupplement = MealSupplementBillbackElement(
//                    enabled = false,
//                    allowance = null,
//                    currency = null
//                )
//            ),
//            supportedBillbackOptions = HotelSupportedBillbackOptions(
//                breakfast = false,
//                roomRate = false,
//                parking = false,
//                wifi = false,
//                mealSupplement = false
//            )
//        )
//
//        val expected = 1
//        val actual = booking.durationOfStayInDays()
//
//        assertEquals(expected, actual)
//
//    }

}