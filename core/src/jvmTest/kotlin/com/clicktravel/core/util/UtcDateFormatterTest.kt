package com.clicktravel.core.util

import com.soywiz.klock.DateTime
import org.junit.Test
import kotlin.test.assertEquals

class UtcDateFormatterTest {

    @Test
    fun `utc date is able to be parsed successfully by the formatter`() {
        with(UtcDateFormatter()) {
            assertEquals(
                DateTime.invoke(2019, 11, 22, 7, 2, 0,0),
                parse("2019-11-22T07:02:00+0000").local
            )
        }
    }
}