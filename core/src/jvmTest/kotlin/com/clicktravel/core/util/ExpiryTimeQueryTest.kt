package com.clicktravel.core.util

import com.soywiz.klock.DateTime
import com.soywiz.klock.hours
import org.junit.Test
import kotlin.math.roundToLong
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ExpiryTimeQueryTest {

    @Test
    fun `expiry time query test`() {
        with(ExpiryTimeQueryImpl()) {
            assertTrue(hasTimeExpired(DateTime.nowUnixLong() - 1))
            assertFalse(hasTimeExpired(DateTime.nowUnixLong() + 5))
        }
    }

    @Test
    fun `test that timeNowPlusExpiryTime returns correct value`() {

        val mockDateTime = DateTime.now()

        with(ExpiryTimeQueryImpl { mockDateTime }) {
            assertEquals(
                DateTime.fromUnix(
                    timeNowPlusExpiryTime(
                        expiryTimeInSeconds = 4.hours.seconds.roundToLong()
                    )
                ),
                mockDateTime.plus(4.hours)
            )
        }
    }
}