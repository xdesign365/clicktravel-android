package com.clicktravel.core.util

import com.soywiz.klock.DateTime
import org.junit.Test
import kotlin.test.assertEquals


class DisplayableDateFormatterTest {

    @Test
    fun `test that displayable date formatter formats correctly`() {
        with(
            YearDisplayFormatter().format(DateTime.invoke(2019, 11, 22, 7, 2, 0, 0).local)
        ) {
            assertEquals("22/11/19", this)
        }
    }
}