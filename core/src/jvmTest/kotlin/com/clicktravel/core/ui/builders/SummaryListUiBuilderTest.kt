package com.clicktravel.core.ui.builders

import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.UniqueJourney
import com.clicktravel.core.model.UniqueJourneyList
import com.clicktravel.core.testutils.booking
import com.clicktravel.core.testutils.ferryBooking
import com.clicktravel.core.testutils.flightBookingMatchingObject
import com.clicktravel.core.ui.models.DateHeaderListItem
import com.clicktravel.core.ui.models.UniqueJourneyListItem
import com.clicktravel.core.util.UtcDateFormatter
import com.soywiz.klock.DateTime
import org.junit.Test
import kotlin.test.assertEquals

class SummaryListUiBuilderTest {

    private val globalparser = UtcDateFormatter()

    @Test
    fun `test that we can convert a list of unique journeys to a summary list ui`() {
        val result = SummaryListUiBuilder()
            .build(
                UniqueJourneyList(ferryBooking.journeys.mapIndexed { index, _ ->
                    UniqueJourney(ferryBooking, index)
                }
                )
            )

        assertEquals(2, result.count())
        assertEquals(
            DateHeaderListItem(
                "27 November 2019",
                DateTime.parse("2019-11-27T16:35:00+0000")
            ), result[0]
        )
        assertEquals(
            UniqueJourneyListItem(
                bookingId = ferryBooking.bookingId,
                journeyNumber = 0,
                travelType = TravelTypes.TRAIN,
                title = "16:35 Ryde St Johns Road (RYR)",
                subtitle = "18:08 Southampton Central (SOU)",
                isReturnTicket = false,
                bookingReferenceTitle = null,
                railcard = null,
                ticketNumber = null,
                time = "1hrs 33mins",
                isEticket = true,
                dateTimeTz = ferryBooking.journeys.first().segments.first().legDeparture
            ), result[1]
        )
    }

    @Test
    fun `test that we can convert a list of unique journeys to a summary list ui different list`() {
        val result = SummaryListUiBuilder()
            .build(
                UniqueJourneyList(booking.journeys.mapIndexed { index, _ ->
                    UniqueJourney(booking, index)
                }
                )
            )

        assertEquals(4, result.count())
        assertEquals(
            DateHeaderListItem(
                "7 October 2019",
                globalparser.parse("2019-10-07T16:30:00+0100")
            ), result[0]
        )
        assertEquals(
            UniqueJourneyListItem(
                bookingId = booking.bookingId,
                journeyNumber = 0,
                travelType = TravelTypes.TRAIN,
                title = "16:30 Edinburgh (EDB)",
                subtitle = "23:31 Portsmouth & Southsea (PMS)",
                ticketNumber = "Ticket Collection Reference",
                railcard = null,
                bookingReferenceTitle = "3TK45KFG",
                isReturnTicket = true,
                time = "7hrs 1mins",
                dateTimeTz = booking.journeys.first().segments.first().legDeparture
            ), result[1]
        )
        assertEquals(
            DateHeaderListItem(
                "16 October 2019",
                globalparser.parse("2019-10-16T17:19:00+0100")
            ), result[2]
        )
        assertEquals(
            UniqueJourneyListItem(
                booking.bookingId, 1,
                TravelTypes.TRAIN,
                "17:19 Portsmouth & Southsea (PMS)",
                "08:07 Edinburgh (EDB)",
                ticketNumber = "Ticket Collection Reference",
                railcard = null,
                bookingReferenceTitle = "3TK45KFG",
                isReturnTicket = true,
                time = "14hrs 48mins",
                dateTimeTz = booking.journeys.last().segments.first().legDeparture
            ), result[3]
        )
    }


    @Test
    fun `test that we can convert a list of unique journeys to a summary list ui flight list`() {
        val result = SummaryListUiBuilder()
            .build(
                UniqueJourneyList(flightBookingMatchingObject.journeys.mapIndexed { index, _ ->
                    UniqueJourney(flightBookingMatchingObject, index)
                }
                )
            )

        assertEquals(4, result.count())
        assertEquals(
            DateHeaderListItem(
                "31 October 2019",
                globalparser.parse("2019-10-31T06:00:00+0000")
            ), result[0]
        )
        assertEquals(
            UniqueJourneyListItem(
                bookingId = flightBookingMatchingObject.bookingId,
                journeyNumber = 0,
                travelType = TravelTypes.FLIGHT,
                title = "06:00 Birmingham International Airport (BHX)",
                subtitle = "09:45 Charles de Gaulle Airport (CDG)",
                bookingReferenceTitle = "Booking Ref. 7f028287-74d1-47ad-a186-aa4e84ba5a96",
                ticketNumber = "Ticket no. TicketNumberForMhairiDuff",
                isReturnTicket = false,
                time = "2hrs 45mins",
                dateTimeTz = flightBookingMatchingObject.journeys.first().segments.first().legDeparture
            ), result[1]
        )
        assertEquals(
            DateHeaderListItem(
                "7 November 2019",
                globalparser.parse("2019-11-07T13:00:00+0100")
            ), result[2]
        )
        assertEquals(
            UniqueJourneyListItem(
                bookingId = flightBookingMatchingObject.bookingId,
                journeyNumber = 1,
                travelType = TravelTypes.FLIGHT,
                title = "13:00 Charles de Gaulle Airport (CDG)",
                subtitle = "13:15 Birmingham International Airport (BHX)",
                time = "1hrs 15mins",
                bookingReferenceTitle = "Booking Ref. 7f028287-74d1-47ad-a186-aa4e84ba5a96",
                ticketNumber = "Ticket no. TicketNumberForMhairiDuff",
                dateTimeTz = flightBookingMatchingObject.journeys.last().segments.first().legDeparture
            ), result[3]
        )
    }

    // todo add tests for hotel and travel card
}