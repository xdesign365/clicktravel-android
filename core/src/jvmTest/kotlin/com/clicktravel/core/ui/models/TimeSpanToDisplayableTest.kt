package com.clicktravel.core.ui.models

import com.soywiz.klock.TimeSpan
import org.junit.Test
import kotlin.test.assertEquals

class TimeSpanToDisplayableTest {

    val oneMinute = TimeSpan(60000.0)
    val oneHour = TimeSpan(oneMinute.milliseconds * 60)
    val oneDay = TimeSpan(oneHour.milliseconds * 24)

    @Test
    fun `test hours gets converted correctly`() {
        assertEquals(
            "1hrs 2mins",
            oneHour.plus(oneMinute).plus(oneMinute).toDisplayable()
        )
    }

    @Test
    fun `test days gets converted correctly`() {
        assertEquals(
            "1days 2hrs 1mins",
            oneDay.plus(oneHour).plus(oneHour).plus(oneMinute).toDisplayable()
        )
    }

    @Test
    fun `test mins gets converted correctly`() {
        assertEquals(
            "10mins",
            (oneMinute * 10).toDisplayable()
        )
    }


    @Test
    fun `test mins gets converted correctly one hour exactly`() {
        assertEquals(
            "1hrs 0mins",
            (oneMinute * 60).toDisplayable()
        )
    }

    @Test
    fun `test that one hour can be parsed correctly from long`() {
        assertEquals("1hrs 0mins", (60L).minutesToDisplayableTime())
    }

    @Test
    fun `test that one hour one min can be parsed correctly from long`() {
        assertEquals("1hrs 1mins", (61L).minutesToDisplayableTime())
    }

}