package com.clicktravel.core.ui.models

import com.clicktravel.core.model.BookingItem
import com.clicktravel.core.model.trains.ConnectionOrJourneyType
import com.clicktravel.core.testutils.*
import com.clicktravel.core.model.UniqueJourney
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

class TicketUiExtensionTest {

    // Return journey: Different arrival and departure stations
    // SuperOffPeakReturn tickets
    @Test
    fun `test that when the ticket title is Supper off peak return`() {
        with(UniqueJourney(booking as BookingItem, 0).toTicketUi().first { it.selectedTicket }) {
            assertEquals(Icon.TRAIN_ICON, topIcon)
            assertEquals(Icon.TRAIN_DESTINATION_ICON, bottomIcon)
            assertEquals("Super Off-Peak Return", ticketTitle)
            assertEquals("23:31 Portsmouth & Southsea (PMS)", arrivalTitle)
            assertEquals("07/10/19", ticketDate)
            assertEquals("16:30 ${edinburgh.name} (${edinburgh.trainCrs})", departureTitle)

            with(legs) {
                assertEquals(3, count())

                with((this[0] as JourneyLegUi)) {
                    assertNull(journeyDepartureTitle)
                    assertEquals(
                        "20:53 ${kingsCross.name} (${kingsCross.trainCrs})",
                        journeyArrivalTitle
                    )
                    assertEquals("4hrs 23mins", journeyTime)
                    assertEquals(londonEastern.name, providerName)
                    assertEquals(ConnectionOrJourneyType.TRAIN, type)
                    assertEquals("Train has First & Standard class seating", seatingClass)
                    assertEquals(Icon.GREY_ICON, bottomIcon)
                    assertEquals(Icon.NONE, topIcon)

                }

                with((this[1] as ConnectionLegUi)) {
                    assertEquals(
                        "Take the underground from Kings Cross N Lt to Waterloo (Underground)",
                        connectionText
                    )
                    assertEquals(ConnectionOrJourneyType.UNDERGROUND, type)
                }

                with((this[2] as JourneyLegUi)) {
                    assertEquals("22:00 ${waterloo.name} (${waterloo.trainCrs})", journeyDepartureTitle)
                    assertNull(journeyArrivalTitle)
                    assertEquals(southWesternRailway.name, providerName)
                    assertEquals(ConnectionOrJourneyType.TRAIN, type)
                    assertEquals("Train has First & Standard class seating", seatingClass)
                    assertEquals(Icon.NONE, bottomIcon)
                    assertEquals(Icon.GREY_ICON, topIcon)
                }
            }
        }
    }

    // Return journey: Different arrival and departure stations
    // SuperOffPeakReturn tickets
    @Test
    fun `test that when the ticket title return journey`() {
        with(UniqueJourney(booking as BookingItem, 1).toTicketUi().first { it.selectedTicket }) {
            assertEquals(Icon.TRAIN_ICON, topIcon)
            assertEquals(Icon.TRAIN_DESTINATION_ICON, bottomIcon)
            assertEquals("Super Off-Peak Return", ticketTitle)
            assertEquals("08:07 Edinburgh (EDB)", arrivalTitle)
            assertEquals("17:19 Portsmouth & Southsea (PMS)", departureTitle)
            assertEquals("16/10/19", ticketDate)

            with(legs) {
                assertEquals(5, count())

                with((this[0] as JourneyLegUi)) {
                    assertEquals(southWesternRailway.name, providerName)
                    assertEquals(ConnectionOrJourneyType.TRAIN, type)
                    assertEquals("Train has First & Standard class seating", seatingClass)
                    assertNull(journeyDepartureTitle)
                    assertEquals("18:59 ${waterloo.name} (${waterloo.trainCrs})", journeyArrivalTitle)

                    assertEquals(Icon.GREY_ICON, bottomIcon)
                    assertEquals(Icon.NONE, topIcon)
                }

                with((this[1] as ConnectionLegUi)) {
                    assertEquals(
                        "Take the underground from Waterloo (Underground) to Kings Cross N Lt",
                        connectionText
                    )

                    assertEquals(ConnectionOrJourneyType.UNDERGROUND, type)
                }

                with((this[2] as JourneyLegUi)) {
                    assertEquals(northEasternRailway.name, providerName)
                    assertEquals(ConnectionOrJourneyType.TRAIN, type)
                    assertEquals("Train has First & Standard class seating", seatingClass)
                    assertEquals(
                        "20:00 ${kingsCross.name} (${kingsCross.trainCrs})",
                        journeyDepartureTitle
                    )
                    assertEquals("22:54 ${newcastle.name} (${newcastle.trainCrs})", journeyArrivalTitle)

                    assertEquals(Icon.GREY_ICON, topIcon)
                    assertEquals(Icon.GREY_ICON, bottomIcon)
                }

                with((this[3] as ConnectionLegUi)) {
                    assertEquals(
                        "Connection\n7hrs 28mins",
                        connectionText
                    )

                    assertEquals(ConnectionOrJourneyType.WAIT, type)
                }

                with((this[4] as JourneyLegUi)) {
                    assertEquals(londonEastern.name, providerName)
                    assertEquals(ConnectionOrJourneyType.TRAIN, type)
                    assertEquals("Train has First & Standard class seating", seatingClass)
                    assertEquals(
                        "06:22 ${newcastle.name} (${newcastle.trainCrs})",
                        journeyDepartureTitle
                    )
                    assertNull(journeyArrivalTitle)

                    assertEquals(Icon.GREY_ICON, topIcon)
                    assertEquals(Icon.NONE, bottomIcon)
                }
            }
        }
    }

    @Test
    fun `test that our ferry journey displays correctly`() {
        with(UniqueJourney(ferryBooking, 0).toTicketUi().first()) {
            assertEquals(Icon.TRAIN_ICON, topIcon)
            assertEquals(Icon.TRAIN_DESTINATION_ICON, bottomIcon)
            assertEquals("16:35 Ryde St Johns Road (RYR)", departureTitle)
            assertEquals("18:08 Southampton Central (SOU)", arrivalTitle)
            assertEquals("Anytime Day Single", ticketTitle)
            assertEquals("27/11/19", ticketDate)

            with(legs) {
                assertEquals(5, size)
                with(this[0] as JourneyLegUi) {
                    assertNull(journeyDepartureTitle)
                    assertEquals("16:42 Ryde Pier Head (RYP)", journeyArrivalTitle)
                    assertEquals("Island Line", providerName)
                    assertEquals(Icon.GREY_ICON, bottomIcon)
                    assertEquals("Train has Standard class seating only", seatingClass)
                    assertEquals(
                        "Valid for travel by any permitted route and Wightlink crossing.",
                        ticketRoute
                    )
                }

                with(this[1] as ConnectionLegUi) {
                    assertEquals("Connection\n5mins", connectionText)
                    assertEquals(type, ConnectionOrJourneyType.WAIT)
                }

                with(this[2] as JourneyLegUi) {
                    assertEquals("17:09 Portsmouth Harbour (PMH)", journeyArrivalTitle)
                    assertEquals("South Western Railway", providerName)
                    assertEquals("16:47 Ryde Pier Head (RYP)", journeyDepartureTitle)
                    assertEquals(Icon.GREY_ICON, bottomIcon)
                    assertEquals("Train has Standard class seating only", seatingClass)
                    assertEquals(
                        "Valid for travel by any permitted route and Wightlink crossing.",
                        ticketRoute
                    )

                    assertEquals(Icon.FERRY_ICON, topIcon)
                }

                with(this[3] as ConnectionLegUi) {

                    assertEquals("Connection\n14mins", connectionText)
                    assertEquals(type, ConnectionOrJourneyType.WAIT)
                }

                with(this[4] as JourneyLegUi) {
                    assertEquals("17:23 Portsmouth Harbour (PMH)", journeyDepartureTitle)
                    assertEquals("Great Western Railway", providerName)
                    assertEquals("Train has Standard class seating only", seatingClass)
                    assertEquals(
                        "Valid for travel by any permitted route and Wightlink crossing.",
                        ticketRoute
                    )
                    assertEquals(Icon.TRAIN_ICON, topIcon)
                    assertNull(journeyArrivalTitle)
                }
            }
        }
    }

    @Test
    fun `test that our ferry journey with one one ferry leg`() {
        with(UniqueJourney(ferryBookingSingleLeg, 0).toTicketUi().first()) {
            assertEquals(Icon.FERRY_ICON, topIcon)
            assertEquals(Icon.LIGHTHOUSE_ICON, bottomIcon)
            assertEquals("16:47 Ryde Pier Head (RYP)", departureTitle)
            assertEquals("17:09 Portsmouth Harbour (PMH)", arrivalTitle)
            assertEquals("Anytime Day Single", ticketTitle)
            assertEquals("27/11/19", ticketDate)

            with(legs) {
                assertEquals(1, size)

                with(this[0] as JourneyLegUi) {
                    assertNull(journeyArrivalTitle)
                    assertNull(journeyDepartureTitle)
                    assertEquals("South Western Railway", providerName)
                    assertEquals("Train has Standard class seating only", seatingClass)
                    assertEquals(
                        "Valid for travel by any permitted route and Wightlink crossing.",
                        ticketRoute
                    )
                }
            }
        }
    }

    @Test
    fun `test that we can create a displayable journey leg from other flight`() {
        with(UniqueJourney(matchingOtherJson, 0).toTicketUi().first()) {
            assertEquals(Icon.FLIGHT_ICON, topIcon)
            assertEquals(Icon.AIRPORT_ARRIVAL_ICON, bottomIcon)

            with (legs) {
                assertEquals(3, count())
            }
        }
    }

    @Test
    fun `test that we can create a displayable journey leg from other flight return`() {
        with(UniqueJourney(matchingOtherJson, 1).toTicketUi().first()) {
            assertEquals(Icon.FLIGHT_ICON, topIcon)
            assertEquals(Icon.AIRPORT_ARRIVAL_ICON, bottomIcon)

            with (legs) {
                // There's two legs - which means one leg + one wait = 3
                assertEquals(3, count())
            }
        }
    }


    @Test
    fun `test that we can create a displayable journey leg from flight`() {
        with(
            UniqueJourney(
                flightBookingMatchingObject,
                0
            ).toTicketUi().first()) {
            assertEquals(Icon.FLIGHT_ICON, topIcon)
            assertEquals(Icon.AIRPORT_ARRIVAL_ICON, bottomIcon)

            with (legs) {

                assertEquals(1, count())
            }
        }
    }
}