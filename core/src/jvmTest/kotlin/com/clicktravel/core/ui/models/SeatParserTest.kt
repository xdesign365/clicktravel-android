package com.clicktravel.core.ui.models

import com.clicktravel.core.model.trains.SeatReservation
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

class SeatParserTest {

    @Test
    fun `parse standard nopl`() {

        with (SeatParser()) {
            assertNull(
                parseSeat(seatingClass = "STANDARD", seatNumber = "NOPL")
            )
        }
    }

    @Test
    fun `parse standard seating`() {
        with (SeatParser()) {

            assertEquals(
                SeatReservation("Standard", "A", 14),
                parseSeat(seatingClass = "STANDARD", seatNumber = "A14")
            )
        }
    }

    @Test
    fun `parse standard seating less than 10`() {
        with (SeatParser()) {
            assertEquals(
                SeatReservation("Standard", "D", 8),
                parseSeat(seatingClass = "STANDARD", seatNumber = "D8")
            )
        }
    }

    @Test
    fun `parse standard seating less than with direction`() {
        with (SeatParser()) {
            assertEquals(
                SeatReservation("Standard", "F", 22),
                parseSeat(seatingClass = "STANDARD", seatNumber = "F22F")
            )
        }
    }
}