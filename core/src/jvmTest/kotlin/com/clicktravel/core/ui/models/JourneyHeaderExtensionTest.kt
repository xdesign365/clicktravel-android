package com.clicktravel.core.ui.models

import com.clicktravel.core.testutils.booking
import com.clicktravel.core.testutils.bookingWithRailcard
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.testutils.flightBookingMatchingObject
import com.clicktravel.core.testutils.matchingOtherJson
import com.clicktravel.core.model.UniqueJourney
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class JourneyHeaderExtensionTest {

    @Test
    fun `test that we get a valid response from dummy data`() {
        with(UniqueJourney(booking, 0).toJourneyHeader()) {
            assertEquals("Edinburgh → Portsmouth & Southsea", title)
            assertEquals(TravelTypes.TRAIN, travelType)
            assertFalse(isHasRailcard())
            assertTrue(isShowTicketReference())
        }
    }

    @Test
    fun `test that we get a valid response from dummy data return journey`() {
        with(UniqueJourney(booking, 1).toJourneyHeader()) {
            assertEquals("Portsmouth & Southsea → Edinburgh", title)
            assertEquals(TravelTypes.TRAIN, travelType)
            assertFalse(isHasRailcard())
            assertTrue(isShowTicketReference())
        }
    }

    @Test
    fun `test that we get a valid response from dummy data with a railcard`() {
        with(UniqueJourney(bookingWithRailcard, 0).toJourneyHeader()) {
            assertEquals("Edinburgh → Portsmouth & Southsea", title)
            assertEquals(TravelTypes.TRAIN, travelType)
            assertEquals(bookingWithRailcard.railcard?.name, railcard?.name)
            assertTrue(isHasRailcard())
            assertFalse(isShowTicketReference())
        }
    }


    @Test
    fun `test that we get a valid response from dummy data with a special delivery`() {
        with(UniqueJourney(bookingWithRailcard, 0).toJourneyHeader()) {
            assertEquals("Edinburgh → Portsmouth & Southsea", title)
            assertEquals(TravelTypes.TRAIN, travelType)
            assertEquals(bookingWithRailcard.railcard?.name, railcard?.name)
            assertTrue(isHasRailcard())
            assertFalse(isShowTicketReference())
        }
    }

    @Test
    fun `test journey header gives correct information for first flight`() {
        with(
            UniqueJourney(
                flightBookingMatchingObject,
                0
            ).toJourneyHeader()) {
            assertEquals(
                "Birmingham International Airport, Birmingham (BHX) → Charles de Gaulle Airport, Paris (CDG)",
                title
            )
            assertTrue(isShowTicketReference())
            assertEquals(flightBookingMatchingObject.ticketCollectionReference, ticketReference)
            assertEquals(
                flightBookingMatchingObject.ticketNumbers.first(),
                subtitleText
            )
        }
    }

    @Test
    fun `test journey header gives correct information for other flight`() {
        with(UniqueJourney(matchingOtherJson, 0).toJourneyHeader()) {
            assertEquals("EDI → MUC → BKK", title)
            assertTrue(isShowTicketReference())
            assertEquals(matchingOtherJson.ticketNumbers.first(), subtitleText)
            assertEquals(matchingOtherJson.ticketCollectionReference, ticketReference)
        }
    }

}