package com.clicktravel.core.ui.models

import com.clicktravel.core.testutils.booking
import com.clicktravel.core.testutils.bookingWithRailcard
import com.clicktravel.core.testutils.bookingWithRailcardAndFirstClassPostDeliveryOption
import com.clicktravel.core.testutils.bookingWithRailcardAndSpecialDeliveryOption
import com.clicktravel.core.model.trains.enums.DeliveryOption
import com.clicktravel.core.model.UniqueJourney
import com.clicktravel.core.model.common.toCsv
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class DeliveryHeaderExtensionTest {

    @Test
    fun `when delivery is collect at station address is null`() {
        // Given
        with(UniqueJourney(booking, 0).toDeliveryAddress()) {
            assertNotNull(this)
            assertEquals(DeliveryOption.COLLECT_AT_STATION, deliveryOption)
            assertNull(deliveryAddress)
        }
    }

    @Test
    fun `when delivery is post address is set`() {
        // Given
        with(UniqueJourney(bookingWithRailcard, 0).toDeliveryAddress()) {
            assertNotNull(this)
            assertEquals(DeliveryOption.POST, deliveryOption)
            assertEquals(bookingWithRailcard.address.toCsv(), deliveryAddress)
        }
    }

    @Test
    fun `when delivery is special delivery address is set`() {
        // Given
        with(
            UniqueJourney(
                bookingWithRailcardAndSpecialDeliveryOption,
                0
            ).toDeliveryAddress()
        ) {
            assertNotNull(this)
            assertEquals(DeliveryOption.SPECIAL_DELIVERY, deliveryOption)
            assertEquals(
                bookingWithRailcardAndSpecialDeliveryOption.address.toCsv(),
                deliveryAddress
            )
            assertEquals(
                bookingWithRailcardAndSpecialDeliveryOption.forTheAttentionOf,
                forTheAttentionOf
            )
        }
    }

    @Test
    fun `when delivery is FIRST_CLASS_POST address is set`() {
        // Given
        with(
            UniqueJourney(
                bookingWithRailcardAndFirstClassPostDeliveryOption,
                0
            ).toDeliveryAddress()
        ) {
            assertNotNull(this)
            assertEquals(DeliveryOption.FIRST_CLASS_POST, deliveryOption)
            assertEquals(
                bookingWithRailcardAndFirstClassPostDeliveryOption.address.toCsv(),
                deliveryAddress
            )
            assertEquals(
                bookingWithRailcardAndFirstClassPostDeliveryOption.forTheAttentionOf,
                forTheAttentionOf
            )
        }
    }
}