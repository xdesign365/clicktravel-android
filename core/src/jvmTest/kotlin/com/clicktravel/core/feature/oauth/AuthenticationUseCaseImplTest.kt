@file:Suppress("SpellCheckingInspection", "SpellCheckingInspection")

package com.clicktravel.core.feature.oauth

import com.clicktravel.core.Settings
import com.clicktravel.core.model.auth.OauthModel
import com.clicktravel.core.model.team.Team
import com.clicktravel.core.usecases.AuthenticationUseCaseImpl
import com.clicktravel.core.usecases.UserJourneysUseCase
import com.clicktravel.core.util.ExpiryTimeQuery
import com.clicktravel.core.util.ExpiryTimeQueryImpl
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.repository.retriever.ArgumentRetriever
import com.clicktravel.repository.retriever.NoArgumentRetriever
import com.clicktravel.repository.retriever.RefreshAuthRetriever
import com.soywiz.klock.DateTime
import com.soywiz.klock.TimeSpan
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import kotlin.math.roundToLong
import kotlin.test.*
import kotlin.time.ExperimentalTime
import kotlin.time.hours

@ExperimentalTime
class AuthenticationUseCaseImplTest {

    private val loginUrl = "http://aabc"
    private val redirectUrl = "http://bcd"
    private val clientId = "asdasd"

    @RelaxedMockK
    lateinit var encoder: Encoder

    @RelaxedMockK
    lateinit var hasher: Hasher

    @RelaxedMockK
    lateinit var finalStageTokenRetriever: ArgumentRetriever<String, OauthModel>

    @RelaxedMockK
    lateinit var refreshAuthRetriever: RefreshAuthRetriever

    @RelaxedMockK
    lateinit var saveLocalOauth: WriteOnlyRepository<OauthModel>

    @RelaxedMockK
    lateinit var teamRepository: WriteOnlyRepository<Team>

    @RelaxedMockK
    lateinit var selectedTeamRepository: WriteOnlyRepository<Team>

    @RelaxedMockK
    lateinit var retrieveLocalOauth: NoArgumentRetriever<OauthModel?>

    @RelaxedMockK
    lateinit var journeys: UserJourneysUseCase

    @RelaxedMockK
    lateinit var timeQuery: ExpiryTimeQuery

    @Before
    fun setup() {
        Settings.setupDefaults(
            mapOf(
                Settings.LOGIN_URL to loginUrl,
                Settings.REDIRECT_URI to redirectUrl,
                Settings.CLIENT_ID to clientId
            )
        )

        MockKAnnotations.init(this)
    }

    private fun getPropertyForUrl(url: String, property: String): String {
        val map = mutableMapOf<String, String>()

        url.split("?").last()
            .split("&")
            .forEach {

                with(it.split("=")) {
                    map[first()] = last()
                }
            }

        return map[property]!!
    }

    @Test
    fun `authentication will build a url with a random state by default`() {
        val authA = getUseCase()

        with(authA.buildLoginUrl()) {
            assertTrue(contains(loginUrl))
            assertEquals(getPropertyForUrl(this, "redirect_uri"), redirectUrl)
            assertEquals(getPropertyForUrl(this, "client_id"), clientId)
        }
    }

    @Test
    fun `authentication when a valid oauth model will already be logged in`() {
        runBlocking {
            every { timeQuery.hasTimeExpired(any()) } returns false

            coEvery { retrieveLocalOauth.execute() } returns OauthModel(
                accessToken = "A",
                expiresIn = DateTime.now().plus(TimeSpan(1.hours.inMilliseconds)).unixMillisLong,
                tokenType = "C",
                refreshToken = "refreshToken"
            )

            val authA = getUseCase()

            assertNotNull(authA.isUserAlreadyLoggedIn())
        }
    }

    @ExperimentalTime
    @Test
    fun `authentication when expired will return null if refresh auth fails`() {
        runBlocking {
            val authA = getUseCase()

            every { timeQuery.hasTimeExpired(any()) } returns true

            coEvery { retrieveLocalOauth.execute() } returns OauthModel(
                accessToken = "A",
                expiresIn = DateTime.now().minus(TimeSpan(1.hours.inMilliseconds)).unixMillisLong,
                tokenType = "C",
                refreshToken = "refreshToken"
            )

            assertNull(authA.isUserAlreadyLoggedIn())
        }
    }

    @ExperimentalTime
    @Test
    fun `authentication when expired will return a new auth after refreshing`() {
        runBlocking {
            val authA = getUseCase()

            every { timeQuery.hasTimeExpired(any()) } returns true

            coEvery { retrieveLocalOauth.execute() } returns OauthModel(
                accessToken = "A",
                expiresIn = DateTime.now().minus(TimeSpan(1.hours.inMilliseconds)).unixMillisLong,
                tokenType = "C",
                refreshToken = "refreshToken"
            )

            val newAuth = OauthModel(
                accessToken = "newAccesstoken",
                expiresIn = 1L,
                tokenType = "newTokenType",
                refreshToken = "newRefreshToken"
            )

            coEvery { refreshAuthRetriever.refresh("refreshToken") } returns newAuth

            assertEquals(
                expected = newAuth,
                actual = authA.isUserAlreadyLoggedIn()
            )
        }
    }

    @ExperimentalTime
    @Test
    fun `authentication when storing oauth will add an hour to the current time`() {
        runBlocking {

            val dummyDate = DateTime.now()
            val expectedExpiry = dummyDate.plus(TimeSpan(5.hours.inMilliseconds))

            val expiryTimeQuery = ExpiryTimeQueryImpl { dummyDate }

            val authA = AuthenticationUseCaseImpl(
                hasher = hasher,
                encoder = encoder,
                finalStageTokenRetriever = finalStageTokenRetriever,
                retrieveLocalOauth = retrieveLocalOauth,
                refreshAuthRetriever = refreshAuthRetriever,
                saveLocalOauth = saveLocalOauth,
                timeQuery = expiryTimeQuery,
                teamRepository = teamRepository,
                selectedTeamRepository = selectedTeamRepository,
                journeys = journeys
            )


            coEvery { finalStageTokenRetriever.execute(any()) } returns OauthModel(
                accessToken = "A",
                expiresIn = 5.hours.inSeconds.roundToLong(),
                tokenType = "B",
                refreshToken = "refreshToken"
            )

            authA.completeLogin("code=asdasd")

            coVerify {
                saveLocalOauth.storeData(
                    OauthModel(
                        accessToken = "A",
                        expiresIn = expectedExpiry.unixMillisLong,
                        tokenType = "B",
                        refreshToken = "refreshToken"
                    )
                )
            }
        }
    }

    @Test
    fun `logout clears auth, team, bookings and selected team data`() {
        runBlocking {

            val useCase = getUseCase()

            useCase.logout()
            coVerify { saveLocalOauth.clearData() }
            coVerify { teamRepository.clearData() }
            coVerify { selectedTeamRepository.clearData() }
            coVerify { journeys.clearData() }
        }
    }

    private fun getUseCase() = AuthenticationUseCaseImpl(
        hasher = hasher,
        encoder = encoder,
        finalStageTokenRetriever = finalStageTokenRetriever,
        retrieveLocalOauth = retrieveLocalOauth,
        refreshAuthRetriever = refreshAuthRetriever,
        saveLocalOauth = saveLocalOauth,
        timeQuery = timeQuery,
        teamRepository = teamRepository,
        selectedTeamRepository = selectedTeamRepository,
        journeys = journeys
    )
}