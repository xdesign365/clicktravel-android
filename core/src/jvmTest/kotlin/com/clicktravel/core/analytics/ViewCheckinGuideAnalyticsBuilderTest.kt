package com.clicktravel.core.analytics

import com.clicktravel.core.testutils.flightBookingMatchingObject
import com.soywiz.klock.DateTime
import com.soywiz.klock.DateTimeTz
import com.soywiz.klock.TimezoneOffset
import org.junit.Test
import kotlin.test.assertEquals

class ViewCheckinGuideAnalyticsBuilderTest {

    @Test
    fun `test that checkin guide analytics builder produces correct value`() {

        val fakeToday =
            {
                DateTimeTz.local(
                    DateTime.createUnchecked(2019, 10, 29, 12, 0, 0),
                    TimezoneOffset(0.0)
                )
            }

        // 2019-10-31T06:00:00+0000
        // 2019-11-07T13:00:00+0100

        ViewCheckinGuideAnalyticsBuilder(fakeToday)
            .build(flightBookingMatchingObject)
            .let {
                assertEquals("team-id-goes-here-when-available", it[AnalyticsStatics.teamId])
                assertEquals("user-id-goes-here-when-available", it[AnalyticsStatics.userId])
                assertEquals("2", it[AnalyticsStatics.daysUntilOutbound])
                assertEquals("9", it[AnalyticsStatics.daysUntilReturn])
            }
    }
}