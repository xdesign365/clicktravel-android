package com.clicktravel.core

import com.clicktravel.repository.retriever.NoArgumentRetriever
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class CoroutinesExecutor<T>(
    val noArgumentRetriever: NoArgumentRetriever<T>
) {
    val coroutineScope: CoroutineScope = MainScope()

    fun execute(completion : (T) -> Unit) = coroutineScope.launch {
        completion(noArgumentRetriever.execute())
    }
}