package com.clicktravel.itinerary.steps

/*import android.view.View
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import com.agoda.kakao.common.views.KView
import com.agoda.kakao.image.KImageView
import com.agoda.kakao.recycler.KRecyclerItem
import com.agoda.kakao.recycler.KRecyclerView
import com.agoda.kakao.screen.Screen
import com.agoda.kakao.screen.Screen.Companion.onScreen
import com.agoda.kakao.text.KButton
import com.agoda.kakao.text.KTextView
import com.clicktravel.core.usecases.AuthenticationUseCase
import com.clicktravel.core.model.FlightBooking
import com.clicktravel.core.model.TrainBooking
import com.clicktravel.core.model.UniqueJourney
import com.clicktravel.core.model.auth.OauthModel
import com.clicktravel.core.testutils.*
import com.clicktravel.core.util.exhaustive
import com.clicktravel.itinerary.ActivityFinisher
import com.clicktravel.itinerary.MainActivity
import com.clicktravel.mobile.android.R
import com.clicktravel.repository.usecases.UserJourneysUseCase
import com.soywiz.klock.DateTime
import com.soywiz.klock.TimeSpan
import com.soywiz.klock.hours
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.hamcrest.Matcher
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import org.koin.dsl.module

class FakeDummy : UserJourneysUseCase {

    override suspend fun getAllJourneys(): List<UniqueJourney> = listOf(
        UniqueJourney(booking, 0),
        UniqueJourney(bookingWithRailcard, 1),
        UniqueJourney(
            bookingWithRailcardAndSpecialDeliveryOption,
            0
        ),
        UniqueJourney(
            bookingWithRailcardAndFirstClassPostDeliveryOption,
            1
        ),
        UniqueJourney(
            cheltenhamBookingWithETicket,
            0
        ),
        UniqueJourney(ferryBooking, 0),
        UniqueJourney(ferryBookingSingleLeg, 0),
        UniqueJourney(matchingOtherJson, 0)
    )


    override suspend fun getBookingFromId(bookingId: String) = withContext(Dispatchers.IO) {
        getAllJourneys().first {
            when (it.bookingItem) {
                is TrainBooking -> (it.bookingItem as TrainBooking).bookingId == bookingId
                is FlightBooking -> (it.bookingItem as FlightBooking).bookingId == bookingId
            }.exhaustive
        }.bookingItem
    }
}

class AuthenticationUseCaseMock :
    AuthenticationUseCase {
    override fun buildLoginUrl() = ""
    override suspend fun completeLogin(url: String) = OauthModel(
        "A",
        DateTime.now().plus(TimeSpan(1.hours.milliseconds)).unixMillisLong,
        "B"
    )

    override suspend fun isUserAlreadyLoggedIn() = OauthModel(
        "A",
        DateTime.now().plus(TimeSpan(1.hours.milliseconds)).unixMillisLong,
        "B"
    )

    override suspend fun logout() {
    }

    override fun shouldKeepWebView(url: String): Boolean = false
}

@LargeTest
@RunWith(AndroidJUnit4ClassRunner::class)
class SingleTicketScreenTests {
    private val activityRule = ActivityTestRule(MainActivity::class.java, false, false)

    class JourneyLegsScreen : Screen<JourneyLegsScreen>() {

        class SingleJourneyLeg(parent: Matcher<View>) : KRecyclerItem<SingleJourneyLeg>(parent) {
            val companyName = KTextView(parent) { withId(R.id.companyName) }
            val journeyLegDuration = KTextView(parent) { withId(R.id.journeyLegDuration) }
            val seatCoach = KTextView(parent) { withId(R.id.seatCoach) }
            val seatingClass = KTextView(parent) { withId(R.id.seatingClass) }
            val routeInformation = KTextView(parent) { withId(R.id.routeInformation) }

            val departureHeader = KTextView(parent) { withId(R.id.trainDepartureHeader) }
            val arrivalHeader = KTextView(parent) { withId(R.id.trainArrivalHeader) }
            val sideIcon = KImageView(parent) { withId(R.id.sideBarIcon) }
            val topIcon = KImageView(parent) { withId(R.id.topIcon) }
            val baggageAllowance = KTextView(parent) { withId(R.id.baggageAllowance) }

        }

        class ConnectionJourneyLeg(parent: Matcher<View>) :
            KRecyclerItem<ConnectionJourneyLeg>(parent) {
            val connectionText = KTextView(parent) { withId(R.id.connectionText) }
            val travelTypeIcon = KImageView(parent) { withId(R.id.travelTypeIcon) }
        }

        val ticketsListView = KRecyclerView(builder = {
            withId(R.id.journeyLegs)
            withMatcher(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE))
        }, itemTypeBuilder = {
            itemType(JourneyLegsScreen::SingleJourneyLeg)
            itemType(JourneyLegsScreen::ConnectionJourneyLeg)
        })

    }

    class RecyclerScreen : Screen<RecyclerScreen>() {
        class SingleTicket(parent: Matcher<View>) : KRecyclerItem<SingleTicket>(parent) {
            val ticketTitle = KTextView(parent) { withId(R.id.ticketTitle) }
            val ticketDate = KTextView(parent) { withId(R.id.ticketDate) }
            val eTicket = KButton(parent) { withId(R.id.eTicketButton) }
            val stationTimeAndName = KTextView(parent) { withId(R.id.stationNameAndTime) }
            val destinationNameAndTime = KTextView(parent) { withId(R.id.destinationNameAndTime) }
            val journeyLegs = JourneyLegsScreen()

            val topTicketIcon = KImageView(parent) { withId(R.id.topTicketIcon) }
            val bottomTicketIcon = KImageView(parent) { withId(R.id.bottomTicketIcon) }
        }

        val ticketsListView = KRecyclerView({
            withId(R.id.ticketsListView)
        }, itemTypeBuilder = {
            itemType(::SingleTicket)
        })
    }

    class JourneyScreen : Screen<JourneyScreen>() {
        val journeyTitle = KTextView {
            withId(R.id.tripStartToDestination)
        }

        val railcard = KTextView { withId(R.id.railcardDetail) }

        val ticketCollectionView = KView { withId(R.id.ticketCollectionView) }

        val ticketReference = KTextView { withId(R.id.ticketReferenceFlight) }

        val delivery = KTextView { withId(R.id.delivery) }
        val ticketReferenceFlight = KTextView { withId(R.id.ticketReferenceFlight) }

        val deliveryType = KTextView { withId(R.id.deliveryType) }
        val recyclerScreen = RecyclerScreen()
        val notes = KTextView { withId(R.id.notes) }

    }

    private val modules = listOf(module {
        single<UserJourneysUseCase>(override = true) {
            FakeDummy()
        }

        val authentication = AuthenticationUseCaseMock()
        single<AuthenticationUseCase>(override = true) {
            authentication
        }

    })

    class SelectJourneyScreen : Screen<SelectJourneyScreen>() {
        val mainViewList = KRecyclerView({
            withId(R.id.allJourneysList)
        }, itemTypeBuilder = {
        })
    }

    @Before
    fun before() {
        loadKoinModules(modules)
        Thread.sleep(500)
    }

    @LargeTest
    @Test
    fun testThatTheFirstValueWillHaveTheCorrectJourneyHeader() {

        activityRule.launchActivity(null)
        onScreen<SelectJourneyScreen> {
            mainViewList.childAt<KRecyclerItem<*>>(1) {
                click()
            }
        }

        onScreen<JourneyScreen> {
            journeyTitle {
                hasText("Edinburgh → Portsmouth & Southsea")
            }


            railcard.isGone()


            ticketReference {
                hasText("3TK45KFG")
            }

            delivery.isGone()
            deliveryType.isGone()

            notes {
                hasText("gfnfcgncdncdcnbc")
            }

            recyclerScreen.ticketsListView {

                isVisible()
                hasSize(2)

                childAt<RecyclerScreen.SingleTicket>(0) {

                    stationTimeAndName {
                        hasText("16:30 Edinburgh (EDB)")
                    }

                    destinationNameAndTime {
                        hasText("23:31 Portsmouth & Southsea (PMS)")
                    }

                    ticketTitle {
                        hasText("Super Off-Peak Return")
                    }

                    ticketDate {
                        hasText("07/10/19")
                    }

                    topTicketIcon.hasDrawable(R.drawable.ic_train)
                    bottomTicketIcon.hasDrawable(R.drawable.ic_pin)

                    journeyLegs.ticketsListView {
                        firstChild<JourneyLegsScreen.SingleJourneyLeg> {
                            departureHeader.isGone()
                            companyName.hasText("London North Eastern Railway")
                            journeyLegDuration.hasText("4hrs 23mins")
                            seatCoach.isGone()
                            seatingClass.hasText("Train has First & Standard class seating")
                            routeInformation.hasText("Travel is allowed via any permitted route.")
                            arrivalHeader.hasText("20:53 London Kings Cross (KGX)")
                            sideIcon.hasDrawable(R.drawable.ic_rail)
                            topIcon.isGone()
                        }

                        childAt<JourneyLegsScreen.ConnectionJourneyLeg>(1) {
                            connectionText.hasText("Take the underground from Kings Cross N Lt to Waterloo (Underground)")
                            travelTypeIcon.hasDrawable(R.drawable.ic_underground)
                        }

                        childAt<JourneyLegsScreen.SingleJourneyLeg>(2) {
                            departureHeader.hasText("22:00 London Waterloo (WAT)")
                            companyName.hasText("South Western Railway")
                            journeyLegDuration.hasText("1hrs 31mins")
                            seatCoach.isGone()
                            seatingClass.hasText("Train has First & Standard class seating")
                            routeInformation.hasText("Travel is allowed via any permitted route.")
                            sideIcon.hasDrawable(R.drawable.ic_rail)
                            topIcon.hasDrawable(R.drawable.ic_grey_dot)
                        }
                    }

                    eTicket.isGone()
                }


                lastChild<RecyclerScreen.SingleTicket> {
                    topTicketIcon.hasDrawable(R.drawable.ic_train)
                    bottomTicketIcon.hasDrawable(R.drawable.ic_pin)
                    ticketTitle {
                        hasText("Super Off-Peak Return")
                    }

                    ticketDate {
                        hasText("16/10/19")
                    }

                    stationTimeAndName {
                        hasText("17:19 Portsmouth & Southsea (PMS)")
                    }

                    destinationNameAndTime {
                        hasText("08:07 Edinburgh (EDB)")
                    }

                    eTicket.isGone()

                }
            }
        }

    }

    @LargeTest
    @Test
    fun testThatTheFirstValueWillHaveTheCorrectJourneyHeader2() {

        activityRule.launchActivity(null)
        onScreen<SelectJourneyScreen> {
            mainViewList.childAt<KRecyclerItem<*>>(3) {
                click()
            }
        }

        onScreen<JourneyScreen> {

            journeyTitle {
                hasText("Portsmouth & Southsea → Edinburgh")
            }

            railcard {
                hasText("Remember to travel with your 16-25 Railcard")
            }

            ticketCollectionView.isGone()

            deliveryType {
                hasText("Royal Mail standard delivery for the attention of Test name")
            }

            delivery {
                hasText("AnotherAddress, 9 Belford Road, Edinburgh, EH4 3DE")
            }

            notes {
                hasText("gfnfcgncdncdcnbc")
            }

            recyclerScreen.ticketsListView {

                isVisible()
                hasSize(2)

                childAt<RecyclerScreen.SingleTicket>(0) {
                    topTicketIcon.hasDrawable(R.drawable.ic_train)
                    bottomTicketIcon.hasDrawable(R.drawable.ic_pin)
                    stationTimeAndName {
                        hasText("16:30 Edinburgh (EDB)")
                    }

                    destinationNameAndTime {
                        hasText("23:31 Portsmouth & Southsea (PMS)")
                    }

                    ticketTitle {
                        hasText("Super Off-Peak Return")
                    }

                    ticketDate {
                        hasText("07/10/19")
                    }

                    eTicket.isGone()
                }


                lastChild<RecyclerScreen.SingleTicket> {
                    topTicketIcon.hasDrawable(R.drawable.ic_train)
                    bottomTicketIcon.hasDrawable(R.drawable.ic_pin)
                    stationTimeAndName {
                        hasText("17:19 Portsmouth & Southsea (PMS)")
                    }

                    destinationNameAndTime {
                        hasText("08:07 Edinburgh (EDB)")
                    }

                    ticketTitle {
                        hasText("Super Off-Peak Return")
                    }

                    ticketDate {
                        hasText("16/10/19")
                    }

                    eTicket.isGone()

                    journeyLegs.ticketsListView {
                        firstChild<JourneyLegsScreen.SingleJourneyLeg> {
                            departureHeader.isGone()
                            companyName.hasText("South Western Railway")
                            journeyLegDuration.hasText("1hrs 40mins")
                            seatCoach.isGone()
                            seatingClass.hasText("Train has First & Standard class seating")
                            routeInformation.hasText("Travel is allowed via any permitted route.")
                            arrivalHeader.hasText("18:59 London Waterloo (WAT)")
                            sideIcon.hasDrawable(R.drawable.ic_rail)
                            topIcon.isGone()
                        }

                        childAt<JourneyLegsScreen.ConnectionJourneyLeg>(1) {
                            connectionText.hasText("Take the underground from Waterloo (Underground) to Kings Cross N Lt")
                            travelTypeIcon.hasDrawable(R.drawable.ic_underground)
                        }

                        childAt<JourneyLegsScreen.SingleJourneyLeg>(2) {
                            departureHeader.hasText("20:00 London Kings Cross (KGX)")
                            arrivalHeader.hasText("22:54 Newcastle (NCL)")
                            companyName.hasText("London North Eastern Railway")
                            journeyLegDuration.hasText("2hrs 54mins")
                            seatCoach.isGone()
                            seatingClass.hasText("Train has First & Standard class seating")
                            routeInformation.hasText("Travel is allowed via any permitted route.")
                            sideIcon.hasDrawable(R.drawable.ic_rail)
                            topIcon.hasDrawable(R.drawable.ic_grey_dot)
                        }

                        childAt<JourneyLegsScreen.ConnectionJourneyLeg>(3) {
                            connectionText.hasText("Connection\n7hrs 28mins")
                            travelTypeIcon.hasDrawable(R.drawable.ic_coffee)
                        }

                        childAt<JourneyLegsScreen.SingleJourneyLeg>(4) {
                            departureHeader.hasText("06:22 Newcastle (NCL)")
                            arrivalHeader.isGone()
                            companyName.hasText("London North Eastern Railway")
                            journeyLegDuration.hasText("1hrs 45mins")
                            seatCoach.isGone()
                            seatingClass.hasText("Train has First & Standard class seating")
                            routeInformation.hasText("Travel is allowed via any permitted route.")
                            sideIcon.hasDrawable(R.drawable.ic_rail)
                            topIcon.hasDrawable(R.drawable.ic_grey_dot)
                        }

                    }
                }
            }
        }
    }

    @LargeTest
    @Test
    fun testThatWeCanSeeRailcardInOurThirdSelection() {

        activityRule.launchActivity(null)
        onScreen<SelectJourneyScreen> {
            mainViewList.childAt<KRecyclerItem<*>>(5) {
                click()
            }
        }

        onScreen<JourneyScreen> {

            journeyTitle {
                hasText("Edinburgh → Portsmouth & Southsea")
            }

            railcard {
                hasText("Remember to travel with your 16-25 Railcard")
            }

            ticketCollectionView.isGone()

            deliveryType {
                hasText("Royal Mail Special Delivery for the attention of Test name")
            }

            delivery {
                hasText("AnotherAddress, 9 Belford Road, Edinburgh, EH4 3DE")
            }

            notes {
                hasText("gfnfcgncdncdcnbc")
            }

            recyclerScreen.ticketsListView {

                isVisible()
                hasSize(2)

                childAt<RecyclerScreen.SingleTicket>(0) {
                    ticketTitle {
                        hasText("Super Off-Peak Return")
                    }

                    ticketDate {
                        hasText("07/10/19")
                    }

                    destinationNameAndTime {
                        hasText("23:31 Portsmouth & Southsea (PMS)")
                    }

                    stationTimeAndName {
                        hasText("16:30 Edinburgh (EDB)")
                    }

                    eTicket.isGone()
                    topTicketIcon.hasDrawable(R.drawable.ic_train)
                    bottomTicketIcon.hasDrawable(R.drawable.ic_pin)
                    journeyLegs.ticketsListView {
                        firstChild<JourneyLegsScreen.SingleJourneyLeg> {
                            departureHeader.isGone()
                            companyName.hasText("London North Eastern Railway")
                            journeyLegDuration.hasText("4hrs 23mins")
                            seatCoach.isGone()
                            seatingClass.hasText("Train has First & Standard class seating")
                            routeInformation.hasText("Travel is allowed via any permitted route.")
                            arrivalHeader.hasText("20:53 London Kings Cross (KGX)")
                            sideIcon.hasDrawable(R.drawable.ic_rail)
                            topIcon.isGone()
                        }

                        childAt<JourneyLegsScreen.ConnectionJourneyLeg>(1) {
                            connectionText.hasText("Take the underground from Kings Cross N Lt to Waterloo (Underground)")
                            travelTypeIcon.hasDrawable(R.drawable.ic_underground)
                        }

                        childAt<JourneyLegsScreen.SingleJourneyLeg>(2) {
                            departureHeader.hasText("22:00 London Waterloo (WAT)")
                            companyName.hasText("South Western Railway")
                            journeyLegDuration.hasText("1hrs 31mins")
                            seatCoach.isGone()
                            seatingClass.hasText("Train has First & Standard class seating")
                            routeInformation.hasText("Travel is allowed via any permitted route.")
                            sideIcon.hasDrawable(R.drawable.ic_rail)
                            topIcon.hasDrawable(R.drawable.ic_grey_dot)
                        }
                    }

                }


                lastChild<RecyclerScreen.SingleTicket> {
                    topTicketIcon.hasDrawable(R.drawable.ic_train)
                    bottomTicketIcon.hasDrawable(R.drawable.ic_pin)
                    ticketTitle {
                        hasText("Super Off-Peak Return")
                    }

                    ticketDate {
                        hasText("16/10/19")
                    }

                    destinationNameAndTime {
                        hasText("08:07 Edinburgh (EDB)")
                    }

                    stationTimeAndName {
                        hasText("17:19 Portsmouth & Southsea (PMS)")
                    }

                    eTicket.isGone()

                }
            }
        }
    }

    @LargeTest
    @Test
    fun testThatWeCanSeeOurTicketCollectionWhenCollectAtStation() {


        activityRule.launchActivity(null)
        onScreen<SelectJourneyScreen> {
            mainViewList.childAt<KRecyclerItem<*>>(7) {
                click()
            }
        }

        onScreen<JourneyScreen> {

            journeyTitle {
                hasText("Portsmouth & Southsea → Edinburgh")
            }

            railcard.isGone()

            ticketCollectionView.isGone()

            deliveryType {
                hasText("Royal Mail First Class post for the attention of Test name")
            }

            delivery {
                hasText("AnotherAddress, 9 Belford Road, Edinburgh, EH4 3DE")
            }

            notes {
                hasText("gfnfcgncdncdcnbc")
            }


            recyclerScreen.ticketsListView {

                isVisible()
                hasSize(2)

                childAt<RecyclerScreen.SingleTicket>(0) {
                    ticketTitle {
                        hasText("Super Off-Peak Return")
                    }
                    topTicketIcon.hasDrawable(R.drawable.ic_train)
                    bottomTicketIcon.hasDrawable(R.drawable.ic_pin)
                    ticketDate {
                        hasText("07/10/19")
                    }

                    stationTimeAndName {
                        hasText("16:30 Edinburgh (EDB)")
                    }

                    destinationNameAndTime {
                        hasText("23:31 Portsmouth & Southsea (PMS)")
                    }

                    journeyLegs.ticketsListView {
                        firstChild<JourneyLegsScreen.SingleJourneyLeg> {
                            departureHeader.isGone()
                            companyName.hasText("South Western Railway")
                            journeyLegDuration.hasText("1hrs 40mins")
                            seatCoach.isGone()
                            seatingClass.hasText("Train has First & Standard class seating")
                            routeInformation.hasText("Travel is allowed via any permitted route.")
                            arrivalHeader.hasText("18:59 London Waterloo (WAT)")
                            sideIcon.hasDrawable(R.drawable.ic_rail)
                            topIcon.isGone()
                        }

                        childAt<JourneyLegsScreen.ConnectionJourneyLeg>(1) {
                            connectionText.hasText("Take the underground from Waterloo (Underground) to Kings Cross N Lt")
                            travelTypeIcon.hasDrawable(R.drawable.ic_underground)
                        }

                        childAt<JourneyLegsScreen.SingleJourneyLeg>(2) {
                            departureHeader.hasText("20:00 London Kings Cross (KGX)")
                            arrivalHeader.hasText("22:54 Newcastle (NCL)")
                            companyName.hasText("London North Eastern Railway")
                            journeyLegDuration.hasText("2hrs 54mins")
                            seatCoach.isGone()
                            seatingClass.hasText("Train has First & Standard class seating")
                            routeInformation.hasText("Travel is allowed via any permitted route.")
                            sideIcon.hasDrawable(R.drawable.ic_rail)
                            topIcon.hasDrawable(R.drawable.ic_grey_dot)
                        }

                        childAt<JourneyLegsScreen.ConnectionJourneyLeg>(3) {
                            connectionText.hasText("Connection\n7hrs 28mins")
                            travelTypeIcon.hasDrawable(R.drawable.ic_coffee)
                        }

                        childAt<JourneyLegsScreen.SingleJourneyLeg>(4) {
                            departureHeader.hasText("06:22 Newcastle (NCL)")
                            arrivalHeader.isGone()
                            companyName.hasText("London North Eastern Railway")
                            seatCoach.isGone()
                            journeyLegDuration.hasText("1hrs 45mins")
                            seatingClass.hasText("Train has First & Standard class seating")
                            routeInformation.hasText("Travel is allowed via any permitted route.")
                            sideIcon.hasDrawable(R.drawable.ic_rail)
                            topIcon.hasDrawable(R.drawable.ic_grey_dot)
                        }

                    }

                    eTicket.isGone()

                }


                lastChild<RecyclerScreen.SingleTicket> {
                    ticketTitle {
                        hasText("Super Off-Peak Return")
                    }
                    topTicketIcon.hasDrawable(R.drawable.ic_train)
                    bottomTicketIcon.hasDrawable(R.drawable.ic_pin)
                    ticketDate {
                        hasText("16/10/19")
                    }

                    stationTimeAndName {
                        hasText("17:19 Portsmouth & Southsea (PMS)")
                    }

                    destinationNameAndTime {
                        hasText("08:07 Edinburgh (EDB)")
                    }
                    eTicket.isGone()

                }
            }
        }
    }

    @LargeTest
    @Test
    fun testThatWeCanSeeOurTicketCollectionWhenItsAnETicket() {

        activityRule.launchActivity(null)
        onScreen<SelectJourneyScreen> {
            mainViewList.childAt<KRecyclerItem<*>>(9) {
                click()
            }
        }

        onScreen<JourneyScreen> {


            journeyTitle {
                hasText("Birmingham New Street → Cheltenham Spa")
            }

            railcard.isGone()
            ticketCollectionView.isGone()
            deliveryType.isGone()
            delivery.isGone()
            notes.isGone()

            recyclerScreen.ticketsListView {

                isVisible()
                hasSize(1)

                childAt<RecyclerScreen.SingleTicket>(0) {
                    ticketTitle {
                        hasText("Advance Single")
                    }
                    topTicketIcon.hasDrawable(R.drawable.ic_train)
                    bottomTicketIcon.hasDrawable(R.drawable.ic_pin)
                    ticketDate {
                        hasText("29/11/19")
                    }

                    stationTimeAndName {
                        hasText("07:42 Birmingham New Street (BHM)")
                    }

                    destinationNameAndTime {
                        hasText("08:24 Cheltenham Spa (CNM)")
                    }

                    eTicket.isVisible()

                    journeyLegs.ticketsListView {
                        firstChild<JourneyLegsScreen.SingleJourneyLeg> {
                            departureHeader.isGone()
                            companyName.hasText("CrossCountry")
                            seatCoach.hasText("Standard, Coach D: Seat 17")
                            seatingClass.hasText("Train has First & Standard class seating")
                            routeInformation.hasText("Valid on CrossCountry services only. Advance tickets are valid only on the booked services.")
                            arrivalHeader.isGone()
                        }
                    }
                }
            }
        }
    }

    @LargeTest
    @Test
    fun testThatWeCanSeeOurTicketCollectionWhenItsAFerry() {

        activityRule.launchActivity(null)
        onScreen<SelectJourneyScreen> {
            mainViewList.childAt<KRecyclerItem<*>>(11) {
                click()
            }
        }

        onScreen<JourneyScreen> {


            journeyTitle {
                hasText("Ryde St Johns Road → Southampton Central")
            }

            railcard.isGone()
            ticketCollectionView.isGone()
            deliveryType.isGone()
            delivery.isGone()
            notes.hasText("Sample text")

            recyclerScreen.ticketsListView {

                isVisible()
                hasSize(1)

                childAt<RecyclerScreen.SingleTicket>(0) {
                    ticketTitle {
                        hasText("Anytime Day Single")
                    }
                    topTicketIcon.hasDrawable(R.drawable.ic_train)
                    bottomTicketIcon.hasDrawable(R.drawable.ic_pin)

                    ticketDate {
                        hasText("27/11/19")
                    }

                    stationTimeAndName {
                        hasText("16:35 Ryde St Johns Road (RYR)")
                    }

                    destinationNameAndTime {
                        hasText("18:08 Southampton Central (SOU)")
                    }

                    eTicket.isVisible()

                    journeyLegs.ticketsListView {
                        firstChild<JourneyLegsScreen.SingleJourneyLeg> {
                            departureHeader.isGone()
                            companyName.hasText("Island Line")
                            seatCoach.isGone()
                            seatingClass.hasText("Train has Standard class seating only")
                            routeInformation.hasText("Valid for travel by any permitted route and Wightlink crossing.")
                            arrivalHeader.hasText("16:42 Ryde Pier Head (RYP)")
                            sideIcon.hasDrawable(R.drawable.ic_rail)
                            topIcon.isGone()
                        }

                        childAt<JourneyLegsScreen.ConnectionJourneyLeg>(1) {
                            connectionText.hasText("Connection\n5mins")
                            travelTypeIcon.hasDrawable(R.drawable.ic_coffee)
                        }

                        childAt<JourneyLegsScreen.SingleJourneyLeg>(2) {
                            departureHeader.hasText("16:47 Ryde Pier Head (RYP)")
                            companyName.hasText("South Western Railway")
                            seatCoach.isGone()
                            seatingClass.hasText("Train has Standard class seating only")
                            routeInformation.hasText("Valid for travel by any permitted route and Wightlink crossing.")
                            arrivalHeader.hasText("17:09 Portsmouth Harbour (PMH)")
                            sideIcon.hasDrawable(R.drawable.ic_wave)
                            topIcon.hasDrawable(R.drawable.ic_ferry)
                        }

                        childAt<JourneyLegsScreen.ConnectionJourneyLeg>(3) {
                            connectionText.hasText("Connection\n14mins")
                            travelTypeIcon.hasDrawable(R.drawable.ic_coffee)
                        }

                        childAt<JourneyLegsScreen.SingleJourneyLeg>(4) {
                            departureHeader.hasText("17:23 Portsmouth Harbour (PMH)")
                            companyName.hasText("Great Western Railway")
                            seatCoach.isGone()
                            seatingClass.hasText("Train has Standard class seating only")
                            routeInformation.hasText("Valid for travel by any permitted route and Wightlink crossing.")
                            arrivalHeader.isGone()
                            sideIcon.hasDrawable(R.drawable.ic_rail)
                            topIcon.hasDrawable(R.drawable.ic_train)
                        }
                    }
                }
            }
        }
    }

    @LargeTest
    @Test
    fun testThatWeCanSeeOurTicketCollectionWhenItsAFerrySingleLeg() {

        activityRule.launchActivity(null)

        onScreen<SelectJourneyScreen> {
            mainViewList.childAt<KRecyclerItem<*>>(12) {
                click()
            }
        }

        onScreen<JourneyScreen> {


            journeyTitle {
                hasText("Ryde St Johns Road → Southampton Central")
            }

            railcard.isGone()
            ticketCollectionView.isGone()
            deliveryType.isGone()
            delivery.isGone()
            notes.hasText("Sample text")

            recyclerScreen.ticketsListView {

                isVisible()
                hasSize(1)

                childAt<RecyclerScreen.SingleTicket>(0) {
                    ticketTitle {
                        hasText("Anytime Day Single")
                    }
                    topTicketIcon.hasDrawable(R.drawable.ic_ferry)
                    bottomTicketIcon.hasDrawable(R.drawable.ic_lighthouse)

                    ticketDate {
                        hasText("27/11/19")
                    }

                    stationTimeAndName {
                        hasText("16:47 Ryde Pier Head (RYP)")
                    }

                    destinationNameAndTime {
                        hasText("17:09 Portsmouth Harbour (PMH)")
                    }

                    eTicket.isVisible()

                    journeyLegs.ticketsListView {
                        firstChild<JourneyLegsScreen.SingleJourneyLeg> {
                            companyName.hasText("South Western Railway")
                            seatCoach.isGone()
                            seatingClass.hasText("Train has Standard class seating only")
                            routeInformation.hasText("Valid for travel by any permitted route and Wightlink crossing.")
                            sideIcon.hasDrawable(R.drawable.ic_wave)
                            topIcon.isGone()
                        }
                    }
                }
            }
        }
    }

    @LargeTest
    @Test
    fun testThatWeCanParseAnAirportItemCorrectly() {


        activityRule.launchActivity(null)
        onScreen<SelectJourneyScreen> {
            mainViewList.childAt<KRecyclerItem<*>>(14) {
                click()
            }
        }


        onScreen<JourneyScreen> {

            journeyTitle.isVisible()
            railcard.isGone()
            ticketReferenceFlight {
                hasText("QS8NOR")
            }

            deliveryType.isGone()
            delivery.isGone()
            notes.isGone()

            recyclerScreen.ticketsListView {

                isVisible()
                hasSize(1)

                childAt<RecyclerScreen.SingleTicket>(0) {
                    ticketTitle.isGone()
                    topTicketIcon.hasDrawable(R.drawable.ic_plane_blue)
                    bottomTicketIcon.hasDrawable(R.drawable.ic_airport)

                    ticketDate {
                        hasText("09/11/19")
                    }

                    stationTimeAndName {
                        hasText("11:20 Edinburgh Airport (EDI)")
                    }

                    destinationNameAndTime {
                        hasText("10:50 Suvarnabhumi Airport (BKK)")
                    }

                    eTicket.isGone()

                    journeyLegs.ticketsListView {
                        firstChild<JourneyLegsScreen.SingleJourneyLeg> {
                            companyName.hasText("Lufthansa (LH 2493)")
                            seatCoach.hasText("Economy - Q")
                            journeyLegDuration.hasText("2hrs 5mins")
                            baggageAllowance.hasText("1 x 23kg bag")
                            arrivalHeader.hasText("14:25 Franz Josef Strauss Airport (MUC)")
                            seatingClass.isGone()
                            routeInformation.isGone()
                            sideIcon.hasDrawable(R.drawable.ic_cloud)
                            topIcon.isGone()
                        }

                        childAt<JourneyLegsScreen.ConnectionJourneyLeg>(1) {
                            travelTypeIcon.hasDrawable(R.drawable.ic_coffee)
                            connectionText.hasText("Connection\n3hrs 55mins")
                        }

                        lastChild<JourneyLegsScreen.SingleJourneyLeg> {
                            companyName.hasText("Eurowings (EW 1206)")
                            departureHeader.hasText("18:20 Franz Josef Strauss Airport (MUC)")
                            arrivalHeader.isGone()
                            seatCoach.hasText("Economy - Q")
                            journeyLegDuration.hasText("10hrs 30mins")
                            baggageAllowance.hasText("1 x 23kg bag")
                            seatingClass.isGone()
                            routeInformation.isGone()
                            sideIcon.hasDrawable(R.drawable.ic_cloud)
                        }
                    }
                }
            }
        }
    }

    @After
    fun after() {
        unloadKoinModules(modules)
        ActivityFinisher.finishOpenActivities()
    }

}*/