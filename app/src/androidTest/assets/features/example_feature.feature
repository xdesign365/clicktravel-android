Feature: Open up the app

  @smoke
  @e2e
  Scenario Outline: Adding text to the textbox
    Given My train is from <source> and to <destination>
    When I load the app
    Then I expect to see <expected>
    Examples:
      | source  | destination | expected |
      | Edinburgh | Portsmouth |
      | Portsmouth   | Edinburgh |