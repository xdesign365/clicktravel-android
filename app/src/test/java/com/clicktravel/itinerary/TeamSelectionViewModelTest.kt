package com.clicktravel.itinerary

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.clicktravel.core.model.team.Team
import com.clicktravel.core.model.team.TeamsAndSelectedTeamIndex
import com.clicktravel.core.usecases.PostUserLoginUseCase
import com.clicktravel.itinerary.features.teamselection.ErrorState
import com.clicktravel.itinerary.features.teamselection.TeamSelected
import com.clicktravel.itinerary.features.teamselection.TeamSelectionRequired
import com.clicktravel.itinerary.features.teamselection.TeamSelectionViewModel
import com.jraska.livedata.test
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.junit.rules.TestRule

class TeamSelectionViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @RelaxedMockK
    lateinit var useCase: PostUserLoginUseCase

    @Before
    fun setup() {
        Dispatchers.setMain(mainThreadSurrogate)
        MockKAnnotations.init(this)
    }


    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    @Ignore
    fun `test that if we dont need to select a team then we get that event from our state`() {
        coEvery { useCase.teamSelectionRequired() } returns false

        val viewModel = TeamSelectionViewModel(useCase)

        viewModel
            .initialise(false)
            .invokeOnCompletion {
                viewModel
                    .state
                    .test()
                    .assertValue(TeamSelected)
            }
    }

    @Test
    @Ignore
    fun `test that if we need to select a team then we get that event from our state`() {
        val teamsAndSelectedTeamIndex = TeamsAndSelectedTeamIndex(teams = emptyList())

        coEvery { useCase.teamSelectionRequired() } returns true
        coEvery { useCase.getTeams() } returns teamsAndSelectedTeamIndex


        val viewModel = TeamSelectionViewModel(useCase)

        viewModel
            .initialise(false)
            .invokeOnCompletion {
                viewModel
                    .state
                    .test()
                    .assertValue(TeamSelectionRequired(teamsAndSelectedTeamIndex))
            }
    }

    @Test
    @Ignore
    fun `updateSelectedTeam selects the team`() {
        val team = Team(
            id = "id",
            name = "name",
            ownerId = "ownerId",
            shortId = "shortId"

        )
        coEvery { useCase.selectTeam(team) } coAnswers { Unit }


        TeamSelectionViewModel(useCase).apply {
            initialise(isChangingTeam = true)
            updateSelectedTeam(team)
        }

        coVerify { useCase.selectTeam(team) }
    }

    @Test
    @Ignore
    fun `if the team is being changed the available teams are shown in the state`() {
        val teamsAndSelectedTeamIndex = TeamsAndSelectedTeamIndex(
            teams = listOf(
                Team(
                    id = "id",
                    name = "name",
                    ownerId = "ownerId",
                    shortId = "shortId"
                )
            )
        )

        coEvery { useCase.teamSelectionRequired() } returns true
        coEvery { useCase.getTeams() } returns teamsAndSelectedTeamIndex


        val viewModel = TeamSelectionViewModel(useCase)

        viewModel
            .initialise(false)
            .invokeOnCompletion {
                viewModel
                    .state
                    .test()
                    .assertValue(TeamSelectionRequired(teamsAndSelectedTeamIndex))
            }
    }

    @Test
    @Ignore
    fun `continueButtonClicked changes the state to TeamSelected`() {
        TeamSelectionViewModel(useCase).apply {
            initialise(false)
                .invokeOnCompletion {
                    continueButtonClicked()
                    state
                        .test()
                        .assertValue(TeamSelected)
                }
        }
    }

    @Test
    @Ignore
    fun `if there is an error getting teams an error state is shown`() {
        coEvery { useCase.getTeams() } coAnswers { throw Exception() }

        TeamSelectionViewModel(useCase).apply {
            initialise(false)
                .invokeOnCompletion {
                    state
                        .test()
                        .assertValue(ErrorState(Exception()))
                }
        }
    }
}