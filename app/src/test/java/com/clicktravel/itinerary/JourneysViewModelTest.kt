package com.clicktravel.itinerary

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.clicktravel.core.model.UserDetails
import com.clicktravel.core.usecases.UserJourneysUseCase
import com.clicktravel.core.util.Event
import com.clicktravel.itinerary.features.journeys.JourneysViewModel
import com.clicktravel.itinerary.repository.SelectedTeamRepository
import com.clicktravel.repository.retriever.NoArgumentRetriever
import com.clicktravel.repository.retriever.NumberOfTimesAppOpenedRepo
import com.jraska.livedata.test
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class JourneysViewModelTest {

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @RelaxedMockK
    private lateinit var journeys: UserJourneysUseCase

    @RelaxedMockK
    private lateinit var selectedTeamRepository: SelectedTeamRepository

    @RelaxedMockK
    private lateinit var numberOfTimesAppOpenedRepo: NumberOfTimesAppOpenedRepo

    @RelaxedMockK
    private lateinit var userDetailsRepository: NoArgumentRetriever<UserDetails?>

    private lateinit var viewModel: JourneysViewModel

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(mainThreadSurrogate)
        MockKAnnotations.init(this)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }


    @Test
    fun `test that when we call visible state changed and the current position is less than the selected we have a trigger to scroll up`() =
        runBlockingTest {
            coEvery { numberOfTimesAppOpenedRepo.getNumber() } returns 1

            viewModel = getViewModel()
            viewModel.visibleStateChanged(1, 0)
            viewModel.toggleState.test()
                .assertValue(JourneysViewModel.ToggleState.SHOW_UP_ICON)
        }


    @Test
    fun `test that when we call visible state changed and the current position is less than the selected we have a trigger to scroll down`() =
        runBlockingTest {
            coEvery { numberOfTimesAppOpenedRepo.getNumber() } returns 1

            viewModel = getViewModel()
            viewModel.visibleStateChanged(0, 1)
            viewModel.toggleState.test()
                .assertValue(JourneysViewModel.ToggleState.SHOW_DOWN_ICON)
        }

    @Test
    fun `test hitting the scroll button resets the toggle state and resets the location to scroll`() =
        runBlockingTest {
            coEvery { numberOfTimesAppOpenedRepo.getNumber() } returns 1

            viewModel = getViewModel()
            viewModel.backToTodayButtonClicked()
            viewModel.toggleState.test()
                .assertValue(JourneysViewModel.ToggleState.HIDE_TOGGLE)

            viewModel.scrollBackToToday.test()
                .assertHasValue()
        }

    @Test
    fun `the user is shown the add mobile number screen if applicable`() = runBlockingTest {
        coEvery { numberOfTimesAppOpenedRepo.getNumber() } returns 5
        coEvery { userDetailsRepository.execute() } returns UserDetails(
            id = "id",
            firstName = "firstName",
            lastName = "lastName",
            mobileNumber = null,
            phoneNumber = null,
            primaryEmailAddress = null,
            title = null
        )
        viewModel = getViewModel()
        viewModel.showAddMobileNumberScreen.test().awaitNextValue().assertValue(Event(Unit))
    }

    @Test
    fun `the user is not shown the add mobile number screen if they already have set one`() =
        runBlockingTest {
            coEvery { numberOfTimesAppOpenedRepo.getNumber() } returns 5
            coEvery { userDetailsRepository.execute() } returns UserDetails(
                id = "id",
                firstName = "firstName",
                lastName = "lastName",
                mobileNumber = "07931110871",
                phoneNumber = null,
                primaryEmailAddress = null,
                title = null
            )
            viewModel = getViewModel()
            viewModel.backToTodayButtonClicked()
            viewModel.showAddMobileNumberScreen.test().assertNoValue()
        }

    private fun getViewModel() = JourneysViewModel(
        journeys = journeys,
        selectedTeamRepository = selectedTeamRepository,
        numberOfTimesAppOpenedRepo = numberOfTimesAppOpenedRepo,
        userDetailsRepository = userDetailsRepository
    )
}