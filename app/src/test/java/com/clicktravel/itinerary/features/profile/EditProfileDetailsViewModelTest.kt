package com.clicktravel.itinerary.features.profile

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.clicktravel.core.usecases.UserDetailsUseCase
import com.clicktravel.core.util.Event
import com.jraska.livedata.test
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
class EditProfileDetailsViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @RelaxedMockK
    private lateinit var userDetailsUseCase: UserDetailsUseCase

    private val data = UserDetailsData(
        id = "id",
        firstName = "firstName",
        lastName = "lastName",
        mobile = "mobileNumber",
        email = "primaryEmailAddress",
        title = "title"
    )

    @Before
    fun setup() {
        Dispatchers.setMain(mainThreadSurrogate)
        MockKAnnotations.init(this)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `initialise correctly sets up live data`() {
        getViewModel().apply {
            initialise(data)

            firstName.test().assertValue("firstName")
            lastName.test().assertValue("lastName")
            email.test().assertValue("primaryEmailAddress")
            mobile.test().assertValue("mobileNumber")
        }
    }

    @Test
    fun `if details have changed the user is prompted before leaving the screen`() {
        getViewModel().apply {
            initialise(data)
            detailsHaveChanged.observeForever { }

            firstName.postValue("new name")
            navigatingAwayFromScreen()

            confirmNavigateAwayWithUnsavedChanges.test().assertValue(Event(Unit))
            leaveEditProfileScreen.test().assertNoValue()
        }
    }

    @Test
    fun `if details have not changed the user is not prompted when leaving the screen`() {
        getViewModel().apply {
            initialise(data)
            detailsHaveChanged.observeForever { }

            navigatingAwayFromScreen()

            leaveEditProfileScreen.test().assertValue(Event(Unit))
            confirmNavigateAwayWithUnsavedChanges.test().assertNoValue()
        }
    }

    @Test
    fun `on success saving new user details success is shown`() = runBlockingTest {
        getViewModel().apply {
            initialise(data)

            firstName.postValue("new name")

            saveChanges().invokeOnCompletion {
                showDetailsUpdatedSuccessfully.test().assertValue(Event(Unit))
                showError.test().assertNoValue()
            }
        }
    }

    @Test
    fun `on error saving new user details an error is shown`() {
        coEvery {
            userDetailsUseCase.updateCurrentUserDetails(any())
        } coAnswers { throw Exception("") }

        getViewModel().apply {
            initialise(data)

            firstName.postValue("new name")
            saveChanges().invokeOnCompletion {
                showError.test().assertValue(Event(Unit))
                showDetailsUpdatedSuccessfully.test().assertNoValue()
            }
        }
    }

    private fun getViewModel() = EditProfileDetailsViewModel(
        userDetailsUseCase = userDetailsUseCase
    )

}