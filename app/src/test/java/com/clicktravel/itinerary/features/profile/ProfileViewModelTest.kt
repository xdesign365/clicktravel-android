package com.clicktravel.itinerary.features.profile

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.clicktravel.core.model.UserDetails
import com.clicktravel.core.usecases.AuthenticationUseCase
import com.clicktravel.core.usecases.UserDetailsUseCase
import com.clicktravel.core.util.Event
import com.clicktravel.itinerary.features.profile.ProfileViewModel.Companion.CLICK_PHONE_NUMBER
import com.jraska.livedata.test
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
class ProfileViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @RelaxedMockK
    private lateinit var authenticationUseCase: AuthenticationUseCase

    @RelaxedMockK
    private lateinit var userDetailsUseCase: UserDetailsUseCase

    private val userDetails = UserDetails(
        id = "id",
        firstName = "firstName",
        lastName = "lastName",
        mobileNumber = "mobileNumber",
        phoneNumber = "phoneNumber",
        primaryEmailAddress = "primaryEmailAddress",
        title = "title"
    )

    @Before
    fun setup() {
        Dispatchers.setMain(mainThreadSurrogate)
        MockKAnnotations.init(this)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `user details are loaded on refreshUserDetails`() = runBlockingTest {
        coEvery { userDetailsUseCase.getUserDetails() } answers { userDetails }

        every {
            userDetailsUseCase.getAvatarUrl(
                firstName = "firstName",
                lastName = "lastName"
            )
        } returns ("FL")

        getViewModel().apply {
            refreshUserDetails()
                .invokeOnCompletion {
                    userDetailsUi
                        .test()
                        .assertValue(
                            UserDetailsUi(
                                fullName = "firstName lastName",
                                email = "primaryEmailAddress",
                                avatarUrl = "FL"
                            )
                        )
                }
        }
    }

    @Test
    fun `an error is shown when error fetching user details`() = runBlockingTest {
        coEvery { userDetailsUseCase.getUserDetails() } coAnswers { throw Exception() }

        getViewModel().apply {
            refreshUserDetails()
                .invokeOnCompletion {
                    showError
                        .test()
                        .awaitValue()
                        .assertValue(Event(Unit))
                }
        }
    }

    @Test
    fun `on editDetailsClicked the user is taken to edit details screen`() = runBlockingTest {
        coEvery { userDetailsUseCase.getUserDetails() } answers { userDetails }

        every {
            userDetailsUseCase.getAvatarUrl(
                firstName = "firstName",
                lastName = "lastName"
            )
        } returns ("FL")

        getViewModel().apply {
            refreshUserDetails()
                .invokeOnCompletion {
                    editDetailsClicked()

                    gotoEditDetailsScreen
                        .test()
                        .awaitValue()
                        .assertValue(Event(userDetails))
                }
        }
    }

    @Test
    fun `on callUsClicked the user is shown the dialer`() = runBlockingTest {
        getViewModel().apply {
            callUsClicked()

            dialClickTravel
                .test()
                .awaitValue()
                .assertValue(Event(CLICK_PHONE_NUMBER))
        }
    }

    @Test
    fun `on logoutClicked the user logged out and kicked to login screen`() = runBlockingTest {
        getViewModel().apply {
            logoutClicked()

            logout
                .test()
                .awaitValue()
                .assertValue(Event(Unit))

            coVerify { authenticationUseCase.logout() }
        }
    }

    private fun getViewModel() = ProfileViewModel(
        userDetailsUseCase = userDetailsUseCase,
        authenticationUseCase = authenticationUseCase
    )

}