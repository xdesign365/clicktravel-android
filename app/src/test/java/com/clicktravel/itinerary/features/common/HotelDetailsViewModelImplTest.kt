package com.clicktravel.itinerary.features.common

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.clicktravel.core.model.*
import com.clicktravel.core.model.hotels.Occupants
import com.clicktravel.core.ui.models.HotelDetailsUi
import com.clicktravel.core.ui.models.TextAndAlignment
import com.clicktravel.core.usecases.HotelUseCase
import com.soywiz.klock.DateTimeTz
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.CoroutineScope
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import kotlin.test.assertEquals

class HotelDetailsViewModelImplTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @RelaxedMockK
    lateinit var useCase: HotelUseCase

    @RelaxedMockK
    lateinit var scope: CoroutineScope

    lateinit var viewModel: HotelDetailsViewModel

    private val booking = HotelBooking(
        bookingId = "bookingId",
        bookingTravelType = TravelTypes.HOTEL,
        cancellationPolicy = "cancellationPolicy",
        numberOfAdults = 3,
        numberOfChildren = 2,
        roomType = "Double",
        mealIncluded = "Breakfast included",
        specialRequest = "Dummy special request",
        checkinDate = DateTimeTz.fromUnixLocal(1579691913000), // Wed, 22 Jan 2020 11:18:33
        checkoutDate = DateTimeTz.fromUnixLocal(1580123913000), // Mon, 27 Jan 2020 11:18:33
        hotelName = "hotelName",
        hotelAddress = "hotelAddress",
        notes = null,
        fields = emptyList(),
        team = "team",
        billingType = BillingType.ROOM_ONLY,
        isPremierInn = false,
        travellerEmail = "travellerEmail",
        billbackElements = HotelBillbackElements(
            allCosts = false,
            breakfast = false,
            roomRate = false,
            parking = false,
            wifi = false,
            mealSupplement = MealSupplementBillbackElement(
                enabled = false,
                allowance = null,
                currency = null
            )
        ),
        supportedBillbackOptions = HotelSupportedBillbackOptions(
            breakfast = false,
            roomRate = false,
            parking = false,
            wifi = false,
            mealSupplement = false
        )
    )

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        viewModel = HotelDetailsViewModelImpl(
            useCase = useCase,
            scope = scope
        )
    }

    @Test
    fun `post and get details works as expected`() {
        with(viewModel) {
            postDetails(booking)

            val expected = HotelDetailsUi(
                occupants = Occupants(
                    adults = 3,
                    children = 2
                ),
                roomDescription = "Double",
                mealIncluded = "Breakfast included",
                specialRequest = "Dummy special request",
                lengthOfStay = 5,
                checkInDate = "22/01/20",
                checkOutDate = "27/01/20",
                billbackText = TextAndAlignment(
                    text = "This booking was paid by credit/debit card.",
                    centerAligned = true
                )
            )

            val actual = getDetails().value

            assertEquals(expected, actual)
        }
    }

    @Test
    fun `resendPaymentInstructionsClicked appropriately shows success state`() {
    }

    // todo can't get these tests to pass
    @Test
    fun `resendPaymentInstructionsClicked appropriately shows error state`() {
//        with(viewModel) {
//
//            coEvery {
//                useCase.resendPaymentInstructions(
//                    teamId = booking.teamId,
//                    bookingId = booking.bookingId,
//                    email = booking.travellerEmail
//                )
//            } coAnswers {
//                throw Exception("")
//            }
//
//            resendPaymentInstructionsClicked(booking)
//
//            val expected = HotelDetailsViewModelState.RESEND_PAYMENT_INSTRUCTIONS_FAILURE
//            val actual = requireNotNull(viewModel.state.value?.getContentIfNotHandled())
//
//            assertEquals(expected, actual)
//        }
    }

}