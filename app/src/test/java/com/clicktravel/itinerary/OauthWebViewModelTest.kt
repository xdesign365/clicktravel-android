package com.clicktravel.itinerary

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.clicktravel.core.model.auth.OauthModel
import com.clicktravel.core.usecases.AuthenticationUseCase
import com.clicktravel.core.usecases.PostUserLoginUseCase
import com.clicktravel.core.usecases.UserDetailsUseCase
import com.clicktravel.itinerary.features.oauth.AuthenticationState
import com.clicktravel.itinerary.features.oauth.OauthWebViewModel
import com.jraska.livedata.test
import com.soywiz.klock.DateTime
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule
import kotlin.test.Test
import kotlin.time.ExperimentalTime
import kotlin.time.hours

@ExperimentalTime
class OauthWebViewModelTest {

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @RelaxedMockK
    private lateinit var authentication: AuthenticationUseCase

    @RelaxedMockK
    private lateinit var userDetailsUseCase: UserDetailsUseCase

    @RelaxedMockK
    private lateinit var postLogin: PostUserLoginUseCase

    private lateinit var viewModel: OauthWebViewModel

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `test initial state should be view splash screen`() = runBlockingTest {
        every { runBlocking { authentication.isUserAlreadyLoggedIn() } } returns null

        viewModel = OauthWebViewModel(
            auth = authentication,
            postLogin = postLogin,
            userDetailsUseCase = userDetailsUseCase,
            oauthUserRepository = mockk(relaxed = true)
        )

        viewModel.state.test()
            .awaitValue()
            .assertValue(AuthenticationState.SHOW_SPLASH)

    }

    @Test
    fun `test that the viewmodel displays the login on first launch`() = runBlockingTest {
        val mockResponse = "Something else"

        // If
        every { authentication.shouldKeepWebView(mockResponse) } returns false
        coEvery { authentication.completeLogin(mockResponse) } returns OauthModel(
            "",
            5.hours.toLongMilliseconds(),
            "",
            ""
        )
        coEvery { authentication.isUserAlreadyLoggedIn() } returns null

        viewModel = OauthWebViewModel(
            auth = authentication,
            postLogin = postLogin,
            userDetailsUseCase = userDetailsUseCase,
            oauthUserRepository = mockk(relaxed = true)
        )

        // When
        viewModel.overrideUrlCallback(mockResponse)

        // Then

        //TODO Fix Threading issue
//        viewModel.state.test()
//            .assertValue(AuthenticationState.SHOW_SPLASH)
//            .awaitNextValue()
//            .assertValue(AuthenticationState.GETTING_CODE)
//            .awaitNextValue()
//            .assertValue(AuthenticationState.DONE)
    }

    @Test
    fun `test that the viewmodel skips login if already authenticated`() = runBlockingTest {
        // If
        coEvery { authentication.isUserAlreadyLoggedIn() } returns OauthModel(
            "",
            DateTime.nowUnixLong() + 10000,
            "",
            ""
        )

        coEvery { postLogin.teamSelectionRequired() } coAnswers { false }

        viewModel = OauthWebViewModel(
            auth = authentication,
            postLogin = postLogin,
            userDetailsUseCase = userDetailsUseCase,
            oauthUserRepository = mockk(relaxed = true)
        )

        viewModel.state.test()
            .awaitValue()
            .assertValue(AuthenticationState.DONE)
    }
}