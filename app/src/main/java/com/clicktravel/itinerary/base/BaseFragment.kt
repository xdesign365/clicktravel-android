package com.clicktravel.itinerary.base

import android.content.DialogInterface
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.addCallback
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.clicktravel.itinerary.features.common.ErrorScrenFragmentDirections
import com.clicktravel.mobile.android.R
import com.clicktravel.itinerary.analytics.CompositeAnalytics
import com.clicktravel.itinerary.analytics.annotations.AnalyticsPageView
import com.clicktravel.itinerary.analytics.annotations.toAnalyticsEvent


open class BaseFragment : Fragment() {

    fun navigateTo(direction: NavDirections) {
        findNavController().navigate(direction)
    }

    fun back() {
        findNavController().navigateUp()
    }

    fun hideBottomBar() = (requireActivity() as BaseActivity).hideBottomBar()

    fun showBottomBar() = (requireActivity() as BaseActivity).showBottomBar()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        this::class.annotations.filterIsInstance<AnalyticsPageView>()
            .map(AnalyticsPageView::toAnalyticsEvent)
            .forEach(CompositeAnalytics::logEvent)
    }

    fun setupToolbar(toolbar: Toolbar) {
        with((requireActivity() as AppCompatActivity)) {
            setSupportActionBar(toolbar)
            toolbar.setNavigationOnClickListener {
                back()
            }
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back)

            title = ""
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                back()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun overrideHardwareBackButtonBehavior(onHardwareBackPressed: () -> Unit) =
        requireActivity()
            .onBackPressedDispatcher
            .addCallback(this) { onHardwareBackPressed() }

    fun showAlertDialog(
        title: String = getString(R.string.oops),
        message: String = getString(R.string.default_error),
        buttonText: String = "Ok",
        onPressed: (DialogInterface, Int) -> Unit = { dialog, _ -> dialog.dismiss() },
        cancelable: Boolean = true,
        onCancel: (DialogInterface) -> Unit = { _ -> Unit }
    ): AlertDialog =
        AlertDialog.Builder(requireContext())
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(buttonText, onPressed)
            .setCancelable(cancelable)
            .setOnCancelListener(onCancel)
            .show()

    fun Fragment.showConfirmationDialog(
        title: String,
        message: String,
        positiveButtonText: String,
        negativeButtonText: String = getString(R.string.cancel),
        onPositiveButtonPressed: (DialogInterface, Int) -> Unit,
        onNegativeButtonPressed: (DialogInterface, Int) -> Unit = { dialog, _ -> dialog.dismiss() },
        cancelable: Boolean = true,
        onCancel: (DialogInterface) -> Unit = { _ -> Unit }
    ): AlertDialog =
        AlertDialog.Builder(requireContext())
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positiveButtonText, onPositiveButtonPressed)
            .setNegativeButton(negativeButtonText, onNegativeButtonPressed)
            .setCancelable(cancelable)
            .setOnCancelListener(onCancel)
            .show()

    open fun showErrorScreen() {
        findNavController().navigate(ErrorScrenFragmentDirections.actionGlobalErrorScrenFragment())
    }

    fun dismissLoadingScreen() {
        val fragmentTransaction = childFragmentManager.beginTransaction()
        val prev = childFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }

        fragmentTransaction.commit()
    }

    fun showLoadingScreen() {
        dismissLoadingScreen()
        val fragmentTransaction = childFragmentManager.beginTransaction()
        fragmentTransaction.addToBackStack(null)
        val dialogFragment = LoadingFragment()
        dialogFragment.show(fragmentTransaction, "dialog")
    }
}
