package com.clicktravel.itinerary.base

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.clicktravel.core.analytics.AnalyticsStatics
import com.clicktravel.core.usecases.AuthenticationUseCase
import com.clicktravel.itinerary.analytics.events.AnalyticsEvent
import com.clicktravel.itinerary.analytics.CompositeAnalytics
import com.clicktravel.itinerary.features.oauth.LoginActivity
import com.clicktravel.mobile.android.BuildConfig
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject


@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    private val authentication: AuthenticationUseCase by inject()
    private val scope = CoroutineScope(Dispatchers.IO)

    override fun onStart() {
        super.onStart()
        if (BuildConfig.DEBUG) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
                val intent =
                    Intent(
                        Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:$packageName")
                    )
                startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION)
            } else {
//                startService(Intent(this, ChatHeadService::class.java))
            }
        }

        if (this::class.java != LoginActivity::class.java) {
            scope.launch {
                if (authentication.isUserAlreadyLoggedIn() == null) {
                    authentication.logout()
                    recordForcedLogoutEvent()

                    withContext(Dispatchers.Main) {
                        this@BaseActivity.startActivity(
                            Intent(
                                this@BaseActivity,
                                LoginActivity::class.java
                            )
                        )
                    }
                }
            }
        }

    }


    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        if (requestCode == CODE_DRAW_OVER_OTHER_APP_PERMISSION) { //Check if the permission is granted or not.
            if (resultCode == Activity.RESULT_OK) {
//                startService(Intent(this, ChatHeadService::class.java))
            } else { //Permission is not available
                Toast.makeText(
                    this,
                    "Draw over other app permission not available.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun hideBottomBar() {
        bottom_nav.visibility = View.GONE
    }

    fun showBottomBar() {
        bottom_nav.visibility = View.VISIBLE
    }

    private fun recordForcedLogoutEvent() {
        CompositeAnalytics.analytics.forEach { analytics ->
            analytics.logEvent(
                AnalyticsEvent(
                    AnalyticsStatics.forcedLogout,
                    mapOf(
                        AnalyticsStatics.logoutLocation to "BaseActivity",
                        AnalyticsStatics.logoutReason to "Triggered User not logged in"
                    )
                )
            )
        }
    }

    companion object {
        private const val CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084
    }

}