package com.clicktravel.itinerary.base


import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.clicktravel.mobile.android.R
import kotlinx.android.synthetic.main.fragment_loading.*

open class LoadingFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return inflater.inflate(R.layout.fragment_loading, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val loadingAnimation = AnimatedVectorDrawableCompat
            .create(requireContext(), R.drawable.animated_loader)

        animatedLoadingIcon.setImageDrawable(loadingAnimation as Drawable)

        loadingAnimation.start()

        loadingAnimation.registerAnimationCallback(
            object : Animatable2Compat.AnimationCallback() {
                var handler = Handler(Looper.getMainLooper())

                override fun onAnimationEnd(drawable: Drawable?) {
                    handler.post { loadingAnimation.start() }
                }
            }
        )
    }
}
