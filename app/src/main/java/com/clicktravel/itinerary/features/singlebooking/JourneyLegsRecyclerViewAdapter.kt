package com.clicktravel.itinerary.features.singlebooking

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.clicktravel.core.ui.models.ConnectionLegUi
import com.clicktravel.core.ui.models.JourneyLegUi
import com.clicktravel.core.ui.models.LegUi
import com.clicktravel.mobile.android.R

class JourneyLegsRecyclerViewAdapter(private val legs: List<LegUi>) :
    RecyclerView.Adapter<TrainLegViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        TrainLegViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                viewType,
                parent,
                false
            )
        )


    override fun getItemCount() = legs.size

    override fun onBindViewHolder(holder: TrainLegViewHolder, position: Int) {
        holder.bind(legs[position])
    }

    override fun getItemViewType(position: Int) =
        when (legs[position]) {
            is JourneyLegUi -> R.layout.train_leg
            is ConnectionLegUi -> R.layout.connection_leg
        }
}