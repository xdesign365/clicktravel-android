package com.clicktravel.itinerary.features.oauth

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.clicktravel.itinerary.MainActivity
import com.clicktravel.itinerary.base.BaseFragment
import com.clicktravel.itinerary.util.observeEvent
import com.clicktravel.itinerary.util.observeModel
import com.clicktravel.mobile.android.R
import com.clicktravel.mobile.android.databinding.LoginSplashScreenBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginSplashScreenFragment : BaseFragment() {

    private val oauthViewModel: OauthWebViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = with(
        DataBindingUtil.inflate<LoginSplashScreenBinding>(
            inflater, R.layout.login_splash_screen, container, false
        )
    ) {
        viewModel = oauthViewModel
        lifecycleOwner = this@LoginSplashScreenFragment
        root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // We do this here to pre-render the webview so it loads faster if we need it
        preRenderLoginWebView()

        observeLiveData()
    }

    private fun observeLiveData() {
        observeModel(oauthViewModel.state) {
            when (it) {
                AuthenticationState.SHOW_WEB_VIEW -> startLogin()
                AuthenticationState.GETTING_CODE -> startLogin()
                AuthenticationState.DONE -> {
                    Intent(requireContext(), MainActivity::class.java).let {
                        requireActivity().startActivity(it)
                        requireActivity().finish()
                    }
                }
                AuthenticationState.SOMETHING_WENT_WRONG -> showAlertDialog()
                AuthenticationState.SHOW_SPLASH -> {
                } // Do nothing, we're here.
                AuthenticationState.SELECT_TEAM -> {
                    dismissLoadingScreen()
                    findNavController().navigate(
                        LoginSplashScreenFragmentDirections.actionLoginSplashScreenFragmentToTeamSelectionFragment()
                    )
                }
            }
        }

        observeEvent(oauthViewModel.openSignUpUrl) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(getString(R.string.signUpUrl))
                )
            )
        }
    }

    private fun startLogin() {
        navigateTo(LoginSplashScreenFragmentDirections.actionLoginSplashScreenFragmentToWebViewFragment())
    }

    private fun preRenderLoginWebView() {
        val view = WebView(requireContext())
        view.webViewClient = WebViewClient()
        view.loadUrl(oauthViewModel.buildLoginUrl())
    }
}