package com.clicktravel.itinerary.features.eticket

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.clicktravel.itinerary.base.BaseFragment
import com.clicktravel.itinerary.util.observeModel
import com.clicktravel.mobile.android.R
import kotlinx.android.synthetic.main.fragment_pdf_web_view.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class PdfWebViewFragment : BaseFragment() {

    val viewModel: PdfViewModel by viewModel()
    val args: PdfWebViewFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_pdf_web_view, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.load(args.booking)

        overrideHardwareBackButtonBehavior { back() }
        setupToolbar(selectTeamToolbar)

        observeModel(viewModel.shouldShowLoadingScreen) {
            if (it) {
                dismissLoadingScreen()
                showLoadingScreen()
            } else {
                dismissLoadingScreen()
            }
        }

        observeModel(viewModel.showErrorMessage) {
            back()
            showAlertDialog()
        }


        observeModel(viewModel.data) {
            dismissLoadingScreen()
            pdfView.fromBytes(it)
                .defaultPage(0)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .enableDoubletap(true)
                .load()
            pdfView.zoomTo(1.5f)
        }
    }
}
