package com.clicktravel.itinerary.features.common

import androidx.lifecycle.MutableLiveData
import com.clicktravel.core.analytics.AnalyticsStatics
import com.clicktravel.core.analytics.ViewCheckinGuideAnalyticsBuilder
import com.clicktravel.core.model.FlightBooking
import com.clicktravel.core.util.Event
import com.clicktravel.itinerary.util.trigger
import com.clicktravel.itinerary.analytics.CompositeAnalytics
import com.clicktravel.itinerary.analytics.events.AnalyticsEvent

interface ViewCheckInGuideViewModel {

    val checkInEmail: MutableLiveData<String>
    val checkInGuideClicked: MutableLiveData<Event<Unit>>

    fun initialise(booking: FlightBooking)
    fun clickedCheckInGuide()
}


class ViewCheckInGuideViewModelImpl(
    private val checkingGuideAnalyticsBuilderTest: ViewCheckinGuideAnalyticsBuilder = ViewCheckinGuideAnalyticsBuilder()
) : ViewCheckInGuideViewModel {

    override val checkInEmail = MutableLiveData<String>()
    override val checkInGuideClicked = MutableLiveData<Event<Unit>>()

    private lateinit var internalBooking: FlightBooking

    override fun clickedCheckInGuide() =
        checkInGuideClicked.trigger()
            .also {
                CompositeAnalytics.logEvent(
                    AnalyticsEvent(
                        AnalyticsStatics.viewCheckinGuide,
                        checkingGuideAnalyticsBuilderTest.build(internalBooking)
                    )
                )
            }

    override fun initialise(booking: FlightBooking) {
        internalBooking = booking
        if (booking.emailRequired) {
            checkInEmail.postValue(booking.emailForCheckIn)
        }
    }
}