package com.clicktravel.itinerary.features.summaryview

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.clicktravel.core.ui.models.*
import com.clicktravel.core.util.exhaustive
import com.clicktravel.core.util.sameDay
import com.clicktravel.mobile.android.R
import com.soywiz.klock.DateTime

@BindingAdapter("dateHeader", requireAll = false)
fun dateHeader(view: TextView, dateHeader: DateHeaderListItem?) {
    dateHeader?.let { dateHeader ->
        if (dateHeader.dateTimeTz.sameDay(DateTime.nowLocal())) {
            view.text = view.context.getString(R.string.today, dateHeader.headerTitle)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                view.setTextColor(view.context.resources.getColor(R.color.accentBlue, null))
            } else {
                view.setTextColor(view.context.resources.getColor(R.color.accentBlue))
            }
        } else {
            view.text = view.context.getString(
                R.string.space_separated,
                dateHeader.dateTimeTz.dayOfWeek.toString(),
                dateHeader.headerTitle
            )
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                view.setTextColor(view.context.resources.getColor(R.color.notTodayTextColor, null))
            } else {
                view.setTextColor(view.context.resources.getColor(R.color.notTodayTextColor))
            }
        }
    }
}

@BindingAdapter("dateHeaderBorder", requireAll = false)
fun dateHeader(view: View, booking: SummaryListItemUi?) {
    booking?.let { booking ->

        when (booking) {
            is UniqueJourneyListItem ->
                if (booking.dateTimeTz.sameDay(DateTime.nowLocal())) {
                    view.background =
                        view.context.resources.getDrawable(R.drawable.summary_today_border, null)
                } else {
                    view.background = null
                }
            is HotelStayListItem -> if (booking.dateTimeTz.sameDay(DateTime.nowLocal())) {
                view.background =
                    view.context.resources.getDrawable(R.drawable.summary_today_border, null)
            } else {
                view.background = null
            }
        }
    }
}


class SummaryViewRecyclerViewAdapter(private val itemSelected: (String, Int) -> Unit) :
    RecyclerView.Adapter<SummaryViewRecyclingViewHolder>() {

    var bookingItem: List<SummaryListItemUi> = listOf()
        set(v) {
            field = v
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) =
        when (viewType) {
            R.layout.summary_list_detail -> {
                UniqueJourneyListItemViewHolder(
                    DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        viewType,
                        parent,
                        false
                    )
                )
            }
            R.layout.date_header_list_item -> {
                DateHeaderListItemViewHolder(
                    DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        viewType,
                        parent,
                        false
                    )
                )
            }
            R.layout.summary_list_hotel -> {
                HotelListItemViewHolder(
                    DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        viewType,
                        parent,
                        false
                    )
                )
            }
            R.layout.summary_list_travel_card -> {
                TravelCardListItemViewHolder(
                    DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        viewType,
                        parent,
                        false
                    )
                )

            }
            else -> {
                throw IllegalArgumentException()
            }
        }

    override fun getItemCount() = bookingItem.size

    override fun onBindViewHolder(holder: SummaryViewRecyclingViewHolder, position: Int) {
        holder.bind(bookingItem[position])

        bookingItem[position].let { selectedItem ->
            when (selectedItem) {
                is UniqueJourneyListItem -> holder.itemView.setOnClickListener {
                    itemSelected(selectedItem.bookingId, selectedItem.journeyNumber)
                }
                is HotelStayListItem -> holder.itemView.setOnClickListener {
                    itemSelected(selectedItem.bookingId, 0)
                }
                is TravelCardListItem -> holder.itemView.setOnClickListener {
                    itemSelected(selectedItem.bookingId, 0)
                }
                is DateHeaderListItem -> Unit // don't add click listener for a date header
            }.exhaustive
        }
    }

    override fun getItemViewType(position: Int) = when (bookingItem[position]) {
        is DateHeaderListItem -> R.layout.date_header_list_item
        is UniqueJourneyListItem -> R.layout.summary_list_detail
        is HotelStayListItem -> R.layout.summary_list_hotel
        is TravelCardListItem -> R.layout.summary_list_travel_card
    }
}


