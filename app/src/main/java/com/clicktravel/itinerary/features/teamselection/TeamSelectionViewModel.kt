package com.clicktravel.itinerary.features.teamselection

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.clicktravel.core.analytics.AnalyticsStatics
import com.clicktravel.core.model.team.Team
import com.clicktravel.core.model.team.TeamsAndSelectedTeamIndex
import com.clicktravel.core.usecases.PostUserLoginUseCase
import com.clicktravel.itinerary.util.CompositeLogger
import com.clicktravel.itinerary.analytics.CompositeAnalytics
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch

sealed class TeamSelectionState
object TeamSelected : TeamSelectionState()
object Loading : TeamSelectionState()
data class TeamSelectionRequired(val team: TeamsAndSelectedTeamIndex) : TeamSelectionState()
data class ErrorState(val error: Exception) : TeamSelectionState()


class TeamSelectionViewModel(
    private val teamSelectionUseCase: PostUserLoginUseCase
) : ViewModel() {

    val state = MutableLiveData<TeamSelectionState>()

    fun initialise(isChangingTeam: Boolean) = viewModelScope.launch(errorHandler()) {
        state.postValue(Loading)
        if (isChangingTeam) setupForChangingTeam() else setupForInitialTeamSelection()
    }

    fun continueButtonClicked() = state.postValue(TeamSelected)

    fun updateSelectedTeam(team: Team) {
        viewModelScope.launch {
            teamSelectionUseCase.selectTeam(team)
        }
    }

    private suspend fun setupForChangingTeam() =
        state.postValue(TeamSelectionRequired(getTeams()))

    private suspend fun setupForInitialTeamSelection() =
        if (teamSelectionUseCase.teamSelectionRequired()) {
            val teams = getTeams().also {
                if (it.teams.isNotEmpty()) {
                    updateSelectedTeam(it.teams.first())
                }
            }
            state.postValue(TeamSelectionRequired(teams))
        } else {
            state.postValue(TeamSelected)
        }

    private suspend fun getTeams(): TeamsAndSelectedTeamIndex =
        teamSelectionUseCase.getTeams().also {
            setHasMultipleTeamsUserProperty(hasMultipleTeams = it.teams.size > 1)
        }

    private fun setHasMultipleTeamsUserProperty(hasMultipleTeams: Boolean) =
        CompositeAnalytics.setBooleanUserProperty(
            AnalyticsStatics.hasMultipleTeams,
            hasMultipleTeams
        )

    private fun errorHandler() = CoroutineExceptionHandler { _, error ->
        CompositeLogger.logError(error)
        state.postValue(ErrorState(error as Exception))
    }
}