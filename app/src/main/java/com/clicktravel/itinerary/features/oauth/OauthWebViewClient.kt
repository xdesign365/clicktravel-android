package com.clicktravel.itinerary.features.oauth

import android.webkit.WebView
import android.webkit.WebViewClient

class OauthWebViewClient(private val viewModel: OauthWebViewModel) : WebViewClient() {

    override fun shouldOverrideUrlLoading(view: WebView, url: String) = false.also {
        viewModel.overrideUrlCallback(url)
    }

}