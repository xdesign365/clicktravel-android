package com.clicktravel.itinerary.features.singlebooking

enum class ArrivalOrDeparture {
    ARRIVAL,
    DEPARTURE
}