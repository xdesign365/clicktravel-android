package com.clicktravel.itinerary.features.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.clicktravel.core.analytics.AnalyticsStatics
import com.clicktravel.core.model.UserDetails
import com.clicktravel.core.usecases.AuthenticationUseCase
import com.clicktravel.core.usecases.UserDetailsUseCase
import com.clicktravel.core.util.DateFormatter
import com.clicktravel.core.util.Event
import com.clicktravel.core.util.UtcDateFormatter
import com.clicktravel.itinerary.util.trigger
import com.soywiz.klock.DateTimeTz
import com.clicktravel.itinerary.analytics.CompositeAnalytics
import com.clicktravel.itinerary.analytics.events.AnalyticsEvent
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch

data class UserDetailsUi(
    val fullName: String,
    val email: String?,
    val avatarUrl: String
)

class ProfileViewModel(
    private val userDetailsUseCase: UserDetailsUseCase,
    private val authenticationUseCase: AuthenticationUseCase,
    private val dateFormatter: DateFormatter = UtcDateFormatter()
) : ViewModel() {

    val dialClickTravel = MutableLiveData<Event<String>>()
    val showError = MutableLiveData<Event<Unit>>()
    val logout = MutableLiveData<Event<Unit>>()
    val showLoading = MutableLiveData<Event<Unit>>()
    val dismissLoading = MutableLiveData<Event<Unit>>()
    val gotoEditDetailsScreen = MutableLiveData<Event<UserDetails>>()
    val userDetailsUi = MutableLiveData<UserDetailsUi>()

    lateinit var userDetails: UserDetails

    fun refreshUserDetails() = loadUserDetails()

    fun editDetailsClicked() = gotoEditDetailsScreen.trigger(userDetails)

    fun callUsClicked() {
        dialClickTravel.trigger(CLICK_PHONE_NUMBER)
        logInitiateCallAnalyticsEvent()
    }

    fun logoutClicked() = viewModelScope.launch(errorHandler()) {
        authenticationUseCase.logout()
        logout.trigger()
    }

    private fun logInitiateCallAnalyticsEvent() = viewModelScope.launch {
        CompositeAnalytics.logEvent(
            AnalyticsEvent(
                AnalyticsStatics.initiateCall,
                mapOf(
                    AnalyticsStatics.date to dateFormatter.format(DateTimeTz.nowLocal())
                )
            )
        )
    }

    private fun loadUserDetails() = viewModelScope.launch(errorHandler()) {
        userDetailsUseCase.getUserDetails().let {
            userDetails = it
            userDetailsUi.value = UserDetailsUi(
                fullName = "${it.firstName} ${it.lastName}",
                email = it.primaryEmailAddress,
                avatarUrl = userDetailsUseCase.getAvatarUrl(
                    firstName = it.firstName,
                    lastName = it.lastName
                )
            )
        }
    }

    private fun errorHandler() = CoroutineExceptionHandler { _, _ ->
        showError.trigger()
    }

    companion object {
        const val CLICK_PHONE_NUMBER = "0844 745 9649"
    }

}