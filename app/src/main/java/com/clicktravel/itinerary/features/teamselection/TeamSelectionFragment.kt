package com.clicktravel.itinerary.features.teamselection

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.clicktravel.core.util.exhaustive
import com.clicktravel.itinerary.MainActivity
import com.clicktravel.itinerary.base.BaseFragment
import com.clicktravel.itinerary.util.observeModel
import com.clicktravel.mobile.android.R
import kotlinx.android.synthetic.main.team_selection_screen.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class TeamSelectionFragment : BaseFragment() {

    private val teamSelectionViewModel: TeamSelectionViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.team_selection_screen, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        teamSelectionViewModel.initialise(isChangingTeam = false)

        observeModel(teamSelectionViewModel.state) {
            dismissLoadingScreen()

            when (it) {
                Loading -> showLoadingScreen()
                TeamSelected -> handleDone()
                is TeamSelectionRequired -> {
                    teamListSelection.adapter = SingleTeamRecyclerViewAdapter(
                        teamsAndSelectedTeamIndex = it.team,
                        onTeamSelected = teamSelectionViewModel::updateSelectedTeam
                    )
                }
                is ErrorState -> showErrorScreen()
            }.exhaustive
        }

        goToTravelButton.setOnClickListener {
            teamSelectionViewModel.continueButtonClicked()
        }
    }

    override fun showErrorScreen() =
        findNavController().navigate(TeamSelectionFragmentDirections.actionTeamSelectionFragmentToErrorScrenFragmentLogin())

    private fun handleDone() {
        startActivity(Intent(requireContext(), MainActivity::class.java))
        requireActivity().finish()
    }
}