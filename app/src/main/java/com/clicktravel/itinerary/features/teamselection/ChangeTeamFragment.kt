package com.clicktravel.itinerary.features.teamselection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.clicktravel.core.util.exhaustive
import com.clicktravel.itinerary.base.BaseFragment
import com.clicktravel.itinerary.util.observeModel
import com.clicktravel.mobile.android.R
import kotlinx.android.synthetic.main.team_selection_screen.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class ChangeTeamFragment : BaseFragment() {

    private val teamSelectionViewModel: TeamSelectionViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.team_selection_screen, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar(selectTeamToolbar)
        selectTeamToolbar.setNavigationOnClickListener { back() }
        overrideHardwareBackButtonBehavior { back() }
        goToTravelButton.visibility = View.GONE

        teamSelectionViewModel.initialise(isChangingTeam = true)

        observeState()
    }

    override fun showErrorScreen() {
        back()
        showAlertDialog()
    }

    private fun observeState() = observeModel(teamSelectionViewModel.state) {
        dismissLoadingScreen()

        if (it is TeamSelectionRequired) {
            teamListSelection.adapter = SingleTeamRecyclerViewAdapter(
                teamsAndSelectedTeamIndex = it.team,
                onTeamSelected = { team ->
                    teamSelectionViewModel.updateSelectedTeam(team)
                }
            )
        }
    }
}