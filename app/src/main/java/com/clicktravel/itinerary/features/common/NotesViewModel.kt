package com.clicktravel.itinerary.features.common

import androidx.lifecycle.MutableLiveData
import com.clicktravel.core.model.trains.Note

interface NotesViewModel {
    fun getNotes(): MutableLiveData<Note>
    fun postNote(note: Note)
    fun expandNotes(): MutableLiveData<Boolean>
    fun expandNotesClicked()
}

class NotesViewModelImpl : NotesViewModel {
    private val internalNotes = MutableLiveData<Note>()
    private val expandNote = MutableLiveData<Boolean>().apply { value = true }

    override fun getNotes() = internalNotes
    override fun postNote(note: Note) {
        internalNotes.postValue(note)
    }

    override fun expandNotes() = expandNote
    override fun expandNotesClicked() {
        expandNote.postValue(expandNote.value?.not())
    }
}