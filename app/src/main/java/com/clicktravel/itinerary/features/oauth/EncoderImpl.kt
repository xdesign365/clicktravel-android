package com.clicktravel.itinerary.features.oauth

import android.util.Base64
import com.clicktravel.core.feature.oauth.Encoder
import java.nio.charset.Charset

class EncoderImpl : Encoder {
    override fun base64Encode(stringToEncode: String) =
        Base64.encodeToString(
            stringToEncode.toByteArray(charset = Charset.defaultCharset()),
            Base64.URL_SAFE
        ).replace("=", "")

    override fun base64Decode(bytesToDecode: ByteArray): ByteArray =
        Base64.decode(bytesToDecode, Base64.DEFAULT)

    override fun base64Encode(bytesToEncode: ByteArray): String =
        Base64.encodeToString(bytesToEncode, Base64.URL_SAFE)
}