package com.clicktravel.itinerary.features.eticket

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.clicktravel.backend.retriever.PdfRetrieverArgs
import com.clicktravel.core.model.TrainBooking
import com.clicktravel.core.model.team.Team
import com.clicktravel.core.usecases.UserJourneysUseCase
import com.clicktravel.core.util.Event
import com.clicktravel.itinerary.navigation.SelectedJourney
import com.clicktravel.itinerary.repository.SelectedTeamRepository
import com.clicktravel.itinerary.util.CompositeLogger
import com.clicktravel.repository.retriever.NoArgumentRetriever
import com.clicktravel.repository.usecases.GetPdfForTicketUseCase
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch

class PdfViewModel(
    private val userJourneysUseCase: UserJourneysUseCase,
    private val pdfUseCase: GetPdfForTicketUseCase,
    private val selectedTeamRetriever: SelectedTeamRepository
) :
    ViewModel() {

    val data = MutableLiveData<ByteArray>()
    val shouldShowLoadingScreen = MutableLiveData<Boolean>()
    val showErrorMessage = MutableLiveData<Event<Unit>>()


    fun load(bookingItem: SelectedJourney) = viewModelScope.launch(context = errorHandler()) {

        val item =
            userJourneysUseCase.getBookingFromId(bookingItem.bookingId) as TrainBooking

        shouldShowLoadingScreen.postValue(true)
        data.postValue(
            pdfUseCase.getPdfForTicket(
                PdfRetrieverArgs(
                    bookingId = bookingItem.bookingId,
                    couponId = item.journeys.first { it.journeyId == bookingItem.journeyId }.couponId!!,
                    teamId = selectedTeamRetriever.execute()?.id!!
                )
            )
        )
        shouldShowLoadingScreen.postValue(false)
    }

    private fun errorHandler() = CoroutineExceptionHandler { _, throwable ->
        showErrorMessage.postValue(Event(Unit))
        CompositeLogger.logError(throwable)
    }


}