package com.clicktravel.itinerary.features.singlebooking

import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.clicktravel.core.ui.models.ConnectionLegUi
import com.clicktravel.core.ui.models.JourneyLegUi
import com.clicktravel.core.ui.models.LegUi
import com.clicktravel.core.util.exhaustive

class TrainLegViewHolder(private val binding: ViewDataBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(legUi: LegUi) {

        when (legUi) {
            is JourneyLegUi -> {
                binding.setVariable(BR.leg, legUi)
            }
            is ConnectionLegUi -> {
                binding.setVariable(BR.connection, legUi)
            }
        }.exhaustive


    }

}
