package com.clicktravel.itinerary.features.singlebooking

import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.core.view.isGone
import androidx.databinding.BindingAdapter
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.clicktravel.core.model.trains.ConnectionOrJourneyType
import com.clicktravel.core.ui.models.Icon
import com.clicktravel.core.ui.models.LegUi
import com.clicktravel.core.ui.models.TicketUi
import com.clicktravel.core.util.exhaustive
import com.clicktravel.itinerary.util.setDrawable
import com.clicktravel.itinerary.util.toDrawable
import com.clicktravel.mobile.android.R
import com.clicktravel.mobile.android.databinding.SingleTicketItemBinding


@BindingAdapter("sideBarIcon", requireAll = false)
fun sideBarIcon(view: View, trainTicketType: ConnectionOrJourneyType) {
    view.setBackgroundResource(
        when (trainTicketType) {
            ConnectionOrJourneyType.FERRY -> R.drawable.repeating_wave
            ConnectionOrJourneyType.TRAIN -> R.drawable.repeating_rail
            ConnectionOrJourneyType.FLIGHT -> R.drawable.ic_cloud
            ConnectionOrJourneyType.BUS -> R.drawable.ic_cloud
            else -> R.drawable.ic_lined_connection
        }.exhaustive
    )
}

@BindingAdapter("seatReservationVisibility", requireAll = false)
fun seatReservationVisibility(view: View, seatReservation: String?) {
    view.isGone = seatReservation == null
}

@BindingAdapter("icon", requireAll = false)
fun showIcon(view: ImageView, icon: Icon) {
    when (icon) {
        Icon.NONE -> view.visibility = View.GONE
        else -> view.setDrawable(icon.toDrawable())
    }
}

@BindingAdapter("baggageAllowanceVisibility", requireAll = false)
fun baggageAllowanceVisibility(view : View, baggageString : String?) {
    view.isGone = baggageString.isNullOrBlank()
}

@BindingAdapter("connectionIcon", requireAll = false)
fun getConnectionIcon(view: ImageView, connectionLeg: LegUi?) {
    connectionLeg?.type?.toDrawable()
        ?.let { view.setDrawable(it) }
}


interface ExpandClickListener {
    fun getExpanded(): MutableLiveData<Boolean>
    fun expandClicked()
}

class TrainTicketViewHolder(private val binding: SingleTicketItemBinding, private val eTicketClick : (TicketUi) -> Unit) :
    RecyclerView.ViewHolder(binding.root), ExpandClickListener, LifecycleOwner {

    private val liveData = MutableLiveData<Boolean>()
    private val lifecycleRegistry = LifecycleRegistry(this)

    override fun getExpanded() = liveData

    override fun expandClicked() {
        if (liveData.hasActiveObservers().not()) {
            markAttach()
        }
        liveData.postValue(liveData.value?.not())
    }

    init {
        lifecycleRegistry.currentState = Lifecycle.State.INITIALIZED
    }

    fun markAttach() {
        lifecycleRegistry.currentState = Lifecycle.State.STARTED
    }

    fun markDetach() {
        lifecycleRegistry.currentState = Lifecycle.State.DESTROYED
    }


    fun bind(ticketUi: TicketUi) {
        liveData.postValue(ticketUi.selectedTicket)
        binding.ticket = ticketUi
        binding.clickListener = this
        binding.lifecycleOwner = this

        with(binding.root.findViewById<RecyclerView>(R.id.journeyLegs)) {
            layoutManager = LinearLayoutManager(context)
            adapter = JourneyLegsRecyclerViewAdapter(ticketUi.legs)
        }

        binding.root.findViewById<Button>(R.id.eTicketButton).setOnClickListener {
            eTicketClick(ticketUi)
        }

    }

    override fun getLifecycle() = lifecycleRegistry
}