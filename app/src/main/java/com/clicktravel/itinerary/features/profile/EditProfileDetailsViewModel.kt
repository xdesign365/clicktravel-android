package com.clicktravel.itinerary.features.profile

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.clicktravel.core.analytics.AnalyticsStatics
import com.clicktravel.core.model.UserDetails
import com.clicktravel.core.usecases.UserDetailsUseCase
import com.clicktravel.core.util.Event
import com.clicktravel.itinerary.util.trigger
import com.clicktravel.itinerary.analytics.CompositeAnalytics
import com.clicktravel.itinerary.analytics.events.AnalyticsEvent
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch

class EditProfileDetailsViewModel(
    private val userDetailsUseCase: UserDetailsUseCase
) : ViewModel() {

    val showError = MutableLiveData<Event<Unit>>()
    val showLoading = MutableLiveData<Event<Unit>>()
    val dismissLoading = MutableLiveData<Event<Unit>>()
    val showDetailsUpdatedSuccessfully = MutableLiveData<Event<Unit>>()
    val confirmNavigateAwayWithUnsavedChanges = MutableLiveData<Event<Unit>>()
    val leaveEditProfileScreen = MutableLiveData<Event<Unit>>()

    val firstName = MutableLiveData<String>()
    val lastName = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val mobile = MutableLiveData<String>()

    val detailsHaveChanged = MediatorLiveData<Boolean>().apply { value = false }

    private lateinit var originalUserDetails: UserDetails
    private lateinit var userDetails: UserDetails

    fun initialise(data: UserDetailsData) {
        originalUserDetails = data.toUserDetails()
        userDetails = data.toUserDetails()
            .also {
                firstName.value = it.firstName
                lastName.value = it.lastName
                email.value = it.primaryEmailAddress
                mobile.value = it.mobileNumber
            }
        listenForDetailsBeingChanged()
    }

    fun navigatingAwayFromScreen() = if (detailsHaveChanged.value == true) {
        confirmNavigateAwayWithUnsavedChanges.trigger()
    } else {
        leaveEditProfileScreen.trigger()
    }

    fun saveChanges() = viewModelScope.launch(errorHandler()) {
        showLoading.trigger()
        userDetailsUseCase.updateCurrentUserDetails(userDetails)
        dismissLoading.trigger()
        showDetailsUpdatedSuccessfully.trigger()
        detailsHaveChanged.value = false
        fireUpdateProfileAnalyticsEvent()
        originalUserDetails = userDetails
    }

    private fun fireUpdateProfileAnalyticsEvent() {
        val nameUpdated =
            (originalUserDetails.firstName != userDetails.firstName) ||
                    (originalUserDetails.lastName != userDetails.lastName)
        val emailUpdated =
            originalUserDetails.primaryEmailAddress != userDetails.primaryEmailAddress
        val mobileUpdated =
            originalUserDetails.mobileNumber != userDetails.mobileNumber

        CompositeAnalytics.logEvent(
            AnalyticsEvent(
                event = AnalyticsStatics.updateProfile,
                properties = mapOf(
                    AnalyticsStatics.nameUpdated to nameUpdated,
                    AnalyticsStatics.emailUpdated to emailUpdated,
                    AnalyticsStatics.mobileUpdated to mobileUpdated
                )
            )
        )
    }

    private fun listenForDetailsBeingChanged() {
        detailsHaveChanged.apply {
            addSource(firstName) { firstName ->
                if (firstName != userDetails.firstName) {
                    value = true
                    userDetails = userDetails.copy(firstName = firstName)
                }
            }
            addSource(lastName) { lastName ->
                if (lastName != userDetails.lastName) {
                    value = true
                    userDetails = userDetails.copy(lastName = lastName)
                }
            }
            addSource(email) { email ->
                if (email != userDetails.primaryEmailAddress) {
                    value = true
                    userDetails = userDetails.copy(primaryEmailAddress = email)
                }
            }
            addSource(mobile) { mobile ->
                if (mobile != userDetails.mobileNumber) {
                    value = true
                    userDetails = userDetails.copy(mobileNumber = mobile)
                }
            }
        }
    }

    private fun errorHandler() = CoroutineExceptionHandler { _, _ ->
        showError.trigger()
    }

}
