package com.clicktravel.itinerary.features.teamselection

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.clicktravel.core.model.team.Team
import com.clicktravel.core.model.team.TeamsAndSelectedTeamIndex
import com.clicktravel.mobile.android.R
import kotlinx.android.synthetic.main.single_team_item.view.*

data class TeamUi(
    val selected: Boolean,
    val team: Team
)


class TeamListItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    fun bind(
        teamUi: TeamUi,
        onTeamSelected: (TeamUi) -> Unit
    ) {
        view.apply {
            teamName.text = teamUi.team.name
            teamSelected.isChecked = teamUi.selected
            teamItem.setOnClickListener {
                onTeamSelected(teamUi)
            }
        }
    }
}


class SingleTeamRecyclerViewAdapter(
    teamsAndSelectedTeamIndex: TeamsAndSelectedTeamIndex,
    val onTeamSelected: (Team) -> Unit
) : RecyclerView.Adapter<TeamListItemViewHolder>() {

    private var teams = teamsAndSelectedTeamIndex.teams.mapIndexed { index, team ->
        TeamUi(
            selected = index == teamsAndSelectedTeamIndex.selectedIndex,
            team = team
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        TeamListItemViewHolder(
            LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        )

    override fun getItemCount() = teams.size

    override fun onBindViewHolder(holder: TeamListItemViewHolder, position: Int) =
        holder.bind(
            teamUi = teams[position],
            onTeamSelected = { teamUi ->

                if (teamUi.selected.not()) {
                    onTeamSelected(teamUi.team)

                    val newTeams = teams.mapIndexed { index, item ->
                        item.copy(selected = index == position)
                    }

                    teams = newTeams
                    notifyDataSetChanged()
                }
            }
        )

    override fun getItemViewType(position: Int) = R.layout.single_team_item
}
