package com.clicktravel.itinerary.features.singlebooking


import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.clicktravel.core.Settings
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.hotels.Occupants
import com.clicktravel.core.model.trains.enums.DeliveryOption
import com.clicktravel.core.ui.models.ClickLocation
import com.clicktravel.core.ui.models.DeliveryAddressUi
import com.clicktravel.core.ui.models.JourneyHeader
import com.clicktravel.core.ui.models.TicketUi
import com.clicktravel.core.util.YearDisplayFormatter
import com.clicktravel.core.util.exhaustive
import com.clicktravel.itinerary.base.BaseFragment
import com.clicktravel.itinerary.features.common.CustomFieldsRecyclerViewAdapter
import com.clicktravel.itinerary.features.common.HotelDetailsViewModelState
import com.clicktravel.itinerary.util.observeEvent
import com.clicktravel.itinerary.util.observeModel
import com.clicktravel.mobile.android.R
import com.clicktravel.mobile.android.databinding.SingleJourneyBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.appbar.MaterialToolbar
import com.soywiz.klock.DateTimeTz
import kotlinx.android.synthetic.main.custom_field_view.*
import kotlinx.android.synthetic.main.single_journey.*
import org.koin.android.ext.android.inject


@BindingAdapter("toolbarTitle", requireAll = false)
fun toolbarTitle(toolbar: MaterialToolbar, title: String?) {
    title?.let { toolbar.title = it }
}

@SuppressLint("SetTextI18n")
@BindingAdapter("flightCheckInEmail", requireAll = false)
fun flightCheckInEmail(view: TextView, email: String?) {
    view.text = view.context.getString(R.string.read_our_check_in_guide_for_instructions) +
            if (email != null)
                " ${view.context.getString(R.string.use_email_to_check_in_online, email)}"
            else
                ""
}

@BindingAdapter("yearDisplayTravelDate", requireAll = false)
fun yearDisplayTravelDate(view: TextView, date: DateTimeTz?) {
    date?.let {
        view.text = String.format(
            view.context.getString(
                R.string.travel_date,
                YearDisplayFormatter().format(it)
            )
        )
    }
}

@BindingAdapter("hasRailcard", requireAll = false)
fun hasRailcard(view: View, journeyHeader: JourneyHeader?) {
    view.isGone = journeyHeader?.isHasRailcard()?.not() ?: true
}

@BindingAdapter("hasDelivery", requireAll = false)
fun hasDelivery(view: View, deliveryAddress: DeliveryAddressUi?) {
    view.isGone = deliveryAddress?.deliveryAddress == null
}

@BindingAdapter("hasTrainTicketCollection", requireAll = false)
fun hasTrainTicketCollection(view: View, journeyHeader: JourneyHeader?) {
    val shouldHide = journeyHeader?.isShowTicketReference()?.not() ?: true
    view.isGone = shouldHide || journeyHeader?.travelType != TravelTypes.TRAIN
}

@BindingAdapter("hasFlightTicketCollection", requireAll = false)
fun hasFlightTicketCollection(view: View, journeyHeader: JourneyHeader?) {
    val shouldHide = journeyHeader?.isShowTicketReference()?.not() ?: true
    view.isGone = shouldHide || journeyHeader?.travelType != TravelTypes.FLIGHT
}

@BindingAdapter("hasTravelcardCollection", requireAll = false)
fun hasTravelcardCollection(view: View, journeyHeader: JourneyHeader?) {
    val shouldHide = journeyHeader?.isShowTicketReference()?.not() ?: true
    view.isGone = shouldHide || journeyHeader?.travelType != TravelTypes.TRAVELCARD
}

@BindingAdapter("hasETicket", requireAll = false)
fun hasETicket(view: View, ticket: TicketUi?) {
    view.isGone = ticket?.ticketType != DeliveryOption.E_TICKET
}


@BindingAdapter("deliveryOptionText", requireAll = false)
fun deliveryOptionTextView(view: TextView, deliveryAddress: DeliveryAddressUi?) {
    deliveryAddress?.let {
        val type: Int? = when (it.deliveryOption) {
            DeliveryOption.POST -> R.string.post
            DeliveryOption.SPECIAL_DELIVERY -> R.string.royal_mail_special_delivery
            DeliveryOption.FIRST_CLASS_POST -> R.string.first_class_post
            else -> {
                view.visibility = View.GONE
                null
            }
        }.exhaustive

        type?.let {
            view.text = with(StringBuilder()) {
                append(view.context.getString(it))

                deliveryAddress.forTheAttentionOf?.let { fao ->
                    append(" ")
                    append(
                        String.format(
                            view.context.getString(
                                R.string.for_the_attention_of,
                                fao
                            )
                        )
                    )
                }

                toString()
            }
        }
    }
}

@BindingAdapter("journeyTitle", "departureOrArrival", requireAll = false)
fun locationTitle(view: TextView, ticket: TicketUi?, departureOrArrival: ArrivalOrDeparture?) {
    view.text = when (departureOrArrival) {
        ArrivalOrDeparture.ARRIVAL -> (ticket?.arrivalTitle)
        ArrivalOrDeparture.DEPARTURE -> (ticket?.departureTitle)
        null -> ""
    }
}

@BindingAdapter("occupants", requireAll = false)
fun hotelOccupantsTextView(view: TextView, occupants: Occupants?) {
    occupants?.apply {
        val childrenExist = occupants.children != 0
        val adultsString = view.context.resources.getQuantityString(
            R.plurals.number_of_adults,
            occupants.adults,
            occupants.adults
        )
        val childrenString = view.context.resources.getQuantityString(
            R.plurals.number_of_children,
            occupants.children,
            occupants.children
        )
        val occupantsString = "$adultsString${if (childrenExist) ", $childrenString" else ""}"
        view.text = occupantsString
    }
}

class SingleJourneyDetailsFragment : BaseFragment() {

    private val singleJourneyDetailsViewModel: SingleJourneyDetailsViewModel by inject()
    private val args: SingleJourneyDetailsFragmentArgs by navArgs()
    private val trainTickerAdapter =
        TrainTicketRecyclerViewAdapter(singleJourneyDetailsViewModel::clickedETicket)

    private val customFieldListAdapter = CustomFieldsRecyclerViewAdapter()

    private lateinit var menu: Menu

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = with(
        DataBindingUtil.inflate<SingleJourneyBinding>(
            inflater, R.layout.single_journey, container, false
        )
    ) {
        viewModel = singleJourneyDetailsViewModel
        tripSummary.viewModel = singleJourneyDetailsViewModel
        tripSummary.ticketCollection.viewModel = singleJourneyDetailsViewModel
        tripSummary.ticketCollectionFlight.viewModel = singleJourneyDetailsViewModel
        deliveryView.viewModel = singleJourneyDetailsViewModel
        fullHotelSection.singleJourneyDetailsViewModel = singleJourneyDetailsViewModel
        fullHotelSection.hotelDetailsViewModel = singleJourneyDetailsViewModel.hotelDetailsViewModel
        fullHotelSection.hotelDetails.viewModel =
            singleJourneyDetailsViewModel.hotelDetailsViewModel
        fullHotelSection.hotelStayDuration.viewModel =
            singleJourneyDetailsViewModel.hotelDetailsViewModel
        notesView.viewModel = singleJourneyDetailsViewModel
        customFieldView.viewModel = singleJourneyDetailsViewModel
        checkinGuide.viewModel = singleJourneyDetailsViewModel
        lifecycleOwner = this@SingleJourneyDetailsFragment
        cancellationPolicy.viewModel = singleJourneyDetailsViewModel
        hotelHeader.viewModel = singleJourneyDetailsViewModel
        travelCardHeader.viewModel = singleJourneyDetailsViewModel
        travelCardHeader.cardCollectionBubble.viewModel = singleJourneyDetailsViewModel
        root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ticketsListView.layoutManager = LinearLayoutManager(requireContext())
        ticketsListView.adapter = trainTickerAdapter

        customFieldList.layoutManager = LinearLayoutManager(requireContext())
        customFieldList.adapter = customFieldListAdapter

        mapView.onCreate(savedInstanceState)

        setupToolbar(selectTeamToolbar)
        setHasOptionsMenu(true)
        setHardwareBackButtonBehavior()

        observeJourneyHeader()
        observeCustomFields()
        observeJourneyLegs()
        observeETicket()
        observeCheckInGuideClicked()
        observeViewBookingClicked()
        observeCancelBookingClicked()
        observeHotelViewModelState()
        observeCallHotel()

        singleJourneyDetailsViewModel.initialise(args.booking)
    }

    override fun onStart() {
        super.onStart()
        mapView?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView?.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        this.menu = menu
        inflater.inflate(R.menu.manage_booking, menu)
        observeCallHotelMenuOptionVisible()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.manage_booking -> singleJourneyDetailsViewModel.clickedManagedBooking()
            R.id.cancel_booking -> singleJourneyDetailsViewModel.clickedCancelBooking()
            R.id.call_hotel -> singleJourneyDetailsViewModel.callHotelClicked()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun observeCheckInGuideClicked() {
        observeEvent(singleJourneyDetailsViewModel.checkInGuideClicked) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(getString(R.string.checkinUrl))
                )
            )
        }
    }

    private fun observeHotelViewModelState() =
        observeEvent(singleJourneyDetailsViewModel.hotelDetailsViewModel.state) { state ->
            when (state) {
                HotelDetailsViewModelState.SHOW_LOADING -> showLoadingScreen()
                HotelDetailsViewModelState.RESEND_PAYMENT_INSTRUCTIONS_SUCCESS -> {
                    dismissLoadingScreen()
                    showAlertDialog(
                        title = getString(R.string.payment_instructions),
                        message = getString(R.string.resend_payment_instructions_successful)
                    )
                }
                HotelDetailsViewModelState.RESEND_PAYMENT_INSTRUCTIONS_FAILURE -> {
                    dismissLoadingScreen()
                    showAlertDialog()
                }
            }
        }

    private fun setHardwareBackButtonBehavior() =
        overrideHardwareBackButtonBehavior { back() }

    private fun observeViewBookingClicked() {
        observeEvent(singleJourneyDetailsViewModel.manageBookingClicked) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(
                        Settings.getProperty(Settings.VIEW_BOOKING_URL).replace(
                            "{bookingID}",
                            it
                        )
                    )
                )
            )

            singleJourneyDetailsViewModel.logManageBookingAnalyticsEvent("View")
        }
    }

    private fun observeCancelBookingClicked() {
        observeEvent(singleJourneyDetailsViewModel.cancelBookingClicked) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(
                        Settings.getProperty(Settings.CANCEL_BOOKING_URL).replace(
                            "{bookingID}",
                            it
                        )
                    )
                )
            )
            singleJourneyDetailsViewModel.logManageBookingAnalyticsEvent("Cancel")
        }
    }

    private fun observeJourneyLegs() {
        observeModel(singleJourneyDetailsViewModel.legs) {
            trainTickerAdapter.tickets = it
        }
    }

    private fun observeETicket() {
        observeEvent(singleJourneyDetailsViewModel.showETicketEvent) {
            navigateTo(
                SingleJourneyDetailsFragmentDirections.actionSingleTrainDetailsToPdfWebViewFragment(
                    it
                )
            )
        }
    }

    private fun observeCustomFields() {
        observeModel(singleJourneyDetailsViewModel.getCustomFields()) {
            customFieldListAdapter.requiredInfo = it
        }
    }

    private fun observeJourneyHeader() {
        observeModel(singleJourneyDetailsViewModel.journeyHeader) {
            it.mapReference?.let { mapReference ->
                mapView.getMapAsync { map ->
                    with(LatLng(mapReference.lat, mapReference.lon)) {
                        map.addMarker(MarkerOptions().position(this))
                        map.moveCamera(CameraUpdateFactory.newLatLng(this))
                        map.setMinZoomPreference(15.0f)
                        map.setMaxZoomPreference(18.0f)
                    }

                    map.setOnMapClickListener { showMap(mapReference) }

                }
            }
        }
    }

    private fun observeCallHotel() =
        observeEvent(singleJourneyDetailsViewModel.callHotel) { number ->
            ContextCompat.startActivity(
                requireContext(),
                Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", number, null)),
                null
            )
        }

    private fun observeCallHotelMenuOptionVisible() =
        observeModel(singleJourneyDetailsViewModel.callHotelMenuOptionVisible) { isVisible ->
            menu.findItem(R.id.call_hotel).isVisible = isVisible
        }

    private fun showMap(clickLocation: ClickLocation) {
        val intent = Intent(Intent.ACTION_VIEW).apply {
            data =
                Uri.parse("geo:${clickLocation.lat},${clickLocation.lon}?q=${clickLocation.lat},${clickLocation.lon}")
        }
        startActivity(Intent.createChooser(intent, getString(R.string.open_with)))
        singleJourneyDetailsViewModel.logShowMapAnalyticsEvent()
    }
}
