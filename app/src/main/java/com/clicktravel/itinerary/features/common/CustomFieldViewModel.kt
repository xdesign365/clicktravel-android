package com.clicktravel.itinerary.features.common

import androidx.lifecycle.MutableLiveData
import com.clicktravel.core.model.common.RequiredInformation

interface CustomFieldViewModel {
    fun getCustomFields(): MutableLiveData<List<RequiredInformation>>
    fun postCustomFields(fields: List<RequiredInformation>)
    fun expandCustomFields(): MutableLiveData<Boolean>
    fun expandCustomFieldsClicked()
}

class CustomFieldViewModelImpl : CustomFieldViewModel {

    private val internal = MutableLiveData<List<RequiredInformation>>()
    private val expand = MutableLiveData<Boolean>().apply { value = true }

    override fun getCustomFields() = internal

    override fun postCustomFields(fields: List<RequiredInformation>) {
        internal.postValue(fields)
    }

    override fun expandCustomFields() = expand
    override fun expandCustomFieldsClicked() {
        expand.postValue(expand.value?.not())
    }

}