package com.clicktravel.itinerary.features.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.clicktravel.itinerary.base.BaseFragment
import com.clicktravel.mobile.android.R
import kotlinx.android.synthetic.main.error_page.*

class ErrorScrenFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.error_page, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar(selectTeamToolbar)

        val drawable = AnimatedVectorDrawableCompat
            .create(requireContext(), R.drawable.error_animated)

        busImage.setImageDrawable(drawable)

        (drawable as Animatable2Compat).apply {
            start()
        }
    }
}