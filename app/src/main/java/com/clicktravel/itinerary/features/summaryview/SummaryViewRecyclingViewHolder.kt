package com.clicktravel.itinerary.features.summaryview

import android.content.Context
import android.view.LayoutInflater
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.clicktravel.core.ui.models.*
import com.clicktravel.mobile.android.R
import com.clicktravel.mobile.android.databinding.DateHeaderListItemBinding
import com.clicktravel.mobile.android.databinding.SummaryListDetailBinding
import com.clicktravel.mobile.android.databinding.SummaryListHotelBinding
import com.clicktravel.mobile.android.databinding.SummaryListTravelCardBinding

sealed class SummaryViewRecyclingViewHolder(private val view: ViewDataBinding) :
    RecyclerView.ViewHolder(view.root) {
    abstract fun bind(summaryListItemUi: SummaryListItemUi)
}

class DateHeaderListItemViewHolder(private val v: DateHeaderListItemBinding) :
    SummaryViewRecyclingViewHolder(v) {

    override fun bind(summaryListItemUi: SummaryListItemUi) {
        v.dateHeader = (summaryListItemUi as DateHeaderListItem)
    }
}

class HotelListItemViewHolder(private val v: SummaryListHotelBinding) :
    SummaryViewRecyclingViewHolder(v) {

    override fun bind(summaryListItemUi: SummaryListItemUi) {
        v.booking = (summaryListItemUi as HotelStayListItem)
    }

}

class TravelCardListItemViewHolder(private val v: SummaryListTravelCardBinding) :
    SummaryViewRecyclingViewHolder(v) {

    override fun bind(summaryListItemUi: SummaryListItemUi) {
        v.booking = (summaryListItemUi as TravelCardListItem)
    }

}

class UniqueJourneyListItemViewHolder(private val v: SummaryListDetailBinding) :
    SummaryViewRecyclingViewHolder(v) {

    val context: Context
        get() = v.root.context

    override fun bind(summaryListItemUi: SummaryListItemUi) {
        v.booking = (summaryListItemUi as UniqueJourneyListItem)

        val itemsToDisplay = mutableListOf<Pair<Int, String>>().apply {

            add(Pair(R.drawable.ic_time, summaryListItemUi.time))

            if (summaryListItemUi.isReturnTicket) {
                add(
                    Pair(
                        R.drawable.ic_return, context.getString(
                            R.string.return_ticket
                        )
                    )
                )
            }

            if (summaryListItemUi.isEticket) {
                add(
                    Pair(
                        R.drawable.ic_eticket, context.getString(
                            R.string.is_eticket
                        )
                    )
                )
            }
        }
        v.tagGridView.adapter =
            SummaryTagItemsAdapter(
                itemsToDisplay,
                LayoutInflater.from(context)
            )
    }
}