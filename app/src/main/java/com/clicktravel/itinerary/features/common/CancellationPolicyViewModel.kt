package com.clicktravel.itinerary.features.common

import androidx.lifecycle.MutableLiveData

interface CancellationPolicyViewModel {

    fun getCancellationPolicy(): MutableLiveData<String>
    fun postCancellationPolicy(data: String)
    fun expandCancellationPolicy(): MutableLiveData<Boolean>
    fun expandCancellationPolicyClicked()
}

class CancellationPolicyViewModelImpl : CancellationPolicyViewModel {

    private val internal = MutableLiveData<String>()
    private val expand = MutableLiveData<Boolean>().apply { value = true }

    override fun getCancellationPolicy() = internal

    override fun postCancellationPolicy(data: String) {
        internal.postValue(data)
    }

    override fun expandCancellationPolicy() = expand
    override fun expandCancellationPolicyClicked() {
        expand.postValue(expand.value?.not())
    }

}