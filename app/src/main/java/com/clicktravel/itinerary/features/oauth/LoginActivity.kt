package com.clicktravel.itinerary.features.oauth

import android.os.Bundle
import com.clicktravel.itinerary.base.BaseActivity
import com.clicktravel.mobile.android.R

class LoginActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_nav_host)
    }
}