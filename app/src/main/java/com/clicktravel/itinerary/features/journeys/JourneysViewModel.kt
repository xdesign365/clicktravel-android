package com.clicktravel.itinerary.features.journeys

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.clicktravel.core.analytics.AnalyticsStatics
import com.clicktravel.core.model.UniqueJourneyList
import com.clicktravel.core.model.UserDetails
import com.clicktravel.core.model.team.Team
import com.clicktravel.core.ui.builders.SummaryListUiBuilder
import com.clicktravel.core.ui.models.SummaryListItemUi
import com.clicktravel.core.usecases.UserJourneysUseCase
import com.clicktravel.core.util.Event
import com.clicktravel.core.util.filteredList
import com.clicktravel.itinerary.repository.SelectedTeamRepository
import com.clicktravel.itinerary.util.CompositeLogger
import com.clicktravel.itinerary.util.trigger
import com.clicktravel.repository.retriever.NoArgumentRetriever
import com.clicktravel.repository.retriever.NumberOfTimesAppOpenedRepo
import com.clicktravel.itinerary.analytics.CompositeAnalytics
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch

class JourneysViewModel(
    private val journeys: UserJourneysUseCase,
    private val builder: SummaryListUiBuilder = SummaryListUiBuilder(),
    private val selectedTeamRepository: SelectedTeamRepository,
    private val numberOfTimesAppOpenedRepo: NumberOfTimesAppOpenedRepo,
    private val userDetailsRepository: NoArgumentRetriever<UserDetails?>
) : ViewModel() {

    val displayableJourneys = MutableLiveData<List<SummaryListItemUi>>()
    val showError = MutableLiveData<Event<JourneysErrorState>>()
    val toggleState = MutableLiveData<ToggleState>()
    val scrollBackToToday = MutableLiveData<Event<Unit>>()
    val showAddMobileNumberScreen = MutableLiveData<Event<Unit>>()
    val isRefreshingJourneysList = MutableLiveData<Boolean>()

    var shouldScrollToPositionForToday = true
    private var teamIdForVisibleData = ""

    init {
        viewModelScope.launch {
            val numberOfTimeAppOpened = numberOfTimesAppOpenedRepo.getNumber()
            if (numberOfTimeAppOpened == 5) {
                val userDetails = userDetailsRepository.execute()
                if (userDetails?.mobileNumber == null) {
                    showAddMobileNumberScreen.trigger()
                }
            }
        }
    }

    fun updateJourneysIfTeamHasChanged() = viewModelScope.launch(context = errorHandler()) {
        val selectedTeam = selectedTeamRepository.execute()
        selectedTeam?.id?.let {
            if (teamIdForVisibleData != it) {
                teamIdForVisibleData = it
                updateJourneysForTeam(selectedTeam)
                scrollBackToToday()
            }
        }
    }

    fun refresh() = viewModelScope.launch(context = errorHandler()) {
        showError.trigger(JourneysErrorState.NONE)
        isRefreshingJourneysList.postValue(true)
        try {
            journeys.sync() // will fail if no internet connection
        } catch (e: Throwable) {
            isRefreshingJourneysList.postValue(false)
            showError.trigger(JourneysErrorState.OFFLINE)
        }

        selectedTeamRepository.execute()?.let { team ->
            teamIdForVisibleData = team.id
            updateJourneysForTeam(team)
        }
        isRefreshingJourneysList.postValue(false)
    }

    fun visibleStateChanged(currentPosition: Int, positionForToday: Int) {
        if (currentPosition != -1) { // this was happening when there was a same-day return journey
            toggleState.postValue(
                when {
                    currentPosition > positionForToday -> ToggleState.SHOW_UP_ICON
                    currentPosition < positionForToday -> ToggleState.SHOW_DOWN_ICON
                    else -> ToggleState.HIDE_TOGGLE
                }
            )
        }
    }

    fun backToTodayButtonClicked() = scrollBackToToday()

    private suspend fun updateJourneysForTeam(team: Team) {
        val allJourneys = journeys.getAllJourneys()

        displayableJourneys.postValue(
            builder.build(
                UniqueJourneyList(allJourneys.filteredList(team))
            )
        )

        setHasBookingsAnalyticsProperty(hasBookings = allJourneys.isNotEmpty())
    }

    private fun setHasBookingsAnalyticsProperty(hasBookings: Boolean) =
        CompositeAnalytics.setBooleanUserProperty(AnalyticsStatics.hasBookings, hasBookings)

    private fun scrollBackToToday() {
        scrollBackToToday.postValue(Event(Unit))
        toggleState.postValue(ToggleState.HIDE_TOGGLE)
    }

    private fun errorHandler() = CoroutineExceptionHandler { _, throwable ->
        isRefreshingJourneysList.postValue(false)
        showError.trigger(JourneysErrorState.ERROR)
        CompositeLogger.logError(throwable)
    }

    enum class ToggleState {
        SHOW_UP_ICON,
        SHOW_DOWN_ICON,
        HIDE_TOGGLE
    }
}

enum class JourneysErrorState {
    NONE,
    OFFLINE,
    ERROR
}