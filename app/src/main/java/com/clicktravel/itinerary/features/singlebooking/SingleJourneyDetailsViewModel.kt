package com.clicktravel.itinerary.features.singlebooking

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.clicktravel.core.analytics.AnalyticsStatics
import com.clicktravel.core.model.*
import com.clicktravel.core.model.common.RequiredInformation
import com.clicktravel.core.model.trains.enums.DeliveryOption
import com.clicktravel.core.ui.models.*
import com.clicktravel.core.usecases.HotelUseCase
import com.clicktravel.core.usecases.UserJourneysUseCase
import com.clicktravel.core.util.Event
import com.clicktravel.core.util.daysUntilBooking
import com.clicktravel.itinerary.features.common.*
import com.clicktravel.itinerary.navigation.SelectedBooking
import com.clicktravel.itinerary.navigation.SelectedJourney
import com.clicktravel.itinerary.util.trigger
import com.clicktravel.itinerary.analytics.CompositeAnalytics
import com.clicktravel.itinerary.analytics.events.AnalyticsEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SingleJourneyDetailsViewModel(
    private val journeys: UserJourneysUseCase,
    private val notes: NotesViewModel,
    private val customFieldViewModel: CustomFieldViewModel,
    private val viewCheckInGuideViewModel: ViewCheckInGuideViewModel,
    private val cancellationPolicyViewModel: CancellationPolicyViewModel,
    hotelUseCase: HotelUseCase
) : ViewModel(), NotesViewModel by notes, CustomFieldViewModel by customFieldViewModel,
    ViewCheckInGuideViewModel by viewCheckInGuideViewModel,
    CancellationPolicyViewModel by cancellationPolicyViewModel {

    private lateinit var booking: SelectedBooking
    lateinit var selectedJourney: BookingItem

    private val productType: String
        get() = when (selectedJourney) {
            is TrainBooking -> "Train"
            is FlightBooking -> "Flight"
            is HotelBooking -> "Hotel"
            is TravelCard -> "Travelcard"
        }

    private val daysUntilBooking: Int
        get() = when (selectedJourney) {
            is TrainBooking -> (selectedJourney as TrainBooking).journeys.first().departureDateTime
            is FlightBooking -> (selectedJourney as FlightBooking).journeys.first().segments.first().legDeparture
            is HotelBooking -> (selectedJourney as HotelBooking).checkinDate
            is TravelCard -> (selectedJourney as TravelCard).travelDate
        }.let { daysUntilBooking(it) }

    val hotelDetailsViewModel: HotelDetailsViewModel = HotelDetailsViewModelImpl(
        useCase = hotelUseCase,
        scope = viewModelScope
    )

    val toolbarTitle = MutableLiveData<String>()
    val journeyHeader = MutableLiveData<JourneyHeader>()
    val address = MutableLiveData<DeliveryAddressUi>()
    val legs = MutableLiveData<List<TicketUi>>()
    val showETicketEvent = MutableLiveData<Event<SelectedJourney>>()
    val manageBookingClicked = MutableLiveData<Event<String>>()
    val cancelBookingClicked = MutableLiveData<Event<String>>()
    val callHotel = MutableLiveData<Event<String>>()
    val callHotelMenuOptionVisible = MutableLiveData<Boolean>().apply { value = false }

    private var mapAvailable = false

    fun initialise(journey: SelectedBooking) = viewModelScope.launch(context = Dispatchers.IO) {

        booking = journey
        selectedJourney =
            journeys.getBookingFromId(journey.selectedBookingId).also { selectedJourney ->
                with(
                    UniqueJourney(
                        selectedJourney,
                        journey.journeyNumber
                    )
                ) {
                    val header = toJourneyHeader()
                    mapAvailable = header.mapReference != null
                    journeyHeader.postValue(header)
                    toDeliveryAddress()?.let { address.postValue(it) }
                    legs.postValue(toTicketUi())
                    bookingItem.note?.let { postNote(it) }
                    postCustomFields(
                        listOf(RequiredInformation("Booking ID", "#${booking.selectedBookingId}"))
                            .plus(selectedJourney.field)
                    )
                    if (selectedJourney is FlightBooking) {
                        initialise(selectedJourney)
                    } else if (selectedJourney is HotelBooking) {
                        selectedJourney.cancellationPolicy?.let(::postCancellationPolicy)
                        hotelDetailsViewModel.postDetails(selectedJourney)
                        callHotelMenuOptionVisible.postValue((selectedJourney.hotelPhoneNumber != null))
                    }
                }
            }



        toolbarTitle.postValue(productType.toLowerCase().capitalize())
        logViewBookingAnalyticsEvent()
    }

    fun clickedManagedBooking() =
        manageBookingClicked.postValue(Event(booking.selectedBookingId))

    fun clickedCancelBooking() =
        cancelBookingClicked.postValue(Event(booking.selectedBookingId))

    fun clickedETicket(ticketUi: TicketUi) =
        showETicketEvent.postValue(
            Event(
                SelectedJourney(
                    ticketUi.bookingId,
                    ticketUi.journeyId
                )
            )
        )
            .also {
                CompositeAnalytics.logEvent(
                    AnalyticsEvent(
                        AnalyticsStatics.viewETicket, mapOf(
                            AnalyticsStatics.journeyType to (ticketUi.ticketTitle ?: "N/A")
                        )
                    )
                )
            }

    fun callHotelClicked() =
        callHotel.trigger((selectedJourney as HotelBooking).hotelPhoneNumber!!)

    private fun logViewBookingAnalyticsEvent() = CompositeAnalytics.logEvent(
        AnalyticsEvent(
            event = AnalyticsStatics.viewBookingDetails,
            properties = mapOf(
                AnalyticsStatics.productType to productType,
                AnalyticsStatics.daysUntilBooking to daysUntilBooking,
                AnalyticsStatics.hasETicket to hasETicket(),
                AnalyticsStatics.mapAvailable to mapAvailable
            )
        )
    )

    private fun hasETicket(): Boolean =
        (selectedJourney is TrainBooking &&
                (selectedJourney as TrainBooking).deliveryOption == DeliveryOption.E_TICKET)

    fun logShowMapAnalyticsEvent() = CompositeAnalytics.apply {
        logEvent(
            AnalyticsEvent(
                event = AnalyticsStatics.getDirections,
                properties = mapOf(AnalyticsStatics.productType to productType)
            )
        )

        setBooleanUserProperty(
            key = when (productType) {
                "Train" -> AnalyticsStatics.neededTrainDirections
                "Flight" -> AnalyticsStatics.neededAirportDirections
                else -> AnalyticsStatics.neededHotelDirections
            },
            value = true
        )
    }


    fun logManageBookingAnalyticsEvent(manageType: String) = CompositeAnalytics.logEvent(
        AnalyticsEvent(
            event = AnalyticsStatics.manageBooking,
            properties = mapOf(
                AnalyticsStatics.productType to productType,
                AnalyticsStatics.manageType to manageType
            )
        )
    )
}