package com.clicktravel.itinerary.features.summaryview

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.clicktravel.mobile.android.R

class SummaryTagItemsAdapter(
    val list: List<Pair<Int, String>>,
    private val layoutInflater: LayoutInflater
) : BaseAdapter() {


    // Suggestion doesn't apply to grid view as far as I can find
    @SuppressLint("ViewHolder")
    override fun getView(position: Int, view: View?, group: ViewGroup?): View =
        layoutInflater.inflate(R.layout.summary_chip, null).apply {
            findViewById<TextView>(R.id.textViewDetail).text = list[position].second
            findViewById<ImageView>(R.id.imageViewDetailIcon).setImageDrawable(
                view?.context?.getDrawable(
                    list[position].first
                )
            )
        }

    override fun getItem(p0: Int) = list[p0]
    override fun getItemId(p0: Int) = p0.toLong()
    override fun getCount() = list.size

}