package com.clicktravel.itinerary.features.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.clicktravel.core.analytics.AnalyticsStatics
import com.clicktravel.core.util.UtcDateFormatter
import com.clicktravel.itinerary.MainActivity
import com.clicktravel.itinerary.base.BaseFragment
import com.clicktravel.itinerary.util.observeEvent
import com.clicktravel.mobile.android.R
import com.clicktravel.mobile.android.databinding.AddMobileNumberScreenBinding
import com.soywiz.klock.DateTimeTz
import com.clicktravel.itinerary.analytics.CompositeAnalytics
import com.clicktravel.itinerary.analytics.events.AnalyticsEvent
import kotlinx.android.synthetic.main.add_mobile_number_screen.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class AddMobileNumberFragment : BaseFragment() {

    private val viewModel: AddMobileNumberViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = with(
        DataBindingUtil.inflate<AddMobileNumberScreenBinding>(
            inflater, R.layout.add_mobile_number_screen, container, false
        )
    ) {
        vm = viewModel
        lifecycleOwner = this@AddMobileNumberFragment
        root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideBottomBar()
        setupToolbar(addMobileNumberToolbar)
        overrideHardwareBackButtonBehavior { back() }

        observeEvent(viewModel.addMobileNumberClicked) {
            back()
            (requireActivity() as MainActivity).moveToProfileTab()
            logRequestMobileNumberAnalyticsEvent()
        }

        observeEvent(viewModel.notNowClicked) {
            back()
        }
    }

    private fun logRequestMobileNumberAnalyticsEvent() = CompositeAnalytics.logEvent(
        AnalyticsEvent(
            event = AnalyticsStatics.requestMobileNumber,
            properties = mapOf(
                AnalyticsStatics.date to UtcDateFormatter().format(DateTimeTz.nowLocal())
            )
        )
    )

}