package com.clicktravel.itinerary.features.journeys

import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.clicktravel.core.util.exhaustive
import com.clicktravel.core.util.indexOfCurrentDate
import com.clicktravel.itinerary.base.BaseFragment
import com.clicktravel.itinerary.features.summaryview.SummaryViewRecyclerViewAdapter
import com.clicktravel.itinerary.navigation.SelectedBooking
import com.clicktravel.itinerary.util.observeEvent
import com.clicktravel.itinerary.util.observeModel
import com.clicktravel.itinerary.util.setDrawable
import com.clicktravel.mobile.android.BuildConfig
import com.clicktravel.mobile.android.R
import com.soywiz.klock.DateTimeTz
import kotlinx.android.synthetic.main.empty_state.*
import kotlinx.android.synthetic.main.error_page.*
import kotlinx.android.synthetic.main.select_journey_view.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class JourneySelectionFragment : BaseFragment() {

    private val journeysViewModel: JourneysViewModel by viewModel()
    private lateinit var loadingAnimation: AnimatedVectorDrawableCompat
    private lateinit var journeysAdapter: SummaryViewRecyclerViewAdapter
    private lateinit var smoothScroller: LinearSmoothScroller

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.select_journey_view, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showBottomBar()
        startLoadingAnimation()

        swipeRefreshLayout.setOnRefreshListener {
            journeysViewModel.refresh()
        }

        journeysAdapter = SummaryViewRecyclerViewAdapter { bookingId, journeyNumber ->
            navigateTo(
                JourneySelectionFragmentDirections.actionJourneySelectionFragmentToSingleTrainDetails(
                    SelectedBooking(bookingId, journeyNumber)
                )
            )
        }

        smoothScroller = object : LinearSmoothScroller(requireActivity()) {
            override fun getVerticalSnapPreference() = SNAP_TO_START
        }

        backToTodayButton.setOnClickListener { journeysViewModel.backToTodayButtonClicked() }
        teamsButton.setOnClickListener { gotoChangeTeamFragment() }

        handleShowError()
        handleDisplayableJourneysModel()
        handleIconChanged()

        observeEvent(journeysViewModel.showAddMobileNumberScreen) {
            navigateTo(
                JourneySelectionFragmentDirections.actionJourneySelectionFragmentToAddMobileNumberFragment2()
            )
        }

        observeModel(journeysViewModel.isRefreshingJourneysList) { isRefreshing ->
            if (isRefreshing && animatedLoadingIcon.visibility == View.GONE) {
                startLoadingAnimation()
            } else if (isRefreshing.not() && animatedLoadingIcon.visibility == View.VISIBLE) {
                stopLoadingAnimation()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        journeysViewModel.updateJourneysIfTeamHasChanged()
    }

    private fun gotoChangeTeamFragment() = findNavController().navigate(
        JourneySelectionFragmentDirections
            .actionJourneySelectionFragmentToChangeTeamFragment()
    )


    private fun startLoadingAnimation() {
        loadingContainer.visibility = View.VISIBLE
        animatedLoadingIcon.visibility = View.VISIBLE

        loadingAnimation = AnimatedVectorDrawableCompat
            .create(requireContext(), R.drawable.animated_loader)!!

        animatedLoadingIcon.setImageDrawable(loadingAnimation as Drawable)

        loadingAnimation.start()

        loadingAnimation.registerAnimationCallback(
            object : Animatable2Compat.AnimationCallback() {
                var handler = Handler(Looper.getMainLooper())

                override fun onAnimationEnd(drawable: Drawable?) {
                    handler.post { loadingAnimation.start() }
                }
            }
        )
    }

    private fun stopLoadingAnimation() {
        loadingContainer.visibility = View.GONE
        animatedLoadingIcon.visibility = View.GONE
        loadingAnimation.clearAnimationCallbacks()
    }

    private fun handleIconChanged() {
        observeModel(journeysViewModel.toggleState) {
            when (it) {
                JourneysViewModel.ToggleState.SHOW_UP_ICON -> {
                    backToTodayButton.visibility = View.VISIBLE
                    backToTodayButton.setDrawable(R.drawable.ic_up)
                }
                JourneysViewModel.ToggleState.SHOW_DOWN_ICON -> {
                    backToTodayButton.visibility = View.VISIBLE
                    backToTodayButton.setDrawable(R.drawable.ic_down)
                }
                JourneysViewModel.ToggleState.HIDE_TOGGLE -> backToTodayButton.visibility =
                    View.GONE
            }.exhaustive
        }

        observeEvent(journeysViewModel.scrollBackToToday) {
            smoothScroller.targetPosition = journeysViewModel
                .displayableJourneys
                .value
                ?.indexOfCurrentDate(DateTimeTz.nowLocal())
                ?: 0

            allJourneysList.layoutManager?.startSmoothScroll(smoothScroller)
        }

    }

    private fun handleDisplayableJourneysModel() {
        observeModel(journeysViewModel.displayableJourneys) { journeys ->
            stopLoadingAnimation()

            if (journeys.isEmpty()) {
                showEmptyState()
            } else {
                allJourneysList.isGone = false
                emptyState.isGone = true
                swipeRefreshLayout.isRefreshing = false
                allJourneysList.adapter = journeysAdapter
                journeysAdapter.bookingItem = journeys

                if (journeysViewModel.shouldScrollToPositionForToday) {
                    allJourneysList.scrollToPosition(journeys.indexOfCurrentDate(DateTimeTz.nowLocal()))
                    journeysViewModel.shouldScrollToPositionForToday = false
                }

                allJourneysList.addOnScrollListener(
                    object : RecyclerView.OnScrollListener() {
                        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                            super.onScrolled(recyclerView, dx, dy)
                            journeysViewModel.visibleStateChanged(
                                (allJourneysList.layoutManager as LinearLayoutManager)
                                    .findFirstCompletelyVisibleItemPosition(),
                                journeys.indexOfCurrentDate(DateTimeTz.nowLocal())
                            )
                        }
                    }
                )
            }
        }
    }

    private fun handleShowError() = observeEvent(journeysViewModel.showError) {
        when (it) {
            JourneysErrorState.NONE -> {
                errorState.isGone = true
            }
            JourneysErrorState.OFFLINE -> {
                showAlertDialog(message = "There was a problem retrieving latest bookings. Please check your internet connection.")
            }
            JourneysErrorState.ERROR -> {
                stopLoadingAnimation()
                showErrorMessage()
            }
        }
    }

    private fun showErrorMessage() {
        allJourneysList.isGone = true
        emptyState.isGone = true
        errorState.isGone = false
        swipeRefreshLayout.isRefreshing = false
        selectTeamToolbar.isGone = true

        val drawable = AnimatedVectorDrawableCompat
            .create(requireContext(), R.drawable.error_animated)

        busImage.setImageDrawable(drawable)

        (drawable as Animatable2Compat).apply {
            start()
        }
    }

    private fun showEmptyState() {
        allJourneysList.isGone = true
        emptyState.isGone = false
        swipeRefreshLayout.isRefreshing = false
        makeABookingButton.visibility = View.GONE
        makeABookingButton.setOnClickListener { openUrl(BuildConfig.CREATE_BOOKING_URL) }

        val drawable = AnimatedVectorDrawableCompat
            .create(requireContext(), R.drawable.no_booking_found)

        noItineraryImage.setImageDrawable(drawable)
        (drawable as Animatable2Compat).start()
    }

    private fun openUrl(url: String) {
        startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse(url)
            )
        )
    }
}