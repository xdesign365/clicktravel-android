package com.clicktravel.itinerary.features.profile

import com.clicktravel.core.model.UserDetails
import java.io.Serializable

data class UserDetailsData(
    val id: String,
    val firstName: String,
    val lastName: String,
    val mobile: String?,
    val email: String?,
    val title: String?
) : Serializable

fun UserDetailsData.toUserDetails() = UserDetails(
    id = id,
    firstName = firstName,
    lastName = lastName,
    mobileNumber = mobile,
    phoneNumber = null,
    primaryEmailAddress = email,
    title = title
)

fun UserDetails.toUserDetailsData() = UserDetailsData(
    id = id,
    firstName = firstName,
    lastName = lastName,
    mobile = mobileNumber,
    email = primaryEmailAddress,
    title = title
)