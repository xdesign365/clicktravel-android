package com.clicktravel.itinerary.features.oauth

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.clicktravel.core.model.team.Team
import com.clicktravel.core.usecases.AuthenticationUseCase
import com.clicktravel.core.usecases.PostUserLoginUseCase
import com.clicktravel.core.usecases.UserDetailsUseCase
import com.clicktravel.core.util.Event
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.itinerary.util.CompositeLogger
import com.clicktravel.itinerary.util.trigger
import com.clicktravel.mobile.android.BuildConfig
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

enum class AuthenticationState {
    SHOW_SPLASH,
    SHOW_WEB_VIEW,
    GETTING_CODE,
    SOMETHING_WENT_WRONG,
    SELECT_TEAM,
    DONE
}

class OauthWebViewModel(
    auth: AuthenticationUseCase,
    private val postLogin: PostUserLoginUseCase,
    private val userDetailsUseCase: UserDetailsUseCase,
    private val oauthUserRepository: WriteOnlyRepository<Team>
) :
    ViewModel(),
    AuthenticationUseCase by auth {

    val state = MutableLiveData<AuthenticationState>()

    val openSignUpUrl = MutableLiveData<Event<Unit>>()

    init {
        viewModelScope.launch(context = errorHandler()) {
            if (isUserAlreadyLoggedIn() != null) {
                if (postLogin.teamSelectionRequired()) {
                    state.postValue(AuthenticationState.SELECT_TEAM)
                } else {
                    state.postValue(AuthenticationState.DONE)
                }
            } else {
                state.postValue(AuthenticationState.SHOW_SPLASH)
            }
        }
    }

    fun signUpClicked() = openSignUpUrl.trigger()

    fun overrideUrlCallback(url: String) = viewModelScope.launch(errorHandler()) {
        try {
            withContext(Dispatchers.IO) {
                if (shouldKeepWebView(url)) {
                    state.postValue(AuthenticationState.SHOW_WEB_VIEW)
                } else {
                    state.postValue(AuthenticationState.GETTING_CODE)
                    completeLogin(url)
                        .also {
                            if (BuildConfig.DEBUG) {
                                Log.v("Auth", it.accessToken)
                            }
                            userDetailsUseCase.updateCurrentUser()
                        }
                    if (postLogin.teamSelectionRequired()) {
                        state.postValue(AuthenticationState.SELECT_TEAM)
                    } else {
                        state.postValue(AuthenticationState.DONE)
                    }
                }
            }
        } catch (exception: Exception) {
            CompositeLogger.logError(exception)
            FirebaseCrashlytics.getInstance().recordException(exception)
            state.postValue(AuthenticationState.SOMETHING_WENT_WRONG)
        }
    }

    fun clickedLoginFromSplash() = viewModelScope.launch {
        userDetailsUseCase.deleteUser()
        oauthUserRepository.clearData()
        state.postValue(AuthenticationState.SHOW_WEB_VIEW)
    }

    private fun errorHandler() = CoroutineExceptionHandler { _, throwable ->
        CompositeLogger.logError(throwable)
        state.postValue(AuthenticationState.SOMETHING_WENT_WRONG)
    }
}