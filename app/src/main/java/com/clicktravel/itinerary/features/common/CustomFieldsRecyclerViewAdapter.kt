package com.clicktravel.itinerary.features.common

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.clicktravel.core.model.common.RequiredInformation
import com.clicktravel.mobile.android.R
import com.clicktravel.mobile.android.databinding.SimpleRecyclerItemBinding

class CustomFieldsRecyclerViewAdapter :
    RecyclerView.Adapter<CustomFieldsRecyclerViewAdapter.RequiredInformationViewHolder>() {

    var requiredInfo: List<RequiredInformation> = listOf()
        set(v) {
            field = v
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        RequiredInformationViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.simple_recycler_item,
                parent,
                false
            )
        )

    override fun getItemCount() = requiredInfo.size

    override fun onBindViewHolder(holder: RequiredInformationViewHolder, position: Int) {
        holder.bind(requiredInfo[position])
    }

    class RequiredInformationViewHolder(private val viewBinding: SimpleRecyclerItemBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bind(required: RequiredInformation) {
            viewBinding.item = required
        }
    }
}