package com.clicktravel.itinerary.features.profile

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import coil.api.load
import coil.transform.CircleCropTransformation
import com.clicktravel.core.analytics.AnalyticsStatics
import com.clicktravel.itinerary.analytics.events.AnalyticsEvent
import com.clicktravel.itinerary.analytics.CompositeAnalytics
import com.clicktravel.itinerary.base.BaseFragment
import com.clicktravel.itinerary.features.oauth.LoginActivity
import com.clicktravel.itinerary.features.teamselection.SingleTeamRecyclerViewAdapter
import com.clicktravel.itinerary.features.teamselection.TeamSelectionRequired
import com.clicktravel.itinerary.features.teamselection.TeamSelectionViewModel
import com.clicktravel.itinerary.util.observeEvent
import com.clicktravel.itinerary.util.observeModel
import com.clicktravel.mobile.android.R
import com.clicktravel.mobile.android.databinding.ProfileScreenBinding
import kotlinx.android.synthetic.main.profile_screen.*
import org.koin.androidx.viewmodel.ext.android.viewModel

@BindingAdapter("imageUrl", requireAll = false)
fun loadImage(view: ImageView, imageUrl: String?) {
    imageUrl?.let {
        view.load(it) {
            transformations(CircleCropTransformation())
            placeholder(R.drawable.ic_user)
            error(R.drawable.ic_user)
        }
    }
}

class ProfileFragment : BaseFragment() {

    private val profileViewModel: ProfileViewModel by viewModel()
    private val teamSelectionViewModel: TeamSelectionViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = with(
        DataBindingUtil.inflate<ProfileScreenBinding>(
            inflater, R.layout.profile_screen, container, false
        )
    ) {
        vm = profileViewModel
        lifecycleOwner = this@ProfileFragment
        root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showBottomBar()

        teamSelectionViewModel.initialise(isChangingTeam = true)
        observeLiveData()
    }

    override fun onResume() {
        super.onResume()
        profileViewModel.refreshUserDetails()
    }

    private fun observeLiveData() {
        observeEvent(profileViewModel.dialClickTravel) { number ->
            ContextCompat.startActivity(
                requireContext(),
                Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", number, null)),
                null
            )
        }

        observeEvent(profileViewModel.showError) {
            dismissLoadingScreen()
            showAlertDialog()
        }

        observeEvent(profileViewModel.logout) {
            recordLogoutEvent()
            requireContext().startActivity(
                Intent(context, LoginActivity::class.java).apply {
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                }
            )
            requireActivity().finish()
        }

        observeEvent(profileViewModel.showLoading) { showLoadingScreen() }

        observeEvent(profileViewModel.dismissLoading) { dismissLoadingScreen() }

        observeEvent(profileViewModel.gotoEditDetailsScreen) {
            navigateTo(
                ProfileFragmentDirections.actionProfileFragmentToEditProfileDetailsFragment(
                    userDetails = it.toUserDetailsData()
                )
            )
        }

        observeModel(teamSelectionViewModel.state) {
            if (it is TeamSelectionRequired) {
                teamListSelection.adapter = SingleTeamRecyclerViewAdapter(
                    teamsAndSelectedTeamIndex = it.team,
                    onTeamSelected = { team ->
                        teamSelectionViewModel.updateSelectedTeam(team)
                    }
                )
            }
        }

    }

    private fun recordLogoutEvent() {
        CompositeAnalytics.analytics.forEach { analytics ->
            analytics.logEvent(
                AnalyticsEvent(
                    AnalyticsStatics.forcedLogout,
                    mapOf(
                        AnalyticsStatics.logoutLocation to "Profile Fragment",
                        AnalyticsStatics.logoutReason to "User taped logout"
                    )
                )
            )
        }
    }
}