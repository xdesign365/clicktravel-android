package com.clicktravel.itinerary.features.singlebooking

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.clicktravel.mobile.android.databinding.SingleTicketItemBinding
import com.clicktravel.core.ui.models.TicketUi
import com.clicktravel.mobile.android.R

class TrainTicketRecyclerViewAdapter(private val eTicketClick: (TicketUi) -> Unit) :
    RecyclerView.Adapter<TrainTicketViewHolder>() {

    var tickets: List<TicketUi> = listOf()
        set(v) {
            field = v
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DataBindingUtil.inflate<SingleTicketItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.single_ticket_item,
            parent,
            false
        ).let {
            TrainTicketViewHolder(it, eTicketClick).also { viewHolder ->
                it.lifecycleOwner = viewHolder
            }
        }

    override fun onViewAttachedToWindow(holder: TrainTicketViewHolder) {
        super.onViewAttachedToWindow(holder)
        holder.markAttach()
    }


    override fun getItemCount() = tickets.size

    override fun onBindViewHolder(holder: TrainTicketViewHolder, position: Int) {
        holder.bind(tickets[position])
    }
}