package com.clicktravel.itinerary.features.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.clicktravel.core.util.Event
import com.clicktravel.itinerary.util.trigger

class AddMobileNumberViewModel : ViewModel() {

    val notNowClicked = MutableLiveData<Event<Unit>>()
    val addMobileNumberClicked = MutableLiveData<Event<Unit>>()

    fun addMobileNumberClicked() = addMobileNumberClicked.trigger()

    fun notNowClicked() = notNowClicked.trigger()
}
