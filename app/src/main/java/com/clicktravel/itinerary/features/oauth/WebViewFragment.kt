package com.clicktravel.itinerary.features.oauth

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.clicktravel.itinerary.MainActivity
import com.clicktravel.itinerary.base.BaseFragment
import com.clicktravel.itinerary.util.observeModel
import com.clicktravel.mobile.android.R
import kotlinx.android.synthetic.main.login_activity.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class WebViewFragment : BaseFragment() {

    private val viewModel: OauthWebViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.login_activity, container, false)

    override fun showErrorScreen() {
        findNavController().navigate(R.id.errorScrenFragmentLogin)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        overrideHardwareBackButtonBehavior { back() }

        oauthWebView.webViewClient = OauthWebViewClient(viewModel)
        oauthWebView.settings.javaScriptEnabled = true
        oauthWebView.settings.domStorageEnabled = true

        with(viewModel.buildLoginUrl()) {
            Log.v("Url", this)
            oauthWebView.loadUrl(this)
        }

        observeState()
    }

    private fun observeState() = observeModel(viewModel.state) {
        when (it) {
            AuthenticationState.SHOW_WEB_VIEW -> oauthWebView.visibility = View.VISIBLE
            AuthenticationState.GETTING_CODE -> {
                oauthWebView.visibility = View.GONE
                showLoadingScreen()
            }
            AuthenticationState.DONE -> {
                dismissLoadingScreen()
                Intent(requireContext(), MainActivity::class.java).let {
                    requireActivity().startActivity(it)
                    requireActivity().finish()
                }
            }
            AuthenticationState.SOMETHING_WENT_WRONG -> {
                dismissLoadingScreen()
                back()
                showErrorScreen()
            }
            AuthenticationState.SHOW_SPLASH -> {
            }
            AuthenticationState.SELECT_TEAM -> {
                dismissLoadingScreen()
                findNavController().navigate(WebViewFragmentDirections.actionWebViewFragmentToTeamSelectionFragment())
            }
        }
    }
}