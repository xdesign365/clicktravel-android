package com.clicktravel.itinerary.features.oauth

import android.util.Base64
import com.clicktravel.core.feature.oauth.Hasher
import java.security.MessageDigest

class HasherImpl : Hasher {
    override fun hash(str: String): String = with(MessageDigest.getInstance("SHA-256")) {
        val v = digest(str.toByteArray())
        Base64.encodeToString(v, Base64.URL_SAFE)
            .removeSuffix("\n")
            .removeSuffix("=")
            .removeSuffix("=")

    }
}