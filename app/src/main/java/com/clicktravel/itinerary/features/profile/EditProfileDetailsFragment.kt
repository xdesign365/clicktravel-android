package com.clicktravel.itinerary.features.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.navArgs
import com.clicktravel.itinerary.base.BaseFragment
import com.clicktravel.itinerary.util.observeEvent
import com.clicktravel.itinerary.util.observeModel
import com.clicktravel.mobile.android.R
import com.clicktravel.mobile.android.databinding.EditProfileDetailsScreenBinding
import kotlinx.android.synthetic.main.edit_profile_details_screen.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class EditProfileDetailsFragment : BaseFragment() {

    private val viewModel: EditProfileDetailsViewModel by viewModel()

    private val args: EditProfileDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = with(
        DataBindingUtil.inflate<EditProfileDetailsScreenBinding>(
            inflater, R.layout.edit_profile_details_screen, container, false
        )
    ) {
        vm = viewModel
        lifecycleOwner = this@EditProfileDetailsFragment
        root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideBottomBar()
        setupToolbar(editProfileDetailsToolbar)

        setupListeners()
        viewModel.initialise(args.userDetails)
        observeLiveData()
    }

    private fun setupListeners() {
        overrideHardwareBackButtonBehavior { viewModel.navigatingAwayFromScreen() }

        editProfileDetailsToolbar.setNavigationOnClickListener {
            viewModel.navigatingAwayFromScreen()
        }

        confirmChangesButton.setOnClickListener {
            viewModel.saveChanges()
        }
    }

    private fun observeLiveData() {

        observeEvent(viewModel.showError) {
            dismissLoadingScreen()
            showAlertDialog()
        }

        observeEvent(viewModel.showLoading) { showLoadingScreen() }

        observeEvent(viewModel.dismissLoading) { dismissLoadingScreen() }

        observeEvent(viewModel.showDetailsUpdatedSuccessfully) {
            showAlertDialog(
                title = getString(R.string.success),
                message = getString(R.string.your_details_have_been_updated)
            )
        }

        observeEvent(viewModel.confirmNavigateAwayWithUnsavedChanges) {
            showConfirmationDialog(
                title = getString(R.string.are_you_sure),
                message = getString(R.string.edit_details_warning),
                positiveButtonText = getString(R.string.discard_changes),
                onPositiveButtonPressed = { _, _ -> goBack() }
            )
        }

        observeEvent(viewModel.leaveEditProfileScreen) { goBack() }

        observeModel(viewModel.detailsHaveChanged) { detailsHaveChanged ->
            confirmChangesButton.isVisible = detailsHaveChanged

        }
    }

    private fun goBack() {
        showBottomBar()
        back()
    }
}