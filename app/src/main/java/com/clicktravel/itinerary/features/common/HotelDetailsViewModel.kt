package com.clicktravel.itinerary.features.common

import androidx.lifecycle.MutableLiveData
import com.clicktravel.core.analytics.AnalyticsStatics
import com.clicktravel.core.model.BillingType
import com.clicktravel.core.model.HotelBooking
import com.clicktravel.core.model.durationOfStayInDays
import com.clicktravel.core.model.hotels.Occupants
import com.clicktravel.core.ui.models.HotelDetailsUi
import com.clicktravel.core.ui.models.TextAndAlignment
import com.clicktravel.core.usecases.HotelUseCase
import com.clicktravel.core.util.DateFormatter
import com.clicktravel.core.util.Event
import com.clicktravel.core.util.YearDisplayFormatter
import com.clicktravel.core.util.daysUntilBooking
import com.clicktravel.itinerary.analytics.CompositeAnalytics
import com.clicktravel.itinerary.analytics.events.AnalyticsEvent
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

enum class HotelDetailsViewModelState {
    SHOW_LOADING,
    RESEND_PAYMENT_INSTRUCTIONS_SUCCESS,
    RESEND_PAYMENT_INSTRUCTIONS_FAILURE
}

interface HotelDetailsViewModel {

    val state: MutableLiveData<Event<HotelDetailsViewModelState>>

    fun getDetails(): MutableLiveData<HotelDetailsUi>

    fun postDetails(booking: HotelBooking)

    fun resendPaymentInstructionsClicked(booking: HotelBooking)
}

class HotelDetailsViewModelImpl(
    private val useCase: HotelUseCase,
    private val scope: CoroutineScope,
    private val dateFormatter: DateFormatter = YearDisplayFormatter()
) : HotelDetailsViewModel {
    override val state = MutableLiveData<Event<HotelDetailsViewModelState>>()

    private val internalDetails = MutableLiveData<HotelDetailsUi>()

    override fun getDetails(): MutableLiveData<HotelDetailsUi> = internalDetails

    override fun postDetails(booking: HotelBooking) = internalDetails.postValue(
        HotelDetailsUi(
            occupants = Occupants(
                adults = booking.numberOfAdults,
                children = booking.numberOfChildren
            ),
            roomDescription = booking.roomType,
            mealIncluded = booking.mealIncluded,
            specialRequest = booking.specialRequest,
            lengthOfStay = booking.durationOfStayInDays(),
            checkInDate = dateFormatter.format(booking.checkinDate),
            checkOutDate = dateFormatter.format(booking.checkoutDate),
            billbackText = createBillbackString(booking)
        )
    )

    // todo move to use case
    private fun createBillbackString(booking: HotelBooking): TextAndAlignment {
        var result =
            "This booking has been guaranteed only, you will need to pay the final bill at the hotel."
        var needsAsteriskExplanation = false
        var centerAligned = true

        if (booking.billingType == BillingType.GUARANTEE_ONLY) {
            result =
                "This booking has been guaranteed only, you will need to pay the final bill at the hotel."

        } else if (booking.billingType == BillingType.ROOM_ONLY) {
            result = "This booking was paid by credit/debit card."

        } else if (booking.billingType == BillingType.BILLBACK) {
            if (booking.billbackElements?.allCosts == true) {
                result = "All costs supported by the hotel will be paid for on account."
            } else {
                result = "The following items will be paid for on account:"
                if (booking.isPremierInn) {
                    result += ("\n- Room rate and selected additions")
                } else {
                    booking.billbackElements?.let {
                        if (it.breakfast) {
                            result += "\n- Breakfast"
                            centerAligned = false
                            if (booking.supportedBillbackOptions?.breakfast == false) {
                                result += "*"
                                needsAsteriskExplanation = true
                            }
                        }
                        if (it.roomRate) {
                            result += "\n- Room rate"
                            centerAligned = false
                            if (booking.supportedBillbackOptions?.roomRate == false) {
                                result += "*"
                                needsAsteriskExplanation = true
                            }
                        }
                        if (it.parking) {
                            result += "\n- Parking"
                            centerAligned = false
                            if (booking.supportedBillbackOptions?.parking == false) {
                                result += "*"
                                needsAsteriskExplanation = true
                            }
                        }
                        if (it.wifi) {
                            result += "\n- WIFI"
                            centerAligned = false
                            if (booking.supportedBillbackOptions?.wifi == false) {
                                result += "*"
                                needsAsteriskExplanation = true
                            }
                        }
                        if (it.mealSupplement?.enabled == true) {
                            result += "\n- Meal supplement (£${it.mealSupplement!!.allowance} per person per night)"
                            centerAligned = false
                            if (booking.supportedBillbackOptions?.mealSupplement == false) {
                                result += "*"
                                needsAsteriskExplanation = true
                            }
                        }
                    }
                }
            }

        }
        if (needsAsteriskExplanation) {
            result += "\n\nItems marked * may not be available or it may not be possible to charge them to account.\n\nYou will need to pay for any additional items yourself."
        }
        return TextAndAlignment(
            text = result,
            centerAligned = centerAligned
        )
    }

    override fun resendPaymentInstructionsClicked(booking: HotelBooking) {
        state.postValue(Event(HotelDetailsViewModelState.SHOW_LOADING))

        scope.launch(context = errorHandler()) {
            useCase.resendPaymentInstructions(
                teamId = booking.teamId,
                bookingId = booking.bookingId,
                travellerEmail = booking.travellerEmail,
                hotelEmail = booking.hotelEmail
            )
            booking.hotelEmail
            state.postValue(Event(HotelDetailsViewModelState.RESEND_PAYMENT_INSTRUCTIONS_SUCCESS))
            CompositeAnalytics.logEvent(
                AnalyticsEvent(
                    event = AnalyticsStatics.resendPaymentInstructions,
                    properties = mapOf(
                        AnalyticsStatics.daysUntilBooking to daysUntilBooking(booking.checkinDate)
                    )
                )
            )
        }
    }

    private fun errorHandler() = CoroutineExceptionHandler { _, _ ->
        state.postValue(Event(HotelDetailsViewModelState.RESEND_PAYMENT_INSTRUCTIONS_FAILURE))
    }

}