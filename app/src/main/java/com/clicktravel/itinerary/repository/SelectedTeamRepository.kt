package com.clicktravel.itinerary.repository

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.clicktravel.core.model.team.Team
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.repository.retriever.NoArgumentRetriever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SelectedTeamRepository(
    private val context: Context,
    private val teams: NoArgumentRetriever<List<Team>>
) : NoArgumentRetriever<Team?>, WriteOnlyRepository<Team> {

    private val sharedPrefs: SharedPreferences by lazy {
        context.getSharedPreferences(TEAMS_KEY, Context.MODE_PRIVATE)
    }

    override suspend fun execute() = withContext(Dispatchers.IO) {
        sharedPrefs.getString(SELECTED_TEAM, null)?.let { id ->
            teams.execute().firstOrNull { it.id == id }
        }
    }


    override suspend fun clearData() = withContext(Dispatchers.IO) {
        sharedPrefs.edit().remove(SELECTED_TEAM).apply()
    }

    override suspend fun storeData(storeData: Team) = withContext(Dispatchers.IO) {
        sharedPrefs.edit().putString(SELECTED_TEAM, storeData.id).apply()
    }

    companion object {
        const val TEAMS_KEY = "teams"
        const val SELECTED_TEAM = "SelectedTeam"
    }
}