package com.clicktravel.itinerary.repository

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.clicktravel.core.model.team.Team
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.itinerary.repository.SelectedTeamRepository.Companion.SELECTED_TEAM
import com.clicktravel.repository.retriever.NoArgumentRetriever
import com.clicktravel.repository.retriever.NumberOfTimesAppOpenedRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NumberOfTimesAppOpenedRepoImpl(
    private val context: Context
) : NumberOfTimesAppOpenedRepo {

    override suspend fun getNumber(): Int = withContext(Dispatchers.IO) {
        val number = sharedPrefs.getInt(TIMES, 0)
        number
    }

    override suspend fun incrementNumber() {
        withContext(Dispatchers.IO) {
            val currentNumber = getNumber()
            sharedPrefs.edit().putInt(TIMES, currentNumber + 1).apply()
        }
    }

    private val sharedPrefs: SharedPreferences by lazy {
        context.getSharedPreferences(NUMBER_OF_TIMES_APP_OPENED, Context.MODE_PRIVATE)
    }

    companion object {
        const val TIMES = "TIMES"
        const val NUMBER_OF_TIMES_APP_OPENED = "NUMBER_OF_TIMES_APP_OPENED"
    }
}