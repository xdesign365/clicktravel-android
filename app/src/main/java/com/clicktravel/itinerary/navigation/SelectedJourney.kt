package com.clicktravel.itinerary.navigation

import java.io.Serializable

data class SelectedJourney(val bookingId : String, val journeyId : String) : Serializable