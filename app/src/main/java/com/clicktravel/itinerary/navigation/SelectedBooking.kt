package com.clicktravel.itinerary.navigation

import java.io.Serializable

data class SelectedBooking(val selectedBookingId: String, val journeyNumber: Int) : Serializable