package com.clicktravel.itinerary

import android.app.Application
import android.os.StrictMode
import android.util.Log
import com.clicktravel.backend.exception.ApiException
import com.clicktravel.backend.retriever.FutureBookingsRetriever
import com.clicktravel.backend.retriever.GetCurrentUserProfile
import com.clicktravel.backend.retriever.PaymentInstructionsRetrieverImpl
import com.clicktravel.backend.retriever.UpdateCurrentUserProfileImpl
import com.clicktravel.core.Settings
import com.clicktravel.core.feature.oauth.Encoder
import com.clicktravel.core.feature.oauth.Hasher
import com.clicktravel.core.usecases.HotelUseCase
import com.clicktravel.core.usecases.HotelUseCaseImpl
import com.clicktravel.core.usecases.UserDetailsUseCase
import com.clicktravel.core.usecases.UserDetailsUseCaseImpl
import com.clicktravel.core.util.ExpiryTimeQueryImpl
import com.clicktravel.database.*
import com.clicktravel.itinerary.debug.DebugMenuConfiguration
import com.clicktravel.itinerary.features.common.CancellationPolicyViewModelImpl
import com.clicktravel.itinerary.features.common.CustomFieldViewModelImpl
import com.clicktravel.itinerary.features.common.NotesViewModelImpl
import com.clicktravel.itinerary.features.common.ViewCheckInGuideViewModelImpl
import com.clicktravel.itinerary.features.journeys.JourneysViewModel
import com.clicktravel.itinerary.features.oauth.EncoderImpl
import com.clicktravel.itinerary.features.oauth.HasherImpl
import com.clicktravel.itinerary.features.profile.AddMobileNumberViewModel
import com.clicktravel.itinerary.features.profile.EditProfileDetailsViewModel
import com.clicktravel.itinerary.features.profile.ProfileViewModel
import com.clicktravel.itinerary.features.singlebooking.SingleJourneyDetailsViewModel
import com.clicktravel.itinerary.koin.loginFeatureModules
import com.clicktravel.itinerary.koin.pdfFeatureModules
import com.clicktravel.itinerary.koin.teamSelectionModule
import com.clicktravel.itinerary.koin.userJourneysUseCaseModule
import com.clicktravel.itinerary.repository.NumberOfTimesAppOpenedRepoImpl
import com.clicktravel.itinerary.util.CompositeLogger
import com.clicktravel.itinerary.util.analytics.AmplitudeAnalytics
import com.clicktravel.itinerary.util.analytics.FirebaseAnalyticsImpl
import com.clicktravel.itinerary.workmanager.BookingsSyncWorkManagerScheduler
import com.clicktravel.itinerary.workmanager.TeamsSyncWorkManagerScheduler
import com.clicktravel.itinerary.workmanager.UserDetailsSyncWorkManagerScheduler
import com.clicktravel.mobile.android.BuildConfig
import com.clicktravel.repository.retriever.NumberOfTimesAppOpenedRepo
import com.clicktravel.repository.retriever.PaymentInstructionsRetriever
import com.clicktravel.repository.retriever.RefreshAuthRetriever
import com.clicktravel.repository.retriever.UpdateCurrentUserProfile
import com.facebook.stetho.Stetho
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.features.HttpResponseValidator
import io.ktor.client.features.defaultRequest
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import org.koin.core.inject
import org.koin.core.qualifier.named
import org.koin.dsl.module
import java.util.concurrent.TimeUnit

@Suppress("unused")
class ClickTravelApplication : Application(), KoinComponent {
    override fun onCreate() {

        AppCenter.start(
            this, "bb62de16-fcdf-41bf-9f52-6bc62fb4d1d7",
            Analytics::class.java, Crashes::class.java
        )

        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(
                StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectCustomSlowCalls()
                    .penaltyLog()
                    .build()
            )

            StrictMode.setVmPolicy(
                StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build()
            )
        }


        setupDefaults()
        CompositeLogger.register {
            if (BuildConfig.DEBUG) {
                Log.e("Error", it.message, it)
                Analytics.trackEvent("Error event $it.message")
                FirebaseCrashlytics.getInstance().recordException(it)
            }
        }

        Stetho.initializeWithDefaults(this)


        startKoin {
            androidContext(this@ClickTravelApplication)

            modules(
                listOf(
                    userJourneysUseCaseModule(this@ClickTravelApplication),
                    module {

                        single<SqlDriver> {
                            AndroidSqliteDriver(
                                ClickTravelDb.Schema,
                                this@ClickTravelApplication,
                                "clicktravel.db"
                            )
                        }

                        single<com.clicktravel.itinerary.analytics.Analytics>(named("Amplitude")) {
                            AmplitudeAnalytics(
                                app = get(),
                                selectedTeamRetriever = get(named("selectedTeamRepo")),
                                readLocalUserDetailsRepo = UserDetailsRepository(sqlDriver = get())
                            )
                        }

                        single<com.clicktravel.itinerary.analytics.Analytics>(named("Firebase")) {
                            FirebaseAnalyticsImpl(
                                firebaseAnalytics = FirebaseAnalytics.getInstance(applicationContext),
                                selectedTeamRetriever = get(named("selectedTeamRepo")),
                                readLocalUserDetailsRepo = UserDetailsRepository(sqlDriver = get())
                            )
                        }

                        single {
                            BookingsSyncWorkManagerScheduler(application = this@ClickTravelApplication)
                        }

                        single {
                            UserDetailsSyncWorkManagerScheduler(application = this@ClickTravelApplication)
                        }

                        single {
                            TeamsSyncWorkManagerScheduler(application = this@ClickTravelApplication)
                        }

                        single<Encoder> {
                            EncoderImpl()
                        }

                        single<Hasher> {
                            HasherImpl()
                        }

                        single {
                            FutureBookingsRetriever(
                                client = get(),
                                userTokenRetriever = get(),
                                userTokenWriter = get(),
                                logger = { a, b, e ->
                                    Log.e(a, b, e)
                                    if (e != null) {
                                        FirebaseCrashlytics.getInstance().recordException(e)
                                    }
                                }
                            )
                        }

                        single { DebugMenuConfiguration(get(), get(), get()) }

                        single<HotelUseCase> {
                            HotelUseCaseImpl(retriever = get())
                        }

                        single<PaymentInstructionsRetriever> {
                            PaymentInstructionsRetrieverImpl(client = get())
                        }

                        single<UpdateCurrentUserProfile> {
                            UpdateCurrentUserProfileImpl(client = get())
                        }

                        single<NumberOfTimesAppOpenedRepo> {
                            NumberOfTimesAppOpenedRepoImpl(context = get())
                        }

                        single {
                            LocalBookingsRepository(
                                flightRepository = get(),
                                trainRepository = get(),
                                hotelRepository = get(),
                                travelCardRepository = get()
                            )
                        }

                        single {
                            FlightRepository(
                                sqlDriver = get(),
                                databaseScope = Dispatchers.IO
                            )
                        }

                        single {
                            TrainRepository(
                                sqlDriver = get(),
                                databaseScope = Dispatchers.IO
                            )
                        }

                        single {
                            HotelRepository(
                                sqlDriver = get(),
                                databaseScope = Dispatchers.IO
                            )
                        }

                        single {
                            TravelCardRepository(
                                sqlDriver = get(),
                                databaseScope = Dispatchers.IO
                            )
                        }

                        single<UserDetailsUseCase> {
                            UserDetailsUseCaseImpl(
                                remoteUserDetailsRetriever = GetCurrentUserProfile(client = get()),
                                updateCurrentUserProfile = get(),
                                writeLocalUserDetailsRepo = UserDetailsRepository(sqlDriver = get()),
                                readLocalUserDetailsRepo = UserDetailsRepository(sqlDriver = get())
                            )
                        }

                        single(named("pdfClient")) {
                            HttpClient(OkHttp) {

                                val auth = get<LoggedInUserRepository>()
                                val refreshAuthRetriever: RefreshAuthRetriever = get()
                                val timeQuery = ExpiryTimeQueryImpl()

                                defaultRequest {
                                    runBlocking {
                                        buildDefaultRequest(auth, refreshAuthRetriever, timeQuery)
                                    }

                                    HttpResponseValidator {
                                        validateResponse {
                                            it.checkForFailedResponse()
                                        }
                                    }
                                }

                                engine {
                                    config {
                                        setTimeoutConfig()
                                    }
                                }
                            }
                        }

                        single {

                            HttpClient(OkHttp) {
//                                install(Logging) {
//                                    logger = object : Logger {
//                                        override fun log(message: String) {
//                                            Log.v("Ktor", message)
//                                        }
//
//                                    }
//                                    level = LogLevel.ALL
//                                }

                                install(JsonFeature) {
                                    serializer = KotlinxSerializer(kotlinx.serialization.json.Json {
                                        prettyPrint = true
                                        isLenient = true
                                        ignoreUnknownKeys = true
                                    })
                                }

                                val auth = get<LoggedInUserRepository>()
                                val refreshAuthRetriever: RefreshAuthRetriever = get()
                                val timeQuery = ExpiryTimeQueryImpl()

                                defaultRequest {
                                    runBlocking {
                                        buildDefaultRequest(auth, refreshAuthRetriever, timeQuery)
                                    }

                                    HttpResponseValidator {
                                        validateResponse {
                                            it.checkForFailedResponse()
                                        }
                                    }
                                }

                                engine {
                                    config {
                                        setTimeoutConfig()
                                    }
                                }
                            }

                        }

                        viewModel {
                            SingleJourneyDetailsViewModel(
                                journeys = get(),
                                notes = NotesViewModelImpl(),
                                customFieldViewModel = CustomFieldViewModelImpl(),
                                viewCheckInGuideViewModel = ViewCheckInGuideViewModelImpl(),
                                cancellationPolicyViewModel = CancellationPolicyViewModelImpl(),
                                hotelUseCase = get()
                            )
                        }

                        viewModel {
                            JourneysViewModel(
                                journeys = get(),
                                selectedTeamRepository = get(named("selectedTeamRepo")),
                                numberOfTimesAppOpenedRepo = get(),
                                userDetailsRepository = UserDetailsRepository(sqlDriver = get())
                            )
                        }

                        viewModel {
                            ProfileViewModel(
                                userDetailsUseCase = get(),
                                authenticationUseCase = get()
                            )
                        }

                        viewModel {
                            EditProfileDetailsViewModel(
                                userDetailsUseCase = get()
                            )
                        }

                        viewModel {
                            AddMobileNumberViewModel()
                        }

                    },
                    pdfFeatureModules,
                    loginFeatureModules,
                    teamSelectionModule
                )
            )
        }


        increaseNumberOfTimesAppOpened()
        setupDebugMenu()
        super.onCreate()
    }

    private fun setupDebugMenu() {
        if (BuildConfig.DEBUG) {
            val debugMenu: DebugMenuConfiguration by inject()
            debugMenu.setupDebugMenu()
        }
    }

    private fun increaseNumberOfTimesAppOpened() {
        val numberOfTimesAppOpenedRepo: NumberOfTimesAppOpenedRepo by inject()
        GlobalScope.launch(Dispatchers.IO) {
            numberOfTimesAppOpenedRepo.incrementNumber()
        }
    }

    private fun setupDefaults() = Settings.setupDefaults(
        mapOf(
            Settings.MAIN_URL to BuildConfig.MAIN_URL,
            Settings.USER_URL to BuildConfig.USER_URL,
            Settings.LOGIN_URL to BuildConfig.LOGIN_URL,
            Settings.CLIENT_ID to BuildConfig.CLIENT_ID,
            Settings.REDIRECT_URI to BuildConfig.REDIRECT_URI,
            Settings.CANCEL_BOOKING_URL to BuildConfig.CANCEL_BOOKING_URL,
            Settings.VIEW_BOOKING_URL to BuildConfig.VIEW_BOOKING_URL,
            "authentication" to "Live"
        )
    )

    private suspend fun HttpRequestBuilder.buildDefaultRequest(
        auth: LoggedInUserRepository,
        refreshAuthRetriever: RefreshAuthRetriever,
        timeQuery: ExpiryTimeQueryImpl
    ) {
        var authModel = auth.execute()

        authModel?.let {
            val hasExpired = timeQuery.hasTimeExpired(it.expiresIn)

            if (hasExpired) {
                try {
                    authModel =
                        refreshAuthRetriever.refresh(it.refreshToken)
                } catch (apiException: ApiException) {
                    CompositeLogger.logError(apiException)
                    if (apiException.code in 400..499) {
                        auth.clearData()
                    }
                } catch (exception: Throwable) {
                    // do nothing with exception
                    CompositeLogger.logError(exception)
                }
            }
        }

        authModel
            ?.accessToken
            ?.let { header("Authorization", "clickplatform $it") }
    }

    private fun OkHttpClient.Builder.setTimeoutConfig() {
        connectTimeout(45, TimeUnit.SECONDS)
        callTimeout(45, TimeUnit.SECONDS)
        writeTimeout(45, TimeUnit.SECONDS)
        callTimeout(45, TimeUnit.SECONDS)
        readTimeout(45, TimeUnit.SECONDS)
    }

    private fun HttpResponse.checkForFailedResponse() {
        Log.v("Response code", status.toString())
        if (status != HttpStatusCode.OK) {
            throw ApiException(
                status.description,
                status.value
            )
        }
    }
}

