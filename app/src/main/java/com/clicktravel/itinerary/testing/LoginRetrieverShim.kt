package com.clicktravel.itinerary.testing

import com.clicktravel.core.Settings
import com.clicktravel.core.model.auth.OauthModel
import com.clicktravel.repository.retriever.ArgumentRetriever

class LoginRetrieverShim(private val map: Map<String, ArgumentRetriever<String, OauthModel>>) :
    ArgumentRetriever<String, OauthModel> {

    override suspend fun execute(request: String) =
        map[Settings.getProperty("authentication")]!!.execute(request)

}