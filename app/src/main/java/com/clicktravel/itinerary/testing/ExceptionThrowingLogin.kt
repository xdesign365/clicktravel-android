package com.clicktravel.itinerary.testing

import com.clicktravel.backend.exception.ApiException
import com.clicktravel.core.model.auth.OauthModel
import com.clicktravel.repository.retriever.ArgumentRetriever

class ExceptionThrowingLogin : ArgumentRetriever<String, OauthModel> {
    override suspend fun execute(request: String) = throw ApiException("Soemthing went wrong", -1)
}