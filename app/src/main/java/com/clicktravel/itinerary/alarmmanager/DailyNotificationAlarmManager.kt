package com.clicktravel.itinerary.alarmmanager

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.clicktravel.itinerary.SplashScreenActivity
import com.soywiz.klock.DateTime
import com.soywiz.klock.DateTimeSpan
import java.util.*


class DailyNotificationAlarmManager(private val context: Context) {

    fun set() {
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, DailyNotificationReceiver::class.java)
        val alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0)

        clearNotifications(alarmManager)

        val currentTime = DateTime.now()

        var alarmTime = DateTime(
            Calendar.getInstance().apply {
                timeInMillis = System.currentTimeMillis()
                set(Calendar.HOUR_OF_DAY, 9)
                set(Calendar.MINUTE, 0)
                set(Calendar.SECOND, 0)
                set(Calendar.MILLISECOND, 0)
            }.timeInMillis
        )

        val alarmTimeIsBeforeCurrentTime = alarmTime.compareTo(currentTime) == -1

        if (alarmTimeIsBeforeCurrentTime) {
            alarmTime = alarmTime.plus(DateTimeSpan(days = 1))
        }

        alarmManager.setRepeating(
            AlarmManager.RTC_WAKEUP,
            alarmTime.unixMillisLong,
            AlarmManager.INTERVAL_DAY,
            alarmIntent
        )
    }

    private fun clearNotifications(alarmManager: AlarmManager) {
        val pendingIntent = PendingIntent.getService(
            context,
            0,
            Intent(context, SplashScreenActivity::class.java),
            0
        )
        alarmManager.cancel(pendingIntent)
    }

}