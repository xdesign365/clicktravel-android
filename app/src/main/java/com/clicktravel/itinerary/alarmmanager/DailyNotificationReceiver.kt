package com.clicktravel.itinerary.alarmmanager

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.clicktravel.core.analytics.AnalyticsStatics
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.usecases.UserJourneysUseCase
import com.clicktravel.itinerary.SplashScreenActivity
import com.clicktravel.mobile.android.R
import com.clicktravel.itinerary.analytics.CompositeAnalytics
import com.clicktravel.itinerary.analytics.events.AnalyticsEvent
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject

class DailyNotificationReceiver : BroadcastReceiver(), KoinComponent {

    val journeys: UserJourneysUseCase by inject()

    private lateinit var notificationManager: NotificationManager

    override fun onReceive(context: Context, intent: Intent) {
        notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        GlobalScope.launch {
            val travelTypesForToday = journeys.getJourneyTypesForCurrentDay()
            if (travelTypesForToday.isNotEmpty()) {
                createNotification(
                    context = context,
                    travelTypes = travelTypesForToday
                )
            }
            logViewNotificationAnalyticsEvent(travelTypesForToday)
        }
    }

    private fun createNotification(
        context: Context,
        travelTypes: List<TravelTypes>
    ) {
        val builder = NotificationCompat.Builder(context, CHANNEL_ID)

        createNotificationChannel(context)

        @Suppress("DEPRECATION")
        val color = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            context.resources.getColor(R.color.accentBlue, null)
        } else {
            context.resources.getColor(R.color.accentBlue)
        }

        val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val emojis = travelTypes.joinToString(
            separator = " ",
            transform = ::toEmoji
        )

        val intent = PendingIntent.getActivity(
            context,
            0,
            Intent(context, SplashScreenActivity::class.java),
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val bookingOrBookingsText = if (travelTypes.size == 1) {
            context.getString(R.string.booking)
        } else {
            context.getString(R.string.bookings)
        }

        val content = context.getString(
            R.string.you_have_x_bookings_today,
            emojis,
            travelTypes.size,
            bookingOrBookingsText
        )

        builder.setSmallIcon(
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
                R.drawable.logo_white
            } else {
                R.drawable.ic_transparent
            }
        )

        builder
            .setContentTitle(context.getString(R.string.click_travel))
            .setColor(color)
            .setContentText(content)
            .setContentIntent(intent)
            .setSound(alarmSound)
            .setVibrate(longArrayOf(1000L, 1000L, 1000L))
            .setAutoCancel(true)
            .setGroup(GROUP_ID)
            .build()
            .let { notificationManager.notify(0, it) }
    }

    private fun logViewNotificationAnalyticsEvent(travelTypes: List<TravelTypes>) =
        CompositeAnalytics.logEvent(
            AnalyticsEvent(
                event = AnalyticsStatics.viewNotification,
                properties = mapOf(
                    "Train" to travelTypes.contains(TravelTypes.TRAIN),
                    "Hotel" to travelTypes.contains(TravelTypes.HOTEL),
                    "Flight" to travelTypes.contains(TravelTypes.FLIGHT),
                    "Travelcard" to travelTypes.contains(TravelTypes.TRAVELCARD)
                )
            )
        )

    private fun createNotificationChannel(context: Context) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = CHANNEL_ID
            val descriptionText = context.getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }

            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun toEmoji(travelType: TravelTypes): String = when (travelType) {
        TravelTypes.FLIGHT -> 0x2708
        TravelTypes.TRAIN -> 0x1F684
        TravelTypes.HOTEL -> 0x1F3E8
        TravelTypes.TRAVELCARD -> 0x1F516
    }.let { String(Character.toChars(it)) }

    companion object {
        const val CHANNEL_ID = "click-travel-channel"
        const val GROUP_ID = "click-travel-upcoming-journeys"
    }

}