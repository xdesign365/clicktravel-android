package com.clicktravel.itinerary

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.navigation.NavController
import com.clicktravel.itinerary.alarmmanager.DailyNotificationAlarmManager
import com.clicktravel.itinerary.base.BaseActivity
import com.clicktravel.itinerary.util.setupWithNavController
import com.clicktravel.itinerary.workmanager.BookingsSyncWorkManagerScheduler
import com.clicktravel.itinerary.workmanager.TeamsSyncWorkManagerScheduler
import com.clicktravel.itinerary.workmanager.UserDetailsSyncWorkManagerScheduler
import com.clicktravel.mobile.android.R
import com.clicktravel.itinerary.analytics.Analytics
import com.clicktravel.itinerary.analytics.CompositeAnalytics
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.core.qualifier.named

class MainActivity : BaseActivity() {

    private val amplitudeAnalytics: Analytics by inject(named("Amplitude"))
    private val firebaseAnalytics: Analytics by inject(named("Firebase"))

    private val bookingsSync: BookingsSyncWorkManagerScheduler by inject()
    private val userDetailsSync: UserDetailsSyncWorkManagerScheduler by inject()
    private val teamsSync: TeamsSyncWorkManagerScheduler by inject()

    private var currentNavController: LiveData<NavController>? = null

    private val navGraphIds = listOf(
        R.navigation.main_navigation_view,
        R.navigation.profile_navigation
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupBottomNavigationBar()

        bookingsSync.synchroniseInBackground()
        userDetailsSync.synchroniseInBackground()
        teamsSync.synchroniseInBackground()

        DailyNotificationAlarmManager(context = this@MainActivity).set()
        CompositeAnalytics.analytics.add(amplitudeAnalytics)
        CompositeAnalytics.analytics.add(firebaseAnalytics)
    }

    fun moveToProfileTab() {
        bottom_nav.selectedItemId = bottom_nav.menu.getItem(1).itemId
    }

    private fun setupBottomNavigationBar() {
        val controller = bottom_nav.setupWithNavController(
            navGraphIds = navGraphIds,
            fragmentManager = supportFragmentManager,
            containerId = R.id.nav_host_fragment,
            intent = intent
        )

        currentNavController = controller
    }

}
