package com.clicktravel.itinerary.workmanager

import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.clicktravel.itinerary.ClickTravelApplication
import java.util.concurrent.TimeUnit

class BookingsSyncWorkManagerScheduler(private val application: ClickTravelApplication) {

    fun synchroniseInBackground() {
        val request = PeriodicWorkRequestBuilder<BookingsSyncJob>(1, TimeUnit.HOURS)
            .build()

        WorkManager.getInstance(application).enqueue(request)
    }
}