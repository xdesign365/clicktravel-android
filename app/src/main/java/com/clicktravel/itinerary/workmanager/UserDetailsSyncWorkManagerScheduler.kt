package com.clicktravel.itinerary.workmanager

import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.clicktravel.itinerary.ClickTravelApplication

class UserDetailsSyncWorkManagerScheduler(private val application: ClickTravelApplication) {

    fun synchroniseInBackground() {
        val request = OneTimeWorkRequest.Builder(UserDetailsSyncJob::class.java)
            .build()

        WorkManager.getInstance(application).enqueue(request)
    }
}