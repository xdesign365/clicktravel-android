package com.clicktravel.itinerary.workmanager

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.clicktravel.core.usecases.UserDetailsUseCase
import org.koin.core.KoinComponent
import org.koin.core.inject

class UserDetailsSyncJob(
    context: Context,
    workManagerParameters: WorkerParameters
) : CoroutineWorker(context, workManagerParameters), KoinComponent {

    private val userDetailsUseCase: UserDetailsUseCase by inject()

    override suspend fun doWork(): Result = try {
        userDetailsUseCase.updateCurrentUser()
        Result.success()
    } catch (ex: Exception) {
        Result.failure()
    }
}