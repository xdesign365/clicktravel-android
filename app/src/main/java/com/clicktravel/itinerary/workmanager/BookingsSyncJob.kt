package com.clicktravel.itinerary.workmanager

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.clicktravel.core.usecases.UserJourneysUseCase
import org.koin.core.KoinComponent
import org.koin.core.inject

class BookingsSyncJob(
    context: Context,
    workManagerParameters: WorkerParameters
) : CoroutineWorker(context, workManagerParameters), KoinComponent {

    private val allJourneys: UserJourneysUseCase by inject()

    override suspend fun doWork(): Result = try {
        allJourneys.sync()
        Result.success()
    } catch (ex: Exception) {
        Result.failure()
    }
}