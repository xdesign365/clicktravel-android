package com.clicktravel.itinerary.workmanager

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.clicktravel.repository.usecases.TeamRetrieverUseCase
import org.koin.core.KoinComponent
import org.koin.core.inject

class TeamsSyncJob(
    context: Context,
    workManagerParameters: WorkerParameters
) : CoroutineWorker(context, workManagerParameters), KoinComponent {

    private val teamRetrieverUseCase: TeamRetrieverUseCase by inject()

    override suspend fun doWork(): Result = try {
        teamRetrieverUseCase.refreshTeams()
        Result.success()
    } catch (ex: Exception) {
        Result.failure()
    }
}