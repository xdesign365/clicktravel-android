package com.clicktravel.itinerary.koin

import com.clicktravel.backend.retriever.PdfRetrieverArgs
import com.clicktravel.backend.retriever.RemotePdfRetriever
import com.clicktravel.itinerary.features.eticket.PdfViewModel
import com.clicktravel.repository.retriever.ArgumentRetriever
import com.clicktravel.repository.usecases.GetPdfForTicketUseCase
import com.clicktravel.repository.usecases.GetPdfForTicketUseCaseImpl
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import java.io.InputStream

val pdfFeatureModules = module {

    single<GetPdfForTicketUseCase> {
        GetPdfForTicketUseCaseImpl(get(), get())
    }

    single<ArgumentRetriever<PdfRetrieverArgs, ByteArray>> {
        RemotePdfRetriever(get(named("pdfClient")))
    }

    viewModel {
        PdfViewModel(
            userJourneysUseCase = get(),
            pdfUseCase = get(),
            selectedTeamRetriever = get(named("selectedTeamRepo"))
        )
    }
}