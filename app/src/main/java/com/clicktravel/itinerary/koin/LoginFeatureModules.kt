package com.clicktravel.itinerary.koin

import com.clicktravel.backend.retriever.LoginReceiver
import com.clicktravel.backend.retriever.RefreshAuthRetrieverImpl
import com.clicktravel.core.usecases.AuthenticationUseCase
import com.clicktravel.core.usecases.AuthenticationUseCaseImpl
import com.clicktravel.core.model.auth.OauthModel
import com.clicktravel.core.model.team.Team
import com.clicktravel.core.usecases.PostUserLoginUseCase
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.database.LoggedInUserRepository
import com.clicktravel.itinerary.features.oauth.OauthWebViewModel
import com.clicktravel.itinerary.testing.ExceptionThrowingLogin
import com.clicktravel.itinerary.testing.LoginRetrieverShim
import com.clicktravel.repository.retriever.NoArgumentRetriever
import com.clicktravel.repository.retriever.RefreshAuthRetriever
import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import kotlinx.coroutines.Dispatchers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val loginFeatureModules = module {
    viewModel {
        OauthWebViewModel(get(), get(), get(), get())
    }
    single { LoggedInUserRepository(get(), Dispatchers.IO) }

    single<NoArgumentRetriever<OauthModel?>> {
        get<LoggedInUserRepository>()
    }

    single<WriteOnlyRepository<OauthModel>> {
        get<LoggedInUserRepository>()
    }

    single<RefreshAuthRetriever> {
        RefreshAuthRetrieverImpl(
            saveLocalOauth = get(),
            client = get(named("refreshRetriever"))
        )
    }

    single<AuthenticationUseCase> {

        AuthenticationUseCaseImpl(
            hasher = get(),
            encoder = get(),
            finalStageTokenRetriever = LoginRetrieverShim(
                mapOf("Live" to LoginReceiver(HttpClient {
                    install(JsonFeature) {
                        serializer = KotlinxSerializer()
                    }
                }), "Exception" to ExceptionThrowingLogin())
            ),
            retrieveLocalOauth = get(),
            refreshAuthRetriever = get(),
            saveLocalOauth = get(),
            teamRepository = get(named("localTeamRetriever")),
            selectedTeamRepository = get<PostUserLoginUseCase>() as WriteOnlyRepository<Team>,
            journeys = get()
        )
    }

    single(named("refreshRetriever")) {
        HttpClient(OkHttp) {
            install(JsonFeature) {
                serializer = KotlinxSerializer(kotlinx.serialization.json.Json {
                    prettyPrint = true
                    isLenient = true
                    ignoreUnknownKeys = true
                })
            }
        }
    }
}