package com.clicktravel.itinerary.koin

import android.content.Context
import com.clicktravel.core.usecases.UserJourneysUseCase
import com.clicktravel.repository.usecases.UserJourneysUseCaseLive
import org.koin.dsl.module

@UseExperimental(ExperimentalStdlibApi::class)
fun userJourneysUseCaseModule(context: Context) = module {
    single<UserJourneysUseCase> {
        UserJourneysUseCaseLive(
            remoteFutureBookingsRetriever = get(),
            localBookingsRepository = get()
        )
    }
}