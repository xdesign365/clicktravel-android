package com.clicktravel.itinerary.koin

import com.clicktravel.backend.retriever.TeamRetriever
import com.clicktravel.core.usecases.PostUserLoginUseCase
import com.clicktravel.core.usecases.PostUserLoginUseCaseImpl
import com.clicktravel.database.TeamRepository
import com.clicktravel.itinerary.features.teamselection.TeamSelectionViewModel
import com.clicktravel.itinerary.repository.SelectedTeamRepository
import com.clicktravel.repository.usecases.TeamRetrieverLocalAndRemoteUseCase
import kotlinx.coroutines.Dispatchers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val teamSelectionModule = module {

    single(named("remoteTeamRetriever")) {
        TeamRetriever(get())
    }

    single(named("localTeamRetriever")) {
        TeamRepository(
            sqlDriver = get(),
            coroutineContext = Dispatchers.IO
        )
    }

    single(named("selectedTeamRepo")) {
        SelectedTeamRepository(get(), get(named("localTeamRetriever")))
    }

    single<PostUserLoginUseCase> {
        val localRetriever =
            TeamRetrieverLocalAndRemoteUseCase(
                get(named("localTeamRetriever")),
                get(named("remoteTeamRetriever")),
                get(named("localTeamRetriever"))
            )

        PostUserLoginUseCaseImpl(
            localRetriever,
            get(named("selectedTeamRepo")),
            get(named("selectedTeamRepo"))
        )

    }

    viewModel {
        TeamSelectionViewModel(get())
    }
}