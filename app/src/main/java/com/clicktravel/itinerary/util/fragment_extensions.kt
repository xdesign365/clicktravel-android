package com.clicktravel.itinerary.util

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.clicktravel.core.util.Event


fun <T : Any?> Fragment.observeModel(liveData: LiveData<T>, observer: (T) -> Unit) {
    liveData.observe(this::getLifecycle) { it?.let(observer::invoke) }
}


fun <T : Any?> Fragment.observeNullableModel(liveData: LiveData<T>, observer: (T) -> Unit) {
    liveData.observe(this::getLifecycle) { it.let(observer::invoke) }
}

fun <T : Any?> Fragment.observeEvent(liveData: LiveData<Event<T>>, observer: (T) -> Unit) {
    liveData.observe(this::getLifecycle) {
        it?.getContentIfNotHandled()
            ?.let(observer::invoke)
    }
}

fun <T : Any?> Fragment.observeNullableEvent(liveData: LiveData<Event<T?>>, observer: (T?) -> Unit) {
    liveData.observe(this::getLifecycle) {
        it?.getContentIfNotHandled()
            .let(observer::invoke)
    }
}

fun <T : Any?> AppCompatActivity.observeModel(liveData: LiveData<T>, observer: (T) -> Unit) {
    liveData.observe(this, Observer {
        observer(it)
    })

}
