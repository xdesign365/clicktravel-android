package com.clicktravel.itinerary.util

import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.util.exhaustive
import com.clicktravel.mobile.android.R

fun TravelTypes.toIcon() = when (this) {
    TravelTypes.TRAIN -> R.drawable.train_icon
    TravelTypes.FLIGHT -> R.drawable.ic_plane
    TravelTypes.HOTEL -> R.drawable.ic_hotel
    TravelTypes.TRAVELCARD -> R.drawable.ic_travel_card
}.exhaustive
