package com.clicktravel.itinerary.util

import androidx.lifecycle.MutableLiveData
import com.clicktravel.core.util.Event

fun MutableLiveData<Event<Unit>>.trigger() {
    postValue(Event(Unit))
}

fun <T> MutableLiveData<Event<T>>.trigger(inputValue: T) {
    postValue(Event(inputValue))
}