package com.clicktravel.itinerary.util

import com.clicktravel.core.model.trains.ConnectionOrJourneyType
import com.clicktravel.mobile.android.R

fun ConnectionOrJourneyType.toDrawable() = when (this) {
    ConnectionOrJourneyType.TRAIN -> R.drawable.ic_train
    ConnectionOrJourneyType.WAIT -> R.drawable.ic_coffee
    ConnectionOrJourneyType.FERRY -> R.drawable.ic_ferry
    ConnectionOrJourneyType.UNDERGROUND -> R.drawable.ic_underground
    ConnectionOrJourneyType.WALK -> R.drawable.ic_walking
    ConnectionOrJourneyType.FLIGHT -> R.drawable.ic_plane_blue
    ConnectionOrJourneyType.BUS -> R.drawable.ic_train
}
