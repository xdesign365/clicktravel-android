package com.clicktravel.itinerary.util

import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.clicktravel.core.ui.models.Icon
import com.clicktravel.mobile.android.R

fun Icon.toDrawable() = when(this) {
    Icon.GREY_ICON -> R.drawable.ic_grey_dot
    Icon.FERRY_ICON -> R.drawable.ic_ferry
    Icon.LIGHTHOUSE_ICON -> R.drawable.ic_lighthouse
    Icon.TRAIN_ICON -> R.drawable.ic_train
    Icon.NONE -> R.drawable.ic_grey_dot
    Icon.TRAIN_DESTINATION_ICON -> R.drawable.ic_pin
    Icon.WALK_ICON -> R.drawable.ic_walk_start
    Icon.UNDERGROUND_ICON -> R.drawable.ic_underground_start
    Icon.AIRPORT_ARRIVAL_ICON -> R.drawable.ic_airport
    Icon.FLIGHT_ICON -> R.drawable.ic_plane_blue
    Icon.BUS_ICON -> R.drawable.ic_bus
}

fun ImageView.setDrawable(@DrawableRes drawable : Int) {
    setImageDrawable(resources.getDrawable(drawable, null))
}