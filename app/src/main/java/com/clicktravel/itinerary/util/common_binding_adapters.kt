package com.clicktravel.itinerary.util

import android.content.res.Resources
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.ui.models.HotelJourneyHeader
import com.clicktravel.core.ui.models.JourneyHeader
import com.clicktravel.mobile.android.R

@BindingAdapter("emailText", requireAll = false)
fun emailText(view: View, journeyHeader: JourneyHeader?) {

    if (journeyHeader is HotelJourneyHeader && journeyHeader.email != null) {
        if (view is TextView) {
            view.text = journeyHeader.email
        }
        view.visibility = View.VISIBLE
    } else {
        view.visibility = View.GONE
    }

}

@BindingAdapter("phoneNumberText", requireAll = false)
fun phoneNumberText(view: View, journeyHeader: JourneyHeader?) {

    if (journeyHeader is HotelJourneyHeader && journeyHeader.phone != null) {
        if (view is TextView) {
            view.text = journeyHeader.phone
        }
        view.visibility = View.VISIBLE
    } else {
        view.visibility = View.GONE
    }
}


@BindingAdapter("journeyType", requireAll = false)
fun loadJourneyType(view: ImageView, journeyTypes: TravelTypes?) {
    journeyTypes?.toIcon()?.let {
        view.setDrawable(it)
    }
}

@BindingAdapter("conditionalString", requireAll = false)
fun conditionalString(textView: TextView, stringValue: String?) {

    stringValue?.let {
        textView.text = stringValue

    }

    when (stringValue) {
        null -> textView.visibility = View.GONE
        else -> textView.visibility = View.VISIBLE
    }
}

@BindingAdapter("conditionalStringInvisible", requireAll = false)
fun conditionalStringInvisible(textView: TextView, stringValue: String?) {

    stringValue?.let {
        textView.text = stringValue

    }

    when (stringValue) {
        null -> textView.visibility = View.INVISIBLE
        else -> textView.visibility = View.VISIBLE
    }
}


@BindingAdapter("shouldShow", requireAll = false)
fun setBottomMargin(view: View, shouldShow: Boolean) {

    val layoutParams = view.layoutParams as ViewGroup.MarginLayoutParams

    if (shouldShow) {
        layoutParams.bottomMargin = 20.px
    } else {
        layoutParams.bottomMargin = 0
    }

    view.layoutParams = layoutParams
}

@BindingAdapter("setTopMarginTrue", requireAll = false)
fun setTopMarginIfNull(view: View, shouldShow: Boolean) {

    val layoutParams = view.layoutParams as ViewGroup.MarginLayoutParams

    if (shouldShow) {
        layoutParams.topMargin = 20.px
        view.layoutParams = layoutParams
    }

}

@BindingAdapter("expandIcon", requireAll = false)
fun expandIcon(imageView: ImageView, expanded: Boolean?) {
    imageView.setDrawable(
        if (expanded == true) {
            R.drawable.ic_expand_less
        } else {
            R.drawable.ic_expand_more
        }
    )
}

val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()