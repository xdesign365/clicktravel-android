package com.clicktravel.itinerary.util.analytics

import android.app.Application
import com.amplitude.api.Amplitude
import com.amplitude.api.Identify
import com.clicktravel.core.analytics.AnalyticsStatics
import com.clicktravel.core.model.UserDetails
import com.clicktravel.core.model.team.Team
import com.clicktravel.mobile.android.BuildConfig
import com.clicktravel.repository.retriever.NoArgumentRetriever
import com.clicktravel.itinerary.analytics.Analytics
import com.clicktravel.itinerary.analytics.events.AnalyticsEvent
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject

class AmplitudeAnalytics(
    app: Application,
    private val selectedTeamRetriever: NoArgumentRetriever<Team?>,
    private val readLocalUserDetailsRepo: NoArgumentRetriever<UserDetails?>
) : Analytics {

    private val amplitude = Amplitude
        .getInstance()
        .initialize(app, BuildConfig.AMPLITUDE_API_KEY)
        .enableForegroundTracking(app)

    override fun logEvent(analyticsEvent: AnalyticsEvent) {
        GlobalScope.launch(Dispatchers.IO) {
            val userId = readLocalUserDetailsRepo.execute()?.id ?: "N/A"
            val teamId = selectedTeamRetriever.execute()?.id ?: "N/A"
            amplitude.logEvent(
                analyticsEvent.event,
                analyticsEvent
                    .properties
                    .plus(AnalyticsStatics.userId to userId)
                    .plus(AnalyticsStatics.teamId to teamId)
                    .toJsonObject()
            )
        }
    }

    override fun setBooleanUserProperty(key: String, value: Boolean) = amplitude
        .identify(Identify().set(key, value))
}

fun Map<String, Any>.toJsonObject() = JSONObject().apply {
    for (keyValue in this@toJsonObject) {
        put(keyValue.key, keyValue.value)
    }
}

class FirebaseAnalyticsImpl(
    private val firebaseAnalytics: FirebaseAnalytics,
    private val selectedTeamRetriever: NoArgumentRetriever<Team?>,
    private val readLocalUserDetailsRepo: NoArgumentRetriever<UserDetails?>
) : Analytics {
    override fun logEvent(analyticsEvent: AnalyticsEvent) {
        GlobalScope.launch(Dispatchers.IO) {
            val userId = readLocalUserDetailsRepo.execute()?.id ?: "N/A"
            val teamId = selectedTeamRetriever.execute()?.id ?: "N/A"
            analyticsEvent.apply {
                firebaseAnalytics.logEvent(event) {
                    properties.forEach {
                        param(it.key, it.value.toString())
                    }
                    param(AnalyticsStatics.userId, userId)
                    param(AnalyticsStatics.teamId, teamId)
                }
            }
        }
    }

    override fun setBooleanUserProperty(key: String, value: Boolean) = Unit
}