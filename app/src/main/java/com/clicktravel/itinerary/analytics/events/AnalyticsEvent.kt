package com.clicktravel.itinerary.analytics.events

open class AnalyticsEvent(
    val event : String,
    val properties : Map<String, Any> = emptyMap()
)