package com.clicktravel.itinerary.analytics

import com.clicktravel.itinerary.analytics.events.AnalyticsEvent

interface Analytics {
    fun logEvent(analyticsEvent : AnalyticsEvent)
    fun setBooleanUserProperty(key: String, value: Boolean)
}