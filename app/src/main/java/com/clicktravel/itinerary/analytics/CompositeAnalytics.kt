package com.clicktravel.itinerary.analytics

import com.clicktravel.itinerary.analytics.events.AnalyticsEvent

object CompositeAnalytics : Analytics {

    val analytics = mutableListOf<Analytics>()

    override fun logEvent(analyticsEvent: AnalyticsEvent) = analytics.forEach {
        it.logEvent(analyticsEvent)
    }

    override fun setBooleanUserProperty(key: String, value: Boolean) {
        analytics.forEach {
            it.setBooleanUserProperty(key, value)
        }
    }
}