package com.clicktravel.itinerary.analytics.annotations

import com.clicktravel.itinerary.analytics.events.AnalyticsEvent

annotation class AnalyticsPageView(val screenName: String)

class AnalyticsPageViewEvent(val screenName: String)

fun AnalyticsPageViewEvent.toAnalyticsEvent() = AnalyticsEvent("View Screen", mapOf("Screen" to screenName))
fun AnalyticsPageView.toAnalyticsEvent() = AnalyticsEvent("View Screen", mapOf("Screen" to screenName))