package com.clicktravel.itinerary.debug

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import com.clicktravel.core.Settings.CANCEL_BOOKING_URL
import com.clicktravel.core.Settings.CLIENT_ID
import com.clicktravel.core.Settings.LOGIN_URL
import com.clicktravel.core.Settings.MAIN_URL
import com.clicktravel.core.Settings.REDIRECT_URI
import com.clicktravel.core.Settings.USER_URL
import com.clicktravel.core.Settings.VIEW_BOOKING_URL
import com.clicktravel.core.usecases.AuthenticationUseCase
import com.clicktravel.database.LoggedInUserRepository
import com.clicktravel.itinerary.features.oauth.LoginActivity
import com.xdesign.DebugMenu
import com.xdesign.enums.debugMenu
import com.xdesign.enums.multiTextConfig
import kotlinx.coroutines.runBlocking

class DebugMenuConfiguration(
    private val authenticationUseCase: AuthenticationUseCase,
    private val userRepository: LoggedInUserRepository,
    private val context: Context
) {

    fun setupDebugMenu() = debugMenu {
        freeFieldTextSetting(MAIN_URL)
        freeFieldTextSetting(USER_URL)
        freeFieldTextSetting(LOGIN_URL)
        freeFieldTextSetting(CLIENT_ID)
        freeFieldTextSetting(REDIRECT_URI)
        freeFieldTextSetting(CANCEL_BOOKING_URL)
        freeFieldTextSetting(VIEW_BOOKING_URL)
        button("Logout") {
            runBlocking {
                authenticationUseCase.logout()
                val intent = Intent(context, LoginActivity::class.java).apply {
                    addFlags(FLAG_ACTIVITY_NEW_TASK)
                }
                context.startActivity(intent)
            }
        }
        multipleTextSetting("authentication") {
            listOf(
                multiTextConfig {
                    key = "Live"
                    value = "Live"
                },
                multiTextConfig {
                    key = "Throw exception on login"
                    value = "Exception"
                }
            )
        }
        freeFieldTextSetting(
            "Token expiry time",
            {
                runBlocking {
                    userRepository.execute()
                        ?.copy(expiresIn = it.toLong())
                        ?.also {
                            userRepository.storeData(it)
                        }
                }
            },
            {
                runBlocking {
                    val model = userRepository.execute()?.expiresIn?.toString() ?: "0"
                    model
                }
            }
        )
        freeFieldTextSetting(
            "Refresh token",
            {
                runBlocking {
                    userRepository.execute()
                        ?.copy(refreshToken = it)
                        ?.also {
                            userRepository.storeData(it)
                        }
                }
            },
            {
                runBlocking {
                    val model = userRepository.execute()?.refreshToken ?: "0"
                    model
                }
            }
        )

    }.also {
        DebugMenu.menu = it.build()
    }
}
