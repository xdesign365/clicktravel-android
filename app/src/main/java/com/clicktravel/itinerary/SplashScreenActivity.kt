package com.clicktravel.itinerary

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.clicktravel.itinerary.features.oauth.AuthenticationState
import com.clicktravel.itinerary.features.oauth.LoginActivity
import com.clicktravel.itinerary.features.oauth.OauthWebViewModel
import com.clicktravel.itinerary.util.observeModel
import com.clicktravel.mobile.android.R
import kotlinx.android.synthetic.main.splash_screen.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashScreenActivity : AppCompatActivity() {

    val viewModel: OauthWebViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)

    }

    override fun onStart() {
        super.onStart()
        val drawable = AnimatedVectorDrawableCompat
            .create(this, R.drawable.splash_screen_drawable)

        Thread {
            Thread.sleep(500)
            runOnUiThread {
                observeModel(viewModel.state) {

                    splashLoading.setImageDrawable(drawable)
                    (drawable as Animatable2Compat).apply {
                        start()
                        registerAnimationCallback(object : Animatable2Compat.AnimationCallback() {
                            override fun onAnimationEnd(drawable: Drawable?) {

                                when (it) {
                                    AuthenticationState.DONE -> startActivity(
                                        Intent(
                                            this@SplashScreenActivity,
                                            MainActivity::class.java
                                        )
                                    )
                                    else -> startActivity(
                                        Intent(
                                            this@SplashScreenActivity,
                                            LoginActivity::class.java
                                        )
                                    )
                                }
                                finish()
                            }
                        })
                    }
                }
            }
        }.start()
    }
}

