<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="vm"
            type="com.clicktravel.itinerary.features.profile.ProfileViewModel" />

    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="@color/backgroundColor">

        <com.google.android.material.appbar.MaterialToolbar
            android:id="@+id/profileToolbar"
            android:layout_width="match_parent"
            android:layout_height="?attr/actionBarSize"
            android:background="@color/appBarColor"
            android:theme="@style/AppTheme.MaterialToolbarTheme"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:subtitleTextColor="@android:color/white"
            app:title="@string/profile"
            app:titleTextColor="@android:color/white">

        </com.google.android.material.appbar.MaterialToolbar>

        <androidx.core.widget.NestedScrollView
            android:layout_width="match_parent"
            android:layout_height="0dp"
            android:fillViewport="true"
            android:scrollbars="none"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/profileToolbar">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content">

                <androidx.constraintlayout.widget.ConstraintLayout
                    android:id="@+id/profileDetailsContainer"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:onClick="@{() -> vm.editDetailsClicked()}"
                    android:paddingBottom="24dp"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent">

                    <ImageView
                        android:id="@+id/avatar"
                        android:layout_width="60dp"
                        android:layout_height="60dp"
                        android:layout_marginStart="16dp"
                        android:layout_marginTop="24dp"
                        android:contentDescription="@string/avatar"
                        app:imageUrl="@{vm.userDetailsUi.avatarUrl}"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toTopOf="parent"
                        tools:src="@drawable/ic_user" />

                    <TextView
                        android:id="@+id/fullName"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_marginStart="16dp"
                        android:layout_marginEnd="16dp"
                        android:fontFamily="sans-serif"
                        android:text="@{vm.userDetailsUi.fullName}"
                        android:textColor="#de000000"
                        android:textSize="16sp"
                        android:textStyle="normal"
                        app:layout_constraintBottom_toTopOf="@+id/email"
                        app:layout_constraintEnd_toEndOf="parent"
                        app:layout_constraintStart_toEndOf="@id/avatar"
                        app:layout_constraintTop_toTopOf="@id/avatar"
                        tools:text="John Smith" />

                    <TextView
                        android:id="@+id/email"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_marginStart="16dp"
                        android:layout_marginEnd="16dp"
                        android:text="@{vm.userDetailsUi.email}"
                        app:layout_constraintBottom_toTopOf="@+id/editLabel"
                        app:layout_constraintEnd_toEndOf="parent"
                        app:layout_constraintStart_toEndOf="@id/avatar"
                        app:layout_constraintTop_toBottomOf="@+id/fullName"
                        tools:text="example@email.com" />

                    <TextView
                        android:id="@+id/editLabel"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="4dp"
                        android:layout_marginEnd="24dp"
                        android:text="@string/edit"
                        android:textColor="@color/accentBlue"
                        app:layout_constraintStart_toStartOf="@+id/email"
                        app:layout_constraintTop_toBottomOf="@+id/email" />

                </androidx.constraintlayout.widget.ConstraintLayout>

                <View
                    android:id="@+id/detailsDivider"
                    android:layout_width="0dp"
                    android:layout_height="1dp"
                    android:background="@color/divider_grey"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/profileDetailsContainer" />

                <TextView
                    android:id="@+id/teamLabel"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="15dp"
                    android:layout_marginTop="30dp"
                    android:layout_marginEnd="16dp"
                    android:fontFamily="sans-serif-medium"
                    android:text="@string/team"
                    android:textColor="#000000"
                    android:textSize="14sp"
                    android:textStyle="normal"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintHorizontal_bias="0"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/detailsDivider" />

                <androidx.recyclerview.widget.RecyclerView
                    android:id="@+id/teamListSelection"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="15dp"
                    android:layout_marginTop="15dp"
                    android:layout_marginEnd="15dp"
                    app:layoutManager="androidx.recyclerview.widget.LinearLayoutManager"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/teamLabel"
                    tools:listitem="@layout/single_team_item" />

                <View
                    android:id="@+id/teamDivider"
                    android:layout_width="0dp"
                    android:layout_height="1dp"
                    android:layout_marginTop="30dp"
                    android:background="@color/divider_grey"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/teamListSelection" />

                <TextView
                    android:id="@+id/contactUsLabel"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="15dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:fontFamily="sans-serif-medium"
                    android:text="@string/contact_us"
                    android:textColor="#000000"
                    android:textSize="14sp"
                    android:textStyle="normal"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintHorizontal_bias="0"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/teamDivider" />

                <TextView
                    android:id="@+id/contactUsContent"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="15dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:fontFamily="sans-serif"
                    android:text="@string/contact_us_text"
                    android:textColor="#000000"
                    android:textSize="14sp"
                    android:textStyle="normal"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintHorizontal_bias="0"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/contactUsLabel" />

                <Button
                    android:id="@+id/callUsButton"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="30dp"
                    android:layout_marginTop="20dp"
                    android:layout_marginEnd="30dp"
                    android:layout_marginBottom="30dp"
                    android:background="@drawable/curved_button"
                    android:fontFamily="sans-serif-medium"
                    android:lineSpacingExtra="-4sp"
                    android:onClick="@{() -> vm.callUsClicked()}"
                    android:text="@string/call_us"
                    android:textAllCaps="false"
                    android:textColor="#deffffff"
                    android:textSize="20sp"
                    android:textStyle="normal"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/contactUsContent" />

                <View
                    android:id="@+id/contactUsDivider"
                    android:layout_width="0dp"
                    android:layout_height="1dp"
                    android:layout_marginTop="30dp"
                    android:background="@color/divider_grey"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/callUsButton" />

                <TextView
                    android:id="@+id/logoutButton"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="15dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:clickable="true"
                    android:focusable="true"
                    android:fontFamily="sans-serif-medium"
                    android:onClick="@{() -> vm.logoutClicked()}"
                    android:paddingBottom="32dp"
                    android:text="@string/logout"
                    android:textColor="#000000"
                    android:textSize="14sp"
                    android:textStyle="normal"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintHorizontal_bias="0"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/contactUsDivider" />

            </androidx.constraintlayout.widget.ConstraintLayout>

        </androidx.core.widget.NestedScrollView>

    </androidx.constraintlayout.widget.ConstraintLayout>

</layout>