-keep class * extends Serializable
-keep class com.clicktravel.itinerary.navigation.*
-keep class com.clicktravel.backend.model.*
-keep class com.clicktravel.itinerary.features.profile.UserDetailsData

# Ktor
-keep class io.ktor.** { *; }
-keep class kotlinx.coroutines.** { *; }
-dontwarn kotlinx.atomicfu.**
-dontwarn io.netty.**
-dontwarn com.typesafe.**
-dontwarn org.slf4j.**

# Serialization
-keepattributes *Annotation*, InnerClasses
-dontnote kotlinx.serialization.SerializationKt
-keep,includedescriptorclasses class com.clicktravel.**$$serializer { *; } # <-- change package name to your app's
-keepclassmembers class com.clicktravel.** { # <-- change package name to your app's
    *** Companion;
}
-keepclasseswithmembers class com.clicktravel.** { # <-- change package name to your app's
    kotlinx.serialization.KSerializer serializer(...);
}

-keep class kotlinx.serialization.json.JsonObject
-keep class kotlinx.serialization.json.*
-keep class com.shockwave.**
-keep class com.google.android.gms.ads.** { *; }
-dontwarn okio.**