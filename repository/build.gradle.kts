configurations.create("compileClasspath")

plugins {
    kotlin("plugin.serialization")
    kotlin("multiplatform")
    id("org.jetbrains.kotlin.native.cocoapods")
}

repositories {
    mavenCentral()
    jcenter()
}

val coroutinesVersion: String by project
val libraryVersion: String by project
val xdesignRepositoryVersion: String by project
val serializationVersion : String by project

val buildForIos: String by project
version = libraryVersion


kotlin {
    jvm()

    if (buildForIos == "true") {
        val buildForDevice = project.findProperty("kotlin.native.cocoapods.target") == "ios_arm"
        if (buildForDevice) {
            iosArm64("ios")
            iosArm32("iOS32")

            val iOSMain by sourceSets.creating
            sourceSets["iOS64Main"].dependsOn(iOSMain)
            sourceSets["iOS32Main"].dependsOn(iOSMain)
        } else {
            iosX64("ios")
        }

        cocoapods {
            summary = "Some description for a Kotlin/Native module"
            homepage = "Link to a Kotlin/Native module homepage"
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(project(":core"))
                implementation(project(":backend"))
                implementation(project(":database"))
                implementation(kotlin("stdlib-common"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-common:$coroutinesVersion")

                // Serialization
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.1.0")
            }
        }

        val jvmMain by getting {
            dependencies {
//                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:$serializationVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
            }
        }

        if (buildForIos == "true") {
            val iosMain by getting {
                dependencies {
                    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-native:$coroutinesVersion")
                }
            }
        }
    }
}