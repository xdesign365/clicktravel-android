package com.clicktravel.repository.usecases

import com.clicktravel.core.model.team.Team
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.repository.retriever.NoArgumentRetriever

interface TeamRetrieverUseCase : NoArgumentRetriever<List<Team>> {
    suspend fun retrieveTeams(): List<Team>
    suspend fun refreshTeams()
}

class TeamRetrieverLocalAndRemoteUseCase(
    private val localCopy: NoArgumentRetriever<List<Team>>,
    private val remoteCopy: NoArgumentRetriever<List<Team>>,
    private val localStorer: WriteOnlyRepository<List<Team>>
) : TeamRetrieverUseCase {

    override suspend fun retrieveTeams() =
        with(localCopy.execute()) {
            if (isEmpty()) {
                remoteCopy.execute().also { localStorer.storeData(it) }
            } else {
                this
            }
        }

    override suspend fun refreshTeams() {
        remoteCopy.execute().also { localStorer.storeData(it) }
    }

    override suspend fun execute() = retrieveTeams()

}