package com.clicktravel.repository.usecases

import com.clicktravel.core.model.FutureBookings
import com.clicktravel.repository.callback.GenericCallback
import com.clicktravel.repository.retriever.NoArgumentRetriever
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

interface GetUserDataUseCase {
    fun getUserRemotely(callback: GenericCallback<FutureBookings>)
}

class GetUserDataUseCaseImpl(
    private val remoteApi: NoArgumentRetriever<FutureBookings>,
    private val scope: CoroutineScope
) : GetUserDataUseCase {

    override fun getUserRemotely(callback: GenericCallback<FutureBookings>) {
        scope.launch(CoroutineExceptionHandler { _, throwable -> callback.onError(throwable) }) {
            remoteApi.execute()
                .let {
                    callback.onSuccess(it)
                }
        }
    }
}