package com.clicktravel.repository.usecases

import com.clicktravel.backend.retriever.FutureBookingsRetriever
import com.clicktravel.core.model.*
import com.clicktravel.core.usecases.UserJourneysUseCase
import com.clicktravel.core.util.daysBetween
import com.clicktravel.core.util.exhaustive
import com.clicktravel.database.LocalBookingsRepository
import com.soywiz.klock.DateTimeTz

@ExperimentalStdlibApi
class UserJourneysUseCaseLive(
    private val remoteFutureBookingsRetriever: FutureBookingsRetriever,
    private val localBookingsRepository: LocalBookingsRepository
) : UserJourneysUseCase {

    val data = mutableListOf<BookingItem>()

    override suspend fun getJourneyTypesForCurrentDay(): List<TravelTypes> {
        val types = mutableSetOf<TravelTypes>()

        val journeys = try {
            getAllJourneys()
        } catch (e: Throwable) {
            emptyList<UniqueJourney>()
        }

        journeys.forEach { journey ->
            when (val booking = journey.bookingItem) {
                is TrainBooking -> if (booking.isToday()) {
                    types.add(TravelTypes.TRAIN)
                }
                is FlightBooking -> if (booking.isToday()) {
                    types.add(TravelTypes.FLIGHT)
                }
                is HotelBooking -> if (booking.checkinDate.isToday()) {
                    types.add(TravelTypes.HOTEL)
                }
                is TravelCard -> if (booking.travelDate.isToday()) {
                    types.add(TravelTypes.TRAVELCARD)
                }
            }
        }

        return types.toList()
    }

    override suspend fun getAllJourneys(): List<UniqueJourney> {
        if (data.isEmpty()) {
            val localBookings = localBookingsRepository.getBookings()
            if (localBookings.isNotEmpty()) {
                data.addAll(localBookings)
            } else {
                val remoteBookings = remoteFutureBookingsRetriever.retrieveFutureBookings().items
                data.addAll(remoteBookings)
                localBookingsRepository.storeBookings(remoteBookings)
            }
            data
        }

        return data
            .toList()
            .map { booking ->
                when (booking) {
                    is TrainBooking -> booking.journeys.mapIndexed { index, _ ->
                        UniqueJourney(booking, index)
                    }
                    is FlightBooking -> booking.journeys.mapIndexed { index, _ ->
                        UniqueJourney(booking, index)
                    }
                    is HotelBooking -> listOf(UniqueJourney(booking, 0))
                    is TravelCard -> listOf(UniqueJourney(booking, 0))
                }
            }
            .flatten()
    }

    override suspend fun getBookingFromId(bookingId: String) =
        getAllJourneys().first {
            when (it.bookingItem) {
                is TrainBooking -> (it.bookingItem as TrainBooking).bookingId == bookingId
                is FlightBooking -> (it.bookingItem as FlightBooking).bookingId == bookingId
                is HotelBooking -> (it.bookingItem as HotelBooking).bookingId == bookingId
                is TravelCard -> (it.bookingItem as TravelCard).bookingId == bookingId
            }.exhaustive
        }.bookingItem

    override suspend fun sync() {
        val remoteBookings = remoteFutureBookingsRetriever.retrieveFutureBookings().items
        clearData()
        data.addAll(remoteBookings)
        localBookingsRepository.storeBookings(remoteBookings)
    }

    override suspend fun clearData() {
        localBookingsRepository.clearBookings()
        data.clear()
    }

    private fun TrainBooking.isToday(): Boolean {
        var isToday = false
        journeys.forEach {
            if (it.departureDateTime.isToday()) {
                isToday = true
            }
        }
        return isToday
    }

    private fun FlightBooking.isToday(): Boolean {
        var isToday = false
        journeys.forEach { journey ->
            journey.segments.forEach { segment ->
                if (segment.legDeparture.isToday()) {
                    isToday = true
                }
            }
        }
        return isToday
    }

    private fun DateTimeTz.isToday(): Boolean = daysBetween(DateTimeTz.nowLocal()) == 0

}