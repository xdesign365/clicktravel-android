package com.clicktravel.repository.usecases

import com.clicktravel.core.model.auth.OauthModel
import com.clicktravel.repository.retriever.NoArgumentRetriever

interface GetLoggedInUserUseCase {
    suspend fun getCurrentlyLoggedInUser(): OauthModel?
}

class GetLoggedInUserUseCaseImpl(private val retriever: NoArgumentRetriever<OauthModel?>) :
    GetLoggedInUserUseCase, NoArgumentRetriever<OauthModel?> by retriever {

    override suspend fun getCurrentlyLoggedInUser() = execute()
}