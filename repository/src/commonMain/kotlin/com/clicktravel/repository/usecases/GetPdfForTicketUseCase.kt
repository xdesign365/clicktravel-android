package com.clicktravel.repository.usecases

import com.clicktravel.backend.retriever.PdfRetrieverArgs
import com.clicktravel.core.feature.oauth.Encoder
import com.clicktravel.repository.retriever.ArgumentRetriever

interface GetPdfForTicketUseCase {
    suspend fun getPdfForTicket(args: PdfRetrieverArgs): ByteArray
}

class GetPdfForTicketUseCaseImpl(
    private val retriever: ArgumentRetriever<PdfRetrieverArgs, ByteArray>,
    private val encoder: Encoder
) : GetPdfForTicketUseCase {

    override suspend fun getPdfForTicket(args: PdfRetrieverArgs) =
        retriever.execute(args)
            .let(encoder::base64Decode)
}