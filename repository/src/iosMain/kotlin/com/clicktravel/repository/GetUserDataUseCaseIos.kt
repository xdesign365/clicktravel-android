package com.clicktravel.repository

import com.clicktravel.core.MainScope
import com.clicktravel.core.model.FutureBookings
import com.clicktravel.repository.usecase.GetUserDataUseCase
import com.clicktravel.repository.usecase.GetUserDataUseCaseImpl
import com.clicktravel.repository.retriever.NoArgumentRetriever

class GetUserDataUseCaseIos(remoteApi: NoArgumentRetriever<FutureBookings>) :
    GetUserDataUseCase by GetUserDataUseCaseImpl(
        remoteApi,
        MainScope()
    )