buildscript {

    val navVersion: String by project
    val sqlDelightVersion: String by project

    repositories {
        mavenLocal()
        mavenCentral()
        jcenter()
        google()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:4.2.2")
        classpath("com.squareup.sqldelight:gradle-plugin:1.2.0")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.5.0")
        classpath("org.jetbrains.kotlin:kotlin-serialization:1.5.0")
        classpath("com.google.cloud.tools:appengine-gradle-plugin:2.0.0-rc4")
        classpath("androidx.navigation:navigation-safe-args-gradle-plugin:$navVersion")
        classpath("com.squareup.sqldelight:gradle-plugin:$sqlDelightVersion")

        // Crashlytics setup
        classpath("com.google.gms:google-services:4.3.10")
        classpath("com.google.firebase:firebase-crashlytics-gradle:2.7.1")
    }

}

allprojects {

    repositories {

        mavenLocal()
        mavenCentral()
        jcenter()
        google()

    }
}