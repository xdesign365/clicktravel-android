# Gotchas

## Trains

Note that 'segments' and 'legs' are used interchangably 
* Trains have one or more journeys (usually two, one outbound, one return)
* A train will therefore appear in the list twice, once per 'ticket' (ticket is actually one journey)
* A Journey consists of multiple legs
* A leg can be a 'train' leg, a 'ferry' leg or an 'underground' leg. The code treats 'underground' legs as a 'ConnectionUi' and treats both Ferry and Train journeys as 'JourneyLegUi'
* For the purpose of the UI, we also have something which I'm calling a 'wait' which is a type of 'ConnectionUi' where someone either needs to walk from one platform to another, or they just need to wait
* These 'wait's' don't come from the API, so they are inserted manually as 'connection Uis' when there are two adjacent 'journeys'. E.g. if two train legs are adjacent, a 'wait' is inserted into the intervening part



## From API to UI - the lifecycle of a journey type


We get the data initially in 'FutureBookings' with each booking having a ```com.clicktravel.backend.model.futurebookings.TravelType``` e.g. a train is a travel type. 

A TravelType is a sealed class, each sealed class implementation should be serializable. These Api classes can be converted into 'Core' objects. E.g a ```BookingItem```. These Booking items 
should be convertible into an 'Entity' (or database) object, and also into a 'Ui' object. The UI objects should be the simplest way we can display that object, e.g. where possible we build any strings
to be displayed, format dates appropriately for display etc.


## Things to do going forward

* Some of the things that seemed sensible at first for planes and trains aren't that sensible for airports. For example the ViewModel and Fragment for displaying individual
trains isn't as re-usable as I'd first hoped.
* Some of the classes - particularly journey headers - have been converted to sealed classes but they still have things in the super class that shouldn't be there as they are only relevant to a specific journey type. E.g. hasRailcard shouldn't be handled by an airplane / hotel booking
* The ideal method for parsing a lot of the UI objects should be a factory returning a different builder depending on the type passed in, rather than a single builder and a switch statement in there
* Some strings are currently in core, and they should be in the android resource file
* 