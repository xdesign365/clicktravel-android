configurations.create("compileClasspath")

plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization")
    id("org.jetbrains.kotlin.native.cocoapods")
    id("com.squareup.sqldelight")
}

repositories {
    mavenCentral()
    jcenter()
}

val libraryVersion: String by project
val buildForIos: String by project

version = libraryVersion

sqldelight {
    database("ClickTravelDb") {
        packageName = "com.clicktravel.database"
    }
}

kotlin {

    val sqlDelightVersion: String by project
    val coroutines_version: String by project
    val kotlin_version: String by project
    val mockkVersion: String by project

    if (buildForIos == "true") {
        val buildForDevice = project.findProperty("kotlin.native.cocoapods.target") == "ios_arm"
        if (buildForDevice) {
            iosArm64("ios")
            iosArm32("iOS32")

            val iOSMain by sourceSets.creating
            sourceSets["iOS64Main"].dependsOn(iOSMain)
            sourceSets["iOS32Main"].dependsOn(iOSMain)
        } else {
            iosX64("ios")
        }

        cocoapods {
            summary = "Backend data models for clicktravel"
            homepage = "Backend data models for clicktravel"
        }
    }

    jvm()
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation(project(":core"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-common:$coroutines_version")

                // Serialization
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.1.0")

            }
        }

        val jvmMain by getting {
            dependencies {
                implementation(kotlin("stdlib"))
                api("com.squareup.sqldelight:android-driver:$sqlDelightVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutines_version")

            }
        }

        val jvmTest by getting {
            dependencies {
                dependsOn(jvmMain)
                implementation("com.squareup.sqldelight:sqlite-driver:1.2.0")
                implementation("org.jetbrains.kotlin:kotlin-test:$kotlin_version")
                implementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version")
                implementation("io.mockk:mockk:$mockkVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.3.2")


            }
        }
        if (buildForIos == "true") {
            val iosMain by getting {
                dependencies {
                    api("com.squareup.sqldelight:ios-driver:$sqlDelightVersion")
                    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-native:$coroutines_version")
                }
            }
        }
    }
}