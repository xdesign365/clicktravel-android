package com.clicktravel.database

import com.clicktravel.core.testutils.booking
import com.clicktravel.core.testutils.ferryBooking
import com.clicktravel.core.testutils.matchingDataModelOutbound
import com.clicktravel.core.testutils.matchingDataModelReturn
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.sqlite.driver.JdbcSqliteDriver
import com.squareup.sqldelight.sqlite.driver.JdbcSqliteDriver.Companion.IN_MEMORY
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals


class TrainDatabaseTest {

    private lateinit var driver: SqlDriver

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @Before
    fun setup() {
        driver = JdbcSqliteDriver(IN_MEMORY)
        ClickTravelDb.Schema.create(driver)
        Dispatchers.setMain(mainThreadSurrogate)
    }


    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `test that we can insert a database model and retrieve the same model`() = runBlocking {
        with(TrainRepository(driver, Dispatchers.Main)) {
            storeData(booking)

            val model = execute().first()
            assertEquals(model.journeys[0].segments[0], matchingDataModelOutbound.segments[0])
            assertEquals(model.journeys[0].segments[1], matchingDataModelOutbound.segments[1])
            assertEquals(model.journeys[0].segments[2], matchingDataModelOutbound.segments[2])
            assertEquals(model.journeys[1].segments[0], matchingDataModelReturn.segments[0])
            assertEquals(model.journeys[1].segments[1], matchingDataModelReturn.segments[1])
            assertEquals(model.journeys[1].segments[2], matchingDataModelReturn.segments[2])
            assertEquals(model.journeys[1].segments[3], matchingDataModelReturn.segments[3])
            assertEquals(model.journeys[0],
                matchingDataModelOutbound
            )
            assertEquals(model.journeys[1],
                matchingDataModelReturn
            )
            assertEquals(booking, model)
        }
    }

    @Test
    fun `test that data is cleared when clear model is called`()  = runBlocking {
        with(TrainRepository(driver, Dispatchers.Main)) {
            storeData(booking)
            clearData()

            assertEquals(0, execute().size)
        }
    }

    @Test
    fun `test storing a ferry journey and then retrieving it`() = runBlocking {
        with(TrainRepository(driver, Dispatchers.Main)) {
            storeData(ferryBooking)
            assertEquals(ferryBooking, execute().first())
        }
    }


}