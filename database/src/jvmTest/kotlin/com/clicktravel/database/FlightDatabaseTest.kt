package com.clicktravel.database

import com.clicktravel.core.testutils.flightBookingMatchingObject
import com.clicktravel.core.testutils.matchingOtherJson
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.sqlite.driver.JdbcSqliteDriver
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import kotlin.test.Test
import kotlin.test.assertEquals

class FlightDatabaseTest {


    private lateinit var driver: SqlDriver
    private lateinit var flights: FlightRepository

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @Before
    fun setup() {
        driver = JdbcSqliteDriver(JdbcSqliteDriver.IN_MEMORY)
        ClickTravelDb.Schema.create(driver)
        flights = FlightRepository(driver, Dispatchers.Main)
        Dispatchers.setMain(mainThreadSurrogate)
    }


    @After
    fun tearDown() {
        runBlocking { flights.clearData() }
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }


    @Test
    fun `test we can store and then retrieve a flight booking`() = runBlocking {
        with(flights) {
            storeData(flightBookingMatchingObject)

            val model = execute().first()
            assertEquals(flightBookingMatchingObject, model)
        }
    }

    @Test
    fun `test we can store and then retrieve other flight booking`() = runBlocking {
        with(flights) {
            storeData(matchingOtherJson)

            val model = execute().first()
            assertEquals(matchingOtherJson, model)
        }
    }
}