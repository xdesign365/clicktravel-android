package com.clicktravel.database

import com.clicktravel.core.model.team.Team
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.sqlite.driver.JdbcSqliteDriver
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class TeamRepositoryTest {

    private lateinit var driver: SqlDriver
    private lateinit var repository: TeamRepository

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    private val fakeTeam = Team("a", "b", "c", "d")
    private val fakeTeamA = Team("e", "f", "g", "h")

    // todo fix this: No suitable driver found for jdbc:sqlite
//    @Before
//    fun setup() {
//        driver = JdbcSqliteDriver(JdbcSqliteDriver.IN_MEMORY)
//        ClickTravelDb.Schema.create(driver)
//        repository = TeamRepository(
//            driver, Dispatchers.Main)
//        Dispatchers.setMain(mainThreadSurrogate)
//    }
//
//
//    @After
//    fun tearDown() {
//        runBlocking { repository.clearData() }
//        Dispatchers.resetMain()
//        mainThreadSurrogate.close()
//    }
//
//    @Test
//    fun `test that we can store and then retrieve a team list`() = runBlocking {
//        val temp = listOf(fakeTeam, fakeTeamA)
//        repository.storeData(temp)
//        assertEquals(temp, repository.execute())
//    }
}