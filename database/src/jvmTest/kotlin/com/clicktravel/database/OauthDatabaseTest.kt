package com.clicktravel.database

import com.clicktravel.core.model.auth.OauthModel
import com.soywiz.klock.hours
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.sqlite.driver.JdbcSqliteDriver
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

class OauthDatabaseTest {

    private lateinit var driver: SqlDriver

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @Before
    fun setup() {
        driver = JdbcSqliteDriver(JdbcSqliteDriver.IN_MEMORY)
        ClickTravelDb.Schema.create(driver)
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `test adding a oauth model then retrieving it`() = runBlocking {
        val storeValue = OauthModel("A", 4.hours.millisecondsLong, "C", "R")
        with(LoggedInUserRepository(driver, Dispatchers.Main)) {
            storeData(storeValue)
            assertEquals(storeValue, execute())
        }
    }

    @Test
    fun `test clear a oauth model returns null`() = runBlocking {
        val storeValue = OauthModel("A", 4.hours.millisecondsLong, "C", "R")
        with(LoggedInUserRepository(driver, Dispatchers.Main)) {
            storeData(storeValue)
            assertEquals(storeValue, execute())
            clearData()
            assertNull(execute())
        }
    }
}