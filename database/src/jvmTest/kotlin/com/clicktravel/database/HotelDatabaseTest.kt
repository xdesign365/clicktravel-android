package com.clicktravel.database

import com.clicktravel.core.model.BillingType
import com.clicktravel.core.model.HotelBooking
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.trains.Note
import com.clicktravel.core.testutils.flightBookingMatchingObject
import com.clicktravel.core.testutils.matchingOtherJson
import com.clicktravel.core.ui.models.ClickLocation
import com.clicktravel.core.util.DateFormatter
import com.clicktravel.core.util.UtcDateFormatter
import com.soywiz.klock.DateTimeTz
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.sqlite.driver.JdbcSqliteDriver
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import kotlin.test.Test
import kotlin.test.assertEquals

class HotelDatabaseTest {


    private lateinit var driver: SqlDriver
    private lateinit var hotelRepo: HotelRepository

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")
    private val dateFormatter: DateFormatter = UtcDateFormatter()

    // todo fix tests as cannot run them:
    //  java.sql.SQLException: No suitable driver found for jdbc:sqlite:
//    @Before
//    fun setup() {
//        driver = JdbcSqliteDriver(JdbcSqliteDriver.IN_MEMORY)
//        ClickTravelDb.Schema.create(driver)
//        hotelRepo = HotelRepository(driver, Dispatchers.Main)
//        Dispatchers.setMain(mainThreadSurrogate)
//    }
//
//
//    @After
//    fun tearDown() {
//        runBlocking { hotelRepo.clearData() }
//        Dispatchers.resetMain()
//        mainThreadSurrogate.close()
//    }
//
//
//    @Test
//    fun `test we can store and then retrieve a hotel booking`() {
//        runBlocking {
//            with(hotelRepo) {
//                storeTeams(booking)
//
//                val actual = execute().first()
//                assertEquals(
//                    expected = booking,
//                    actual = actual
//                )
//            }
//        }
//    }
//
//    private val booking = HotelBooking(
//        bookingId = "200120412",
//        bookingTravelType = TravelTypes.HOTEL,
//        cancellationPolicy = "If you need to cancel or change your reservation, you can do so before 1:00 pm  on your arrival date.",
//        numberOfAdults = 1,
//        numberOfChildren = 0,
//        roomType = "Double room, non-smoking",
//        mealIncluded = "Meal Deal - Premier Inn Breakfast, plus ANY 2-course dinner and a selected drink",
////        specialRequest = specialRequest,
//        checkinDate = dateFormatter.parse("2020-01-27T00:00:00Z"),
//        checkoutDate = dateFormatter.parse("2020-01-28T00:00:00Z"),
//        hotelName = "Premier Inn Manchester West Didsbury",
//        hotelPhoneNumber = "0871 527  8722",
//        hotelAddress = "dummy", // todo check
//        notes = null,
//        fields = emptyList(),
//        team = "dummy team", // todo check
//        location = ClickLocation(
//            lat = 53.480759,
//            lon = -2.24263
//        ),
//        billingType = BillingType.ROOM_ONLY,
//        allCostsCovered = true,
//        isPremierInn = true,
//        travellerEmail = "mhairi.duff@xdesign.com"
//    )

}