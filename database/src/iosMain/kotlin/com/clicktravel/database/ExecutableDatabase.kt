package com.clicktravel.database

import com.clicktravel.core.model.UserDetails
import com.squareup.sqldelight.drivers.ios.NativeSqliteDriver
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import com.clicktravel.core.MainScope

class ExecutableDatabase {

    val database: UserDetailsRepository =
        UserDetailsRepository(NativeSqliteDriver(ClickTravelDb.Schema, "clicktravel.db"))
    val scope: CoroutineScope = MainScope()

    fun storeData(userDetails: UserDetails, completionHandler: (UserDetails) -> Unit) =
        scope.launch {
            database.storeData(userDetails)
                .let {
                    completionHandler(database.execute())
                }
        }

}