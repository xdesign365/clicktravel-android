package com.clicktravel.database

import com.clicktravel.core.model.*

class LocalBookingsRepository(
    private val flightRepository: FlightRepository,
    private val trainRepository: TrainRepository,
    private val hotelRepository: HotelRepository,
    private val travelCardRepository: TravelCardRepository
) {

    suspend fun getBookings(): List<BookingItem> = listOf(
        flightRepository.execute(),
        trainRepository.execute(),
        hotelRepository.execute(),
        travelCardRepository.execute()
    ).flatten()

    suspend fun storeBookings(bookings: List<BookingItem>) = bookings.forEach { item ->
        when (item) {
            is FlightBooking -> flightRepository.storeData(item)
            is TrainBooking -> trainRepository.storeData(item)
            is HotelBooking -> hotelRepository.storeData(item)
            is TravelCard -> travelCardRepository.storeData(item)
        }
    }

    suspend fun clearBookings() {
        flightRepository.clearData()
        trainRepository.clearData()
        hotelRepository.clearData()
        travelCardRepository.clearData()
    }

}