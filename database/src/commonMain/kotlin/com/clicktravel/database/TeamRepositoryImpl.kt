package com.clicktravel.database

import com.clicktravel.core.model.team.Team
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.repository.retriever.NoArgumentRetriever
import com.squareup.sqldelight.db.SqlDriver
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class TeamRepository(sqlDriver: SqlDriver, private val coroutineContext: CoroutineContext) :
    NoArgumentRetriever<List<Team>>,
    WriteOnlyRepository<List<Team>> {

    private val database = ClickTravelDb(sqlDriver)

    override suspend fun execute() = withContext(coroutineContext) {
        database.teamsQueries.selectAllTeams()
            .executeAsList().map {
                Team(
                    id = it.id,
                    name = it.name,
                    ownerId = it.ownerId,
                    shortId = it.shortId
                )
            }
    }

    override suspend fun storeData(storeData: List<Team>) = withContext(coroutineContext) {
        clearData()
        storeData.forEach {
            database.teamsQueries.addNewTeam(
                TeamEntity.Impl(
                    id = it.id,
                    name = it.name,
                    ownerId = it.ownerId,
                    shortId = it.shortId
                )
            )
        }
    }

    override suspend fun clearData() = database.teamsQueries.clear()
}