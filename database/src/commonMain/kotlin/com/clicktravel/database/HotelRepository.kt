package com.clicktravel.database

import com.clicktravel.core.model.*
import com.clicktravel.core.model.common.RequiredInformation
import com.clicktravel.core.model.trains.Note
import com.clicktravel.core.ui.models.ClickLocation
import com.clicktravel.core.util.DateFormatter
import com.clicktravel.core.util.UtcDateFormatter
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.repository.retriever.NoArgumentRetriever
import com.squareup.sqldelight.db.SqlDriver
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

class HotelRepository(
    sqlDriver: SqlDriver,
    private val databaseScope: CoroutineDispatcher,
    private val dateFormatter: DateFormatter = UtcDateFormatter()
) : WriteOnlyRepository<HotelBooking>, NoArgumentRetriever<List<HotelBooking>> {

    private val database = ClickTravelDb(sqlDriver)
    private val hotelQueries = database.hotelDetailQueries
    private val commonQueries = database.commonQueries

    /** Mark - Write Repository */
    override suspend fun storeData(storeData: HotelBooking) {
        withContext(databaseScope) {
            hotelQueries.apply {
                storeData.apply {
                    toEntity(dateFormatter).let(::insertHotelBooking)
                    billbackElements?.toEntity(storeData.bookingId)
                        ?.let(::insertHotelBillbackElements)
                    billbackElements?.mealSupplement?.toEntity(storeData.bookingId)
                        ?.let(::insertMealSupplementBillbackElement)
                    supportedBillbackOptions?.toEntity(storeData.bookingId)
                        ?.let(::insertHotelSupportedBillbackOptions)
                }
            }
        }
    }

    override suspend fun execute(): List<HotelBooking> =
        withContext(databaseScope) {
            hotelQueries
                .retrieveAllHotelBookings()
                .executeAsList()
                .map {
                    it.toCore(
                        dateFormatter = dateFormatter,
                        fields = commonQueries.selectRequiredDataForBooking(it.bookingId)
                            .executeAsList().map {
                                RequiredInformation(
                                    it.label,
                                    it.value
                                )
                            }.sortedBy { it.key },
                        billbackElements = hotelQueries
                            .retrieveHotelBillbackElementsForBooking(it.bookingId)
                            .executeAsOneOrNull()
                            ?.toCore(
                                mealSupplement = hotelQueries
                                    .retrieveMealSupplementBillbackElementForBooking(it.bookingId)
                                    .executeAsOne()
                            ),
                        supportedBillbackOptions = hotelQueries
                            .retrieveHotelSupportedBillbackOptionsForBooking(it.bookingId)
                            .executeAsOne()
                            .toCore()
                    )
                }
        }

    override suspend fun clearData() {
        withContext(databaseScope) {
            database
                .hotelDetailQueries
                .apply {
                    removeAllHotelBookings()
                    removeAllHotelBillbackElements()
                    removeAllMealSupplementBillbackElements()
                    removeAllHotelSupportedBillbackOptions()
                }
        }
    }
}

fun HotelBooking.toEntity(dateFormatter: DateFormatter) =
    HotelBookingEntity.Impl(
        bookingId = bookingId,
        bookingTravelType = bookingTravelType.name,
        cancellationPolicy = cancellationPolicy,
        numberOfAdults = numberOfAdults.toLong(),
        numberOfChildren = numberOfChildren.toLong(),
        roomType = roomType,
        mealIncluded = mealIncluded,
        specialRequest = specialRequest,
        checkinDate = dateFormatter.format(checkinDate),
        checkoutDate = dateFormatter.format(checkoutDate),
        hotelName = hotelName,
        hotelEmail = hotelEmail,
        hotelAddress = hotelAddress,
        hotelPhoneNumber = hotelPhoneNumber,
        notes = notes?.note,
        team = team,
        latitude = location?.lat,
        longitude = location?.lon,
        billingType = billingType.name,
        isPremierInn = isPremierInn.toString(),
        travellerEmail = travellerEmail
    )

fun HotelBillbackElements.toEntity(bookingId: String) =
    HotelBillbackElementsEntity.Impl(
        bookingId = bookingId,
        allCosts = allCosts.toString(),
        breakfast = breakfast.toString(),
        roomRate = roomRate.toString(),
        parking = parking.toString(),
        wifi = wifi.toString()
    )

fun MealSupplementBillbackElement.toEntity(bookingId: String) =
    MealSupplementBillbackElementEntity.Impl(
        bookingId = bookingId,
        enabled = enabled.toString(),
        allowance = allowance,
        currency = currency
    )

fun HotelSupportedBillbackOptions.toEntity(bookingId: String) =
    HotelSupportedBillbackOptionsEntity.Impl(
        bookingId = bookingId,
        breakfast = breakfast.toString(),
        roomRate = roomRate.toString(),
        parking = parking.toString(),
        wifi = wifi.toString(),
        mealSupplement = mealSupplement.toString()
    )

fun HotelBookingEntity.toCore(
    dateFormatter: DateFormatter,
    fields: List<RequiredInformation>,
    billbackElements: HotelBillbackElements?,
    supportedBillbackOptions: HotelSupportedBillbackOptions
) = HotelBooking(
    bookingId = bookingId,
    bookingTravelType = TravelTypes.valueOf(bookingTravelType),
    cancellationPolicy = cancellationPolicy,
    numberOfAdults = numberOfAdults.toInt(),
    numberOfChildren = numberOfChildren.toInt(),
    roomType = roomType,
    mealIncluded = mealIncluded,
    specialRequest = specialRequest,
    checkinDate = dateFormatter.parse(checkinDate),
    checkoutDate = dateFormatter.parse(checkoutDate),
    hotelName = hotelName,
    hotelEmail = hotelEmail,
    hotelAddress = hotelAddress,
    hotelPhoneNumber = hotelPhoneNumber,
    notes = notes?.let { Note(it) },
    fields = fields,
    team = team,
    location = latitude?.let { ClickLocation(latitude!!, longitude!!) },
    billingType = BillingType.valueOf(billingType),
    isPremierInn = isPremierInn.toBoolean(),
    travellerEmail = travellerEmail,
    billbackElements = billbackElements,
    supportedBillbackOptions = supportedBillbackOptions
)

fun HotelBillbackElementsEntity.toCore(
    mealSupplement: MealSupplementBillbackElementEntity
) = HotelBillbackElements(
    allCosts = allCosts.toBoolean(),
    breakfast = breakfast.toBoolean(),
    roomRate = roomRate.toBoolean(),
    parking = parking.toBoolean(),
    wifi = wifi.toBoolean(),
    mealSupplement = mealSupplement.toCore()
)

fun MealSupplementBillbackElementEntity.toCore() = MealSupplementBillbackElement(
    enabled = enabled.toBoolean(),
    allowance = allowance,
    currency = currency
)

fun HotelSupportedBillbackOptionsEntity.toCore() = HotelSupportedBillbackOptions(
    breakfast = breakfast.toBoolean(),
    roomRate = roomRate.toBoolean(),
    parking = parking.toBoolean(),
    wifi = wifi.toBoolean(),
    mealSupplement = mealSupplement.toBoolean()
)