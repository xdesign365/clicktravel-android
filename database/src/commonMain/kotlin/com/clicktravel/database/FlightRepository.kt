package com.clicktravel.database

import com.clicktravel.core.model.FlightBooking
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.common.TrainStationOrAirport
import com.clicktravel.core.model.flights.BaggageAllowance
import com.clicktravel.core.model.flights.CabinClass
import com.clicktravel.core.model.flights.DetailedFlightJourney
import com.clicktravel.core.model.flights.FlightSegment
import com.clicktravel.core.model.trains.Note
import com.clicktravel.core.model.trains.Provider
import com.clicktravel.core.ui.models.ClickLocation
import com.clicktravel.core.util.DateFormatter
import com.clicktravel.core.util.UtcDateFormatter
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.repository.retriever.NoArgumentRetriever
import com.squareup.sqldelight.db.SqlDriver
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

class FlightRepository(
    sqlDriver: SqlDriver,
    private val databaseScope: CoroutineDispatcher,
    private val dateFormatter: DateFormatter = UtcDateFormatter()
) : WriteOnlyRepository<FlightBooking>, NoArgumentRetriever<List<FlightBooking>> {

    private val database = ClickTravelDb(sqlDriver)

    override suspend fun storeData(storeData: FlightBooking) = withContext(databaseScope) {
        val queries = database.flightDetailQueries

        queries.insertFlightBookingEntity(
            FlightBookingEntity.Impl(
                bookingId = storeData.bookingId,
                notes = storeData.notes?.note,
                emailRequired = storeData.emailRequired.toString(),
                emailForCheckIn = storeData.emailForCheckIn,
                ticketCollectionReference = storeData.ticketCollectionReference,
                ticketNumbers = storeData.ticketNumbers.joinToString(","),
                teamId = storeData.teamId
            )
        )

        storeData.journeys.forEach { journey ->
            queries.insertFlightJourney(
                FlightJourneyEntity.Impl(
                    journeyId = journey.journeyId,
                    bookingId = storeData.bookingId
                )
            )

            journey.segments.forEach {
                queries.insertFlightSegment(
                    FlightSegmentEntity.Impl(
                        segment = it.segment,
                        journeyId = journey.journeyId,
                        flightNumber = it.flightNumber,
                        cabinClass = it.cabinClass.cabinClass,
                        cabinCode = it.cabinClass.code,
                        travelTime = it.travelTime,
                        legArrival = dateFormatter.format(it.legArrival),
                        legDeparture = dateFormatter.format(it.legDeparture),
                        baggageAllowanceItems = it.baggageAllowance.items.toLong(),
                        baggageAllowanceWeight = it.baggageAllowance.weight,
                        operatingAirlineCode = it.operatingAirline.code,
                        operatingAirlineName = it.operatingAirline.name,
                        departureLocation = it.departureLocation.code,
                        arrivalLocation = it.arrivalLocation.code
                    )
                )

                queries.insertAirport(
                    AirportEntity.Impl(
                        code = it.arrivalLocation.code,
                        name = it.arrivalLocation.name,
                        latitude = it.arrivalLocation.geo?.lat,
                        longitude = it.arrivalLocation.geo?.lon
                    )
                )

                queries.insertAirport(
                    AirportEntity.Impl(
                        code = it.departureLocation.code,
                        name = it.departureLocation.name,
                        latitude = it.departureLocation.geo?.lat,
                        longitude = it.departureLocation.geo?.lon
                    )
                )
            }

        }
    }

    override suspend fun clearData() {
        withContext(databaseScope) {
            database.flightDetailQueries.apply {
                removeFlightBookings()
                removeFlightJourneys()
                removeFlightSegments()
                removeAirports()
            }
        }
    }

    override suspend fun execute() = withContext(databaseScope) {
        database.flightDetailQueries.selectAllBookings()
            .executeAsList()
            .map {
                FlightBooking(
                    bookingId = it.bookingId,
                    ticketCollectionReference = it.ticketCollectionReference,
                    ticketNumbers = it.ticketNumbers.split(","),
                    notes = it.notes?.let { Note(it) },
                    bookingTravelType = TravelTypes.FLIGHT,
                    emailRequired = it.emailRequired.toBoolean(),
                    emailForCheckIn = it.emailForCheckIn,
                    journeys = database.flightDetailQueries.selectAllJourneys(it.bookingId).executeAsList()
                        .map {
                            DetailedFlightJourney(
                                journeyId = it.journeyId,
                                segments = database.flightDetailQueries.selectAllSegments(it.journeyId)
                                    .executeAsList()
                                    .map {
                                        FlightSegment(
                                            segment = it.segment,
                                            flightNumber = it.flightNumber,
                                            cabinClass = CabinClass(it.cabinClass, it.cabinCode),
                                            travelTime = it.travelTime,
                                            legArrival = dateFormatter.parse(it.legArrival),
                                            legDeparture = dateFormatter.parse(it.legDeparture),
                                            baggageAllowance = BaggageAllowance(
                                                it.baggageAllowanceItems.toInt(),
                                                it.baggageAllowanceWeight
                                            ),
                                            operatingAirline = Provider(
                                                it.operatingAirlineName,
                                                it.operatingAirlineCode
                                            ),
                                            departureLocation = getAirport(it.departureLocation),
                                            arrivalLocation = getAirport(it.arrivalLocation)
                                        )
                                    }
                            )
                        },
                    team = it.teamId
                )
            }

    }

    private fun getAirport(code: String) = database.flightDetailQueries.selectAirportsForCode(code)
        .executeAsOne().let { airport ->
            TrainStationOrAirport(
                airport.name,
                airport.code,
                airport.latitude?.let { lat ->
                    airport.longitude?.let { lon ->
                        ClickLocation(lat, lon)
                    }
                })
        }
}

