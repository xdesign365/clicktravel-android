package com.clicktravel.database

import com.clicktravel.core.model.auth.OauthModel
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.repository.retriever.NoArgumentRetriever
import com.squareup.sqldelight.db.SqlDriver
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

class LoggedInUserRepository(
    sqlDriver: SqlDriver,
    private val databaseScope: CoroutineDispatcher
) : WriteOnlyRepository<OauthModel>, NoArgumentRetriever<OauthModel?> {

    private val database by lazy {
        ClickTravelDb(sqlDriver)
            .also {
                ClickTravelDb.Schema.create(sqlDriver)
            }
    }

    override suspend fun storeData(storeData: OauthModel) = withContext(databaseScope) {
        with(storeData) {
            clearData()

            database.loggedInUserQueries.insertNewUser(
                OauthModelEntity.Impl(
                    accessToken = accessToken,
                    tokenType = tokenType,
                    expiresIn = expiresIn,
                    refreshToken = refreshToken
                )
            )
        }
    }

    override suspend fun clearData() {
        database.loggedInUserQueries.clearUser()
    }

    override suspend fun execute() =
        withContext(databaseScope) {
            try {
                database.loggedInUserQueries.getLoggedInUser().executeAsOneOrNull()?.let {
                    OauthModel(it.accessToken, it.expiresIn, it.tokenType, it.refreshToken)
                }
            } catch (e: Exception) {
                database.loggedInUserQueries.clearUser()
                null
            }
        }

}