package com.clicktravel.database

import com.clicktravel.core.model.TrainBooking
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.common.Address
import com.clicktravel.core.model.common.RequiredInformation
import com.clicktravel.core.model.common.TrainStationOrAirport
import com.clicktravel.core.model.trains.*
import com.clicktravel.core.model.trains.enums.DeliveryOption
import com.clicktravel.core.model.trains.enums.PassengerType
import com.clicktravel.core.ui.models.ClickLocation
import com.clicktravel.core.util.DateFormatter
import com.clicktravel.core.util.UtcDateFormatter
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.repository.retriever.NoArgumentRetriever
import com.squareup.sqldelight.db.SqlDriver
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext


class TrainRepository(
    sqlDriver: SqlDriver,
    private val databaseScope: CoroutineDispatcher,
    private val dateFormatter: DateFormatter = UtcDateFormatter()
) :
    WriteOnlyRepository<TrainBooking>, NoArgumentRetriever<List<TrainBooking>> {

    private val database = ClickTravelDb(sqlDriver)

    /** Mark - Write Repository */
    override suspend fun storeData(storeData: TrainBooking) = withContext(databaseScope) {
        val queries = database.trainDetailQueries
        val common = database.commonQueries

        queries.insertTrainBooking(storeData.toEntity())

        storeData.fields.map { RequiredDataEntity.Impl(storeData.bookingId, it.key, it.value) }
            .forEach(common::insertRequiredData)

        storeData.journeys.forEach {

            it.segments.forEachIndexed { index, segment ->
                queries.insertTrainStation(segment.arrivalLocation.toEntity())
                queries.insertTrainStation(segment.departureLocation.toEntity())
                queries.insertJourneySegment(
                    TrainSegmentEntity.Impl(
                        arriveLocation = segment.arrivalLocation.code,
                        departLocation = segment.departureLocation.code,
                        journeyId = it.journeyId,
                        provider = segment.provider.name,
                        connectionTime = segment.connectionTime,
                        travelTime = segment.travelTime,
                        reservation = segment.reservation?.let { reservation ->
                            "${reservation.coachClass},${reservation.coach},${reservation.seat}"
                        },
                        travelType = segment.travelType.name.toUpperCase(),
                        bookingId = storeData.bookingId,
                        arriveDate = dateFormatter.format(segment.legArrival),
                        departDate = dateFormatter.format(segment.legDeparture),
                        segmentId = index.toLong(),
                        trainClassDetail = segment.trainClassInformation.classDetails
                    )
                )

            }

            queries.insertTrainJourney(getDetailedTrainJourneyEntity(it, storeData.bookingId))
        }
    }

    private fun getDetailedTrainJourneyEntity(
        detailedTrainJourney: DetailedTrainJourney,
        bookingId: String
    ) = DetailedTrainJourneyEntity.Impl(
        arrivalStation = detailedTrainJourney.arrivalStation.code,
        departureStation = detailedTrainJourney.departureStation.code,
        bookingId = bookingId,
        departureDateTime = dateFormatter.format(detailedTrainJourney.departureDateTime),
        ticketType = detailedTrainJourney.ticketType.type,
        journeyId = detailedTrainJourney.journeyId,
        couponId = detailedTrainJourney.couponId
    )

    override suspend fun clearData() {
        database.trainDetailQueries.apply {
            removeTrainBookings()
            removeDetailedTrainJourneys()
            removeTrainSegments()
            removeTrainStations()
        }
    }

    /** Mark - Read repository */

    override suspend fun execute() = withContext(databaseScope) {
        database.trainDetailQueries.retrieveAll()
            .executeAsList()
            .map {
                TrainBooking(
                    bookingId = it.bookingId,
                    fareType = it.fareType,
                    fareClass = it.fareClass,
                    ticketCollectionReference = it.ticketCollectionReference,
                    bookingTravelType = TravelTypes.valueOf(it.bookingTravelType),
                    passengerType = PassengerType.valueOf(it.passengerType),
                    railcard = it.railcard?.let { rc -> Railcard(rc) },
                    deliveryOption = DeliveryOption.valueOf(it.deliveryOption),
                    forTheAttentionOf = it.forTheAttentionOf,
                    address = with(it.address.split(",")) {
                        Address(this[0], this[1], this[2])
                    },
                    journeys = getJourneys(it.bookingId),
                    notes = it.notes?.let { note -> Note(note) },
                    fields = database.commonQueries.selectRequiredDataForBooking(it.bookingId)
                        .executeAsList().map {
                            RequiredInformation(
                                it.label,
                                it.value
                            )
                        }.sortedBy { it.key },
                    team = it.teamId
                )
            }
    }

    private fun getJourneys(bookingId: String): List<DetailedTrainJourney> =
        database.trainDetailQueries.retrieveTrainJourney(bookingId).executeAsList().map {
            DetailedTrainJourney(
                departureStation = getStation(it.departureStation),
                arrivalStation = getStation(it.arrivalStation),
                departureDateTime = dateFormatter.parse(it.departureDateTime),
                ticketType = TicketType(it.ticketType),
                journeyId = it.journeyId,
                segments = getSegments(
                    database.trainDetailQueries.retrieveSegmentForJourney(
                        it.journeyId,
                        it.bookingId
                    ).executeAsList()
                ),
                couponId = it.couponId
            )
        }

    private fun getSegments(segments: List<TrainSegmentEntity>) =
        segments.map {
            TrainSegment(
                travelType = ConnectionOrJourneyType.valueOf(it.travelType),
                legArrival = dateFormatter.parse(it.arriveDate),
                travelTime = it.travelTime,
                connectionTime = it.connectionTime,
                provider = Provider(it.provider),
                trainClassInformation = TrainClassInformation(it.trainClassDetail),
                legDeparture = dateFormatter.parse(it.departDate),
                arrivalLocation = getStation(it.arriveLocation),
                departureLocation = getStation(it.departLocation),
                reservation = with(it.reservation?.split(",")) {
                    this?.let { SeatReservation(this[0], this[1], this[2].toInt()) }
                }
            )
        }

    private fun getStation(code: String): TrainStationOrAirport =
        database.trainDetailQueries.retrieveStation(code).executeAsOne().let {
            TrainStationOrAirport(
                name = it.name,
                code = it.code,
                geo = it.latitude?.let { lat ->
                    it.longitude?.let { lon ->
                        ClickLocation(lat, lon)
                    }
                },
                trainCrs = it.trainCrs
            )
        }
}


fun TrainBooking.toEntity() = TrainBookingEntity.Impl(
    bookingId = bookingId,
    ticketCollectionReference = ticketCollectionReference,
    bookingTravelType = bookingTravelType.name.toUpperCase(),
    passengerType = passengerType.name.toUpperCase(),
    address = listOf(address.firstLine, address.secondLine, address.thirdLine).joinToString(","),
    railcard = railcard?.name?.toUpperCase(),
    deliveryOption = deliveryOption.name.toUpperCase(),
    fareClass = fareClass,
    fareType = fareType,
    forTheAttentionOf = forTheAttentionOf,
    notes = notes?.note,
    teamId = team
)


fun TrainStationOrAirport.toEntity() = TrainStationEntity.Impl(
    code = code,
    name = name,
    latitude = geo?.lat,
    longitude = geo?.lon,
    trainCrs = trainCrs
)