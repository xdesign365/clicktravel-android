package com.clicktravel.database

import com.clicktravel.core.model.TravelCard
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.common.Address
import com.clicktravel.core.model.common.RequiredInformation
import com.clicktravel.core.model.trains.Note
import com.clicktravel.core.model.trains.enums.DeliveryOption
import com.clicktravel.core.model.trains.enums.PassengerType
import com.clicktravel.core.util.DateFormatter
import com.clicktravel.core.util.UtcDateFormatter
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.repository.retriever.NoArgumentRetriever
import com.squareup.sqldelight.db.SqlDriver
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

class TravelCardRepository(
    sqlDriver: SqlDriver,
    private val databaseScope: CoroutineDispatcher,
    private val dateFormatter: DateFormatter = UtcDateFormatter()
) :
    WriteOnlyRepository<TravelCard>, NoArgumentRetriever<List<TravelCard>> {

    private val database = ClickTravelDb(sqlDriver)

    /** Mark - Write Repository */
    override suspend fun storeData(storeData: TravelCard) = withContext(databaseScope) {
        database
            .travelCardDetailQueries
            .insertTravelCardBooking(storeData.toEntity(dateFormatter))
    }

    override suspend fun clearData() = withContext(databaseScope) {
        database
            .travelCardDetailQueries
            .removeAll()
    }

    /** Mark - Read repository */
    override suspend fun execute(): List<TravelCard> = withContext(databaseScope) {
        database
            .travelCardDetailQueries
            .retrieveAll()
            .executeAsList()
            .map {
                it.toCore(
                    dateFormatter = dateFormatter,
                    fields = database.commonQueries.selectRequiredDataForBooking(it.bookingId)
                        .executeAsList().map {
                            RequiredInformation(
                                it.label,
                                it.value
                            )
                        }.sortedBy { it.key }
                )
            }
    }

}

fun TravelCard.toEntity(dateFormatter: DateFormatter) =
    TravelCardEntity.Impl(
        bookingId = bookingId,
        travelDate = dateFormatter.format(travelDate),
        deliveryOption = deliveryOption.name,
        totalFare = totalFare,
        fareCurrency = fareCurrency,
        passengerType = passengerType.name,
        ctrReference = ctrReference,
        ticketDescription = ticketDescription,
        ticketType = ticketType,
        notes = notes?.note,
        team = team,
        travelTypes = travelTypes.name,
        address = listOf(
            address.firstLine,
            address.secondLine,
            address.thirdLine
        ).joinToString(","),
        forTheAttentionOf = forTheAttentionOf
    )

fun TravelCardEntity.toCore(
    dateFormatter: DateFormatter,
    fields: List<RequiredInformation>
) = TravelCard(
    bookingId = bookingId,
    travelDate = dateFormatter.parse(travelDate),
    deliveryOption = DeliveryOption.valueOf(deliveryOption),
    totalFare = totalFare,
    fareCurrency = fareCurrency,
    passengerType = PassengerType.valueOf(passengerType),
    ctrReference = ctrReference,
    ticketDescription = ticketDescription,
    ticketType = ticketType,
    notes = notes?.let { Note(it) },
    fields = fields,
    team = team,
    travelTypes = TravelTypes.valueOf(travelTypes),
    address = with(address.split(",")) {
        Address(this[0], this[1], this[2])
    },
    forTheAttentionOf = forTheAttentionOf
)