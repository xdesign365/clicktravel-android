package com.clicktravel.database

import com.clicktravel.core.model.UserDetails
import com.clicktravel.core.writer.WriteOnlyRepository
import com.squareup.sqldelight.db.SqlDriver
import com.clicktravel.repository.retriever.NoArgumentRetriever

class UserDetailsRepository(sqlDriver: SqlDriver) : NoArgumentRetriever<UserDetails?>,
    WriteOnlyRepository<UserDetails> {

    private val database = ClickTravelDb(sqlDriver)

    override suspend fun storeData(storeData: UserDetails) {
        clearData()
        database.userDetailQueries.insertInto(storeData.toEntity())
    }

    override suspend fun clearData() {
        database.userDetailQueries.removeAll()
    }

    override suspend fun execute() =
        database.userDetailQueries.selectUser().executeAsOneOrNull()?.toCore()

}

fun UserDetailsEntity.toCore() = UserDetails(
    id,
    firstName,
    lastName,
    mobileNumber,
    phoneNumber,
    primaryEmailAddress,
    title
)

fun UserDetails.toEntity() = UserDetailsEntity.Impl(
    id,
    firstName,
    lastName,
    mobileNumber,
    phoneNumber,
    primaryEmailAddress,
    title
)