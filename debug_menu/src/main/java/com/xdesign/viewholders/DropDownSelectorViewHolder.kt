package com.xdesign.viewholders

import android.content.Context
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import com.xdesign.R
import com.xdesign.enums.InputType
import com.xdesign.enums.MultiSelectionClass

class DropDownSelectorViewHolder(view: View) : BaseViewHolder(view),
    AdapterView.OnItemSelectedListener {

    lateinit var configuration: MultiSelectionClass

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(adapterView: AdapterView<*>, p1: View?, p2: Int, p3: Long) {
        configuration.onPropertyChanged?.let { callback ->
            (configuration.options[adapterView.selectedItem as String])?.let {
                callback(it)
            }
        }
    }

    override fun bind(obj: InputType) {
        when (obj) {
            is MultiSelectionClass -> {
                configuration = obj

                view.findViewById<TextView>(R.id.dropDownSelectorTitle).apply {
                    text = obj.title
                }

                view.findViewById<Spinner>(R.id.dropDownOptions).apply {
                    adapter = buildArrayAdapter(context)
                    onItemSelectedListener = this@DropDownSelectorViewHolder
                    setSelection(configuration.options.values.indexOf(obj.getCurrentlySelectedValue()))
                }
            }
        }
    }


    private fun buildArrayAdapter(context: Context) =
        ArrayAdapter(context,
            android.R.layout.simple_spinner_dropdown_item,
            ArrayList<String>().apply {
                addAll(configuration.options.keys)
            }
        )
}