package com.xdesign.viewholders

import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.core.widget.addTextChangedListener
import com.xdesign.R
import com.xdesign.enums.FreeFieldTextInput
import com.xdesign.enums.InputType

class FreeFormTextSelectorViewHolder(view: View) : BaseViewHolder(view) {
    lateinit var configuration: FreeFieldTextInput

    override fun bind(obj: InputType) {
        when (obj) {
            is FreeFieldTextInput -> {
                configuration = obj

                view.findViewById<TextView>(R.id.freeFormTextSample).apply {
                    text = configuration.title
                }

                view.findViewById<EditText>(R.id.freeFormCurrentText).apply {
                    setText(configuration.getCurrentValue())
                    addTextChangedListener {
                        configuration.onTextChanged(text.toString())
                    }
                }
            }
        }
    }

}