package com.xdesign.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.xdesign.enums.InputType


abstract class BaseViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    abstract fun bind(obj: InputType)
}

