package com.xdesign.viewholders

import android.view.View
import android.widget.Button
import com.xdesign.R
import com.xdesign.enums.ButtonInput
import com.xdesign.enums.InputType

class ButtonViewHolder(view: View) : BaseViewHolder(view) {
    override fun bind(obj: InputType) {
        when (obj) {
            is ButtonInput -> with(view.findViewById<Button>(R.id.clickyButton)) {
                setOnClickListener {
                    obj.onButtonClicked()
                }
                text = obj.title
            }
        }
    }
}