package com.xdesign.enums


sealed class InputType(val name: String, val title : String)

class MultiSelectionClass(
    title: String,
    val options: Map<String, String>,
    val getCurrentlySelectedValue: () -> String,
    val onPropertyChanged: ((String) -> Unit)? = null
) : InputType("multi", title)


class FreeFieldTextInput(
    title: String,
    val onTextChanged: (String) -> Unit,
    val getCurrentValue: () -> String
) : InputType("free", title)

class ButtonInput(
    title: String,
    val onButtonClicked: () -> Unit
) : InputType("button", title)