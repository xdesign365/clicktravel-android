package com.xdesign.enums

import com.clicktravel.core.Settings


class DebugMenuBuilder {

    val items = mutableListOf<InputType>()

    fun freeFieldTextSetting(
        text: String, onTextChanged: (String) -> Unit, getCurrentValue: () -> String
    ) {
        items.add(FreeFieldTextInput(text, onTextChanged, getCurrentValue))
    }

    fun freeFieldTextSetting(text: String) {
        items.add(
            FreeFieldTextInput(
                text,
                {
                    Settings.setProperty(text, it)
                }
            ) {
                Settings.getProperty(text)
            }
        )
    }

    fun multipleTextSetting(text: String, options: List<MultiTextConfig>) {
        val map = mutableMapOf<String, String>()

        for (option in options) {
            map[option.key] = option.value
        }

        items.add(MultiSelectionClass(text, map, {
            Settings.getProperty(text)
        }) {
            Settings.setProperty(text, it)
        })
    }

    fun multipleTextSetting(text: String, block: () -> List<MultiTextConfig>) =
        multipleTextSetting(text, block())

    fun button(text: String, onClick: () -> Unit) {
        items.add(ButtonInput(text, onClick))
    }

    fun build() = DebugMenuConfiguration(items)
}


fun debugMenu(block: DebugMenuBuilder.() -> Unit) =
    DebugMenuBuilder().apply {
        block()
        build()
    }

data class DebugMenuConfiguration(val configurationItems: List<InputType>)