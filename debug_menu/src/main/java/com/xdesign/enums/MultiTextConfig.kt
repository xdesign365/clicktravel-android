package com.xdesign.enums

data class MultiTextConfig(val key: String, val value: String)

class MultiTextConfigBuilder {
    var value: String? = null
    var key: String? = null

    fun build() = MultiTextConfig(key!!, value!!)
}

fun multiTextConfig(block: MultiTextConfigBuilder.() -> Unit) =
    with(MultiTextConfigBuilder()) {
        block()
        build()
    }
