package com.xdesign.chathead

import android.app.Service
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Build
import android.os.IBinder
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import com.xdesign.DebugMenuActivity
import com.xdesign.R


class ChatHeadService : Service() {
    lateinit var mWindowManager: WindowManager
    lateinit var mChatHeadView: View

    override fun onCreate() {
        super.onCreate()
        mChatHeadView = LayoutInflater.from(this).inflate(R.layout.chat_head, null, false)


        //Add the view to the window.
        val params =
            WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
                } else {
                    WindowManager.LayoutParams.TYPE_PHONE
                },
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT
            )

        params.gravity = Gravity.BOTTOM or Gravity.RIGHT
        params.x = 0
        params.y = 100

        //Add the view to the window
        mWindowManager = getSystemService(WINDOW_SERVICE) as WindowManager
        mWindowManager.addView(mChatHeadView, params)

        setupHeadsUpButton()
        setupChatHeadClickDragger(params)
    }

    private fun setupChatHeadClickDragger(params: WindowManager.LayoutParams) {
        with(mChatHeadView.findViewById<ImageView>(R.id.chat_head_profile_iv)) {
            setOnClickListener {
                val intent =
                    Intent(this@ChatHeadService, DebugMenuActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                stopSelf()
            }
        }
    }

    private fun setupHeadsUpButton() {
        val closeButton: ImageView =
            mChatHeadView.findViewById<View>(R.id.close_btn) as ImageView

        closeButton.setOnClickListener {
            stopSelf()
        }
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }


    override fun onDestroy() {
        super.onDestroy()
        if (mChatHeadView != null) mWindowManager.removeView(mChatHeadView)
    }
}