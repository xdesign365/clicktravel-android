package com.xdesign

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.xdesign.enums.ButtonInput
import com.xdesign.enums.FreeFieldTextInput
import com.xdesign.enums.InputType
import com.xdesign.enums.MultiSelectionClass
import com.xdesign.viewholders.BaseViewHolder
import com.xdesign.viewholders.ButtonViewHolder
import com.xdesign.viewholders.DropDownSelectorViewHolder
import com.xdesign.viewholders.FreeFormTextSelectorViewHolder

abstract class BaseRecyclerViewAdapter :
    RecyclerView.Adapter<BaseViewHolder>() {

    override fun onBindViewHolder(
        holder: BaseViewHolder,
        position: Int
    ) = holder.bind(getObjForPosition(position))

    override fun getItemViewType(position: Int): Int = getLayoutIdForPosition(position)

    protected abstract fun getObjForPosition(position: Int): InputType

    protected abstract fun getLayoutIdForPosition(position: Int): Int
}


class PropertyListAdapter : BaseRecyclerViewAdapter() {

    private val list = mutableListOf<InputType>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        when (viewType) {
            R.layout.drop_down_selector -> DropDownSelectorViewHolder(
                LayoutInflater.from(parent.context).inflate(viewType, parent, false)
            )
            R.layout.free_form_text_input -> FreeFormTextSelectorViewHolder(
                LayoutInflater.from(parent.context).inflate(viewType, parent, false)
            )
            R.layout.input_button -> ButtonViewHolder(
                LayoutInflater.from(parent.context).inflate(viewType, parent, false)
            )
            else -> throw IllegalArgumentException()
        }

    fun updateList(list: List<InputType>) {
        this.list.clear()
        this.list.addAll(list)
        this.notifyDataSetChanged()
    }

    override fun getObjForPosition(position: Int) = list[position]

    override fun getLayoutIdForPosition(position: Int) = when (getObjForPosition(position)) {
        is MultiSelectionClass -> R.layout.drop_down_selector
        is FreeFieldTextInput -> R.layout.free_form_text_input
        is ButtonInput -> R.layout.input_button
    }

    override fun getItemCount() = list.size
}