pipeline {

    agent {
        node {
            label 'android'
        }
    }

    environment {
        APPCENTER_API_KEY = credentials('appcenter-global-key')
        CLICKTRAVEL_RELEASE_KEY_PASSWORD = credentials('CLICKTRAVEL_RELEASE_KEY_PASSWORD')
        CLICKTRAVEL_RELEASE_KEY_ALIAS_PASSWORD = credentials('CLICKTRAVEL_RELEASE_KEY_ALIAS_PASSWORD')
        CLICKTRAVEL_DEBUG_KEY_PASSWORD = credentials('CLICKTRAVEL_DEBUG_KEY_PASSWORD')
        CLICKTRAVEL_DEBUG_KEY_ALIAS_PASSWORD = credentials('CLICKTRAVEL_DEBUG_KEY_ALIAS_PASSWORD')
        AWS_ACCESS_KEY = credentials('AWS_ACCESS_KEY')
        AWS_SECRET_KEY = credentials('AWS_SECRET_KEY')
        SLACK_CHANNEL_NAME = 'clicktravel-dev'
    }

    stages {

        stage('Clean') {
            steps {
                sh "./gradlew --stop"
                sh "./gradlew clean"
            }
        }

        stage('Build core') {
            steps {
                sh "./gradlew --stop"
                sh "./gradlew core:build"
                sh "./gradlew backend:build"
                sh "./gradlew database:build"
                sh "./gradlew repository:build"
                sh "./gradlew app:build"
            }
        }

        stage('Test') {
            steps {
                sh "./gradlew test"
            }
        }

        stage('Assemble') {
            parallel {
                stage('Assemble Debug') {
                    steps {
                        sh "./gradlew assembleDebug"
                        sh "./gradlew bundle"
                    }
                }
                stage('Assemble Release') {
                    steps {
                        sh "./gradlew assembleRelease"
                        sh "./gradlew bundle"
                    }
                }
            }
        }

        stage('Artifact') {
            steps {
                archiveArtifacts '**/*.apk'
                archiveArtifacts '**/*.aab'
                archiveArtifacts 'app/build/outputs/mapping/release/mapping.txt'
            }
        }

        stage('Release to QA') {
            when {
                branch 'develop'
            }
            steps {
                appCenter apiToken: env.APPCENTER_API_KEY, appName: 'Clicktravel-Android-Prod', distributionGroups: 'Collaborators', ownerName: 'xDesign', pathToApp: 'app/build/outputs/apk/release/app-release.apk'
            }
        }

        stage('Set Metrics') {
            steps {
                androidLint canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '**/lint-results*.xml', unHealthy: ''
                junit '*/build/test-results/**/*.xml'
            }
        }
    }

    post {
        failure {
            slackSend channel: SLACK_CHANNEL_NAME, color: 'danger', message: "${env.BUILD_TAG} FAILED!\n${env.JOB_DISPLAY_URL}\n"
        }

        success {
            slackSend channel: SLACK_CHANNEL_NAME, color: 'good', message: "${env.BUILD_TAG} succeeded!\n${env.JOB_DISPLAY_URL}\n"
        }
    }
}