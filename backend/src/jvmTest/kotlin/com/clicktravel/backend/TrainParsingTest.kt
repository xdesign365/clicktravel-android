package com.clicktravel.backend

import com.clicktravel.backend.model.futurebookings.BookingItemApi
import com.clicktravel.backend.model.futurebookings.TrainApi
import com.clicktravel.backend.model.futurebookings.train.toCore
import com.clicktravel.core.testutils.*
import kotlinx.serialization.json.Json
import org.junit.Test
import kotlin.test.assertEquals

class TrainParsingTest {

    private val json = Json {
        ignoreUnknownKeys = true
        isLenient = true
    }

    @Test
    fun `test we can parse train json using our existing model`() {
        // Data is from https://xsolutions.atlassian.net/wiki/spaces/CLIC/pages/707952789/Clicktravel+dummy+data
        // As is the expected model
        val asProduct = json.decodeFromString(BookingItemApi.serializer(), trainJson).product
        val model = (asProduct as TrainApi).toCore("200111619")

        assertEquals(model.journeys[0].segments[0], matchingDataModelOutbound.segments[0], "This means that we have failed to parse something from the json. Maybe a JSON value has changed or the models are not in synch with the json")
        assertEquals(model.journeys[0].segments[1], matchingDataModelOutbound.segments[1], "This means that we have failed to parse something from the json. Maybe a JSON value has changed or the models are not in synch with the json")
        assertEquals(model.journeys[0].segments[2], matchingDataModelOutbound.segments[2], "This means that we have failed to parse something from the json. Maybe a JSON value has changed or the models are not in synch with the json")
        assertEquals(model.journeys[1].segments[0], matchingDataModelReturn.segments[0], "This means that we have failed to parse something from the json. Maybe a JSON value has changed or the models are not in synch with the json")
        assertEquals(model.journeys[1].segments[1], matchingDataModelReturn.segments[1], "This means that we have failed to parse something from the json. Maybe a JSON value has changed or the models are not in synch with the json")
        assertEquals(model.journeys[1].segments[2], matchingDataModelReturn.segments[2], "This means that we have failed to parse something from the json. Maybe a JSON value has changed or the models are not in synch with the json")
        assertEquals(model.journeys[1].segments[3], matchingDataModelReturn.segments[3], "This means that we have failed to parse something from the json. Maybe a JSON value has changed or the models are not in synch with the json")
        assertEquals(model.journeys[0],
            matchingDataModelOutbound, "This means that we have failed to parse something from the json. Maybe a JSON value has changed or the models are not in synch with the json")
        assertEquals(model.journeys[1],
            matchingDataModelReturn, "This means that we have failed to parse something from the json. Maybe a JSON value has changed or the models are not in synch with the json")
        assertEquals(booking, model, "This means that we have failed to parse something from the json. Maybe a JSON value has changed or the models are not in synch with the json")
    }

    @Test
    fun `test we can parse train json using our existing model Cheltenham`() {
        val model = (json.decodeFromString(
            BookingItemApi.serializer(),
            trainJson2
        ).product as TrainApi).toCore("200116080")

        assertEquals(cheltenhamBookingWithETicket, model, "This means that we have failed to parse something from the json. Maybe a JSON value has changed or the models are not in synch with the json")
    }

    @Test
    fun `test that a ferry booking is parsed correctly`() {
        val ferryModel =
                json.decodeFromString(BookingItemApi.serializer(), ferryBookingJson)
        with ((ferryModel.product as TrainApi).toCore("200116513")) {
            assertEquals(ferryBookingJourney.segments[0], journeys.first().segments[0])
            assertEquals(ferryBookingJourney.segments[1], journeys.first().segments[1])
            assertEquals(ferryBookingJourney.segments[2], journeys.first().segments[2])
            assertEquals(ferryBookingJourney, journeys.first())
            assertEquals(ferryBooking, this)
        }
    }
}

