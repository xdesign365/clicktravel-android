package com.clicktravel.backend

import com.clicktravel.backend.model.futurebookings.FlightApi
import com.clicktravel.backend.model.futurebookings.flights.FlightCheckinInfo
import com.clicktravel.backend.model.futurebookings.flights.toCore
import com.clicktravel.core.testutils.flightBookingJson
import com.clicktravel.core.testutils.flightBookingMatchingObject
import com.clicktravel.core.testutils.matchingOtherJson
import com.clicktravel.core.testutils.otherBookingItemJson
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

class FlightParsingTest {

    private val json = Json {
        ignoreUnknownKeys = true
        isLenient = true
    }

    @Test
    fun `test parsing flight data`() {
        with(
            json.decodeFromString(
                FlightApi.serializer(),
                flightBookingJson
            ).toCore(
                bookingId = flightBookingMatchingObject.bookingId,
                flightCheckinInfo = FlightCheckinInfo(
                    emailRequired = true,
                    emailForCheckIn = "checkin@email.com"
                )
            )
        ) {
            assertEquals(flightBookingMatchingObject.bookingId, bookingId)
            assertEquals(flightBookingMatchingObject.bookingTravelType, bookingTravelType)
            assertEquals(flightBookingMatchingObject, this)
        }
    }

    @Test
    fun `test parsing other flight data`() {
        with(
            json.decodeFromString(
                FlightApi.serializer(),
                otherBookingItemJson
            ).toCore(
                bookingId = matchingOtherJson.bookingId,
                flightCheckinInfo = FlightCheckinInfo(
                    emailRequired = false,
                    emailForCheckIn = null
                )
            )
        ) {
            assertEquals(matchingOtherJson.bookingId, bookingId)
            assertEquals(matchingOtherJson.bookingTravelType, bookingTravelType)
            assertEquals(matchingOtherJson, this)
        }
    }
}