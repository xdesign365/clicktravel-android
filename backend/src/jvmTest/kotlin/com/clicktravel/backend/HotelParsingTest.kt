package com.clicktravel.backend

import com.clicktravel.backend.model.futurebookings.HotelApi
import com.clicktravel.core.testutils.selectedHotel
import kotlinx.serialization.json.Json
import org.junit.Test
import kotlin.test.assertEquals

class HotelParsingTest {

    private val json = Json {
        ignoreUnknownKeys = true
        isLenient = true
    }

    @Test
    fun `test that we can parse a hotel`() {
        with (json.decodeFromString(HotelApi.serializer(), selectedHotel)) { }
    }

}