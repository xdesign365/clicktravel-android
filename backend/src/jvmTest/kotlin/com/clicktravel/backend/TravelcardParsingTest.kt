package com.clicktravel.backend

import com.clicktravel.backend.model.futurebookings.TravelCardApi
import com.clicktravel.core.testutils.sampleTravelcard
import kotlinx.serialization.json.Json
import org.junit.Test

class TravelcardParsingTest {

    @Test
    fun `test that parsing a travelcard works`() {
        Json { ignoreUnknownKeys = true }.decodeFromString(TravelCardApi.serializer(), sampleTravelcard)
    }
}