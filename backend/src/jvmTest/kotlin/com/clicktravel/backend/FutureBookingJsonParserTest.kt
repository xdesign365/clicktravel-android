package com.clicktravel.backend

import com.clicktravel.backend.model.futurebookings.BookingApiResponse
import kotlinx.serialization.json.Json
import org.junit.Test
import kotlin.test.assertNotEquals

class FutureBookingJsonParserTest {
    val json = Json {
        isLenient = true
        ignoreUnknownKeys = true
    }

    @Test
    fun `test that future booking retriever is able to parse valid json from server`() {
        assertNotEquals(
            json.decodeFromString(
                BookingApiResponse.serializer(),
                validJson
            ).items.size, 0
        )
    }
}

private val validJson = """
    {
"count": 5,
"totalCount": 5,
"items": [
{
"id": "104248615",
"booker": {
"id": "6bd42fff-4454-4d11-a72b-ba23b937cedb",
"firstName": "John",
"lastName": "Smith",
"emailAddress": "john_smith@customer.co.uk"
},
"payments": {
"virtualCard": {
"paymentId": "9ffe7201-032f-4c5c-ae20-c0e95cafbac5",
"panHint": "9163",
"chargedAmount": {
"amount": "152.00",
"currency": "GBP"
}
}
},
"createdDateTime": "2021-09-02T21:37:11.304Z",
"confirmedDate": "2021-09-02T21:37:24.588Z",
"serviceDate": "2021-11-03",
"status": "CONFIRMED",
"internalStatus": "CONFIRMED",
"cancellable": true,
"refundable": true,
"exchangeable": {
"value": false
},
"product": {
"id": "af194afa-3df0-48af-8725-f68cf5b9a29f",
"travelType": "HOTEL",
"legs": [],
"createdDate": "2021-09-02T21:36:44.480Z",
"serviceDate": "2021-11-03",
"status": "CONFIRMING",
"details": {
"type": "HotelProductDetails",
"countryCode": "GB",
"minRate": {
"billing": {
"amount": "29.00",
"currency": "GBP"
},
"supplier": {
"amount": "29.00",
"currency": "GBP"
},
"preferred": {
"amount": "29.00",
"currency": "GBP"
}
},
"minThreeStarRate": {
"billing": {
"amount": "43.00",
"currency": "GBP"
},
"supplier": {
"amount": "43.00",
"currency": "GBP"
},
"preferred": {
"amount": "43.00",
"currency": "GBP"
}
},
"distance": 0.17327362770019147,
"propertyName": "Hotel Du Vin Stratford",
"address": [
"7 Rother Street",
"Stratford-upon-Avon",
"CV37 6LU"
],
"telephone": "+44 1789 613685",
"upc": "3264534",
"checkInDate": "2021-11-03",
"checkOutDate": "2021-11-04",
"billingConfiguration": {
"type": "BILLBACK",
"billbackElements": {
"allCosts": {
"enabled": false
},
"breakfast": {
"enabled": true
},
"roomRate": {
"enabled": true
},
"parking": {
"enabled": true
},
"wifi": {
"enabled": false
},
"mealSupplement": {
"enabled": true,
"allowance": {
"amount": "30.00",
"currency": "GBP"
}
}
}
},
"profileImage": {
"urls": []
},
"geocode": {
"latitude": 52.191982,
"longitude": -1.7108058
},
"starRating": "4",
"deliveryOptions": [],
"holdable": false,
"heldSeatReservations": [],
"supportedBillbackOptions": {
"breakfast": true,
"parking": true,
"roomRate": true,
"wifi": true,
"mealSupplement": true
},
"legs": [],
"fees": []
},
"formOfPayment": {
"accountType": "CREDIT_ACCOUNT",
"type": "CREDIT_ACCOUNT",
"id": "34f8409d-c763-43b4-9d80-9a756ecbdbe8",
"associatedCostType": "billing",
"creditAccountId": "101259",
"creditAccountName": "customer - Hotels",
"status": "ACTIVE",
"chargeRoutePlan": false
},
"bookingDetails": {
"type": "HotelProductBookingDetails",
"billingConfiguration": {
"selected": "BILLBACK"
},
"billbackOverrides": []
},
"subProducts": [
{
"id": "0",
"reference": "04FGZJ",
"supplierReference": "8784QR001157",
"sfsReference": "H1410665",
"sfsProductId": "1_73a7f58bb1e6c37b8b79232386ae9499_3264534_59",
"channel": "TVL",
"details": {
"type": "HotelSubProductDetails",
"fees": [],
"rateType": "CONTRACT",
"estimatedServiceCost": {
"billing": {
"amount": "82.00",
"currency": "GBP"
},
"supplier": {
"amount": "82.00",
"currency": "GBP"
},
"preferred": {
"amount": "82.00",
"currency": "GBP"
}
},
"totalEstimatedCost": {
"billing": {
"amount": "82.00",
"currency": "GBP"
},
"supplier": {
"amount": "82.00",
"currency": "GBP"
},
"preferred": {
"amount": "82.00",
"currency": "GBP"
}
},
"rules": [],
"surcharges": [],
"terms": [],
"prepay": "NO",
"cancelAmendTerms": "CXL: CXL 1500 HTL TIME ON 02NOV21-FEE 1 NIGHT-INCL TAX-FEES CXL UP TO 3PM 1DAY PRIOR TO ARRIVAL TO AVOID 1ST NIGHT PENALTY",
"roomType": "Customer. Classic Room -1 X King Bed -en Suite ",
"numberOfAdults": 1,
"numberOfChildren": 0,
"description": "Customer. Classic Room -1 X King Bed -en Suite Bathroom - Free Wifi -flat Screen Tv -desk",
"roomRate": {
"billing": {
"amount": "82.00",
"currency": "GBP"
},
"supplier": {
"amount": "82.00",
"currency": "GBP"
},
"preferred": {
"amount": "82.00",
"currency": "GBP"
}
},
"additions": [
{
"type": "BREAKFAST",
"description": "Breakfast",
"included": true,
"available": false,
"selected": true,
"creditAccountRequired": false
}
],
"requirements": [],
"features": [],
"information": [],
"seatReservations": [],
"sentLoyaltySchemes": [],
"refundable": true
},
"traveller": {
"id": "6bd42fff-4454-4d11-a72b-ba23b937cedb",
"userId": "6bd42fff-4454-4d11-a72b-ba23b937cedb",
"organisationId": "5b7f4f0f-e855-4a1d-a36c-2654a145a81e",
"organisationName": "Customer",
"firstName": "John",
"lastName": "Smith",
"emailAddress": "john_smith@customer.co.uk",
"phoneNumber": "+447814370005",
"hasApisDetails": true,
"hasFullApisDetails": false,
"hasPartialApisDetails": true,
"registered": true,
"guest": false,
"passengerAssistanceConfigured": false,
"dateOfBirth": "1972-04-22",
"gender": "MALE"
},
"bookingDetails": {
"type": "HotelSubProductBookingDetails",
"requiredInformation": "[{\"id\":\"cost-code-project-code\",\"type\":\"SELECT\",\"label\":\"Cost Code / Project Code\",\"value\":{\"code\":\"Person@nfumutual.co.uk\",\"label\":\"1008\"}},{\"id\":\"reason-for-travel\",\"type\":\"SELECT\",\"label\":\"Reason for travel\",\"value\":{\"label\":\"Client / Site visit\",\"code\":\"Client / Site visit\"}}]",
"additions": [],
"flightAdditions": [],
"requirements": [],
"passengerAssistanceDeclined": false
},
"policyComplianceState": "COMPLIANT",
"ticketNumbers": [],
"notes": []
}
],
"searchId": "b0270464-9f73-4518-96ac-e58da1bf6e76"
},
"paidWithVouchers": false
},
{
"id": "104248611",
"booker": {
"id": "6bd42fff-4454-4d11-a72b-ba23b937cedb",
"firstName": "John",
"lastName": "Smith",
"emailAddress": "john_smith@customer.co.uk"
},
"payments": {
"virtualCard": {
"paymentId": "8c0c8381-505a-4424-803c-ed056af3fc2a",
"panHint": "7207",
"chargedAmount": {
"amount": "77.96",
"currency": "GBP"
}
}
},
"createdDateTime": "2021-09-02T21:32:22.137Z",
"confirmedDate": "2021-09-02T21:33:04.383Z",
"serviceDate": "2021-11-03",
"status": "CONFIRMED",
"internalStatus": "CONFIRMED",
"cancellable": true,
"refundable": false,
"exchangeable": {
"value": false
},
"product": {
"id": "0789ec9d-34ca-4211-b843-33b074585fd3",
"travelType": "FLIGHT",
"legs": [],
"createdDate": "2021-09-02T21:30:12.964Z",
"serviceDate": "2021-11-03",
"status": "CONFIRMING",
"details": {
"type": "FlightProductDetails",
"minRate": {
"billing": {
"amount": "55.98",
"currency": "GBP"
},
"supplier": {
"amount": "55.98",
"currency": "GBP"
},
"preferred": {
"amount": "55.98",
"currency": "GBP"
}
},
"codeshareCarrier": {
"code": "U2",
"name": "easyJet"
},
"flights": [
{
"id": "QkZTLUJIWC0yMDIxLTExLTAzVDE0OjE1OjAwLTIwMjEtMTEtMDNUMTU6MjA6MDAtVTI=",
"cabinClass": {
"code": "Y",
"name": "ECONOMY"
},
"segments": [
{
"number": 1,
"fareRulesId": "3010d6f908fa00b397eb35acf2460ef9",
"vendorAirline": {
"code": "U2",
"name": "easyJet"
},
"operatingAirline": {
"code": "U2",
"name": "easyJet"
},
"travelTimeMinutes": 65,
"travelDistance": 384,
"aircraft": "TBC",
"flightNumber": "195",
"flightNumbers": [
"195"
],
"cabinClass": {
"code": "Y",
"name": "ECONOMY"
},
"depart": {
"location": {
"code": "BFS",
"name": "Belfast International Airport, Belfast, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 54.662397,
"longitude": -6.217616
},
"dateTime": "2021-11-03T14:15:00+0000"
},
"arrive": {
"location": {
"code": "BHX",
"name": "Birmingham International Airport, Birmingham, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 52.452518,
"longitude": -1.733256
},
"dateTime": "2021-11-03T15:20:00+0000"
},
"baggageAllowance": {
"pieces": 0,
"weight": 0.0
},
"segmentId": "rYA8ZxR4QFOC/xVVUh1/ew==",
"legs": [
{
"aircraft": "TBC",
"arrive": {
"location": {
"code": "BHX",
"name": "Birmingham International Airport, Birmingham, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 52.452518,
"longitude": -1.733256
},
"dateTime": "2021-11-03T15:20:00+0000"
},
"depart": {
"location": {
"code": "BFS",
"name": "Belfast International Airport, Belfast, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 54.662397,
"longitude": -6.217616
},
"dateTime": "2021-11-03T14:15:00+0000"
},
"legId": "rYA8ZxR4QFOC/xVVUh1/ew==",
"number": 1
}
],
"package": {
"name": "Standard",
"title": "NULL",
"image": "NULL",
"baggageAllowance": {
"pieces": 0,
"weight": 0.0
},
"additions": [
{
"id": "4ff20e92-2392-43b9-a0cd-a0fb752bd69e",
"exitRowSeat": false,
"type": "BAGGAGE",
"included": true,
"description": "1 small cabin bag that must fit under the seat in front of you (max 45x36x20cm) - to also take a larger cabin bag, pay for an Up Front or Extra Legroom seat at checkout"
},
{
"exitRowSeat": false,
"type": "SEAT",
"included": false
},
{
"exitRowSeat": false,
"type": "GROUND_TRANSPORTATION",
"included": false
},
{
"exitRowSeat": false,
"type": "LOUNGE",
"included": false
},
{
"exitRowSeat": false,
"type": "MEDICAL",
"included": false
},
{
"exitRowSeat": false,
"type": "MEAL_BEVERAGE",
"included": false
},
{
"exitRowSeat": false,
"type": "PRIORITY_BOARDING",
"included": false
},
{
"exitRowSeat": false,
"type": "SPORTS_EQUIPMENT",
"included": false
},
{
"exitRowSeat": false,
"type": "FLEXIBILITY",
"included": false
},
{
"exitRowSeat": false,
"type": "OTHER",
"included": false
}
]
}
}
]
},
{
"id": "QkhYLUJGUy0yMDIxLTExLTA0VDE3OjIwOjAwLTIwMjEtMTEtMDRUMTg6MjU6MDAtVTI=",
"cabinClass": {
"code": "Y",
"name": "ECONOMY"
},
"segments": [
{
"number": 1,
"fareRulesId": "3010d6f908fa00b397eb35acf2460ef9",
"vendorAirline": {
"code": "U2",
"name": "easyJet"
},
"operatingAirline": {
"code": "U2",
"name": "easyJet"
},
"travelTimeMinutes": 65,
"travelDistance": 384,
"aircraft": "TBC",
"flightNumber": "196",
"flightNumbers": [
"196"
],
"cabinClass": {
"code": "Y",
"name": "ECONOMY"
},
"depart": {
"location": {
"code": "BHX",
"name": "Birmingham International Airport, Birmingham, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 52.452518,
"longitude": -1.733256
},
"dateTime": "2021-11-04T17:20:00+0000"
},
"arrive": {
"location": {
"code": "BFS",
"name": "Belfast International Airport, Belfast, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 54.662397,
"longitude": -6.217616
},
"dateTime": "2021-11-04T18:25:00+0000"
},
"baggageAllowance": {
"pieces": 0,
"weight": 0.0
},
"segmentId": "20aLwqR5QVycVm/iVfSNwQ==",
"legs": [
{
"aircraft": "TBC",
"arrive": {
"location": {
"code": "BFS",
"name": "Belfast International Airport, Belfast, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 54.662397,
"longitude": -6.217616
},
"dateTime": "2021-11-04T18:25:00+0000"
},
"depart": {
"location": {
"code": "BHX",
"name": "Birmingham International Airport, Birmingham, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 52.452518,
"longitude": -1.733256
},
"dateTime": "2021-11-04T17:20:00+0000"
},
"legId": "20aLwqR5QVycVm/iVfSNwQ==",
"number": 2
}
],
"package": {
"name": "Standard",
"title": "NULL",
"image": "NULL",
"baggageAllowance": {
"pieces": 0,
"weight": 0.0
},
"additions": [
{
"id": "b92619ed-ab47-4b96-be2c-82cd65a5fe07",
"exitRowSeat": false,
"type": "BAGGAGE",
"included": true,
"description": "1 small cabin bag that must fit under the seat in front of you (max 45x36x20cm) - to also take a larger cabin bag, pay for an Up Front or Extra Legroom seat at checkout"
},
{
"exitRowSeat": false,
"type": "SEAT",
"included": false
},
{
"exitRowSeat": false,
"type": "GROUND_TRANSPORTATION",
"included": false
},
{
"exitRowSeat": false,
"type": "LOUNGE",
"included": false
},
{
"exitRowSeat": false,
"type": "MEDICAL",
"included": false
},
{
"exitRowSeat": false,
"type": "MEAL_BEVERAGE",
"included": false
},
{
"exitRowSeat": false,
"type": "PRIORITY_BOARDING",
"included": false
},
{
"exitRowSeat": false,
"type": "SPORTS_EQUIPMENT",
"included": false
},
{
"exitRowSeat": false,
"type": "FLEXIBILITY",
"included": false
},
{
"exitRowSeat": false,
"type": "OTHER",
"included": false
}
]
}
}
]
}
],
"deliveryOptions": [],
"estaRequired": false,
"holdable": false,
"heldSeatReservations": [],
"legs": [],
"fees": []
},
"formOfPayment": {
"accountType": "CREDIT_ACCOUNT",
"type": "CREDIT_ACCOUNT",
"id": "7df67955-4479-4774-b335-4532b473ef8e",
"associatedCostType": "billing",
"creditAccountId": "108123",
"creditAccountName": "Customer Group Insurance",
"status": "ACTIVE",
"chargeRoutePlan": false
},
"subProducts": [
{
"id": "0",
"reference": "K2F2ST6",
"supplierReference": "K2F2ST6",
"sfsReference": "F365883",
"sfsProductId": "EZY53d82b8b-6745-4636-9652-be31b2385642_2d94d519-d325-4595-af85-a3066244f2ba",
"channel": "EZY",
"details": {
"type": "FlightSubProductDetails",
"fees": [],
"estimatedServiceCost": {
"billing": {
"amount": "61.98",
"currency": "GBP"
},
"supplier": {
"amount": "61.98",
"currency": "GBP"
},
"preferred": {
"amount": "61.98",
"currency": "GBP"
}
},
"totalEstimatedCost": {
"billing": {
"amount": "61.98",
"currency": "GBP"
},
"supplier": {
"amount": "61.98",
"currency": "GBP"
},
"preferred": {
"amount": "61.98",
"currency": "GBP"
}
},
"totalEstimatedCostWithAdditions": {
"billing": {
"amount": "75.46",
"currency": "GBP"
},
"supplier": {
"amount": "75.46",
"currency": "GBP"
},
"preferred": {
"amount": "75.46",
"currency": "GBP"
}
},
"rules": [],
"surcharges": [],
"terms": [],
"codeshareCarrier": {
"code": "U2",
"name": "easyJet"
},
"passengerType": "ADULT",
"baggageAllowance": {
"pieces": 0,
"weight": 0.0
},
"taxAmount": {
"billing": {
"amount": "26.00",
"currency": "GBP"
},
"supplier": {
"amount": "26.00",
"currency": "GBP"
},
"preferred": {
"amount": "26.00",
"currency": "GBP"
}
},
"baseAmount": {
"billing": {
"amount": "35.98",
"currency": "GBP"
},
"supplier": {
"amount": "35.98",
"currency": "GBP"
},
"preferred": {
"amount": "35.98",
"currency": "GBP"
}
},
"apisRequired": false,
"fullApisRequired": false,
"partialApisRequired": false,
"hasApisDetails": false,
"hasFullApisDetails": false,
"hasPartialApisDetails": false,
"firstNightAccommodationRequired": false,
"requirements": [],
"features": [],
"information": [],
"seatReservations": [],
"canPreBookSeats": true,
"supportsSeatMap": true,
"sentLoyaltySchemes": [],
"mandatorySeatSelection": false
},
"traveller": {
"id": "6bd42fff-4454-4d11-a72b-ba23b937cedb",
"userId": "6bd42fff-4454-4d11-a72b-ba23b937cedb",
"organisationId": "5b7f4f0f-e855-4a1d-a36c-2654a145a81e",
"organisationName": "Customer",
"firstName": "John",
"lastName": "Smith",
"emailAddress": "john_smith@customer.co.uk",
"phoneNumber": "+447814370005",
"hasApisDetails": true,
"hasFullApisDetails": false,
"hasPartialApisDetails": true,
"registered": true,
"guest": false,
"passengerAssistanceConfigured": false,
"dateOfBirth": "1972-04-22",
"gender": "MALE"
},
"bookingDetails": {
"type": "FlightSubProductBookingDetails",
"requiredInformation": "[{\"id\":\"cost-code-project-code\",\"type\":\"SELECT\",\"label\":\"Cost Code / Project Code\",\"value\":{\"code\":\"Person@nfumutual.co.uk\",\"label\":\"1008\"}},{\"id\":\"reason-for-travel\",\"type\":\"SELECT\",\"label\":\"Reason for travel\",\"value\":{\"label\":\"Client / Site visit\",\"code\":\"Client / Site visit\"}}]",
"flightAdditions": [
{
"id": "d9dead1a-7d6a-454a-bc4d-ee8f8da59353",
"exitRowSeat": false,
"type": "SEAT",
"included": false,
"description": "7D",
"totalPrice": {
"billing": {
"amount": "7.49",
"currency": "GBP"
},
"supplier": {
"amount": "7.49",
"currency": "GBP"
},
"preferred": {
"amount": "7.49",
"currency": "GBP"
}
},
"segmentId": "rYA8ZxR4QFOC/xVVUh1/ew=="
},
{
"id": "39510317-7597-4146-91c7-9d2818a1d3c0",
"exitRowSeat": false,
"type": "SEAT",
"included": false,
"description": "7D",
"totalPrice": {
"billing": {
"amount": "5.99",
"currency": "GBP"
},
"supplier": {
"amount": "5.99",
"currency": "GBP"
},
"preferred": {
"amount": "5.99",
"currency": "GBP"
}
},
"segmentId": "20aLwqR5QVycVm/iVfSNwQ=="
}
],
"requirements": [],
"passengerAssistanceDeclined": false
},
"policyComplianceState": "COMPLIANT",
"ticketNumbers": [],
"notes": []
}
],
"searchId": "EZY53d82b8b-6745-4636-9652-be31b2385642"
},
"paidWithVouchers": false,
"checkInInfo": {
"emailRequired": false
}
},
{
"id": "104371944",
"booker": {
"id": "6bd42fff-4454-4d11-a72b-ba23b937cedb",
"firstName": "John",
"lastName": "Smith",
"emailAddress": "john_smith@customer.co.uk"
},
"payments": {
"virtualCard": {
"paymentId": "517de28d-5a74-4dc6-99b5-3e9041e32fff",
"panHint": "4848",
"chargedAmount": {
"amount": "152.00",
"currency": "GBP"
}
}
},
"createdDateTime": "2021-10-04T09:52:21.524Z",
"confirmedDate": "2021-10-04T09:52:31.601Z",
"serviceDate": "2021-11-24",
"status": "CONFIRMED",
"internalStatus": "CONFIRMED",
"cancellable": true,
"refundable": true,
"exchangeable": {
"value": false
},
"product": {
"id": "7a3e9aaf-0c30-4b60-8d2d-98549fdf009b",
"travelType": "HOTEL",
"legs": [],
"createdDate": "2021-10-04T09:51:59.649Z",
"serviceDate": "2021-11-24",
"status": "CONFIRMING",
"details": {
"type": "HotelProductDetails",
"countryCode": "GB",
"minRate": {
"billing": {
"amount": "29.00",
"currency": "GBP"
},
"supplier": {
"amount": "29.00",
"currency": "GBP"
},
"preferred": {
"amount": "29.00",
"currency": "GBP"
}
},
"minThreeStarRate": {
"billing": {
"amount": "48.60",
"currency": "GBP"
},
"supplier": {
"amount": "48.60",
"currency": "GBP"
},
"preferred": {
"amount": "48.60",
"currency": "GBP"
}
},
"distance": 0.17327362770019147,
"propertyName": "Hotel Du Vin Stratford",
"address": [
"7 Rother Street",
"Stratford-upon-Avon",
"CV37 6LU"
],
"telephone": "+44 1789 613685",
"upc": "3264534",
"checkInDate": "2021-11-24",
"checkOutDate": "2021-11-25",
"billingConfiguration": {
"type": "BILLBACK",
"billbackElements": {
"allCosts": {
"enabled": false
},
"breakfast": {
"enabled": true
},
"roomRate": {
"enabled": true
},
"parking": {
"enabled": true
},
"wifi": {
"enabled": false
},
"mealSupplement": {
"enabled": true,
"allowance": {
"amount": "30.00",
"currency": "GBP"
}
}
}
},
"profileImage": {
"urls": []
},
"geocode": {
"latitude": 52.191982,
"longitude": -1.7108058
},
"starRating": "4",
"deliveryOptions": [],
"holdable": false,
"heldSeatReservations": [],
"supportedBillbackOptions": {
"breakfast": true,
"parking": true,
"roomRate": true,
"wifi": true,
"mealSupplement": true
},
"legs": [],
"fees": []
},
"formOfPayment": {
"accountType": "CREDIT_ACCOUNT",
"type": "CREDIT_ACCOUNT",
"id": "122fa928-943d-4843-9e3d-f15ba5ed3eca",
"associatedCostType": "billing",
"creditAccountId": "101259",
"creditAccountName": "Customer - Hotels",
"status": "ACTIVE",
"chargeRoutePlan": false
},
"bookingDetails": {
"type": "HotelProductBookingDetails",
"billingConfiguration": {
"selected": "BILLBACK"
},
"billbackOverrides": []
},
"subProducts": [
{
"id": "0",
"reference": "09JAVW",
"supplierReference": "8784SC041463",
"sfsReference": "H1461317",
"sfsProductId": "1_973d7f8763f7585de7334fe66905176c_3264534_26",
"channel": "TVL",
"details": {
"type": "HotelSubProductDetails",
"fees": [],
"rateType": "CONTRACT",
"estimatedServiceCost": {
"billing": {
"amount": "82.00",
"currency": "GBP"
},
"supplier": {
"amount": "82.00",
"currency": "GBP"
},
"preferred": {
"amount": "82.00",
"currency": "GBP"
}
},
"totalEstimatedCost": {
"billing": {
"amount": "82.00",
"currency": "GBP"
},
"supplier": {
"amount": "82.00",
"currency": "GBP"
},
"preferred": {
"amount": "82.00",
"currency": "GBP"
}
},
"rules": [],
"surcharges": [],
"terms": [],
"prepay": "NO",
"cancelAmendTerms": "CXL: CXL 1500 HTL TIME ON 23NOV21-FEE 1 NIGHT-INCL TAX-FEES CXL UP TO 3PM 1DAY PRIOR TO ARRIVAL TO AVOID 1ST NIGHT PENALTY",
"roomType": "Customer. Classic Room -1 X King Bed -en Suite ",
"numberOfAdults": 1,
"numberOfChildren": 0,
"description": "Customer. Classic Room -1 X King Bed -en Suite Bathroom - Free Wifi -flat Screen Tv -desk",
"roomRate": {
"billing": {
"amount": "82.00",
"currency": "GBP"
},
"supplier": {
"amount": "82.00",
"currency": "GBP"
},
"preferred": {
"amount": "82.00",
"currency": "GBP"
}
},
"additions": [
{
"type": "BREAKFAST",
"description": "Breakfast",
"included": true,
"available": false,
"selected": true,
"creditAccountRequired": false
}
],
"requirements": [],
"features": [],
"information": [],
"seatReservations": [],
"sentLoyaltySchemes": [],
"refundable": true
},
"traveller": {
"id": "6bd42fff-4454-4d11-a72b-ba23b937cedb",
"userId": "6bd42fff-4454-4d11-a72b-ba23b937cedb",
"organisationId": "5b7f4f0f-e855-4a1d-a36c-2654a145a81e",
"organisationName": "Customer",
"firstName": "John",
"lastName": "Smith",
"emailAddress": "john_smith@customer.co.uk",
"phoneNumber": "+447814370005",
"hasApisDetails": true,
"hasFullApisDetails": false,
"hasPartialApisDetails": true,
"registered": true,
"guest": false,
"passengerAssistanceConfigured": false,
"dateOfBirth": "1972-04-22",
"gender": "MALE"
},
"bookingDetails": {
"type": "HotelSubProductBookingDetails",
"requiredInformation": "[{\"id\":\"cost-code-project-code\",\"type\":\"SELECT\",\"label\":\"Cost Code / Project Code\",\"value\":{\"code\":\"Person@nfumutual.co.uk\",\"label\":\"1008\"}},{\"id\":\"reason-for-travel\",\"type\":\"SELECT\",\"label\":\"Reason for travel\",\"value\":{\"label\":\"Client / Site visit\",\"code\":\"Client / Site visit\"}}]",
"additions": [],
"flightAdditions": [],
"requirements": [],
"passengerAssistanceDeclined": false
},
"policyComplianceState": "COMPLIANT",
"ticketNumbers": [],
"notes": []
}
],
"searchId": "8010b4e5-8d1b-498b-84e2-b052ec028423"
},
"paidWithVouchers": false
},
{
"id": "104373688",
"booker": {
"id": "6bd42fff-4454-4d11-a72b-ba23b937cedb",
"firstName": "John",
"lastName": "Smith",
"emailAddress": "john_smith@customer.co.uk"
},
"payments": {
"virtualCard": {
"paymentId": "2ed0d8eb-37d6-4bce-a2a7-fa0c687e195d",
"panHint": "3289",
"chargedAmount": {
"amount": "158.85",
"currency": "GBP"
}
}
},
"createdDateTime": "2021-10-04T12:26:43.304Z",
"confirmedDate": "2021-10-04T12:27:28.054Z",
"serviceDate": "2021-11-24",
"status": "CONFIRMED",
"internalStatus": "CONFIRMED",
"cancellable": false,
"refundable": false,
"exchangeable": {
"value": false
},
"product": {
"id": "98dbbff0-75fe-45f4-a622-f90a0a24d3b9",
"travelType": "FLIGHT",
"legs": [],
"createdDate": "2021-10-04T12:25:28.862Z",
"serviceDate": "2021-11-24",
"status": "CONFIRMING",
"details": {
"type": "FlightProductDetails",
"minRate": {
"billing": {
"amount": "130.35",
"currency": "GBP"
},
"supplier": {
"amount": "130.35",
"currency": "GBP"
},
"preferred": {
"amount": "130.35",
"currency": "GBP"
}
},
"codeshareCarrier": {
"code": "EI",
"name": "Aer Lingus"
},
"flights": [
{
"id": "QkhELUJIWC0yMDIxLTExLTI0VDE1OjUwOjAwLTIwMjEtMTEtMjRUMTY6NTU6MDAtRUk=",
"cabinClass": {
"code": "A",
"name": "ECONOMY"
},
"segments": [
{
"number": 1,
"fareRulesId": "910e3f72ad6c193a521624834a0845cf",
"vendorAirline": {
"code": "EI",
"name": "Aer Lingus"
},
"operatingAirline": {
"code": "EI",
"name": "Aer Lingus"
},
"travelTimeMinutes": 65,
"travelDistance": 363,
"aircraft": "N/A",
"flightNumber": "EI8397",
"flightNumbers": [
"EI8397"
],
"cabinClass": {
"code": "A",
"name": "ECONOMY"
},
"depart": {
"location": {
"code": "BHD",
"name": "George Best Belfast City Airport, Belfast, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 54.61452,
"longitude": -5.870215
},
"dateTime": "2021-11-24T15:50:00+0000"
},
"arrive": {
"location": {
"code": "BHX",
"name": "Birmingham International Airport, Birmingham, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 52.452518,
"longitude": -1.733256
},
"dateTime": "2021-11-24T16:55:00+0000"
},
"baggageAllowance": {
"pieces": 1,
"weight": 20.0
},
"segmentId": "e110b0cb-88c9-4246-918d-99256c463863",
"legs": [
{
"aircraft": "N/A",
"arrive": {
"location": {
"code": "BHX",
"name": "Birmingham International Airport, Birmingham, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 52.452518,
"longitude": -1.733256
},
"dateTime": "2021-11-24T16:55:00+0000"
},
"depart": {
"location": {
"code": "BHD",
"name": "George Best Belfast City Airport, Belfast, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 54.61452,
"longitude": -5.870215
},
"dateTime": "2021-11-24T15:50:00+0000"
},
"legId": "e110b0cb-88c9-4246-918d-99256c463863LEG1",
"number": 1
}
],
"package": {
"name": "PLUS",
"title": "PLUS",
"baggageAllowance": {
"pieces": 1,
"weight": 20.0
},
"additions": [
{
"id": "544a6670-c55c-421d-9756-ee076242c816",
"exitRowSeat": false,
"type": "BAGGAGE",
"included": true,
"description": "1 x 10kg cabin bag plus small personal carry-on item"
},
{
"id": "cddb3699-7592-4c3d-acac-a95c1a941238",
"exitRowSeat": false,
"type": "OTHER",
"included": true,
"description": "Changes subject to fee plus any fare difference"
},
{
"id": "39280613-f2d9-4194-97bc-6b7237670132",
"exitRowSeat": false,
"type": "OTHER",
"included": true,
"description": "No refunds"
},
{
"id": "6695f63b-664b-4fb1-8371-9c9a3668977b",
"exitRowSeat": false,
"type": "OTHER",
"included": true,
"description": "Standby for an earlier flight"
},
{
"id": "c7a985f1-2d13-46e3-afdb-91da431fe489",
"exitRowSeat": false,
"type": "PRIORITY_BOARDING",
"included": true,
"description": "Priority boarding"
},
{
"id": "bd9ffe31-2ff7-4c97-b85e-998e3f664f62",
"exitRowSeat": false,
"type": "SEAT",
"included": true,
"description": "Advance seat selection"
},
{
"exitRowSeat": false,
"type": "GROUND_TRANSPORTATION",
"included": false
},
{
"exitRowSeat": false,
"type": "LOUNGE",
"included": false
},
{
"exitRowSeat": false,
"type": "MEDICAL",
"included": false
},
{
"exitRowSeat": false,
"type": "MEAL_BEVERAGE",
"included": false
},
{
"exitRowSeat": false,
"type": "SPORTS_EQUIPMENT",
"included": false
},
{
"exitRowSeat": false,
"type": "FLEXIBILITY",
"included": false
}
]
}
}
]
},
{
"id": "QkhYLUJIRC0yMDIxLTExLTI1VDE5OjUwOjAwLTIwMjEtMTEtMjVUMjA6NTU6MDAtRUk=",
"cabinClass": {
"code": "A",
"name": "ECONOMY"
},
"segments": [
{
"number": 1,
"fareRulesId": "910e3f72ad6c193a521624834a0845cf",
"vendorAirline": {
"code": "EI",
"name": "Aer Lingus"
},
"operatingAirline": {
"code": "EI",
"name": "Aer Lingus"
},
"travelTimeMinutes": 65,
"travelDistance": 363,
"aircraft": "N/A",
"flightNumber": "EI8398",
"flightNumbers": [
"EI8398"
],
"cabinClass": {
"code": "A",
"name": "ECONOMY"
},
"depart": {
"location": {
"code": "BHX",
"name": "Birmingham International Airport, Birmingham, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 52.452518,
"longitude": -1.733256
},
"dateTime": "2021-11-25T19:50:00+0000"
},
"arrive": {
"location": {
"code": "BHD",
"name": "George Best Belfast City Airport, Belfast, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 54.61452,
"longitude": -5.870215
},
"dateTime": "2021-11-25T20:55:00+0000"
},
"baggageAllowance": {
"pieces": 1,
"weight": 20.0
},
"segmentId": "168a059c-393b-4954-8a35-cd9d3084dd8f",
"legs": [
{
"aircraft": "N/A",
"arrive": {
"location": {
"code": "BHD",
"name": "George Best Belfast City Airport, Belfast, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 54.61452,
"longitude": -5.870215
},
"dateTime": "2021-11-25T20:55:00+0000"
},
"depart": {
"location": {
"code": "BHX",
"name": "Birmingham International Airport, Birmingham, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 52.452518,
"longitude": -1.733256
},
"dateTime": "2021-11-25T19:50:00+0000"
},
"legId": "168a059c-393b-4954-8a35-cd9d3084dd8fLEG1",
"number": 1
}
],
"package": {
"name": "PLUS",
"title": "PLUS",
"baggageAllowance": {
"pieces": 1,
"weight": 20.0
},
"additions": [
{
"id": "0b52705e-4a85-490d-8683-5383f43ab280",
"exitRowSeat": false,
"type": "BAGGAGE",
"included": true,
"description": "1 x 10kg cabin bag plus small personal carry-on item"
},
{
"id": "0c025176-00dd-48eb-8d50-da2dabec0a7f",
"exitRowSeat": false,
"type": "OTHER",
"included": true,
"description": "Changes subject to fee plus any fare difference"
},
{
"id": "d78af8e7-f558-4d4c-aa7e-658c2e04e96b",
"exitRowSeat": false,
"type": "OTHER",
"included": true,
"description": "No refunds"
},
{
"id": "3e563e2e-d520-4360-94b2-7dc2c0fe385c",
"exitRowSeat": false,
"type": "OTHER",
"included": true,
"description": "Standby for an earlier flight"
},
{
"id": "4bf38f68-0717-4c0c-aa98-225bf83f92a9",
"exitRowSeat": false,
"type": "PRIORITY_BOARDING",
"included": true,
"description": "Priority boarding"
},
{
"id": "3c29edc3-b395-49e0-8d13-78381e34eeaf",
"exitRowSeat": false,
"type": "SEAT",
"included": true,
"description": "Advance seat selection"
},
{
"exitRowSeat": false,
"type": "GROUND_TRANSPORTATION",
"included": false
},
{
"exitRowSeat": false,
"type": "LOUNGE",
"included": false
},
{
"exitRowSeat": false,
"type": "MEDICAL",
"included": false
},
{
"exitRowSeat": false,
"type": "MEAL_BEVERAGE",
"included": false
},
{
"exitRowSeat": false,
"type": "SPORTS_EQUIPMENT",
"included": false
},
{
"exitRowSeat": false,
"type": "FLEXIBILITY",
"included": false
}
]
}
}
]
}
],
"deliveryOptions": [],
"estaRequired": false,
"holdable": false,
"heldSeatReservations": [],
"legs": [],
"fees": []
},
"formOfPayment": {
"accountType": "CREDIT_ACCOUNT",
"type": "CREDIT_ACCOUNT",
"id": "ceb57ea2-5180-4f8b-9e17-3f29717f1cd7",
"associatedCostType": "billing",
"creditAccountId": "108123",
"creditAccountName": "Customer Group Insurance",
"status": "ACTIVE",
"chargeRoutePlan": false
},
"subProducts": [
{
"id": "0",
"reference": "63ITR6",
"supplierReference": "63ITR6",
"sfsReference": "F373681",
"sfsProductId": "ALIe1cfc217-5c2b-4b5e-85af-990b787edf2e_c0231323-5203-4f25-aa3c-c7486258aa30",
"channel": "ALI",
"details": {
"type": "FlightSubProductDetails",
"fees": [],
"estimatedServiceCost": {
"billing": {
"amount": "156.35",
"currency": "GBP"
},
"supplier": {
"amount": "156.35",
"currency": "GBP"
},
"preferred": {
"amount": "156.35",
"currency": "GBP"
}
},
"totalEstimatedCost": {
"billing": {
"amount": "156.35",
"currency": "GBP"
},
"supplier": {
"amount": "156.35",
"currency": "GBP"
},
"preferred": {
"amount": "156.35",
"currency": "GBP"
}
},
"totalEstimatedCostWithAdditions": {
"billing": {
"amount": "156.35",
"currency": "GBP"
},
"supplier": {
"amount": "156.35",
"currency": "GBP"
},
"preferred": {
"amount": "156.35",
"currency": "GBP"
}
},
"rules": [],
"surcharges": [],
"terms": [],
"codeshareCarrier": {
"code": "EI",
"name": "Aer Lingus"
},
"passengerType": "ADULT",
"baggageAllowance": {
"pieces": 1,
"weight": 20.0
},
"taxAmount": {
"billing": {
"amount": "66.35",
"currency": "GBP"
},
"supplier": {
"amount": "66.35",
"currency": "GBP"
},
"preferred": {
"amount": "66.35",
"currency": "GBP"
}
},
"baseAmount": {
"billing": {
"amount": "0.00",
"currency": "GBP"
},
"supplier": {
"amount": "0.00",
"currency": "GBP"
},
"preferred": {
"amount": "0.00",
"currency": "GBP"
}
},
"apisRequired": false,
"fullApisRequired": false,
"partialApisRequired": false,
"hasApisDetails": false,
"hasFullApisDetails": false,
"hasPartialApisDetails": false,
"firstNightAccommodationRequired": false,
"requirements": [],
"features": [],
"information": [],
"seatReservations": [],
"canPreBookSeats": false,
"supportsSeatMap": false,
"sentLoyaltySchemes": [],
"mandatorySeatSelection": false
},
"traveller": {
"id": "6bd42fff-4454-4d11-a72b-ba23b937cedb",
"userId": "6bd42fff-4454-4d11-a72b-ba23b937cedb",
"organisationId": "5b7f4f0f-e855-4a1d-a36c-2654a145a81e",
"organisationName": "Customer",
"firstName": "John",
"lastName": "Smith",
"emailAddress": "john_smith@customer.co.uk",
"phoneNumber": "+447814370005",
"hasApisDetails": true,
"hasFullApisDetails": false,
"hasPartialApisDetails": true,
"registered": true,
"guest": false,
"passengerAssistanceConfigured": false,
"dateOfBirth": "1972-04-22",
"gender": "MALE"
},
"bookingDetails": {
"type": "FlightSubProductBookingDetails",
"requiredInformation": "[{\"id\":\"cost-code-project-code\",\"type\":\"SELECT\",\"label\":\"Cost Code / Project Code\",\"value\":{\"code\":\"Person@nfumutual.co.uk\",\"label\":\"1008\"}},{\"id\":\"reason-for-travel\",\"type\":\"SELECT\",\"label\":\"Reason for travel\",\"value\":{\"label\":\"Client / Site visit\",\"code\":\"Client / Site visit\"}}]",
"flightAdditions": [],
"requirements": [],
"passengerAssistanceDeclined": false
},
"policyComplianceState": "COMPLIANT",
"ticketNumbers": [],
"notes": []
}
],
"searchId": "ALIe1cfc217-5c2b-4b5e-85af-990b787edf2e"
},
"paidWithVouchers": false,
"checkInInfo": {
"emailRequired": false
}
},
{
"id": "104260208",
"booker": {
"id": "6bd42fff-4454-4d11-a72b-ba23b937cedb",
"firstName": "John",
"lastName": "Smith",
"emailAddress": "john_smith@customer.co.uk"
},
"payments": {
"virtualCard": {
"paymentId": "cfe765ee-ca26-4604-9cad-c2994f82e936",
"panHint": "0597",
"chargedAmount": {
"amount": "89.96",
"currency": "GBP"
}
}
},
"createdDateTime": "2021-09-06T19:40:04.805Z",
"confirmedDate": "2021-09-06T19:40:48.834Z",
"serviceDate": "2022-01-12",
"status": "CONFIRMED",
"internalStatus": "CONFIRMED",
"cancellable": true,
"refundable": false,
"exchangeable": {
"value": false
},
"product": {
"id": "224d99cf-610e-463b-ae2d-df92dd8838f9",
"travelType": "FLIGHT",
"legs": [],
"createdDate": "2021-09-06T19:37:40.884Z",
"serviceDate": "2022-01-12",
"status": "CONFIRMING",
"details": {
"type": "FlightProductDetails",
"minRate": {
"billing": {
"amount": "65.98",
"currency": "GBP"
},
"supplier": {
"amount": "65.98",
"currency": "GBP"
},
"preferred": {
"amount": "65.98",
"currency": "GBP"
}
},
"codeshareCarrier": {
"code": "U2",
"name": "easyJet"
},
"flights": [
{
"id": "QkZTLUJIWC0yMDIyLTAxLTEyVDA3OjE1OjAwLTIwMjItMDEtMTJUMDg6MjA6MDAtVTI=",
"cabinClass": {
"code": "Y",
"name": "ECONOMY"
},
"segments": [
{
"number": 1,
"fareRulesId": "3010d6f908fa00b397eb35acf2460ef9",
"vendorAirline": {
"code": "U2",
"name": "easyJet"
},
"operatingAirline": {
"code": "U2",
"name": "easyJet"
},
"travelTimeMinutes": 65,
"travelDistance": 384,
"aircraft": "TBC",
"flightNumber": "191",
"flightNumbers": [
"191"
],
"cabinClass": {
"code": "Y",
"name": "ECONOMY"
},
"depart": {
"location": {
"code": "BFS",
"name": "Belfast International Airport, Belfast, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 54.662397,
"longitude": -6.217616
},
"dateTime": "2022-01-12T07:15:00+0000"
},
"arrive": {
"location": {
"code": "BHX",
"name": "Birmingham International Airport, Birmingham, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 52.452518,
"longitude": -1.733256
},
"dateTime": "2022-01-12T08:20:00+0000"
},
"baggageAllowance": {
"pieces": 0,
"weight": 0.0
},
"segmentId": "6XKsPszjQxm8F7bujdkoIA==",
"legs": [
{
"aircraft": "TBC",
"arrive": {
"location": {
"code": "BHX",
"name": "Birmingham International Airport, Birmingham, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 52.452518,
"longitude": -1.733256
},
"dateTime": "2022-01-12T08:20:00+0000"
},
"depart": {
"location": {
"code": "BFS",
"name": "Belfast International Airport, Belfast, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 54.662397,
"longitude": -6.217616
},
"dateTime": "2022-01-12T07:15:00+0000"
},
"legId": "6XKsPszjQxm8F7bujdkoIA==",
"number": 1
}
],
"package": {
"name": "Standard",
"title": "NULL",
"image": "NULL",
"baggageAllowance": {
"pieces": 0,
"weight": 0.0
},
"additions": [
{
"id": "91972f26-80e9-46bb-894b-da65ba08d36a",
"exitRowSeat": false,
"type": "BAGGAGE",
"included": true,
"description": "1 small cabin bag that must fit under the seat in front of you (max 45x36x20cm) - to also take a larger cabin bag, pay for an Up Front or Extra Legroom seat at checkout"
},
{
"exitRowSeat": false,
"type": "SEAT",
"included": false
},
{
"exitRowSeat": false,
"type": "GROUND_TRANSPORTATION",
"included": false
},
{
"exitRowSeat": false,
"type": "LOUNGE",
"included": false
},
{
"exitRowSeat": false,
"type": "MEDICAL",
"included": false
},
{
"exitRowSeat": false,
"type": "MEAL_BEVERAGE",
"included": false
},
{
"exitRowSeat": false,
"type": "PRIORITY_BOARDING",
"included": false
},
{
"exitRowSeat": false,
"type": "SPORTS_EQUIPMENT",
"included": false
},
{
"exitRowSeat": false,
"type": "FLEXIBILITY",
"included": false
},
{
"exitRowSeat": false,
"type": "OTHER",
"included": false
}
]
}
}
]
},
{
"id": "QkhYLUJGUy0yMDIyLTAxLTEzVDEyOjEwOjAwLTIwMjItMDEtMTNUMTM6MTU6MDAtVTI=",
"cabinClass": {
"code": "Y",
"name": "ECONOMY"
},
"segments": [
{
"number": 1,
"fareRulesId": "3010d6f908fa00b397eb35acf2460ef9",
"vendorAirline": {
"code": "U2",
"name": "easyJet"
},
"operatingAirline": {
"code": "U2",
"name": "easyJet"
},
"travelTimeMinutes": 65,
"travelDistance": 384,
"aircraft": "TBC",
"flightNumber": "194",
"flightNumbers": [
"194"
],
"cabinClass": {
"code": "Y",
"name": "ECONOMY"
},
"depart": {
"location": {
"code": "BHX",
"name": "Birmingham International Airport, Birmingham, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 52.452518,
"longitude": -1.733256
},
"dateTime": "2022-01-13T12:10:00+0000"
},
"arrive": {
"location": {
"code": "BFS",
"name": "Belfast International Airport, Belfast, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 54.662397,
"longitude": -6.217616
},
"dateTime": "2022-01-13T13:15:00+0000"
},
"baggageAllowance": {
"pieces": 0,
"weight": 0.0
},
"segmentId": "UtsoLDK7QxecSLMuUFsZYw==",
"legs": [
{
"aircraft": "TBC",
"arrive": {
"location": {
"code": "BFS",
"name": "Belfast International Airport, Belfast, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 54.662397,
"longitude": -6.217616
},
"dateTime": "2022-01-13T13:15:00+0000"
},
"depart": {
"location": {
"code": "BHX",
"name": "Birmingham International Airport, Birmingham, GB",
"timezone": "Europe/London",
"countryCode": "GB",
"latitude": 52.452518,
"longitude": -1.733256
},
"dateTime": "2022-01-13T12:10:00+0000"
},
"legId": "UtsoLDK7QxecSLMuUFsZYw==",
"number": 2
}
],
"package": {
"name": "Standard",
"title": "NULL",
"image": "NULL",
"baggageAllowance": {
"pieces": 0,
"weight": 0.0
},
"additions": [
{
"id": "8865bb5b-c65f-47b6-b9d6-a9b5b96137cb",
"exitRowSeat": false,
"type": "BAGGAGE",
"included": true,
"description": "1 small cabin bag that must fit under the seat in front of you (max 45x36x20cm) - to also take a larger cabin bag, pay for an Up Front or Extra Legroom seat at checkout"
},
{
"exitRowSeat": false,
"type": "SEAT",
"included": false
},
{
"exitRowSeat": false,
"type": "GROUND_TRANSPORTATION",
"included": false
},
{
"exitRowSeat": false,
"type": "LOUNGE",
"included": false
},
{
"exitRowSeat": false,
"type": "MEDICAL",
"included": false
},
{
"exitRowSeat": false,
"type": "MEAL_BEVERAGE",
"included": false
},
{
"exitRowSeat": false,
"type": "PRIORITY_BOARDING",
"included": false
},
{
"exitRowSeat": false,
"type": "SPORTS_EQUIPMENT",
"included": false
},
{
"exitRowSeat": false,
"type": "FLEXIBILITY",
"included": false
},
{
"exitRowSeat": false,
"type": "OTHER",
"included": false
}
]
}
}
]
}
],
"deliveryOptions": [],
"estaRequired": false,
"holdable": false,
"heldSeatReservations": [],
"legs": [],
"fees": []
},
"formOfPayment": {
"accountType": "CREDIT_ACCOUNT",
"type": "CREDIT_ACCOUNT",
"id": "b52031ca-79b8-4f13-b092-a2924394823d",
"associatedCostType": "billing",
"creditAccountId": "108123",
"creditAccountName": "Customer Group Insurance",
"status": "ACTIVE",
"chargeRoutePlan": false
},
"subProducts": [
{
"id": "0",
"reference": "K2G6Y87",
"supplierReference": "K2G6Y87",
"sfsReference": "F366567",
"sfsProductId": "EZYee58e773-0d7d-4a51-81f3-6c49b294fdef_23c885c3-78ee-4ba9-8e7c-ca6c30af17b1",
"channel": "EZY",
"details": {
"type": "FlightSubProductDetails",
"fees": [],
"estimatedServiceCost": {
"billing": {
"amount": "73.98",
"currency": "GBP"
},
"supplier": {
"amount": "73.98",
"currency": "GBP"
},
"preferred": {
"amount": "73.98",
"currency": "GBP"
}
},
"totalEstimatedCost": {
"billing": {
"amount": "73.98",
"currency": "GBP"
},
"supplier": {
"amount": "73.98",
"currency": "GBP"
},
"preferred": {
"amount": "73.98",
"currency": "GBP"
}
},
"totalEstimatedCostWithAdditions": {
"billing": {
"amount": "87.46",
"currency": "GBP"
},
"supplier": {
"amount": "87.46",
"currency": "GBP"
},
"preferred": {
"amount": "87.46",
"currency": "GBP"
}
},
"rules": [],
"surcharges": [],
"terms": [],
"codeshareCarrier": {
"code": "U2",
"name": "easyJet"
},
"passengerType": "ADULT",
"baggageAllowance": {
"pieces": 0,
"weight": 0.0
},
"taxAmount": {
"billing": {
"amount": "26.00",
"currency": "GBP"
},
"supplier": {
"amount": "26.00",
"currency": "GBP"
},
"preferred": {
"amount": "26.00",
"currency": "GBP"
}
},
"baseAmount": {
"billing": {
"amount": "47.98",
"currency": "GBP"
},
"supplier": {
"amount": "47.98",
"currency": "GBP"
},
"preferred": {
"amount": "47.98",
"currency": "GBP"
}
},
"apisRequired": false,
"fullApisRequired": false,
"partialApisRequired": false,
"hasApisDetails": false,
"hasFullApisDetails": false,
"hasPartialApisDetails": false,
"firstNightAccommodationRequired": false,
"requirements": [],
"features": [],
"information": [],
"seatReservations": [],
"canPreBookSeats": true,
"supportsSeatMap": true,
"sentLoyaltySchemes": [],
"mandatorySeatSelection": false
},
"traveller": {
"id": "6bd42fff-4454-4d11-a72b-ba23b937cedb",
"userId": "6bd42fff-4454-4d11-a72b-ba23b937cedb",
"organisationId": "5b7f4f0f-e855-4a1d-a36c-2654a145a81e",
"organisationName": "Customer",
"firstName": "John",
"lastName": "Smith",
"emailAddress": "john_smith@customer.co.uk",
"phoneNumber": "+447814370005",
"hasApisDetails": true,
"hasFullApisDetails": false,
"hasPartialApisDetails": true,
"registered": true,
"guest": false,
"passengerAssistanceConfigured": false,
"dateOfBirth": "1972-04-22",
"gender": "MALE"
},
"bookingDetails": {
"type": "FlightSubProductBookingDetails",
"requiredInformation": "[{\"id\":\"cost-code-project-code\",\"type\":\"SELECT\",\"label\":\"Cost Code / Project Code\",\"value\":{\"code\":\"Person@nfumutual.co.uk\",\"label\":\"1008\"}},{\"id\":\"reason-for-travel\",\"type\":\"SELECT\",\"label\":\"Reason for travel\",\"value\":{\"label\":\"Client / Site visit\",\"code\":\"Client / Site visit\"}}]",
"flightAdditions": [
{
"id": "141b126f-a307-47c2-a64b-9e9edafdbdf0",
"exitRowSeat": false,
"type": "SEAT",
"included": false,
"description": "7D",
"totalPrice": {
"billing": {
"amount": "7.49",
"currency": "GBP"
},
"supplier": {
"amount": "7.49",
"currency": "GBP"
},
"preferred": {
"amount": "7.49",
"currency": "GBP"
}
},
"segmentId": "6XKsPszjQxm8F7bujdkoIA=="
},
{
"id": "75067f29-7f46-44f5-b9ee-89005a844267",
"exitRowSeat": false,
"type": "SEAT",
"included": false,
"description": "7D",
"totalPrice": {
"billing": {
"amount": "5.99",
"currency": "GBP"
},
"supplier": {
"amount": "5.99",
"currency": "GBP"
},
"preferred": {
"amount": "5.99",
"currency": "GBP"
}
},
"segmentId": "UtsoLDK7QxecSLMuUFsZYw=="
}
],
"requirements": [],
"passengerAssistanceDeclined": false
},
"policyComplianceState": "COMPLIANT",
"ticketNumbers": [],
"notes": []
}
],
"searchId": "EZYee58e773-0d7d-4a51-81f3-6c49b294fdef"
},
"paidWithVouchers": false,
"checkInInfo": {
"emailRequired": false
}
}
]
}
""".trimIndent()