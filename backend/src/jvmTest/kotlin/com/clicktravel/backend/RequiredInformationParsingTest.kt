package com.clicktravel.backend

import com.clicktravel.backend.model.futurebookings.common.RequiredInformationApi
import com.clicktravel.backend.model.futurebookings.common.ValueApi
import com.clicktravel.backend.model.futurebookings.common.ValueBoolApi
import com.clicktravel.backend.model.futurebookings.common.ValueStringApi
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

class RequiredInformationParsingTest {

    private val json = Json {
        ignoreUnknownKeys = true
        isLenient = true
    }

    @Test
    fun `test that required information is parsed correctly`() {
        val fromString = json.decodeFromString<List<RequiredInformationApi>>(requiredInformation)
        assertEquals(
            matchingObject,
           fromString
        )
    }

}

val requiredInformation =
    "[" +
            "{" +
                "\"id\":\"what-is-your-favourite-colour\"," +
                "\"type\":\"TEXT\"," +
                "\"label\":\"What is your favourite colour?\"," +
                "\"value\":\"Red\"" +
            "}," +
            "{" +
                "\"id\":\"is-there-money-in-the-banana-stand\"," +
                "\"type\":\"BOOLEAN\"," +
                "\"label\":\"Is there money in the banana stand?\"," +
                "\"value\":false" +
            "}," +
            "{" +
                "\"id\":\"which-department-do-you-work-in\"," +
                "\"type\":\"SELECT\"," +
                "\"label\":\"Which department do you work in?\"," +
                "\"value\":{" +
                    "\"label\":\"Finance\"," +
                    "\"code\":\"finance\"" +
                "}" +
            "}" +
    "]"

val matchingObject =
    listOf(
        RequiredInformationApi(
            "what-is-your-favourite-colour",
            "What is your favourite colour?",
            "TEXT",
            ValueStringApi("Red")
        ),
        RequiredInformationApi(
            "is-there-money-in-the-banana-stand",
            "Is there money in the banana stand?",
            "BOOLEAN",
            ValueBoolApi(false)
        ),
        RequiredInformationApi(
            "which-department-do-you-work-in",
            "Which department do you work in?",
            "SELECT",
            ValueApi(
                "finance",
                "Finance"
            )
        )
    )