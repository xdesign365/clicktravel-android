package com.clicktravel.backend.retriever

import com.clicktravel.backend.exception.ApiException
import com.clicktravel.core.Settings
import com.clicktravel.repository.retriever.PaymentInstructionsRetriever
import io.ktor.client.HttpClient
import io.ktor.client.call.HttpClientCall
import io.ktor.client.call.call
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.ByteArrayContent
import io.ktor.utils.io.core.*
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope

class PaymentInstructionsRetrieverImpl(
    private val client: HttpClient
) : PaymentInstructionsRetriever {

    override suspend fun resendPaymentInstructions(
        teamId: String,
        bookingId: String,
        travellerEmail: String,
        hotelEmail: String?
    ) = coroutineScope {
        val url = "${Settings.getProperty(Settings.MAIN_URL)}bookings/$bookingId/confirmationEmails"

        val travellerEmailRequest = async { makeRequest(teamId, url, travellerEmail) }
        val hotelEmailRequest = hotelEmail?.let { async { makeRequest(teamId, url, it) } }

        val travellerEmailResponse = travellerEmailRequest.await()
        val hotelEmailResponse = hotelEmailRequest?.await()

        if (travellerEmailResponse.isUnsuccessful() || hotelEmailResponse?.isUnsuccessful() == true) {
            throw ApiException(
                "", if (travellerEmailResponse.isUnsuccessful()) {
                    travellerEmailResponse.response.status.value
                } else {
                    hotelEmailResponse?.response?.status?.value ?: -1
                }
            )
        }
    }

    private suspend fun makeRequest(
        teamId: String,
        url: String,
        email: String
    ): HttpResponse =
        client.post<HttpStatement>(url) {
            header("team-id", teamId)
            method = HttpMethod.Post
            body = ByteArrayContent(
                bytes = """{"emailAddress": "$email"}""".toByteArray(),
                contentType = ContentType(
                    contentType = "application",
                    contentSubtype = "vnd.clicktravel.schema-v1+json"
                )
            )
        }.execute()

    private fun HttpResponse.isUnsuccessful() = status != HttpStatusCode.NoContent

}