package com.clicktravel.backend.retriever

import com.clicktravel.backend.exception.ApiException
import com.clicktravel.backend.model.toApi
import com.clicktravel.core.Settings
import com.clicktravel.core.model.UserDetails
import com.clicktravel.repository.retriever.UpdateCurrentUserProfile
import io.ktor.client.HttpClient
import io.ktor.client.call.call
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.ByteArrayContent
import io.ktor.utils.io.core.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class UpdateCurrentUserProfileImpl(private val client: HttpClient) : UpdateCurrentUserProfile {

    override suspend fun updateCurrentUser(userDetails: UserDetails) {

        val url = "${Settings.getProperty(Settings.USER_URL)}users/user/profile"
        val bodyBytes = Json.encodeToString(userDetails.toApi()).toByteArray()

        client
            .put<HttpStatement>(url) {
                method = HttpMethod.Put
                body = ByteArrayContent(
                    bytes = bodyBytes,
                    contentType = ContentType(
                        contentType = "application",
                        contentSubtype = "vnd.clicktravel.schema-v1+json"
                    )
                )
            }
            .execute()
            .let {
                if (it.response.status != HttpStatusCode.NoContent) {
                    throw ApiException("", it.response.status.value)
                }
            }
    }

}




