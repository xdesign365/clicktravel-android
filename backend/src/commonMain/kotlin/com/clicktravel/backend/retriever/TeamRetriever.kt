package com.clicktravel.backend.retriever

import com.clicktravel.backend.model.teams.TeamListApi
import com.clicktravel.core.Settings
import com.clicktravel.core.model.team.Team
import com.clicktravel.repository.retriever.NoArgumentRetriever
import io.ktor.client.HttpClient
import io.ktor.client.request.*
import io.ktor.client.statement.*

class TeamRetriever(private val client: HttpClient) : NoArgumentRetriever<List<Team>> {

    private suspend fun retrieveTeams() =
        client
            .get<TeamListApi>("${Settings.getProperty(Settings.USER_URL)}users/user/teams") {
                header("Accept", "application/vnd.clicktravel.schema-v1+json")
            }
            .teams
            .map {
                Team(
                    id = it.id,
                    ownerId = it.ownerId,
                    shortId = it.shortId,
                    name = it.name
                )
            }


    override suspend fun execute() = retrieveTeams()
}