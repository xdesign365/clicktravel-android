package com.clicktravel.backend.retriever

import com.clicktravel.backend.exception.ApiException
import com.clicktravel.backend.model.UserDetailApi
import com.clicktravel.backend.model.toCore
import com.clicktravel.core.Settings
import com.clicktravel.core.model.UserDetails
import com.clicktravel.repository.retriever.NoArgumentRetriever
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.header
import kotlinx.serialization.json.Json

class GetCurrentUserProfile(private val client: HttpClient) : NoArgumentRetriever<UserDetails> {

    override suspend fun execute() = try {
        client
            .get<UserDetailApi>("${Settings.getProperty(Settings.USER_URL)}users/user/profile") {
                header(
                    "Accept",
                    "application/vnd.clicktravel.schema-v1+json"
                )
            }
            .toCore()

    } catch (exception: Exception) {
        throw ApiException(exception.message, -1)
    }

}