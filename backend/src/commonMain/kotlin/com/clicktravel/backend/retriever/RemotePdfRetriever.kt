package com.clicktravel.backend.retriever

import com.clicktravel.core.Settings
import com.clicktravel.repository.retriever.ArgumentRetriever
import io.ktor.client.HttpClient
import io.ktor.client.call.receive
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.statement.*

data class PdfRetrieverArgs(
    val bookingId: String,
    val couponId: String,
    val teamId: String
)

class RemotePdfRetriever(private val client: HttpClient) :
    ArgumentRetriever<PdfRetrieverArgs, ByteArray> {

    override suspend fun execute(request: PdfRetrieverArgs) =
        client
            .get<HttpStatement>("${Settings.getProperty(Settings.MAIN_URL)}bookings/${request.bookingId}/coupon/${request.couponId}") {
                header("Accept", "application/pdf ")
                header("team-id", request.teamId)
            }
            .execute()
            .receive<ByteArray>()
}