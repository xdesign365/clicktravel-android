package com.clicktravel.backend.retriever

import com.clicktravel.backend.model.OauthModelApi
import com.clicktravel.core.Settings
import com.clicktravel.core.model.auth.OauthModel
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.repository.retriever.RefreshAuthRetriever
import io.ktor.client.HttpClient
import io.ktor.client.request.*

class RefreshAuthRetrieverImpl(
    private val saveLocalOauth: WriteOnlyRepository<OauthModel>,
    private val client: HttpClient
) : RefreshAuthRetriever {

    override suspend fun refresh(refreshToken: String): OauthModel {

        val url = buildString {
            append(Settings.getProperty(Settings.LOGIN_URL))
            append("oauth/token?")
            append("redirect_uri=${Settings.getProperty(Settings.REDIRECT_URI)}")
            append("&grant_type=refresh_token")
            append("&client_id=${Settings.getProperty(Settings.CLIENT_ID)}")
            append("&refresh_token=$refreshToken")
        }

        return client.get<OauthModelApi>(url) {
            header("Accept", "application/json")
        }
            .let {
                OauthModel(
                    accessToken = it.accessToken,
                    expiresIn = it.expiresIn,
                    tokenType = it.tokenType,
                    refreshToken = it.refreshToken
                )
            }
            .also { saveLocalOauth.storeData(it) }
    }
}