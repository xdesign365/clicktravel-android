package com.clicktravel.backend.retriever

import com.clicktravel.backend.exception.ApiException
import com.clicktravel.backend.model.futurebookings.*
import com.clicktravel.backend.model.futurebookings.flights.toCore
import com.clicktravel.backend.model.futurebookings.hotel.toCore
import com.clicktravel.backend.model.futurebookings.train.toCore
import com.clicktravel.backend.model.futurebookings.travelcard.toCore
import com.clicktravel.core.Settings
import com.clicktravel.core.model.FutureBookings
import com.clicktravel.core.model.auth.OauthModel
import com.clicktravel.core.writer.WriteOnlyRepository
import com.clicktravel.repository.retriever.NoArgumentRetriever
import io.ktor.client.*
import io.ktor.client.request.*


class FutureBookingsRetriever(
    private val client: HttpClient,
    private val userTokenWriter: WriteOnlyRepository<OauthModel>,
    private val userTokenRetriever: NoArgumentRetriever<OauthModel?>,
    private val logger: (String, String, Throwable?) -> Unit
) {

    suspend fun retrieveFutureBookings(): FutureBookings = try {
        client
            .get<BookingApiResponse>("${Settings.getProperty(Settings.MAIN_URL)}users/user/futureBookings") {
                header("Accept", "application/vnd.clicktravel.schema-v1+json")
            }
            .let {
                val list =
                    it.items.filter { it.product != Undefined }
                        .map { bookingItem ->
                            when (bookingItem.product) {
                                is TrainApi -> bookingItem.product.toCore(bookingItem.id)
                                is FlightApi -> bookingItem.product.toCore(
                                    bookingItem.id,
                                    bookingItem.flightCheckinInfo
                                )
                                is HotelApi -> bookingItem.product.toCore(bookingItem.id)
                                is TravelCardApi -> bookingItem.product.toCore(bookingItem.id)
                                else -> throw IllegalArgumentException() // Impossible, would be filtered out
                            }
                        }

                FutureBookings(list)
            }
    } catch (apiException: ApiException) {
        logger("error", apiException.message ?: "", apiException)
        if (apiException.code in 400..499) {
            userTokenWriter.clearData()
            throw apiException
        } else {
            throw apiException
        }
    }
}