package com.clicktravel.backend.retriever

import com.clicktravel.backend.model.OauthModelApi
import com.clicktravel.core.model.auth.OauthModel
import com.clicktravel.repository.retriever.ArgumentRetriever
import io.ktor.client.HttpClient
import io.ktor.client.features.json.*
import io.ktor.client.request.get


class LoginReceiver(val client: HttpClient) : ArgumentRetriever<String, OauthModel> {

    override suspend fun execute(request: String) = client.get<OauthModelApi>(request)
        .let {
            OauthModel(
                accessToken = it.accessToken,
                expiresIn = it.expiresIn,
                tokenType = it.tokenType,
                refreshToken = it.refreshToken
            )
        }

}