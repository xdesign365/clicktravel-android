package com.clicktravel.backend.exception

class ApiException(message: String?, val code : Int) : Throwable(message)