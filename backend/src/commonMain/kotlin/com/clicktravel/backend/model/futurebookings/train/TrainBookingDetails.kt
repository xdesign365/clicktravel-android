package com.clicktravel.backend.model.futurebookings.train

import kotlinx.serialization.Serializable

@Serializable
data class TrainBookingDetails(
    val delivery: DeliveryApi,
    val type: String
)