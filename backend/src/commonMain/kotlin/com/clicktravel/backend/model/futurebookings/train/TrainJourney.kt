package com.clicktravel.backend.model.futurebookings.train

import kotlinx.serialization.Serializable

@Serializable
data class TrainJourney(
    val id: String,
    val direction: String,
    val legs: List<TrainLeg>,
    val offPeakAvailable: Boolean,
    val superOffPeakAvailable: Boolean,
    val ticketType: TrainTicketType,
    val ultimateDestination: TrainStationLocationApi? = null,
    val couponId: String? = null
)