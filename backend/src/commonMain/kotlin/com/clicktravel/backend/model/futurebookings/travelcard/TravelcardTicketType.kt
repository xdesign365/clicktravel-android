package com.clicktravel.backend.model.futurebookings.travelcard


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TravelcardTicketType(
    @SerialName("category")
    val category: String,
    @SerialName("code")
    val code: String,
    @SerialName("name")
    val name: String
)