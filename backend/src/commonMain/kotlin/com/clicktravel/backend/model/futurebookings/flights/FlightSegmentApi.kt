package com.clicktravel.backend.model.futurebookings.flights

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class FlightSegmentApi(
    @SerialName("number")
    val segmentNumber: String,
    @SerialName("depart")
    val depart: FlightArriveOrDepartApi,
    @SerialName("arrive")
    val arrive: FlightArriveOrDepartApi,
    @SerialName("travelTimeMinutes")
    val travelTimeMinutes: Long,
    @SerialName("cabinClass")
    val cabinClassApi : CabinClassApi,
    @SerialName("operatingAirline")
    val operatingAirline : OperatingAirlineApi,
    @SerialName("baggageAllowance")
    val baggageAllowance : BaggageAllowanceApi,
    @SerialName("flightNumber")
    val flightNumber : String
)