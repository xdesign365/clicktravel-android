package com.clicktravel.backend.model.futurebookings.hotel

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class HotelProductApi(
    @SerialName("travelType")
    val travelType: String,
    @SerialName("details")
    val details: HotelDetailsApi,
    @SerialName("subProducts")
    val subProducts: List<HotelSubproductApi>
)