package com.clicktravel.backend.model.futurebookings.common


import com.clicktravel.core.model.common.RequiredInformation
import io.ktor.client.features.logging.*
import kotlinx.serialization.*
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.*


@Serializable(with = RequiredInformationApiSerializer::class)
data class RequiredInformationApi(
    @SerialName("id")
    val id: String,
    @SerialName("label")
    val label: String,
    @SerialName("type")
    val type: String,
    @SerialName("value")
    val value: GenericValueApi
)

object RequiredInformationApiSerializer: KSerializer<RequiredInformationApi> {
    // We need to see if this is needed?
    override val descriptor: SerialDescriptor
        get() = String.serializer().descriptor

    override fun deserialize(decoder: Decoder): RequiredInformationApi {
        val data = (decoder as JsonDecoder).decodeJsonElement().jsonObject
        val dataType = data["type"]?.jsonPrimitive?.content ?: "Unknown"

        val type = when (dataType) {
            "BOOLEAN" -> ValueBoolApi(data["value"]?.jsonPrimitive?.boolean ?: throw NullPointerException())
            "TEXT" -> ValueStringApi(data["value"]?.jsonPrimitive?.content ?: "")
            "SELECT" -> Json { ignoreUnknownKeys = true }.decodeFromString(ValueApi.serializer(), data["value"]?.jsonObject!!.toString())
            else -> ValueStringApi("Unknown")
        }

        return RequiredInformationApi(
            data["id"]?.jsonPrimitive?.content ?: "",
            data["label"]?.jsonPrimitive?.content ?: "",
            data["type"]?.jsonPrimitive?.content ?: "",
            type
        )
    }

    override fun serialize(encoder: Encoder, obj: RequiredInformationApi) {
        throw IllegalArgumentException()
    }
}

fun RequiredInformationApi.toCore() = RequiredInformation(
    key = label,
    value = when (value) {
        is ValueStringApi -> value.value
        is ValueBoolApi -> value.bool.toString()
        is ValueApi -> value.label
    }
)