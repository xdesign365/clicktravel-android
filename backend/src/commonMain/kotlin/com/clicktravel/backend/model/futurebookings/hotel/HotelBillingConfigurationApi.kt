package com.clicktravel.backend.model.futurebookings.hotel

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class HotelBillingConfigurationApi(
    @SerialName("type")
    val type: String,
    @SerialName("billbackElements")
    val billbackElements: HotelBillbackElementsApi? = null
)