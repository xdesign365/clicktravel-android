package com.clicktravel.backend.model.futurebookings.hotel


import com.clicktravel.backend.model.futurebookings.common.RequiredInformationApi
import com.clicktravel.backend.model.futurebookings.train.TrainBookingDetailsSummarySerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class BookingDetails(
    @SerialName("requiredInformation")
    @Serializable(with = TrainBookingDetailsSummarySerializer::class)
    val requiredInformation: List<RequiredInformationApi>,
    @SerialName("specialRequest")
    val specialRequest: String? = null,
    @SerialName("type")
    val type: String
)