package com.clicktravel.backend.model.futurebookings.common


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

sealed class GenericValueApi


data class ValueStringApi(val value : String) : GenericValueApi()
data class ValueBoolApi(val bool : Boolean) : GenericValueApi()

@Serializable
data class ValueApi(
    @SerialName("code")
    val code: String,
    @SerialName("label")
    val label: String
) : GenericValueApi()
