package com.clicktravel.backend.model

import com.clicktravel.core.model.UserDetails
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UserDetailApi(

    @SerialName("id")
    val id: String,

    @SerialName("firstName")
    val firstName: String,

    @SerialName("lastName")
    val lastName: String,

    @SerialName("mobileNumber")
    val mobileNumber: String? = null,

    @SerialName("phoneNumber")
    val phoneNumber: String? = null,

    @SerialName("primaryEmailAddress")
    val primaryEmailAddress: String? = null,

    @SerialName("title")
    val title: String? = null
)

fun UserDetailApi.toCore() = UserDetails(
    id,
    firstName,
    lastName,
    mobileNumber,
    phoneNumber,
    primaryEmailAddress,
    title
)

fun UserDetails.toApi() = UserDetailApi(
    id,
    firstName,
    lastName,
    if (mobileNumber?.isNotEmpty() == true) mobileNumber else null,
    phoneNumber,
    primaryEmailAddress,
    title
)