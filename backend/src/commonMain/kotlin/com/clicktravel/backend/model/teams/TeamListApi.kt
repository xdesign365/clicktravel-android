package com.clicktravel.backend.model.teams


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TeamListApi(
    @SerialName("teams")
    val teams: List<TeamApi>
)