package com.clicktravel.backend.model.futurebookings.travelcard


import com.clicktravel.backend.model.futurebookings.common.NoteApi
import com.clicktravel.backend.model.futurebookings.common.RequiredInformationApi
import com.clicktravel.backend.model.futurebookings.train.TrainBookingDetailsSummarySerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class BookingDetails(
    @SerialName("requiredInformation")
    @Serializable(with = TrainBookingDetailsSummarySerializer::class)
    val requiredInformation: List<RequiredInformationApi>,
    @SerialName("type")
    val type: String,
    @SerialName("notes")
    val notes: List<NoteApi> = listOf()
)