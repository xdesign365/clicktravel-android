package com.clicktravel.backend.model.futurebookings.hotel

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class HotelBillbackElementsApi(
    @SerialName("allCosts")
    val allCosts: BoolEnabled? = BoolEnabled(false),
    @SerialName("breakfast")
    val breakfast: BoolEnabled? = BoolEnabled(false),
    @SerialName("roomRate")
    val roomRate: BoolEnabled? = BoolEnabled(false),
    @SerialName("parking")
    val parking: BoolEnabled? = BoolEnabled(false),
    @SerialName("wifi")
    val wifi: BoolEnabled? = BoolEnabled(false),
    @SerialName("mealSupplement")
    val mealSupplement: MealSupplementBillbackElementApi? = null
)

@Serializable
data class BoolEnabled(
    @SerialName("enabled")
    val enabled: Boolean
)