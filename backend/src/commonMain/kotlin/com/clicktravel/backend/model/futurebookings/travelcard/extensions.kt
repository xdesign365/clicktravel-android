package com.clicktravel.backend.model.futurebookings.travelcard

import com.clicktravel.backend.model.futurebookings.TravelCardApi
import com.clicktravel.backend.model.futurebookings.common.toCore
import com.clicktravel.backend.model.futurebookings.train.toCore
import com.clicktravel.core.model.TravelCard
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.common.Address
import com.clicktravel.core.model.trains.Note
import com.clicktravel.core.model.trains.enums.PassengerType
import com.soywiz.klock.DateFormat
import com.soywiz.klock.parse

fun TravelCardApi.toCore(bookingId: String, dateParser: DateFormat = DateFormat("yyyy-MM-dd")) =
    TravelCard(
        bookingId = bookingId,
        fields = product.subProducts.first().bookingDetails.requiredInformation.map { it.toCore() },
        team = product.subProducts.first().traveller.organisationId,
        notes = product.subProducts.first().notes.map { it.value }.let {
            if (it.isEmpty()) {
                null
            } else {
                Note(it.joinToString("\n"))
            }
        },
        ticketDescription = product.subProducts.first().details.name,
        ticketType = product.subProducts.first().details.ticketType.name,
        passengerType = PassengerType.valueOf(product.subProducts.first().details.passengerType),
        ctrReference = product.subProducts.first().details.ctrReference,
        fareCurrency = product.subProducts.first().details.totalEstimatedCost.billingApi.currency,
        totalFare = product.subProducts.first().details.totalEstimatedCost.billingApi.amount,
        travelDate = dateParser.parse(product.details.travelDate),
        travelTypes = TravelTypes.TRAVELCARD,
        deliveryOption = product.bookingDetails.delivery.option.toCore(),
        address = product.bookingDetails.delivery.address.toAddress(),
        forTheAttentionOf = product.bookingDetails.delivery.fao
    )


fun List<String>.toAddress(): Address = Address(
    firstLine = firstOrNull() ?: "",
    secondLine = if (size > 2) {
        this[1]
    } else {
        ""
    },
    thirdLine = lastOrNull() ?: ""
)
