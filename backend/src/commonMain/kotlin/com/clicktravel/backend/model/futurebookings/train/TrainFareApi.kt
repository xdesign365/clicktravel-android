package com.clicktravel.backend.model.futurebookings.train

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class TicketTypeApi(
    @SerialName("name")
    val name : String
)

@Serializable
data class TrainFareApi(
    @SerialName("ticketType")
    val ticketType: TicketTypeApi,
    @SerialName("fareRoute")
    val fareRoute : FareRouteApi,
    @SerialName("fareClass")
    val fareClass : String
)