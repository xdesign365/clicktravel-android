package com.clicktravel.backend.model.futurebookings.train

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TrainLeg(
    @SerialName("arrive")
    val arrive: TrainArriveOrDepartApi,
    @SerialName("depart")
    val depart: TrainArriveOrDepartApi,
    @SerialName("serviceProvider")
    val serviceProvider: TrainServiceProviderApi,
    @SerialName("travelTimeMinutes")
    val travelTimeMinutes: Long,
    @SerialName("seatNumber")
    val seatNumber: String? = null,
    @SerialName("seatingClass")
    val seatingClass: String? = null
)
