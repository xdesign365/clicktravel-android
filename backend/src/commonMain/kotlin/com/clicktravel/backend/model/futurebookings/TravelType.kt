package com.clicktravel.backend.model.futurebookings

import com.clicktravel.backend.model.futurebookings.flights.FlightCheckinInfo
import com.clicktravel.backend.model.futurebookings.flights.FlightProductApi
import com.clicktravel.backend.model.futurebookings.hotel.HotelProductApi
import com.clicktravel.backend.model.futurebookings.train.TrainBookingDetails
import com.clicktravel.backend.model.futurebookings.train.TrainDetails
import com.clicktravel.backend.model.futurebookings.train.TrainSubProduct
import com.clicktravel.backend.model.futurebookings.travelcard.TravelCardProductApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

sealed class TravelType

object Undefined : TravelType()

/**
 * The API response can be split into:
 *
 * Train details
 *      -> List<Journeys>
 *         -> List<Leg>
 */
@Serializable
data class TrainApi(
    @SerialName("bookingDetails")
    val bookingDetails: TrainBookingDetails,
    @SerialName("createdDate")
    val createdDate: String,
    @SerialName("details")
    val details: TrainDetails,
    @SerialName("id")
    val id: String,
    @SerialName("serviceDate")
    val serviceDate: String,
    @SerialName("status")
    val status: String,
    @SerialName("subProducts")
    val subProducts: List<TrainSubProduct>,
    @SerialName("travelType")
    val travelType: String
) : TravelType()

@Serializable
data class FlightApi(
    @SerialName("product")
    val product: FlightProductApi
) : TravelType()


@Serializable
data class HotelApi(
    @SerialName("product")
    val product: HotelProductApi
) : TravelType()

@Serializable
data class TravelCardApi(
    @SerialName("product")
    val product: TravelCardProductApi
) : TravelType()
