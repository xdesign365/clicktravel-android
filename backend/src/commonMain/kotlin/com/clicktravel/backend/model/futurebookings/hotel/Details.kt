package com.clicktravel.backend.model.futurebookings.hotel


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Details(

//    @SerialName("features")
//    val features: List<Any>,
//
//    @SerialName("fees")
//    val fees: List<Any>,
//
//    @SerialName("information")
//    val information: List<Any>,
//
//    @SerialName("requirements")
//    val requirements: List<Any>,

    @SerialName("cancelAmendTerms")
    val cancelAmendTerms: String? = null,
    @SerialName("numberOfAdults")
    val numberOfAdults: Int = 0,
    @SerialName("numberOfChildren")
    val numberOfChildren: Int = 0,
    @SerialName("rateType")
    val rateType: String,
    @SerialName("roomType")
    val roomType: String,
    @SerialName("type")
    val type: String,
    @SerialName("additions")
    val additions: List<HotelAdditionApi>
)
