package com.clicktravel.backend.model.futurebookings

import kotlinx.serialization.Serializable

/**
 * Top level class which has all of the other items contained within it
 */
@Serializable
data class BookingApiResponse(
    val items: List<BookingItemApi>
)