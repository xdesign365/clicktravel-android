package com.clicktravel.backend.model.futurebookings.train

import com.clicktravel.core.model.trains.Provider
import kotlinx.serialization.Serializable

@Serializable
class TrainServiceProviderApi(
    val code: String,
    val mode: String,
    val name: String
)

fun TrainServiceProviderApi.toCore() = Provider(
    name
)