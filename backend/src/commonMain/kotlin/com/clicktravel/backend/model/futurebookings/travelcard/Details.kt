package com.clicktravel.backend.model.futurebookings.travelcard


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Details(
    @SerialName("code")
    val code: String,
    @SerialName("ctrReference")
    val ctrReference: String,
    @SerialName("name")
    val name: String,
    @SerialName("passengerType")
    val passengerType: String,
    @SerialName("ticketType")
    val ticketType: TravelcardTicketType,
    @SerialName("type")
    val type: String,
    @SerialName("totalEstimatedCost")
    val totalEstimatedCost: TravelcardTotalEstimatedCostApi
)

@Serializable
data class TravelcardTotalEstimatedCostApi(
    @SerialName("billing")
    val billingApi: BillingApi
)