package com.clicktravel.backend.model.futurebookings.flights

import com.clicktravel.core.model.common.TrainStationOrAirport
import com.clicktravel.core.ui.models.ClickLocation
import kotlinx.serialization.Serializable

@Serializable
class FlightAirportApi(
    val code: String,
    val name: String,
    val latitude: Double? = null,
    val longitude: Double? = null
)

fun FlightAirportApi.toCore() =
    TrainStationOrAirport(
        name,
        code,
        geo = latitude?.let { longitude?.let { ClickLocation(latitude, longitude) } })