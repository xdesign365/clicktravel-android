package com.clicktravel.backend.model.futurebookings.common


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PassengerApi(
    @SerialName("id")
    val id: String,
    @SerialName("organisationId")
    val organisationId: String,
    @SerialName("userId")
    val userId: String,
    @SerialName("emailAddress")
    val email: String
)