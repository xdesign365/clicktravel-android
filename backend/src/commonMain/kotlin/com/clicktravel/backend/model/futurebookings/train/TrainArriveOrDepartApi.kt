package com.clicktravel.backend.model.futurebookings.train

import kotlinx.serialization.Serializable

@Serializable
class TrainArriveOrDepartApi(
    val dateTime: String,
    val location: TrainStationLocationApi
)