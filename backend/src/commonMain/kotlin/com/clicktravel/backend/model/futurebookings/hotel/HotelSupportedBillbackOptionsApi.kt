package com.clicktravel.backend.model.futurebookings.hotel

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class HotelSupportedBillbackOptionsApi(
    @SerialName("breakfast")
    val breakfast: Boolean,
    @SerialName("roomRate")
    val roomRate: Boolean,
    @SerialName("parking")
    val parking: Boolean,
    @SerialName("wifi")
    val wifi: Boolean,
    @SerialName("mealSupplement")
    val mealSupplement: Boolean
)