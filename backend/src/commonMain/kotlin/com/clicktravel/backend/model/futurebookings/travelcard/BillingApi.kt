package com.clicktravel.backend.model.futurebookings.travelcard


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class BillingApi(
    @SerialName("amount")
    val amount: String,
    @SerialName("currency")
    val currency: String
)