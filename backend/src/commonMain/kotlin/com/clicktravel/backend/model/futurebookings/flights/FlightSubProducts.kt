package com.clicktravel.backend.model.futurebookings.flights

import com.clicktravel.backend.model.futurebookings.common.NoteApi
import com.clicktravel.backend.model.futurebookings.common.PassengerApi
import com.clicktravel.backend.model.futurebookings.train.TrainBookingDetailsSummary
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class FlightSubProducts(
    @SerialName("notes")
    val notes: List<NoteApi> = listOf(),
    @SerialName("supplierReference")
    val supplierReference: String,
    @SerialName("ticketNumbers")
    val ticketNumbers: List<String>,
    @SerialName("bookingDetails")
    val bookingDetails: TrainBookingDetailsSummary,
    @SerialName("traveller")
    val traveller: PassengerApi
)