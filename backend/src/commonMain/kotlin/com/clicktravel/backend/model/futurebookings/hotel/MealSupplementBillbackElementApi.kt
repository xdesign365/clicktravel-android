package com.clicktravel.backend.model.futurebookings.hotel

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MealSupplementBillbackElementApi(
    @SerialName("enabled")
    val enabled: Boolean,
    @SerialName("allowance")
    val allowance: MealSupplementAllowanceApi? = null
)