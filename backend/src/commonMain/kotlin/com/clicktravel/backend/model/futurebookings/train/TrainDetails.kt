package com.clicktravel.backend.model.futurebookings.train

import kotlinx.serialization.Serializable

@Serializable
data class TrainDetails(
    val holdable: Boolean,
    val journeys: List<TrainJourney>,
    val type: String
)