package com.clicktravel.backend.model.futurebookings.hotel

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class HotelAdditionApi(
    @SerialName("type")
    val type: String,
    @SerialName("selected")
    val selected: Boolean
)