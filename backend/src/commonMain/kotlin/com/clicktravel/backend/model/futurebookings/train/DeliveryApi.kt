package com.clicktravel.backend.model.futurebookings.train

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class DeliveryApi(
    @SerialName("address")
    val address: List<String>,
    @SerialName("option")
    val option: DeliveryOptionApi,
    @SerialName("fao")
    val fao: String? = null
)

