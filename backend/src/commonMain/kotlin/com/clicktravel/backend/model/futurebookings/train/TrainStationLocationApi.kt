package com.clicktravel.backend.model.futurebookings.train

import com.clicktravel.core.model.common.TrainStationOrAirport
import com.clicktravel.core.ui.models.ClickLocation
import kotlinx.serialization.Serializable

@Serializable
data class TrainStationLocationApi(
    val code: String,
    val name: String,
    val crs: String?= null,
    val latitude: Double?= null,
    val longitude: Double?= null
)

fun TrainStationLocationApi.toCore() =
    TrainStationOrAirport(
        name,
        code,
        geo = latitude?.let { longitude?.let { ClickLocation(latitude, longitude) } },
        crs
    )