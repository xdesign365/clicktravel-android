package com.clicktravel.backend.model.futurebookings

import com.clicktravel.backend.model.UserDetailApi
import com.clicktravel.backend.model.futurebookings.flights.FlightCheckinInfo
import com.clicktravel.backend.model.futurebookings.flights.FlightProductApi
import com.clicktravel.backend.model.futurebookings.hotel.HotelProductApi
import com.clicktravel.backend.model.futurebookings.travelcard.TravelCardProductApi
import kotlinx.serialization.*
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.jsonObject

/**
 * An individual booking Item. Relating to a specific booking
 *
 * @param booker is the user who booked it. We might need to use this for
 *  querying the organisation of a user in future.
 *
 * @param cancellable hopefully won't need, but if we do handle cancellable we should keep this in
 *
 * @param confirmedDate probably won't need this but kept in for now
 *
 * @param createdDateTime ditto
 *
 * @param id a unique ID for booking. Possibly need this for redirecting to webviews in future
 *
 * @param product this is the crux of the data. Will need to be converted into a specific type of product
 * before we can really use these objects
 *
 * @param refundable keeping this for the same reason as cancellable
 *
 * @param serviceDate good candidate for trimming in future. Looks to be something that we will likely get more info
 * on in the specific object
 *
 * @param status I'm assuming we won't display this, but it might help if we want to display e.g. 'cancelled' in future,
 * or if we need to filter this data out.
 */
@Serializable
data class BookingItemApi(
    val booker: UserDetailApi,
    val cancellable: Boolean,
    val confirmedDate: String,
    val createdDateTime: String,
    val id: String,
    @Serializable(TravelTypeSerializer::class)
    val product: TravelType,
    val refundable: Boolean,
    val serviceDate: String,
    val status: String,
    @SerialName("checkInInfo")
    val flightCheckinInfo: FlightCheckinInfo? = null
)

object TravelTypeSerializer : KSerializer<TravelType> {

    override val descriptor: SerialDescriptor
        get() = String.serializer().descriptor

    private val jsonInstance
        get() = Json {
            isLenient = true
            ignoreUnknownKeys = true
        }

    override fun serialize(encoder: Encoder, value: TravelType) {}

    override fun deserialize(decoder: Decoder): TravelType {
        val jsonObject = (decoder as? JsonDecoder)?.decodeJsonElement()?.jsonObject
            ?: throw NullPointerException()
        val travelType = jsonObject["travelType"].toString().replace("\"", "")

        return when (travelType) {
            "TRAIN" -> {
                return jsonInstance
                    .decodeFromString(TrainApi.serializer(), jsonObject.toString())
            }
            "FLIGHT" -> {
                return FlightApi(
                    product = jsonInstance
                        .decodeFromString(FlightProductApi.serializer(), jsonObject.toString())
                )
            }
            "HOTEL" -> {
                return HotelApi(
                    product = jsonInstance
                        .decodeFromString(HotelProductApi.serializer(), jsonObject.toString())
                )
            }
            "TRAVELCARD" -> {
                return TravelCardApi(
                    product = jsonInstance
                        .decodeFromString(TravelCardProductApi.serializer(), jsonObject.toString())
                )
            }
            else -> {
                Undefined
            }
        }
    }
}