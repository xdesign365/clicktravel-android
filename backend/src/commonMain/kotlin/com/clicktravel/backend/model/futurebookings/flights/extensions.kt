package com.clicktravel.backend.model.futurebookings.flights

import com.clicktravel.backend.model.futurebookings.FlightApi
import com.clicktravel.backend.model.futurebookings.common.toCore
import com.clicktravel.core.model.FlightBooking
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.flights.BaggageAllowance
import com.clicktravel.core.model.flights.CabinClass
import com.clicktravel.core.model.flights.DetailedFlightJourney
import com.clicktravel.core.model.flights.FlightSegment
import com.clicktravel.core.model.trains.Note
import com.clicktravel.core.model.trains.Provider
import com.clicktravel.core.util.DateFormatter
import com.clicktravel.core.util.UtcDateFormatter

fun FlightApi.toCore(
    bookingId: String,
    flightCheckinInfo: FlightCheckinInfo?
) = FlightBooking(
    bookingId = bookingId,
    bookingTravelType = TravelTypes.FLIGHT,
    notes = product.subProducts.first().notes.map { it.value }.let {
        if (it.isEmpty()) {
            null
        } else {
            Note(it.joinToString("\n"))
        }
    },
    journeys = product.details.flights.map {
        it.toCore()
    },
    ticketNumbers = product.subProducts.first().ticketNumbers,
    ticketCollectionReference = product.subProducts.first().supplierReference,
    fields = product.subProducts.first().bookingDetails.requiredInformation.map { it.toCore() },
    emailRequired = flightCheckinInfo?.emailRequired ?: false,
    emailForCheckIn = flightCheckinInfo?.emailForCheckIn,
    team = product.subProducts.first().traveller.organisationId
)

fun FlightJourneyApi.toCore() = DetailedFlightJourney(
    segments = segments.map {
        it.toCore()
    },
    journeyId = id
)

fun FlightSegmentApi.toCore(dateFormatter: DateFormatter = UtcDateFormatter()) = FlightSegment(
    segment = segmentNumber,
    legDeparture = dateFormatter.parse(depart.dateTime),
    legArrival = dateFormatter.parse(arrive.dateTime),
    arrivalLocation = arrive.location.toCore(),
    departureLocation = depart.location.toCore(),
    travelTime = travelTimeMinutes,
    operatingAirline = Provider(operatingAirline.name, operatingAirline.code),
    cabinClass = CabinClass(cabinClassApi.name, cabinClassApi.code),
    baggageAllowance = BaggageAllowance(baggageAllowance.pieces, baggageAllowance.weight),
    flightNumber = flightNumber
)