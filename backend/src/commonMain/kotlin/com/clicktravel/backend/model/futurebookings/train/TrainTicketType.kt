package com.clicktravel.backend.model.futurebookings.train

import kotlinx.serialization.Serializable

@Serializable
class TrainTicketType(
    val code: String,
    val name: String
)