package com.clicktravel.backend.model.futurebookings.train

import com.clicktravel.core.model.trains.Railcard
import kotlinx.serialization.Serializable

@Serializable
data class TrainRailcardApi(val name : String)

fun TrainRailcardApi.toCore() = Railcard(name)