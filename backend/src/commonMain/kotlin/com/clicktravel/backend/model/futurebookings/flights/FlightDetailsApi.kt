package com.clicktravel.backend.model.futurebookings.flights

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class FlightDetailsApi(
    @SerialName("flights")
    val flights: List<FlightJourneyApi>
)