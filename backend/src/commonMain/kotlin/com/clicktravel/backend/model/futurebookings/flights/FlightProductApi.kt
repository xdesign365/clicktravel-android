package com.clicktravel.backend.model.futurebookings.flights

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class FlightProductApi(
    @SerialName("subProducts")
    val subProducts: List<FlightSubProducts>,
    @SerialName("details")
    val details : FlightDetailsApi
)