package com.clicktravel.backend.model.futurebookings.train

import com.clicktravel.backend.model.futurebookings.TrainApi
import com.clicktravel.backend.model.futurebookings.common.toCore
import com.clicktravel.backend.model.futurebookings.travelcard.toAddress
import com.clicktravel.core.model.TrainBooking
import com.clicktravel.core.model.TravelTypes
import com.clicktravel.core.model.trains.*
import com.clicktravel.core.ui.models.SeatParser
import com.clicktravel.core.util.DateFormatter
import com.clicktravel.core.util.UtcDateFormatter

fun TrainApi.toCore(
    bookingId: String,
    dateFormatter: DateFormatter = UtcDateFormatter()
): TrainBooking =
    TrainBooking(
        bookingId = bookingId,
        bookingTravelType = TravelTypes.TRAIN,
        passengerType = subProducts.first().details.passengerType.toCore(),
        railcard = subProducts.first().details.railcard?.toCore(),
        ticketCollectionReference = subProducts.first().details.ctrReference,
        deliveryOption = bookingDetails.delivery.option.toCore(),
        address = bookingDetails.delivery.address.toAddress(),
        forTheAttentionOf = bookingDetails.delivery.fao,
        journeys = details.journeys.map {
            DetailedTrainJourney(
                departureStation = it.legs.first().depart.location.toCore(),
                arrivalStation = it.legs.last().arrive.location.toCore(),
                ticketType = TicketType(subProducts.first().details.fares.first().ticketType.name),
                departureDateTime = dateFormatter.parse(it.legs.first().depart.dateTime),
                segments = it.legs.toCore(fareType = subProducts.first().details.fares.first().fareClass),
                journeyId = it.id,
                couponId = it.couponId
            )
        },
        fareType = subProducts.first().details.fares.first().fareRoute.name,
        fareClass = subProducts.first().details.fares.first().fareClass,
        notes = subProducts.first().notes.map { it.value }.let {
            if (it.isEmpty()) {
                null
            } else {
                Note(it.joinToString("\n"))
            }
        },
        fields = subProducts.first().bookingDetails.requiredInformation.map { it.toCore() }.sortedBy { it.key },
        team = subProducts.first().traveller.organisationId
    )

fun List<TrainLeg>.toCore(dateFormatter: DateFormatter = UtcDateFormatter(), fareType: String) =
    map {
        it.toCore(dateFormatter, fareType = fareType)
    }


fun TrainLeg.toCore(
    dateFormatter: DateFormatter,
    parser: SeatParser = SeatParser(),
    fareType: String
) = TrainSegment(
    reservation = seatNumber?.let { parser.parseSeat(fareType, it) },
    legDeparture = dateFormatter.parse(depart.dateTime),
    legArrival = dateFormatter.parse(arrive.dateTime),
    arrivalLocation = arrive.location.toCore(),
    departureLocation = depart.location.toCore(),
    connectionTime = 0,
    provider = serviceProvider.toCore(),
    trainClassInformation = TrainClassInformation(seatingClass ?: ""),
    travelTime = travelTimeMinutes,
    travelType = try {
        ConnectionOrJourneyType.valueOf(serviceProvider.mode.toUpperCase())
    } catch (exception: Exception) {
        ConnectionOrJourneyType.TRAIN
    }
)
