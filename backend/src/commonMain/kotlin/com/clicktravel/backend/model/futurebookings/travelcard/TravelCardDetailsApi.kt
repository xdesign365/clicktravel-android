package com.clicktravel.backend.model.futurebookings.travelcard


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TravelCardDetailsApi(
    @SerialName("travelDate")
    val travelDate: String
)