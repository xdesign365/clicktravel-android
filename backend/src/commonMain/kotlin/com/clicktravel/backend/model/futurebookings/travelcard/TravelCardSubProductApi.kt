package com.clicktravel.backend.model.futurebookings.travelcard


import com.clicktravel.backend.model.futurebookings.common.NoteApi
import com.clicktravel.backend.model.futurebookings.common.PassengerApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TravelCardSubProductApi(
    @SerialName("bookingDetails")
    val bookingDetails: BookingDetails,
    @SerialName("channel")
    val channel: String,
    @SerialName("details")
    val details: Details,
    @SerialName("id")
    val id: String,
    @SerialName("notes")
    val notes: List<NoteApi> = listOf(),
    @SerialName("reference")
    val reference: String,
    @SerialName("sfsProductId")
    val sfsProductId: String,
    @SerialName("sfsReference")
    val sfsReference: String? = null,
    @SerialName("supplierReference")
    val supplierReference: String,
    @SerialName("traveller")
    val traveller: PassengerApi
)