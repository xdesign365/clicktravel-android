package com.clicktravel.backend.model.futurebookings.hotel

import com.clicktravel.backend.model.futurebookings.HotelApi
import com.clicktravel.backend.model.futurebookings.common.toCore
import com.clicktravel.core.model.*
import com.clicktravel.core.model.trains.Note
import com.clicktravel.core.ui.models.ClickLocation
import com.soywiz.klock.DateFormat
import com.soywiz.klock.parse


fun HotelApi.toCore(
    bookingId: String,
    dateParser: DateFormat = DateFormat("yyyy-MM-dd")
) = HotelBooking(
    bookingId = bookingId,
    team = product.subProducts.first().traveller.organisationId,
    fields = product.subProducts.first().bookingDetails.requiredInformation.map { it.toCore() },
    bookingTravelType = TravelTypes.HOTEL,
    notes = product.subProducts.first().notes.map { it.value }.let {
        if (it.isEmpty()) {
            null
        } else {
            Note(it.joinToString("\n"))
        }
    },
    cancellationPolicy = product.subProducts.first().details.cancelAmendTerms,
    checkinDate = dateParser.parse(product.details.checkInDate),
    checkoutDate = dateParser.parse(product.details.checkOutDate),
    hotelAddress = product.details.address.joinToString(", "),
    hotelEmail = product.details.email,
    hotelPhoneNumber = product.details.telephone,
    hotelName = product.details.propertyName,
    location = product.details.geocode?.let { ClickLocation(it.latitude, it.longitude) },
    numberOfAdults = product.subProducts.first().details.numberOfAdults,
    numberOfChildren = product.subProducts.first().details.numberOfChildren,
    roomType = product.subProducts.first().details.roomType,
    specialRequest = product.subProducts.first().bookingDetails.specialRequest,
    mealIncluded = createMealIncludedString(product.subProducts.first().details.additions),
    billingType = BillingType.valueOf(product.details.billingConfiguration.type),
    isPremierInn = product.subProducts.first().channel == "PTI",
    travellerEmail = product.subProducts.first().traveller.email,
    billbackElements = product.details.billingConfiguration.billbackElements?.toCore(),
    supportedBillbackOptions = product.details.supportedBillbackOptions?.toCore()
)

fun HotelBillbackElementsApi.toCore() = HotelBillbackElements(
    allCosts = allCosts?.enabled ?: false,
    breakfast = breakfast?.enabled ?: false,
    roomRate = roomRate?.enabled ?: false,
    parking = parking?.enabled ?: false,
    wifi = wifi?.enabled ?: false,
    mealSupplement = mealSupplement?.toCore()
)

fun MealSupplementBillbackElementApi.toCore() = MealSupplementBillbackElement(
    enabled = enabled,
    allowance = allowance?.amount,
    currency = allowance?.currency
)

fun HotelSupportedBillbackOptionsApi.toCore() = HotelSupportedBillbackOptions(
    breakfast = breakfast,
    roomRate = roomRate,
    parking = parking,
    wifi = wifi,
    mealSupplement = mealSupplement
)

fun createMealIncludedString(additions: List<HotelAdditionApi>): String? {
    var breakfastIncluded = false
    var mealDealIncluded = false

    additions.forEach {
        if (it.selected) {
            breakfastIncluded = it.type == "BREAKFAST"
            mealDealIncluded = it.type == "MEAL_DEAL"
        }
    }

    return when {
        breakfastIncluded && mealDealIncluded -> "Breakfast and meal deal included"
        breakfastIncluded && mealDealIncluded.not() -> "Breakfast included"
        mealDealIncluded && breakfastIncluded.not() -> "Meal deal included"
        else -> null
    }
}