package com.clicktravel.backend.model.futurebookings.hotel

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class HotelDetailsApi(
    @SerialName("geocode")
    val geocode: HotelGeolocationApi? = null,
    @SerialName("propertyName")
    val propertyName: String,
    @SerialName("address")
    val address: List<String>,
    @SerialName("checkInDate")
    val checkInDate: String,
    @SerialName("checkOutDate")
    val checkOutDate: String,
    @SerialName("supportedBillbackOptions")
    val supportedBillbackOptions: HotelSupportedBillbackOptionsApi?,
    @SerialName("billingConfiguration")
    val billingConfiguration: HotelBillingConfigurationApi,
    @SerialName("emailAddress")
    val email: String? = null,
    @SerialName("telephone")
    val telephone: String? = null
)