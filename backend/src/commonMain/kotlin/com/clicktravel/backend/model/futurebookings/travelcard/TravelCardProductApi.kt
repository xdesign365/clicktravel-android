package com.clicktravel.backend.model.futurebookings.travelcard

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TravelCardProductApi(
    @SerialName("createdDate")
    val createdDate: String,
    @SerialName("details")
    val details: TravelCardDetailsApi,
    @SerialName("id")
    val id: String,
    @SerialName("serviceDate")
    val serviceDate: String,
    @SerialName("status")
    val status: String,
    @SerialName("travelType")
    val travelType: String,
    @SerialName("subProducts")
    val subProducts: List<TravelCardSubProductApi>,
    @SerialName("bookingDetails")
    val bookingDetails: TravelCardBookingDetailsApi
)
