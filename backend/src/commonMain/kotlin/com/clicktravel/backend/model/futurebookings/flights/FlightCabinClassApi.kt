package com.clicktravel.backend.model.futurebookings.flights


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class FlightCabinClassApi(
    @SerialName("code")
    val code: String,
    @SerialName("name")
    val name: String
)