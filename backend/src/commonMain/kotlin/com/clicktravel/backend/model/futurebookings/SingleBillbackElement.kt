package com.clicktravel.backend.model.futurebookings

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SingleBillbackElement(
    @SerialName("enabled")
    val enabled: Boolean
)