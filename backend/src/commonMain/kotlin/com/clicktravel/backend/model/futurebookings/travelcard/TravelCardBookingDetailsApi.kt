package com.clicktravel.backend.model.futurebookings.travelcard

import com.clicktravel.backend.model.futurebookings.train.DeliveryApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TravelCardBookingDetailsApi(
    @SerialName("delivery")
    val delivery: DeliveryApi
)
