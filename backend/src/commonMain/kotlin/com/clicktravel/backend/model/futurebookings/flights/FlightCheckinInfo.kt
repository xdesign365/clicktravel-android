package com.clicktravel.backend.model.futurebookings.flights


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class FlightCheckinInfo(
    @SerialName("emailRequired")
    val emailRequired: Boolean,
    @SerialName("emailForCheckIn")
    val emailForCheckIn: String? = null
)