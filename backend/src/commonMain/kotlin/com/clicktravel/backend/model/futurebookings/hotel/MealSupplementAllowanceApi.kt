package com.clicktravel.backend.model.futurebookings.hotel

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MealSupplementAllowanceApi(
    @SerialName("amount")
    val amount: String,
    @SerialName("currency")
    val currency: String
)