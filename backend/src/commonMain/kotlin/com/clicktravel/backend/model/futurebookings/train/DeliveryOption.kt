package com.clicktravel.backend.model.futurebookings.train

import com.clicktravel.backend.model.futurebookings.train.DeliveryOptionApi.*
import com.clicktravel.core.model.trains.enums.DeliveryOption
import kotlinx.serialization.KSerializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

enum class DeliveryOptionApi {
    COLLECT_AT_STATION,
    POST,
    SPECIAL_DELIVERY,
    FIRST_CLASS_POST,
    E_TICKET
}


object DeliveryOptionApiSerializer: KSerializer<DeliveryOptionApi> {
    override val descriptor: SerialDescriptor
        get() = String.serializer().descriptor

    override fun deserialize(decoder: Decoder): DeliveryOptionApi {
        return valueOf(decoder.decodeString().toUpperCase())
    }

    override fun serialize(encoder: Encoder, obj: DeliveryOptionApi) {
        encoder.encodeString(obj.name.toUpperCase())
    }
}

fun DeliveryOptionApi.toCore(): DeliveryOption = when (this) {
    COLLECT_AT_STATION -> DeliveryOption.COLLECT_AT_STATION
    POST -> DeliveryOption.POST
    SPECIAL_DELIVERY -> DeliveryOption.SPECIAL_DELIVERY
    FIRST_CLASS_POST -> DeliveryOption.FIRST_CLASS_POST
    E_TICKET -> DeliveryOption.E_TICKET
}