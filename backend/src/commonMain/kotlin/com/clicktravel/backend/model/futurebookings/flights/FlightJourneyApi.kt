package com.clicktravel.backend.model.futurebookings.flights

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class FlightJourneyApi(
    @SerialName("cabinClass")
    val cabinClass: FlightCabinClassApi,
    @SerialName("segments")
    val segments : List<FlightSegmentApi>,
    @SerialName("id")
    val id : String
)