package com.clicktravel.backend.model.futurebookings.flights


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class BaggageAllowanceApi(
    @SerialName("pieces")
    val pieces: Int,
    @SerialName("weight")
    val weight: Double
)