package com.clicktravel.backend.model.futurebookings.common

import com.clicktravel.backend.model.futurebookings.common.PassengerTypeApi.*
import com.clicktravel.core.model.trains.enums.PassengerType
import kotlinx.serialization.KSerializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

enum class PassengerTypeApi {
    ADULT,
    CHILD,
    UNKNOWN
}

object PassengerTypeApiSerializer: KSerializer<PassengerTypeApi> {
        override val descriptor: SerialDescriptor
        get() = String.serializer().descriptor

    override fun serialize(encoder: Encoder, obj: PassengerTypeApi) {
        encoder.encodeString(obj.name.toUpperCase())
    }

    override fun deserialize(decoder: Decoder) =
        valueOf(decoder.decodeString().toUpperCase())
}

fun PassengerTypeApi.toCore() : PassengerType = when (this) {
    ADULT -> PassengerType.ADULT
    CHILD -> PassengerType.CHILD
    UNKNOWN -> throw IllegalArgumentException("Failed to parse passenger type.")
}