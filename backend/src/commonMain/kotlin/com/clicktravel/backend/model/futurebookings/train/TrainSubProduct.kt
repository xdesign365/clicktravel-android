package com.clicktravel.backend.model.futurebookings.train

import com.clicktravel.backend.model.futurebookings.common.*
import kotlinx.serialization.*
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.Json

@Serializable
data class TrainSubProduct(
    @SerialName("details")
    val details: TrainSubProductDetails,
    @SerialName("notes")
    val notes: List<NoteApi> = listOf(),
    @SerialName("bookingDetails")
    val bookingDetails: TrainBookingDetailsSummary,
    @SerialName("traveller")
    val traveller : PassengerApi
)

@Serializable
data class TrainBookingDetailsSummary(
    @SerialName("requiredInformation")
    @Serializable(with = TrainBookingDetailsSummarySerializer::class)
    val requiredInformation: List<RequiredInformationApi>
)

object TrainBookingDetailsSummarySerializer: KSerializer<List<RequiredInformationApi>> {

    private val jsonInstance = Json {
        ignoreUnknownKeys = true
        isLenient = true
    }

    override val descriptor: SerialDescriptor
        get() = String.serializer().descriptor

    override fun serialize(encoder: Encoder, value: List<RequiredInformationApi>) { }

    override fun deserialize(decoder: Decoder): List<RequiredInformationApi> =
        jsonInstance.decodeFromString(decoder.decodeString())
}

@Serializable
data class TrainSubProductDetails(
    @Serializable(PassengerTypeApiSerializer::class)
    @SerialName("passengerType")
    val passengerType: PassengerTypeApi,
    @SerialName("railcard")
    val railcard: TrainRailcardApi? = null,
    @SerialName("type")
    val type: String? = null,
    @SerialName("fares")
    val fares: List<TrainFareApi>,
    @SerialName("ctrReference")
    val ctrReference: String
)