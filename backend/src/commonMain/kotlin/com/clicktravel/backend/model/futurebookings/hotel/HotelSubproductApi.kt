package com.clicktravel.backend.model.futurebookings.hotel


import com.clicktravel.backend.model.futurebookings.common.NoteApi
import com.clicktravel.backend.model.futurebookings.common.PassengerApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class HotelSubproductApi(
    @SerialName("bookingDetails")
    val bookingDetails: BookingDetails,
    @SerialName("details")
    val details: Details,
    @SerialName("id")
    val id: String,
    @SerialName("notes")
    val notes: List<NoteApi>,
    @SerialName("pin")
    val pin: String? = null,
    @SerialName("reference")
    val reference: String,
    @SerialName("sfsProductId")
    val sfsProductId: String,
    @SerialName("sfsReference")
    val sfsReference: String? = null,
    @SerialName("supplierReference")
    val supplierReference: String,
    @SerialName("traveller")
    val traveller: PassengerApi,
    @SerialName("channel")
    val channel : String
)