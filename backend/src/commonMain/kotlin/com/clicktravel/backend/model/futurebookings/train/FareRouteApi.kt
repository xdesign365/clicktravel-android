package com.clicktravel.backend.model.futurebookings.train

import kotlinx.serialization.Serializable
import kotlinx.serialization.SerialName

@Serializable
data class FareRouteApi(@SerialName("code") val code : String, @SerialName("name") val name : String)
