package com.clicktravel.backend.model.futurebookings.flights

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class FlightArriveOrDepartApi(
    @SerialName("location")
    val location: FlightAirportApi,
    @SerialName("dateTime")
    val dateTime: String
)