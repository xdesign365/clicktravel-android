package com.clicktravel.backend.model.futurebookings.hotel

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class HotelGeolocationApi(
    @SerialName("latitude") val latitude: Double,
    @SerialName("longitude") val longitude: Double
)