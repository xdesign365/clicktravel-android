package com.clicktravel.backend.model.futurebookings.common

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NoteApi(@SerialName("value") val value: String)