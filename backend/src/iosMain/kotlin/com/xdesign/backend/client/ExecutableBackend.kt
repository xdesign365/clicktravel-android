package com.clicktravel.backend.client

import com.clicktravel.backend.model.OauthModelApi
import com.clicktravel.backend.retriever.GetCurrentUserProfile
import com.clicktravel.core.MainScope
import com.clicktravel.core.model.UserDetails
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class ExecutableBackend() {
    val scope: CoroutineScope = MainScope()
    fun lookupUser(oauthModelApi: OauthModelApi, completionHandler: (UserDetails) -> Unit) {

        scope.launch {
            GetCurrentUserProfile(client(oauthModelApi))
                .execute()
                .let {
                    completionHandler(it)
                }
        }
    }
}