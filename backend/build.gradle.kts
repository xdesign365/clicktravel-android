configurations.create("compileClasspath")

plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization")
    id("org.jetbrains.kotlin.native.cocoapods")
}


val libraryVersion: String by project
val kotlin_version: String by project
val mockkVersion: String by project
val buildForIos: String by project

version = libraryVersion

kotlin {
    val serializationVersion: String by project
    val ktorVersion: String by project

    if (buildForIos == "true") {
        val buildForDevice = project.findProperty("kotlin.native.cocoapods.target") == "ios_arm"
        if (buildForDevice) {
            iosArm64("ios")
            iosArm32("iOS32")

            val iOSMain by sourceSets.creating
            sourceSets["iOS64Main"].dependsOn(iOSMain)
            sourceSets["iOS32Main"].dependsOn(iOSMain)
        } else {
            iosX64("ios")
        }

        cocoapods {
            summary = "Backend module for click travel"
            homepage = "Link to a Kotlin/Native module homepage"
        }
    }

    jvm()

    sourceSets {

        val coroutinesVersion: String by project

        val commonMain by getting {
            dependencies {
                api(project(":core"))
                implementation(kotlin("stdlib-common"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-common:$coroutinesVersion")
                implementation("io.ktor:ktor-client-json:$ktorVersion")
                implementation("io.ktor:ktor-client-core:$ktorVersion")
                implementation("io.ktor:ktor-client-logging:$ktorVersion")
                implementation("io.ktor:ktor-client-features:$ktorVersion")

                // Serialization
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.1.0")
            }
        }

        val jvmMain by getting {
            dependencies {
                dependsOn(commonMain)
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
                implementation("io.ktor:ktor-client-okhttp:$ktorVersion")
                implementation("io.ktor:ktor-client-logging-jvm:$ktorVersion")
                implementation("io.ktor:ktor-client-serialization-jvm:$ktorVersion")
                implementation("io.ktor:ktor-client-json-jvm:$ktorVersion")
            }
        }

        val jvmTest by getting {
            dependencies {
                dependsOn(jvmMain)
                implementation("org.jetbrains.kotlin:kotlin-test:$kotlin_version")
                implementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version")
                implementation("io.mockk:mockk:$mockkVersion")

            }
        }

        if (buildForIos == "true") {
            val iosMain by getting {
                dependencies {
                    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-native:$coroutinesVersion")
                    implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-native:$serializationVersion")
                    implementation("io.ktor:ktor-client-ios:$ktorVersion")
                }
            }
        }
    }
}